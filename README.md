# README #

This README describes how one would build the graph associated with [1] as well as perform the mining described in the same manuscript.

### What is this repository for? ###

* Integrating and scoring gene-disease associations using a Bayesian statistics approach (Log Likelihood Score)
* Building an integrated network with the associations using Neo4J through the GraphInterface provided
* Mine the dataset for semantic subgraphs that may be indicative of drug repositioning opportunities
* Version 1.0

### Who do I talk to? ###

* If you have any questions regarding any aspect of the repository please contact Joseph Mullen on j.mullen[at]ncl.ac.uk



[1] Mullen J, Cockell SJ, Woollard P, Wipat A (2016) [An Integrated Data Driven Approach to Drug Repositioning Using Gene-Disease Associations](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0155811). PLoS ONE 11(5): e0155811. doi:10.1371/journal.pone.0155811