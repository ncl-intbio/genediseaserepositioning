# clear objects  
graphics.off()      # close graphics windows   
library("extrafont")
library("plotrix")
#pdf("/Users/joemullen/Desktop/GeneDiseaseRepositioning/Graphs/druggableANDgd.pdf", height = 4, width = 6.83,family = "Arial", paper = "special", onefile = FALSE)

tiff("/Users/joemullen/Desktop/GeneDiseaseRepositioning/Graphs/Fig2.tiff", width = 6.83, height = 4, units = 'in', res = 300,family = "Arial", compression = 'none')



#layout(matrix(c(1,2), 1, 2, byrow = TRUE), widths=c(6,3), heights=c(3))
layout(matrix(c(2,1), 2, 1, byrow = FALSE), widths=c(4), heights=c(2,2))
graphLabels <- c("A","B")

#############################
# G2 Indications
#############################

#############################
# Read data
#############################
Indications<-read.table("/Users/joemullen/Desktop/GeneDiseaseRepositioning/druggable_disease.txt", sep="\t",col.names=c("MeSHcode", "Druggable", "Freq", "PercentDruggable", "Untreated"))

#############################
# First plot- MeSH distribution
#############################

#transform druggable to the same values as Freq
Indications$PercentDrugReformat<-(max(Indications$Freq)/100)*Indications$PercentDruggable
Indications$Undruggable<- Indications$Freq - Indications$Druggable

#order based on the disease type
#test <- Indications[with(Indications, order(MeSHTree)),]
test <- Indications[with(Indications, order(Indications$MeSHcode)),]

#par(mar = Indications(19, 5, 1, 5) + 0.2)
par(mar = c(3, 4, 0.5, 3) + 0.2)
end_point = 0.5 + nrow(test)-1 #this is the line which does the trick (together with barplot "space = 1" parameter)

ylimit= 5+max(test$Freq)

df.bar <- barplot(test$Freq, col="grey50", main="", ylim=c(0,ylimit), space = 0, xlab = "",las=2, cex.axis = 0.85, xaxt='n', border = NA, cex =0.85)
par (new=T)
barplot(test$Untreated, col="grey75", main="", ylim=c(0,ylimit), space = 0, xlab = "",las=2, cex.axis = 0.85, xaxt='n', border = NA, cex =0.85)

text(seq(0.5,nrow(test),by=1), par("usr")[3]-0.5, srt = 80, adj= 1, xpd = TRUE, labels = paste(test$MeSHcode), cex=0.85)

abline(v=seq(0.5,nrow(test),by=1), col="gray", lty=3)

lines(x = df.bar, y = test$PercentDrugReformat, lwd = 1, col="red")
points(x = df.bar, y = test$PercentDrugReformat, pch=20, col= "red")

par(xpd=T)
text((par("usr")[1]-4),(par("usr")[4]+3), graphLabels[2],cex=1.5)
par(xpd=F)

x2 <- c((ylimit/100)*0,(ylimit/100)*20,(ylimit/100)*40,(ylimit/100)*60,(ylimit/100)*80,(ylimit/100)*100)
x3 <- c(0,20,40,60,80,100)

axis(side =4, labels=F, at =x2, col = "red")
mtext(side =4, at = x2, text = x3,col ="red", line =1, ylim = range(c(0, 100)), cex=0.85, las=2)
mtext("% Treatable", side =4, las=3, line =2, col= "red",cex=0.85)

mtext("MeSH Branch", side =1, line =2, cex = 0.85)
mtext("Frequency", side =2, line =3, cex = 0.85)


#############################
# G1 Gene-Disease
#############################
Indications<-read.table("/Users/joemullen/Desktop/GeneDiseaseRepositioning/gene_disease_distribution.txt", sep="\t",col.names=c("MeSHcode", "Druggable", "Freq", "PercentDruggable", "Untreated"))

#############################
# First plot- MeSH distribution
#############################

#transform druggable to the same values as Freq
Indications$PercentDrugReformat<-(max(Indications$Freq)/100)*Indications$PercentDruggable
Indications$Undruggable<- Indications$Freq - Indications$Druggable

#order based on the disease type
test <- Indications[with(Indications, order(Indications$MeSHcode)),]

#par(mar = Indications(19, 5, 1, 5) + 0.2)
par(mar = c(0, 4, 3, 3) + 0.2)
end_point = 0.5 + nrow(test)-1 #this is the line which does the trick (together with barplot "space = 1" parameter)

ylimit= 5+max(test$Freq)

df.bar <- barplot(test$Freq, col="grey50", main="", ylim=c(0,ylimit), space = 0, xlab = "",las=2, cex.axis = 0.85, xaxt='n', border = NA, cex =0.85)
par (new=T)
barplot(test$Untreated, col="grey75", main="", ylim=c(0,ylimit), space = 0, xlab = "",las=2, cex.axis = 0.85, xaxt='n', border = NA, cex =0.85)

abline(v=seq(0.5,nrow(test),by=1), col="gray", lty=3)

lines(x = df.bar, y = test$PercentDrugReformat, lwd = 1, col="red")
points(x = df.bar, y = test$PercentDrugReformat, pch=20, col= "red")

par(xpd=T)
text((par("usr")[1]-4),(par("usr")[4]+3), graphLabels[1],cex=1.5)
par(xpd=F)

x2 <- c((ylimit/100)*0,(ylimit/100)*20,(ylimit/100)*40,(ylimit/100)*60,(ylimit/100)*80,(ylimit/100)*100)
x3 <- c(0,20,40,60,80,100)

axis(side =4, labels=F, at =x2, col = "red")
mtext(side =4, at = x2, text = x3,col ="red",line =1, ylim = range(c(0, 100)), cex=0.85, las=2)
mtext("% With G-D", side =4, las=3, line =2, col= "red",cex=0.85)

mtext("Frequency", side =2, line =3, cex = 0.85)

dev.off()

