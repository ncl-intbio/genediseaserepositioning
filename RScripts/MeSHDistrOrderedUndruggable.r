
##############################
#1. MeSH distribution with druggables
##############################

par(mfrow=c(1,2))


#############################
# Calculate the number of non-treatable diseases/ class
# test$PercentDruggable2 = c(test$Freq - (test$PercentDruggable/100)* test$Freq )
#############################

c = data.frame(MeSHTree = (c("[C07] stomatognathic diseases","[C08] respiratory tract diseases","[C05] musculoskeletal diseases","[F01] mental disorders","[C06] digestive system diseases", "[C09] otorhinolaryngologic diseases", "[C25] chemically-induced disorders","[C26] wounds and injuries","[C23] pathological conditions, signs and symptoms", "[C24] occupational diseases","[C21] disorders of environmental origin","[C03] parasitic diseases","[F02] psychological phenomena and processes","[F03] psychological phenomena and processes","[C22] animal diseases", "[C04] neoplasms","[C01] bacterial infections and mycoses","[C02] virus diseases", "[C20] immune system diseases","[C16] congenital, hereditary, and neonatal diseases and abnormalities","[C17] skin and connective tissue diseases","[C18] nutritional and metabolic diseases", "[C19] endocrine system diseases", "[C10] nervous system diseases", "[C11] eye diseases", "[C12] urologic and male genital diseases","[C13] female genital diseases and pregnancy complications","[C14] cardiovascular diseases", "[C15] hemic and lymphatic diseases")),MeSHcode =(c("[C07] ","[C08] ","[C05] ","[F01] ","[C06] ", "[C09] ", "[C25] ","[C26] ","[C23] ", "[C24] ","[C21] ","[C03] ","[F02] ","[F03] ","[C22] ", "[C04] ","[C01] ","[C02] ", "[C20] ","[C16] ","[C17] ","[C18] ", "[C19] ", "[C10] ", "[C11] ", "[C12] ","[C13] ","[C14] ", "[C15] ")), Freq = c(1338,607,3834,919,1016,598,128, 323,3255,27,5,223, 307,774, 144, 1865,640,413,548,8616,2043,2321,837,5789,1654,847,1005,1603,1002), Druggable = c(20,71,53,30,80,20,24,9,220,0,0,8,8,57,1,140,41,17,70,70,103,82,66,171,51,71,89,115,73))

#############################
# First plot- MeSH distribution
#############################

#transform druggable to the same values as Freq
c$PercentDruggable<-(c$Druggable / c$Freq)*100
c$PercentDrugReformat<-(5+max(c$Freq) /40)*c$PercentDruggable
c$Undruggable<- c$Freq - c$Druggable

#order based on the disease type
#test <- c[with(c, order(MeSHTree)),]
test <- c[with(c, order(c$MeSHcode)),]

#par(mar = c(19, 5, 1, 5) + 0.2)
par(mar = c(3, 4, 3, 3) + 0.2)
end_point = 0.5 + nrow(test)-1 #this is the line which does the trick (together with barplot "space = 1" parameter)

ylimit= 5+max(test$Freq)

df.bar <- barplot(test$Freq, col="grey50", main="", ylim=c(0,ylimit), space = 0, xlab = "",las=2, cex.axis = 0.65, xaxt='n', border = NA, cex =0.5)
#rotate 60 degrees, srt=60
text(seq(0.5,nrow(test),by=1), par("usr")[3]-0.25, srt = 60, adj= 1, xpd = TRUE, labels = paste(test$MeSHcode), cex=0.4)

lines(x = df.bar, y = test$PercentDrugReformat, lwd = 1, col="orange")
points(x = df.bar, y = test$PercentDrugReformat, pch=20, col= "orange")

x2 <- c((ylimit/40)*0,(ylimit/40)*10,(ylimit/40)*20,(ylimit/40)*30,(ylimit/40)*40)
x3 <- c(0,10,20,30,40)
#axis(side=4,at = x2, labels = x3)

axis(side =4, labels=F, at =x2, col = "orange")
mtext(side =4, at = x2, text = x3,col ="orange", line =1, ylim = range(c(0, 100)), cex=0.65, las=2)
mtext("% Treatable by Drug", side =4, las=3, line =2, col= "orange",cex=0.65)

mtext("MeSH Disease Branch", side =1, line =1, cex = 0.65)
mtext("Frequency", side =2, line =3, cex = 0.65)

legend(18, 7000, c("Frequency", "% Drugged"), lty=c(1,1), lwd = c(3,2), pch=c(NA_integer_,20), col=c("grey50","orange"), cex=0.5, bty="n")

#############################
# Ordered undruggable
#############################

#transform druggable to the same values as Freq
c$Undruggable<- (c$Freq - c$Druggable)

#order based on the disease type
#test <- c[with(c, order(MeSHTree)),]
test <- c[with(c, order(Undruggable, decreasing = TRUE)),]

#par(mar = c(19, 5, 1, 5) + 0.2)
par(mar = c(3, 4, 3, 3) + 0.2)
end_point = 0.5 + nrow(test)-1 #this is the line which does the trick (together with barplot "space = 1" parameter)

ylimit= 5+max(test$Freq)

df.bar <- barplot(test$Undruggable, col="grey50", main="", ylim=c(0,ylimit), space = 0, xlab = "",las=2, cex.axis = 0.65, xaxt='n', border = NA, cex =0.5)
#rotate 60 degrees, srt=60
text(seq(0.5,nrow(test),by=1), par("usr")[3]-0.25, srt = 60, adj= 1, xpd = TRUE, labels = paste(test$MeSHcode), cex=0.4)


mtext("MeSH Disease Branch", side =1, line =1, cex = 0.65)
mtext("Frequency", side =2, line =3, cex = 0.65)

legend(18, 7000, c( "#Undrugged"), lty=c(1,1), lwd = c(3,2), pch=c(NA_integer_,20), col=c("grey50"), cex=0.5, bty="n")



