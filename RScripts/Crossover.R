
rm(list = ls())     # clear objects  
graphics.off()      # close graphics windows   

# Plot 3  TAKEN FROM - http://rgraphics.limnology.wisc.edu/rmargins_layout.php
par(oma=c(0,0,0,0))  
##########################
# Dataframe for disease %
##########################



##########################
# Dataframe for disease number
##########################
MeSH<-read.table("/Users/joemullen/Dropbox/GeneDiseaseAnalysis/Results/overlap_Random.txt", sep="\t",col.names=c("Source", "Score"))      	
Pharma<-read.table("/Users/joemullen/Dropbox/GeneDiseaseAnalysis/Results/overlap_Pharma.txt", sep="\t",col.names=c("Source", "Score"))      	


Sources<-c("CTD", "DisGENet", "GWAS", "OMIM", "Orphanet", "UniProt")

numSources <- 6	


# The layout command is complex. The first parameter is a matrix telling us first the order  
# of the plots (left to right, top to bottom), followed by the parameters for the number of   
# rows and number of columns. This layout is saying this:  
#   2   0  
#   1   3  
# Note that '0' indicates no plot, not a 0-indexed counting system.   
# The first plot that we create will go in the bottom left, then the next in the top left,  
# and the third on the bottom right.  
# The 'widths' parameter tells layout the widths of the columns in inches, and similarly  
# the 'heights' parameter tells layout the heights of the rows.   
# I'm not sure what the last parameter actually does, but it's refered to as 'respect',   
# and it is probably a bad idea to not have any respect. So we leave that as true.  

#nf <- layout(matrix(c(2,0,1,0),2,2,byrow=FALSE), widths=c(8,8), heights=c(5,5), TRUE)  

# If you want to see your layout, this command will create a chart labelling it clearly.  
#layout.show(nf)  

# Second plot (large)
par(mfrow = c(1, 2))

#First plot

par(mar = c(4, 3, 4, 4) + 0.2) #add room for the rotated labels

df.bar<-boxplot(Pharma$Score~Pharma$Source,data=MeSH, outcol="grey50", xlab="", ylab="", las=2, cex.axis = 0.65, xaxt ='n', frame.plot=FALSE) 

means <- tapply(Pharma$Score,Pharma$Source,mean)
points(means,col="red",pch=18)

mtext(expression(paste(italic('n'), " = 6" )), side =3, line =1, cex = 0.65)
mtext("Overlap", side =2, line =2, cex = 0.65)

#axis(side =1, labels=F, at = seq(1,(numSources), by=1),line =1)

text(seq(1,1,by=1), par("usr")[3]-0.25, srt = 60, adj= 1, xpd = TRUE, labels = paste("Pharma"), cex=0.65)  




par(mar = c(4, 3, 4, 4) + 0.2) #add room for the rotated labels

df.bar<-boxplot(MeSH$Score~MeSH$Source,data=MeSH, ylim = range(0,4), outcol="grey50", xlab="", ylab="", las=2, cex.axis = 0.65, xaxt ='n', frame.plot=FALSE) 

means <- tapply(MeSH$Score,MeSH$Source,mean)
points(means,col="red",pch=18)

mtext(expression(paste(italic('n'), " = 5" )), side =3, line =1, cex = 0.65)
mtext("Overlap", side =2, line =2, cex = 0.65)

#axis(side =1, labels=F, at = seq(1,(numSources), by=1),line =1)

text(seq(1,numSources,by=1), par("usr")[3]-0.25, srt = 60, adj= 1, xpd = TRUE, labels = paste(Sources), cex=0.65)	


# Label the outer margin area to show it is still there  


# Further Reading:  
# help(layout)  
# help(par)  
# help(matrix)  
#


#######################################
# Multiplot finished
#######################################

##
# http://stackoverflow.com/questions/15277712/adding-lines-to-barplot-in-a-different-y-axis-in-r look at!!
##


