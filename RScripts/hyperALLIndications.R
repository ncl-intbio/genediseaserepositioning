library("extrafont")
goldStandard <- 'ALL'
postscript(paste0("/Users/joemullen/Dropbox/LatexPapers/GSKPaper/GSKPaper/Graphics/hyperIndication_",goldStandard,".eps"), height = 4, width = 4,family = "Arial", paper = "special", onefile = FALSE,horizontal = FALSE)

#########################################
# Layout for 3 graphs
#########################################

layout(matrix(c(1,2,3), ncol = 1, byrow= TRUE), widths = 1, heights = c(5,5,7), respect = FALSE)
par(mar=c(0, 4, 2, 1), bty = 'o')

#########################################
# F1: get the hyper scores for plotting
#########################################
getHyperScores <- function(fileName){
  dataMGD <- read.table(fileName,
                        col.names=c("sucess","sampleSize", "dsFP", "dsTP", "xscore"))
  dataMGD$yscore <- c(phyper(dataMGD$sucess-1, dataMGD$dsTP, dataMGD$dsFP, dataMGD$sampleSize,  lower.tail=FALSE))
  return(data.frame(dataMGD$xscore, dataMGD$yscore))
}

#########################################
# F2: plot 2: another line on the graph
#########################################
goPlot <- function(df, colour, pchchoice){
  par (new=T)
  print (df$dataMGD.xscore)
  print (df$dataMGD.yscore)
  plot(df$dataMGD.xscore, df$dataMGD.yscore, xlab = "", ylab="",axes = "F", frame.plot=FALSE,type="l",xlim = range(c(xlimitmin, xlimitmax)), ylim = range(c(ylimitmin, ylimitmax)),xaxt='n', lwd =1, pch=pchchoice, col=colour)
  par (new=T)
  plot(df$dataMGD.xscore, df$dataMGD.yscore, xlab = "",cex.axis = 0.65, ylab="",axes = "F", frame.plot=FALSE,xlim = range(c(xlimitmin, xlimitmax)), ylim = range(c(ylimitmin, ylimitmax)), pch=pchchoice, cex = 1, col=colour)
  
  
}

#########################################
# Files
#########################################
directory <- "/Users/joemullen/Dropbox/GeneDiseaseAnalysis/Hyper/Hyper/UniProtGS3_DB42/"

ALLall <- getHyperScores(paste0(directory,goldStandard,"_ALL_minusSEs_0.463_ALLdis_MINUSADME_UNIQUE_HYPER.txt"))
ALLC10 <- getHyperScores(paste0(directory,goldStandard,"_C10_minusSEs_0.463_ALLdis_MINUSADME_UNIQUE_HYPER.txt"))
ALLC20 <- getHyperScores(paste0(directory,goldStandard,"_C20_minusSEs_0.463_ALLdis_MINUSADME_UNIQUE_HYPER.txt"))

COMMONall <- getHyperScores(paste0(directory,goldStandard,"_ALL_minusSEs_0.463_COMMONdis_MINUSADME_UNIQUE_HYPER.txt"))
COMMONC10 <- getHyperScores(paste0(directory,goldStandard,"_C10_minusSEs_0.463_COMMONdis_MINUSADME_UNIQUE_HYPER.txt"))
COMMONC20 <- getHyperScores(paste0(directory,goldStandard,"_C20_minusSEs_0.463_COMMONdis_MINUSADME_UNIQUE_HYPER.txt"))

RAREall <- getHyperScores(paste0(directory,goldStandard,"_ALL_minusSEs_0.463_RAREdis_MINUSADME_UNIQUE_HYPER.txt"))
RAREC10 <- getHyperScores(paste0(directory,goldStandard,"_C10_minusSEs_0.463_RAREdis_MINUSADME_UNIQUE_HYPER.txt"))
RAREC20 <- getHyperScores(paste0(directory,goldStandard,"_C20_minusSEs_0.463_RAREdis_MINUSADME_UNIQUE_HYPER.txt"))

#########################################
# P1: Only need the limits once
#########################################
xlimitmin <- min(all$dataMGD.xscore)
xlimitmax <- max(all$dataMGD.xscore)
ylimitmin <- min(all$dataMGD.yscore)
ylimitmax <- max(all$dataMGD.yscore)

#########################################
# Plot P1
#########################################
plot(ALLall$dataMGD.xscore, ALLall$dataMGD.yscore, xlab = "",cex.axis = 0.65, ylab="",axes = "F", frame.plot=FALSE,type="l",xlim = range(c(xlimitmin, xlimitmax)), ylim = range(c(ylimitmin, ylimitmax)), lwd =1, pch=15, cex = 1, col="grey25")
par (new=T)
plot(ALLall$dataMGD.xscore, ALLall$dataMGD.yscore, xlab = "",cex.axis = 0.65, ylab="",axes = "F", frame.plot=FALSE,xlim = range(c(xlimitmin, xlimitmax)), ylim = range(c(ylimitmin, ylimitmax)), pch=15, cex = 1, col="grey25")

goPlot(ALLC10, "red", 16)
goPlot(ALLC20, "blue", 17)

axis(1, at=seq(0, 1, by=0.05), labels=F, lwd=1, lwd.ticks=1, cex = 0.5)
axis(2, at=seq(0, 1, by=0.2), labels=T, lwd=1, lwd.ticks=1, cex = 0.5)
mtext(substitute(paste(italic("P"), "[X>x]")), side =2, las=3, line =2, col="black", cex = 0.85)

legend("topright", c("ALL", "C10", "C20"), lty=c(1,1,1), lwd = c(2,2,2), pch=c(15,16,17), col=c("black","red", "blue"), cex=1.0, bty="n")

#########################################
# Plot P2
#########################################

par(mar=c(0, 4, 2, 1), bty = 'o')

plot(COMMONall$dataMGD.xscore, COMMONall$dataMGD.yscore, xlab = "",cex.axis = 0.65, ylab="",axes = "F", frame.plot=FALSE,type="l",xlim = range(c(xlimitmin, xlimitmax)), ylim = range(c(ylimitmin, ylimitmax)), lwd =1, pch=15, cex = 1, col="grey25")
par (new=T)
plot(COMMONall$dataMGD.xscore, COMMONall$dataMGD.yscore, xlab = "",cex.axis = 0.65, ylab="",axes = "F", frame.plot=FALSE,xlim = range(c(xlimitmin, xlimitmax)), ylim = range(c(ylimitmin, ylimitmax)), pch=15, cex = 1, col="grey25")

goPlot(COMMONC10, "red", 16)
goPlot(COMMONC20, "blue", 17)

axis(1, at=seq(0, 1, by=0.05), labels=F, lwd=1, lwd.ticks=1, cex = 0.5)
axis(2, at=seq(0, 1, by=0.2), labels=T, lwd=1, lwd.ticks=1, cex = 0.5)
mtext(substitute(paste(italic("P"), "[X>x]")), side =2, las=3, line =2, col="black", cex = 0.85)
legend("topright", c("ALL_C", "C10_C", "C20_C"), lty=c(1,1,1), lwd = c(2,2,2), pch=c(15,16,17), col=c("black","red", "blue"), cex=1.0, bty="n")

#########################################
# Plot P2
#########################################

par(mar=c(4, 4, 2, 1))

plot(RAREall$dataMGD.xscore, RAREall$dataMGD.yscore, xlab = "",cex.axis = 0.65, ylab="",axes = "F", frame.plot=FALSE,type="l",xlim = range(c(xlimitmin, xlimitmax)), ylim = range(c(ylimitmin, ylimitmax)), lwd =1, pch=15, cex = 1, col="grey25")
par (new=T)
plot(RAREall$dataMGD.xscore, RAREall$dataMGD.yscore, xlab = "",cex.axis = 0.65, ylab="",axes = "F", frame.plot=FALSE,xlim = range(c(xlimitmin, xlimitmax)), ylim = range(c(ylimitmin, ylimitmax)), pch=15, cex = 1, col="grey25")

goPlot(RAREC10, "red", 16)
goPlot(RAREC20, "blue", 17)

axis(1, at=seq(0, 1, by=0.05), labels=T, lwd=1, lwd.ticks=1, cex = 0.5)
axis(2, at=seq(0, 1, by=0.2), labels=T, lwd=1, lwd.ticks=1, cex = 0.5)
mtext(substitute(paste(italic("P"), "[X>x]")), side =2, las=3, line =2, col="black", cex = 0.85)
mtext("Associations Score", side =1, las=1, line =2, col="black", cex = 0.85)
legend("topright", c("ALL_R", "C10_R", "C20_R"), lty=c(1,1,1), lwd = c(2,2,2), pch=c(15,16,17), col=c("black","red", "blue"), cex=1.0, bty="n")



par(old.par)
# end———————————————
dev.off()

