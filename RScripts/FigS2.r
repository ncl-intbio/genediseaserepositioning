library("extrafont")
library("plotrix")
graphics.off()  
#pdf(paste0("/Users/joemullen/Desktop/GeneDiseaseRepositioning/Graphs/TAU.pdf"), height = 4, width = 6.83,family = "Arial", paper = "special", onefile = FALSE)
tiff("/Users/joemullen/Desktop/GeneDiseaseRepositioning/Graphs/FigS2.tiff", width = 6.83, height = 4, units = 'in', res = 300,family = "Arial", compression = 'none')


#get data
ind<-read.table("/Users/joemullen/Desktop/GeneDiseaseRepositioning/druggable_disease.txt", sep="\t",col.names=c("MeSHcode", "Druggable", "Freq", "PercentDruggable", "Untreated"))
gd<-read.table("/Users/joemullen/Desktop/GeneDiseaseRepositioning/gene_disease_distribution.txt", sep="\t",col.names=c("MeSHcode", "GD", "Freq", "PercentGD", "No_GD"))
#make sure the data is in the same order, based on the MeSHCodes
ordInd <- ind[with(ind, order(ind$MeSHcode)),]
ordGD <- gd[with(gd, order(gd$MeSHcode)),]
#mergeg the ordered data frames
merge<-data.frame(ordInd$MeSHcode, ordInd$Freq, ordInd$Druggable, ordInd$PercentDruggable, ordInd$Untreated, ordGD$GD, ordGD$PercentGD, ordGD$No_GD) 

calcTAS <- function(merge){
  #probability of being able too with the data we have
    # 1 means we have the data
  prob <- c((1-(merge$ordInd.PercentDruggable/100))*(merge$ordGD.PercentGD/100))
  #max number of diseases in cat
    # 1 means we have the largest data set
  mDis <- max(merge$ordInd.Freq)
  #relevant to the size of the dis category
  max <- c((1/mDis)*merge$ordInd.Freq)
  merge$TSA<- c((prob*max))  
  print (merge$TSA)
  return (merge)  
}

result <- calcTAS(merge)
#order using the mesh therapeutic areas
test <- result[with(result, order(result$TSA)),]

barplot(test$TSA, col="grey75", main="", space = 0, xlab = "",las=2, cex.axis = 0.85, xaxt='n', border = NA, cex =0.85)
abline(v=seq(0.5,nrow(test),by=1), col="gray", lty=3)
text(seq(0.5,nrow(test),by=1), par("usr")[3]-0.0, srt = 80, adj= 1, xpd = TRUE, labels = paste(test$ordInd.MeSHcode), cex=0.85)
mtext("MeSH Branch", side =1,  line =2, col="black", cex = 0.85)
mtext("TAU", side =2, las=3, line =3, col="black", cex = 0.85)
dev.off()

