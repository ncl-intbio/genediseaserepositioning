library("extrafont")
postscript("/Users/joemullen/Desktop/GeneDiseaseRepositioning/Graphs/hyperDiffGS.eps", height = 6, width = 6.83,family = "Arial", paper = "special", onefile = FALSE,horizontal = FALSE)

par(mfrow = c(2, 1),mar = c(5, 3, 5, 1) + 0.2) 
par(mar = c(3, 5, 5, 10) + 0.2,xpd=TRUE)

#########################################
# Define the dvalues used
#########################################
dValues<-vector()
dValues[1]<-"1.0"
dValues[2]<-"1.5"
dValues[3]<-"2.0"
dValues[4]<-"2.5"
dValues[5]<-"3.0"
dValues[6]<-"3.5"
dValues[7]<-"4.0"
dValues[8]<-"4.5"
dValues[9]<-"5.0"
dValues[10]<-"5.5"
dValues[11]<-"6.0"
#########################################
# Define a vector of colours to be used
#########################################
colours<-vector()
colours[1]<-"red"
colours[2]<-"blue"
colours[3]<-"green"
colours[4]<-"black"
colours[5]<-"grey25"
colours[6]<-"gold"
colours[7]<-"purple"
colours[8]<-"brown"
colours[9]<-"grey50"
colours[10]<-"pink"
colours[11]<-"yellow"

#########################################
# FileNames
#########################################
fileNameMGD <-"UNIPROT_1.0.txt"
fileNameSEMREP <-"UNIPROT_1.5.txt"
fileNameGOFLOF <-"UNIPROT_2.0.txt"
fileNameRGD <-"UNIPROT_2.5.txt"
fileNamePHARMA <- "UNIPROT_3.0.txt"
fileNameBEFREE <- "UNIPROT_3.5.txt"
fileNameUNIPROT <- "UNIPROT_4.0.txt"
fileNameORPHANET <- "UNIPROT_4.5.txt"
fileNameOMIM <- "UNIPROT_5.0.txt"
fileNameGWAS <- "UNIPROT_5.5.txt"
fileNameCTD <- "UNIPROT_6.0.txt"
#########################################
# We need this to be correct for the second plot
#########################################
numberOfSources<-length(dValues)
#########################################
# F1: get all data and add it to a dataframe
#########################################
getAllData <- function(){
  results=list()
  for (i in dValues) {
      print(i)
      filename <- (paste0("UNIPROT_",i,".txt") )
      results[[length(results)+1]] <- getNormalisedScores(filename)
  } 
  return (results)
}

#########################################
# F1: plot1: Normalise the x scores function
#########################################
getNormalisedScores <- function(fileName){
  print (fileName)
  dataMGD <- read.table(paste0("/Users/joemullen/Desktop/GeneDiseaseRepositioning/GeneDisease_OP/HyperGeometric_OP/",fileName),
                        col.names=c("sucess","sampleSize", "dsFP", "dsTP", "xscore"))
  dataMGD$yscore <- c(phyper(dataMGD$sucess-1, dataMGD$dsTP, dataMGD$dsFP, dataMGD$sampleSize,  lower.tail=FALSE))
  MGDmin<-min(dataMGD$xscore)
  MGDmax<-max(dataMGD$xscore)
  MGDdiff<-(MGDmax-MGDmin)
  dataMGD$xscoreNormalised <- c(((dataMGD$xscore - MGDmin)/MGDdiff)*100)
  return(data.frame(dataMGD$xscoreNormalised, dataMGD$yscore))
}
#########################################
# F2: plot 2: another line on the graph
#########################################
goPlot <- function(df, colour, pchNum){
  par (new=T)
  print (df$dataMGD.xscoreNormalised)
  print (df$dataMGD.yscore)
  plot(df$dataMGD.xscoreNormalised, df$dataMGD.yscore, xlab = "", ylab="",axes = "F", frame.plot=FALSE,type="l",xlim = range(c(0, xlimitmax)), ylim = range(c(0, ylimitmax)),xaxt='n', lwd =1, pch=19, col=colour)
  par (new=T)
  plot(df$dataMGD.xscoreNormalised, df$dataMGD.yscore, xlab = "", ylab="",axes = "F", frame.plot=FALSE,type="p",xlim = range(c(0, xlimitmax)), ylim = range(c(0, ylimitmax)),xaxt='n', lwd =1, pch=pchNum, col=colour)
}
#########################################
# F3: plot2: Get the point to be plotted
#########################################
getPoint <- function(df) {
  MGDdistance<-c(calculateDifference((1-df$dataMGD.xscoreNormalised *0.01), df$dataMGD.yscore))
  return (sum(MGDdistance)/length(MGDdistance))
}
#########################################
# F4: plot2: Calculate the difference between two points in the graph
#########################################
calculateDifference <- function(y1,y2) {
  print (paste("y1",y1))
  print (paste("y2", y2))
  print (paste("diff", sqrt((y1 - y2)^2)))
  return (sqrt((y1 - y2)^2))
}
#########################################
# Get all normalised scores and assign to df
#########################################
dfMGD<-getNormalisedScores(fileNameMGD)
dfSEMREP<-getNormalisedScores(fileNameSEMREP)
dfGOFLOF<-getNormalisedScores(fileNameGOFLOF)
dfRGD<-getNormalisedScores(fileNameRGD)
dfPHARMA<-getNormalisedScores(fileNamePHARMA)
dfBEFREE<-getNormalisedScores(fileNameBEFREE)
dfUNIPROT<-getNormalisedScores(fileNameUNIPROT)
dfORPHANET<-getNormalisedScores(fileNameORPHANET)
dfOMIM<-getNormalisedScores(fileNameOMIM)
dfGWAS<-getNormalisedScores(fileNameGWAS)
dfCTD<-getNormalisedScores(fileNameCTD)
#########################################
# P1: Only need the limits once
#########################################
xlimitmin <- min(dfMGD$dataMGD.xscoreNormalised)
xlimitmax <- max(dfMGD$dataMGD.xscoreNormalised)
ylimitmin <- min(dfMGD$dataMGD.yscore)
ylimitmax <- max(dfMGD$dataMGD.yscore)
#########################################
# Plot P1
#########################################
plot(dfMGD$dataMGD.xscoreNormalised, dfMGD$dataMGD.yscore,las=2, xlab = "",cex.axis = 0.65, ylab="",axes = "F", frame.plot=FALSE,type="l",xlim = range(c(xlimitmin, xlimitmax)), ylim = range(c(ylimitmin, ylimitmax)), lwd =1, pch=19, col="grey25")
goPlot(dfBEFREE, "red","1")
goPlot(dfCTD, "blue","2")
goPlot(dfGOFLOF, "green","3")
goPlot(dfGWAS, "black", "4")
goPlot(dfOMIM, "gold", "5")
goPlot(dfORPHANET, "purple", "6")
goPlot(dfPHARMA, "brown", "7")
goPlot(dfRGD, "grey50", "8")
goPlot(dfSEMREP, "pink", "9")
goPlot(dfUNIPROT, "yellow", "10")

axis(1, at=seq(0, xlimitmax+10, by=20), labels=T, lwd=1, lwd.ticks=1, cex = 0.5)
axis(2, at=seq(0, ylimitmax, by=0.2), labels=T, lwd=1, lwd.ticks=1, cex = 0.5)
mtext(substitute(paste(italic("P"), "[X>x]")), side =2, las=3, line =2, col="black", cex = 0.85)
mtext("Normalised Associations Score", side =1, las=1, line =2, col="black", cex = 0.85)
mtext("Normalised Hypergeometric Distribution", side =3, las=1, line =2, col="black", cex = 0.85)

legend("topright", inset=c(-0.2,0), dValues, lty=c(1,1,1,1,1,1,1,1,1,1,1,3), lwd = c(2,2,2,2,2,2,2,2,2,2,2,3), col=c("red","blue", "green", "black","grey25","gold", "purple", "brown", "grey50", "pink", "yellow","blue"), cex=0.5, bty="n")
#########################################
# Add the ideal line
#########################################
par(xpd = FALSE) 
x <- seq(100,0, by = -10)
y <- seq(0.0 , 1.0, by = 0.1)
fit.lm <-lm(y ~ x)
slope <- coef(fit.lm)[2]
abline(1, slope, col = "blue", lwd = 1, lty = 3)
#########################################
# Plot P2
# Calculate the distance from each point in the distribution from the best line
# Given the 2 points (x1,y1) and (x2,y2)
# NOTE the function for the straight line is y = 1 - (x*0.01)
#########################################
par(mar = c(5, 5, 3, 10) + 0.2,xpd=TRUE)
vecScores<-numeric(numberOfSources)
vecNames<-vector()
#########################################
# Get points
#########################################
vecScores[1]<-getPoint(dfMGD)
vecScores[2]<-getPoint(dfSEMREP)
vecScores[3]<-getPoint(dfGOFLOF)
vecScores[4]<-getPoint(dfRGD)
vecScores[5]<-getPoint(dfPHARMA)
vecScores[6]<-getPoint(dfBEFREE)
vecScores[7]<-getPoint(dfUNIPROT)
vecScores[8]<-getPoint(dfORPHANET)
vecScores[9]<-getPoint(dfOMIM)
vecScores[10]<-getPoint(dfGWAS)
vecScores[11]<-getPoint(dfCTD)
#########################################
# Create data frame
#########################################
df<- data.frame(vecScores, dValues)
#########################################
# Get max/mins
#########################################
xlimitmin <- 0
xlimitmax <- numberOfSources
ylimitmin <- min(vecScores)
ylimitmax <- max(vecScores)
#########################################
# Order data frame
#########################################
#ordered<- df[order(vecScores),]
#########################################
# Plot
#########################################
plot(df$vecScores, xlab = "",cex.axis = 0.65, ylab="",axes = "F", type = "l")

axis(1, at=seq(1, xlimitmax, by=1), labels=F, lwd=1, cex = 0.5)
text(seq(1,numberOfSources,by=1), par("usr")[3]-0.01, 
     srt = 60, adj= 1, xpd = TRUE,
     labels = paste(df$dValues), cex=0.65)

axis(2, at=seq(0.2, ylimitmax+0.02, by=0.02), labels=T, lwd=1, lwd.ticks=1, cex = 0.5)

mtext("Distance", side =2, las=3, line =2, col="black", cex = 0.85)
mtext("D-Value", side =1, las=1, line =1, col="black", cex = 0.85)
mtext(substitute(paste("Mean ", italic("y"), " distance from 'ideal' (", italic("y")," = 1-(",italic("x"),"*0.01))")), side =3, las=1, line =1, col="black", cex = 0.85)

par(old.par)

# end———————————————
dev.off()

