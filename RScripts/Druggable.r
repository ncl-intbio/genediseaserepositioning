rm(list = ls())     # clear objects  
graphics.off()      # close graphics windows   

library("extrafont")
postscript("/Users/joemullen/Desktop/GeneDiseaseRepositioning/Graphs/druggable.eps", height = 3, width = 6.83,family = "Arial", paper = "special", onefile = FALSE,horizontal = FALSE)

#############################
# Read data
#############################
Indications<-read.table("/Users/joemullen/Desktop/GeneDiseaseRepositioning/druggable_disease.txt", sep="\t",col.names=c("MeSHcode", "Druggable", "Freq", "PercentDruggable", "Untreated"))

#############################
# First plot- MeSH distribution
#############################

#transform druggable to the same values as Freq
Indications$PercentDrugReformat<-(max(Indications$Freq)/100)*Indications$PercentDruggable
Indications$Undruggable<- Indications$Freq - Indications$Druggable

#order based on the disease type
#test <- Indications[with(Indications, order(MeSHTree)),]
test <- Indications[with(Indications, order(Indications$MeSHcode)),]

#par(mar = Indications(19, 5, 1, 5) + 0.2)
par(mar = c(3, 4, 3, 3) + 0.2)
end_point = 0.5 + nrow(test)-1 #this is the line which does the trick (together with barplot "space = 1" parameter)

ylimit= 5+max(test$Freq)

df.bar <- barplot(test$Freq, col="grey50", main="", ylim=c(0,ylimit), space = 0, xlab = "",las=2, cex.axis = 0.85, xaxt='n', border = NA, cex =0.85)
#rotate 60 degrees, srt=60
par (new=T)
barplot(test$Untreated, col="grey75", main="", ylim=c(0,ylimit), space = 0, xlab = "",las=2, cex.axis = 0.85, xaxt='n', border = NA, cex =0.85)

text(seq(0.5,nrow(test),by=1), par("usr")[3]-0.5, srt = 80, adj= 1, xpd = TRUE, labels = paste(test$MeSHcode), cex=0.85)

lines(x = df.bar, y = test$PercentDrugReformat, lwd = 1, col="red")
points(x = df.bar, y = test$PercentDrugReformat, pch=20, col= "red")

x2 <- c((ylimit/100)*0,(ylimit/100)*20,(ylimit/100)*40,(ylimit/100)*60,(ylimit/100)*80,(ylimit/100)*100)
x3 <- c(0,20,40,60,80,100)

axis(side =4, labels=F, at =x2, col = "red")
mtext(side =4, at = x2, text = x3,col ="red", line =1, ylim = range(c(0, 100)), cex=0.85, las=2)
mtext("% Treatable", side =4, las=3, line =2, col= "red",cex=0.85)

mtext("MeSH Branch", side =1, line =2, cex = 0.85)
mtext("Frequency", side =2, line =3, cex = 0.85)

legend(18, 7000, c("Frequency", "% Drugged"), lty=c(1,1), lwd = c(3,2), pch=c(NA_integer_,20), col=c("grey50","red"), cex=0.85, bty="n")
dev.off()

