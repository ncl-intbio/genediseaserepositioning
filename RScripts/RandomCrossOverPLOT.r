
library("extrafont")
postscript("/Users/joemullen/Dropbox/LatexPapers/GSKPaper/GSKPaper/Graphics/randomCrossover.eps", height = 4, width = 5,family = "Arial", paper = "special", onefile = FALSE,horizontal = FALSE)

##########################
# Dataframe for disease number
##########################
MeSH<-read.table("/Users/joemullen/Dropbox/GeneDiseaseAnalysis/Results/overlap_Random.txt", sep="\t",col.names=c("Source", "Score"))    		

Sources<-c("CTD", "CuratedAll", "BeFree", "GOF_LOF", "GWAS", "MGD", "OMIM", "Orphanet","Pharma", "RGD", "SemRep", "UniProt")

numSources <- 12	

par(mar = c(4, 3, 4, 4) + 0.2) #add room for the rotated labels

df.bar<-boxplot(MeSH$Score~MeSH$Source,data=MeSH, ylim = range(0,10), outcol="grey50", xlab="", ylab="", las=2, cex.axis = 0.65, xaxt ='n', frame.plot=FALSE) 

means <- tapply(MeSH$Score,MeSH$Source,mean)
points(means,col="red",pch=18)

mtext(expression(paste(italic('n'), " = 11"  )), side =3, line =0, cex = 0.65)
mtext("Overlap", side =2, line =2, cex = 0.65)

text(seq(1,numSources,by=1), par("usr")[3]-0.25, srt = 60, adj= 1, xpd = TRUE, labels = paste(Sources), cex=0.65)	


#par(old.par)
# end———————————————
dev.off()




