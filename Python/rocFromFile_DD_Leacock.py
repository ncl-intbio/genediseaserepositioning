#!/usr/bin/env python
from __future__ import division
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import os

sns.set_style("whitegrid")
sns.set(font="Arial")

sources = ["ALL","CTD","PREDICT","SIDER","NDFRT"]

directory="/Users/joemullen/Desktop/GeneDiseaseRepositioning/GeneDisease_OP/ROC_OP/Leacock/"
colours = ['g','r','b','y','0.75','k','c','m','#FF6961','#03C03C','#bc82bd','#555555']
random = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]

def plot(file, colour, descrip):
    y = []
    x = []
    specs = []
    sens = []
    tuples = [(0,0)]
    for line in open(file):
        t = line.split("\t")
        specs.append(float(t[0]))
        sens.append(float(t[1]))

    for i in range(0,len(specs)):
        tuples.append((1-specs[i],sens[i]))

    tuples.append((1,1))

    plot_data = sorted(tuples, key=lambda pair: pair[0])

    for pair in plot_data:
        if (pair[0] <= 1.0) & (pair[1] <= 1.0):
            x.append(pair[0])
            y.append(pair[1])
	
    auc = np.trapz(y,x)
    plt.plot(x, y, color = colour, marker = "o", label = str("{0:.3f}".format(auc))+" (Sim="+descrip+")", markersize = 5)

def run(sourcename):
    plt.clf()
    count =0
    for filename in os.listdir(directory):
        if filename.startswith(sourcename):
            print filename
            s, disCat, min, sco, disType, b, f, thresh, e = filename.split("_")
            plot(directory+filename, colours[count], str(thresh[0:5]))
            count += 1

    plt.xlabel("1 - Specificity")
    plt.ylabel("Sensitivity")
    plt.plot(random,random, linestyle='dashed', color='red', linewidth=1, label='random')
    plt.legend(loc=0)
    #plt.show()
    plt.savefig("/Users/joemullen/Desktop/GeneDiseaseRepositioning/Graphs/ROC_DD_Leacock_"+sourcename+".eps", format = 'eps', dpi = 600, bbox_inches = "tight")

for sr in sources:
    run(sr)
