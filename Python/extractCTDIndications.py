#!/usr/bin/python

import re
import sys
from sets import Set


############################
#
# 1. Do we want only the associations that aren't supported by evidence? (NO)
#
# 2. Think about- do we want the highest scoring association for each drug-disease or do we want each drug-gene disease?
#
############################


mapping = {}
associations= {}
DBIDs = Set()

count = 0

def getDBFromCTD():
    mapping = {}
    for line in open(str(sys.argv[2])):
        if not line.startswith('#'):
            split = line.split('\t')
            chemName = split[0]
            chemID = split[1]
            CasRN= split[2]
            definition = split[3]
            parentID = split[4]
            treeNumbers = split [5]
            parentTreeNum = split[6]
            Synoynm = split [7]
            drugBank = str(split[8].replace('\n',''))
            if drugBank.startswith('DB'):
                DBIDs.add(drugBank)
                chemID = chemID.replace('MESH:', '')
                mapping[chemID] = str(drugBank)
    print 'We have '+str(len(DBIDs))+ ' DrugBank IDs'
    return mapping

############################
#
# Get the curated drug-disease associations from CTD
#
############################

def getCuratedDrugDisease():
    assCheck= Set()
    for line in open(str(sys.argv[1])):
        if not line.startswith('#'):
            split = line.split('\t')
            chemName = split[0]
            chemID = split[1]
            CasRN= split[2]
            disName = split[3]
            disID = split[4]
            dirEv = split[5]
            inferGeneSym = split[6]
            inferScore = split[7]
            if dirEv:
                #print dirEv
                if mapping.has_key(chemID):
                    drugBank = mapping.get(chemID)
                    #print drugBank
                    if 'therapeutic' in dirEv:
                        assCheck.add(drugBank+'\t'+disID.replace('MESH:', ''))
    return assCheck

def orderAndWriteCurated(assCheck):
    count =0
    outputFile = open(str(sys.argv[3])+'CTD_Curated_Indications.txt', 'w')
    for ass in assCheck:
        if '|' in ass.split('\t')[0]:
            for s in ass.split('\t')[0].split('|'):
                outputFile.write(s+'\t'+ass.split('\t')[1]+'\n')
                count +=1
        else:
            outputFile.write(ass+'\n')
            count +=1
    outputFile.close()
    print 'Added '+ str(count) + ' curated associations to file...'

mapping = getDBFromCTD()
ass = getCuratedDrugDisease()
orderAndWriteCurated(ass)
#print 'We have '+str(len(assCheck)) + ' associations'

############################
#
# Get the inferred drug-disease associations from CTD
#
############################

def getInferredDrugDisease():
    for line in open(str(sys.argv[1])):
        if not line.startswith('#'):
            split = line.split('\t')
            chemName = split[0]
            chemID = split[1]
            CasRN= split[2]
            disName = split[3]
            disID = split[4]
            dirEv = split[5]
            inferGeneSym = split[6]
            inferScore = split[7]
            if not dirEv:
                if mapping.has_key(chemID):
                    drugBank = mapping.get(chemID)
                    associations.update({drugBank+'\t'+disID+'\t'+inferGeneSym:float(inferScore)})
            else:
                print dirEv


def orderAndWrite():
    count =0
    outputFile = open('CTDInferred.txt', 'w')
    sortedAss = sorted(associations, key=associations.__getitem__,reverse=True)
    for ass in sortedAss:
        if '|' in ass.split('\t')[0]:
            for s in ass.split('\t')[0].split('|'):
                outputFile.write(s+'\t'+ass.split('\t')[1]+'\t'+ass.split('\t')[2]+'\t'+str(associations.get(ass))+'\n')
                count +=1
        else:
            outputFile.write(ass+'\t'+str(associations.get(ass))+'\n')
            count +=1
    outputFile.close()
    print 'Added '+ str(count) + ' inferred associations to file...'

#mapping = getDBFromCTD()
#getDrugDisease()
#orderAndWrite()




