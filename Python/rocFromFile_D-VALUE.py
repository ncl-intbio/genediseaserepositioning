#!/usr/bin/env python
from __future__ import division
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
sns.set(font="Arial")
sns.set_style("whitegrid")


directory="/Users/joemullen/Desktop/GeneDiseaseRepositioning/GeneDisease_OP/LLS_OP/Plot/"

dValues = [1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0]
colours = ['g','r','b','y','0.75','k','c','m']
random = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]

def plot(file, colour, dv):
    y = []
    x = []
    specs = []
    sens = []
    tuples = []
    for line in open(file):
        a, b, c, sen, spec = line.split("\t")
        specs.append(float(spec))
        sens.append(float(sen))

    for i in range(0,len(specs)):
        tuples.append((1-specs[i],sens[i]))

    plot_data = sorted(tuples, key=lambda pair: pair[0])

    for pair in plot_data:
        if (pair[0] < 1.0) & (pair[1] < 1.0):
            x.append(pair[0])
            y.append(pair[1])
	
    auc = np.trapz(y,x)
    plt.plot(x, y, color = colour, marker = "o", label = str("{0:.3f}".format(auc))+" (DV="+str(dv)+")", markersize = 5)

count =0
for dv in dValues:
    plot(directory+"scored_Associations_"+str(dv)+"_UNIPROT.txt", colours[count], dv)
    count += 1

plt.xlabel("1 - Specificity ")
plt.ylabel("Sensitivity")
plt.plot(random,random, linestyle='dashed', color='red', linewidth=1, label='random')
plt.legend(loc=0)
#plt.show()
plt.savefig("/Users/joemullen/Desktop/GeneDiseaseRepositioning/Graphs/ROC_GD.eps", format = 'eps', dpi = 600, bbox_inches = "tight")
