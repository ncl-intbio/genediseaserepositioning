/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.Export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.MetaDataInterface;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Exports a graph to tsv. Produces 3 files; NodeSem (all nodes and associated
 * atts), RelSem (all edges and associated atts) and EdgeList (all TO FROM and
 * Edges).
 */
public class TSV {

    private Neo4J handler;
    private String outPutFile;
    private String outPutNodeSem;
    private String outPutRelSem;
    private Map<String, MetaDataInterface> jbeanMap;
    private String[] adjacencyMatrix;
    private Map<String, String> nodeID2Node;
    private String[] orderNodeAtts;
    private String[] orderRelAtts;

    public static void main(String[] args) {

        Neo4J handler = new Neo4J("/Volumes/MyPassport/PLOS_Paper_GRAPH_GD");
        String op = "PLOS_CSV";
        TSV t = new TSV(handler, op);
        t.run();
    }

    /**
     * Constructor.
     *
     * @param handler Point to a graph that is to be exported.
     * @param outPutFile Provide a project name (DOES NOT END IN .tsv)
     */
    public TSV(Neo4J handler, String outPutFile) {
        this.handler = handler;
        this.outPutFile = outPutFile + "_edgeList.tsv";
        this.outPutNodeSem = outPutFile + "_NodeSem.tsv";
        this.outPutRelSem = outPutFile + "_RelSem.tsv";
        this.nodeID2Node = new HashMap<String, String>();
    }

    /**
     * Run the entire export.
     */
    public void run() {

        handler.initialiseDatabaseConnection();
        jbeanMap = handler.returnAllNodesMap();
        handler.finaliseDatabaseConnectionNoUPDATES();

        this.adjacencyMatrix = new String[jbeanMap.size()];

        //first get all the possible attributes
        Set<String> nodeAtt = new HashSet<String>();
        Set<String> relAtt = new HashSet<String>();

        for (String keys : jbeanMap.keySet()) {
            Map<String, Object> props = jbeanMap.get(keys).getAttributes();
            for (String p : props.keySet()) {
                if (!p.contains("Prov")) {
                    nodeAtt.add(p.replaceAll("\\d", ""));
                    //firstname1 = firstname1.replaceAll("\\d","");
                    // System.out.println(p+"\t"+props.get(p));
                }
            }
            //get each java bean object relations as set of destination java bean objects
            Map<MetaDataInterface, SSIPRelationType> relations = jbeanMap.get(keys).getRelations();
            for (MetaDataInterface met : relations.keySet()) {
                if (!relations.get(met).getAttributes().keySet().isEmpty()) {
                    for (String edgeAtts : relations.get(met).getAttributes().keySet()) {
                        if (!edgeAtts.contains("Prov") && !edgeAtts.equals("weight")) {
                            relAtt.add(edgeAtts);
                        }
                    }
                }
            }
            relAtt.add("type");
        }

        this.orderNodeAtts = nodeAtt.toArray(new String[0]);
        this.orderRelAtts = relAtt.toArray(new String[0]);

        getAllNodeInfo();
        getAllRelInfo();
    }

    /**
     * Creates the node semantic file.
     */
    public void getAllNodeInfo() {
        BufferedWriter bnode = null;
        try {
            bnode = new BufferedWriter(new FileWriter(new File(outPutNodeSem)));
        } catch (IOException ex) {
            Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        //re-order node atts so that the type is first
        String repl = orderNodeAtts[0];
        int replace = 0;
        for (int i = 0; i < orderNodeAtts.length; i++) {
            if (orderNodeAtts[i].equals("type")) {
                replace = i;
                break;
            }
        }

        orderNodeAtts[0] = "type";
        orderNodeAtts[replace] = repl;

        // NODE SEMANTIC INFO
        int count = 0;
        StringBuilder bui = new StringBuilder();
        for (int i = 0; i < orderNodeAtts.length; i++) {
            bui.append("\t").append(orderNodeAtts[i]);
        }
        bui.append("\n");

        try {
            bnode.append(bui.toString());
        } catch (IOException ex) {
            Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (String key : jbeanMap.keySet()) {
            bui = new StringBuilder();
            String nodeID = jbeanMap.get(key).getId();
            adjacencyMatrix[count] = nodeID;//[0] = nodeID;
            bui.append("v_").append(count);
            nodeID2Node.put(nodeID, "v_" + count);
            Map<String, Object> props = jbeanMap.get(key).getAttributes();

            for (int i = 0; i < orderNodeAtts.length; i++) {
                StringBuilder buiProp = new StringBuilder();
                for (String propsNode : props.keySet()) {
                    if (propsNode.replaceAll("\\d", "").equals(orderNodeAtts[i])) {
                        buiProp.append(";" + props.get(propsNode));
                    }

                }
                if (buiProp.toString().startsWith(";")) {
                    bui.append("\t").append(buiProp.toString().substring(1));
                } else {
                    bui.append("\t").append("-");
                }
            }

            //only add it if it has a type
            if (props.containsKey("type")) {
                bui.append("\n");
                try {
                    bnode.append(bui.toString());
                } catch (IOException ex) {
                    Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
                }
                count++;
            }

        }

        try {
            bnode.close();
        } catch (IOException ex) {
            Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Creates the rel semantic file and the edgelist file.
     */
    public void getAllRelInfo() {

        // RELATIONS SEMANTIC INFO
        BufferedWriter brel = null;
        try {
            brel = new BufferedWriter(new FileWriter(new File(outPutRelSem)));
        } catch (IOException ex) {
            Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedWriter relList = null;
        try {
            relList = new BufferedWriter(new FileWriter(new File(outPutFile)));
        } catch (IOException ex) {
            Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            relList.append("From\tTo\tEdge\n");
        } catch (IOException ex) {
            Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        //re-order rel atts so that the type is first
        String repl = orderRelAtts[0];
        int replace = 0;
        for (int i = 0; i < orderRelAtts.length; i++) {
            if (orderRelAtts[i].equals("type")) {
                replace = i;
                break;
            }
        }

        orderRelAtts[0] = "type";
        orderRelAtts[replace] = repl;

        StringBuilder bui = new StringBuilder();
        for (int i = 0; i < orderRelAtts.length; i++) {
            bui.append("\t").append(orderRelAtts[i]);
        }
        bui.append("\n");
        try {
            brel.append(bui.toString());
        } catch (IOException ex) {
            Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        int count = 0;
        //iterate through java bean list again to create 
        for (String fromNodeString : jbeanMap.keySet()) {
            Map<MetaDataInterface, SSIPRelationType> relations = jbeanMap.get(fromNodeString).getRelations();
            String fromID = nodeID2Node.get(jbeanMap.get(fromNodeString).getId());
            for (MetaDataInterface met : relations.keySet()) {
                String toID = nodeID2Node.get(met.getId());
                bui = new StringBuilder();
                bui.append("e_").append(count);
                //if (!relations.get(met).getAttributes().keySet().isEmpty()) {
                for (int i = 0; i < orderRelAtts.length; i++) {
                    if (orderRelAtts[i].equals("type")) {
                        bui.append("\t").append(trimType(relations.get(met).getType()));
                    } else {
                        if (relations.get(met).getAttributes().keySet().contains(orderRelAtts[i])) {
                            bui.append("\t").append(relations.get(met).getAttributes().get(orderRelAtts[i]).toString());
                        } else {
                            bui.append("\t").append("-");
                        }
                    }
                }

                try {
                    relList.append(fromID + "\t" + toID + "\te_" + count + "\n");
                } catch (IOException ex) {
                    Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
                }

                count++;
                bui.append("\n");
                try {
                    brel.append(bui.toString());
                } catch (IOException ex) {
                    Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        try {
            brel.close();
        } catch (IOException ex) {
            Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            relList.close();
        } catch (IOException ex) {
            Logger.getLogger(TSV.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String trimType(String untrimmed) {
        return untrimmed.replace("RelationshipTypeToken[name:", "").split(",")[0].toLowerCase();
    }
}
