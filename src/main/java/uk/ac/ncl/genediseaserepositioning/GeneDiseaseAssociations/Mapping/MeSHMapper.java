/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author joe
 *
 * http://www.disi.unige.it/person/MascardiV/Download/MetricheOntologieFinal.pdf
 *
 * Look at this paper: http://nas.uhcl.edu/almubaid/tsmcc-al-mubaid.pdf (notes
 * below are from here) Semantic similarity measuring techniques can be rough;ly
 * classified into two classes: 1. structure-based measures are the measures
 * that use the ontology hierarchy structure 2. Information content (IC) based
 * measures are those measures that use the IC of concepts derived from corpus
 * statistics.
 *
 * The primitive approach is to use the shortest path.
 *
 * Class takes 2 sets of MeSH tree terms and tries to map them. 2 options: 1 try
 * and map at any level. 2. try and map at a given level.
 *
 * Def: DEPTH: depth is the depth of the hierarchy- i.e. the number of relations
 * to get from node in the graph to another.
 *
 *
 *
 */
public class MeSHMapper {

    public static void main(String[] args) {

        MeSHMapper mm = new MeSHMapper(false);
        String t1 = "C10.456.456.56.228.678.456.345.23.";//.450.565.575.650.557.450.565.575.650";
        String t2 = "C10.456.456.56";

        System.out.println(mm.LeacAndChod(mm.getSetFromString(t1), mm.getSetFromString(t2)));
    }

    public int shortestPathLength;
    /**
     * Leacock uses the number of NODES not the number of EDGES. Why does it
     * work with 10 and not 11??
     */
    private static double MAX_DEPTH = 10.0;
    private static double MAX_CHILD_DEPTH = (2.0 / MAX_DEPTH);
    private boolean child = false;
    private boolean breadthOnly;

    public MeSHMapper(boolean breadthONLY) {
        this.breadthOnly = breadthONLY;
    }

    /**
     * An implementation of the Leacock and Chodorow measure. Has been
     * implemented in the medical domain before. Note that this algorithm uses
     * node count as opposed to edge count. Therefore we must add 1 to the
     * pathlength (which we calculate using edge score).
     *
     * :http://www.dtc.umn.edu/publications/reports/2005_12.pdf
     *
     * The higher the score the better!
     *
     * @param termsFrom
     * @param termsTo
     * @return
     */
    public double LeacAndChod(Set<String> termsFrom, Set<String> termsTo) {

        double maxMappingScore = -Math.log((1) / (2 * MAX_DEPTH));
        double score = 0.0;
        if (map(termsFrom, termsTo)) {
            double len = getShortestPathLength();
            //method uses node count not edgecount and so we add 1 

            if (breadthOnly) {
                len += 1;
                score = -Math.log((len) / (2 * MAX_DEPTH));
                //normalize the score
                score = 1 / maxMappingScore * score;
            } else {
                if (child) {
                    len = (len * MAX_CHILD_DEPTH) + 1;
                } else {
                    len += 1;
                }
                score = -Math.log((len) / (2 * MAX_DEPTH));
                //normalize the score
                score = 1 / maxMappingScore * score;
            }

            //  return score;
        }
        return score;
    }

    /**
     * Tries to map two terms at any level/path length. Returns true if mapping
     * possible at any depth. Assigns the lowest level mapping of mapping to
     * mappedAt.
     *
     * @param termsFrom
     * @param termsToo
     * @return
     */
    public boolean map(Set<String> termsFrom, Set<String> termsTo) {

        /**
         * See if the terms are exacts
         */
        for (String from : termsFrom) {
            for (String to : termsTo) {
                if (from.equals(to)) {
                    shortestPathLength = 0;
                    return true;
                }
            }
        }

        /**
         * Next we test to see if any of the terms are children of any of the
         * others
         */
        //change to
        for (int i = 0; i < getMaxDepthOfSet(termsTo); i++) {
            for (String from : termsFrom) {
                for (String to : termsTo) {
                    String prunedTo = getMeshTreeAtDepth(to, (getDepthOfTerm(to) - (i)));
                    if (from.equals(prunedTo)) {
                        shortestPathLength = i + 1;
                        child = true;
                        return true;
                    }
                }
            }
        }

        //change from
        for (int i = 0; i < getMaxDepthOfSet(termsFrom); i++) {
            for (String from : termsFrom) {
                String prunedFrom = getMeshTreeAtDepth(from, (getDepthOfTerm(from) - (i)));
                for (String to : termsTo) {
                    //String prunedTo = getMeshTreeAtDepth(to, (i));
                    if (prunedFrom.equals(to)) {
                        shortestPathLength = i + 1;
                        child = true;
                        return true;
                    }
                }
            }
        }

        /**
         * If the terms are not exacts and are neither parents or children of
         * each other then we will try and identify their least common subsumer
         * (LCS)
         */
        int count = 0;
        String fromBEST = "";
        String toBEST = "";

        for (int i = 0; i < getMaxDepthOfSet(termsFrom) + 1; i++) {
            for (String from : termsFrom) {
                String prunedFrom = getMeshTreeAtDepth(from, (i));
                for (String to : termsTo) {
                    String prunedTo = getMeshTreeAtDepth(to, (i));
                    if (prunedFrom.equals(prunedTo)) {
                        if (i >= count) {

                            if (count == 0) {
                                count = i;
                                fromBEST = from;
                                toBEST = to;
                            } else if (getDepthOfTerm(from) + getDepthOfTerm(to) <= getDepthOfTerm(fromBEST) + getDepthOfTerm(toBEST)) {
                                count = i;
                                fromBEST = from;
                                toBEST = to;
                            }
                        }

                    }
                }
            }
        }

        /**
         * Knowing the LCS we can then calculate the path length
         */
        int pathLength = 0;

        if (count > 0) {
            pathLength += (getDepthOfTerm(fromBEST)) - (count - 1);
            pathLength += getDepthOfTerm(toBEST) - (count - 1);
            shortestPathLength = pathLength;
            child = false;
            return true;
        }

        return false;
    }

    /**
     * Returns a Set representation of a String of MeSH tree terms.
     *
     * @param treeTerms
     * @return
     */
    public Set<String> getSetFromString(String treeTerms) {
        Set<String> terms = new HashSet<>();
        String[] split = treeTerms.split("\\|");
        terms.addAll(Arrays.asList(split));
        return terms;
    }

    /**
     * Return the max depth of a set of terms term.
     */
    public int getMaxDepthOfSet(Set<String> term) {

        int max = 0;
        for (String terms : term) {
            String[] splitt = terms.split("\\.");
            if (splitt.length > max) {
                max = splitt.length;
            }
        }

        return max - 1;

    }

    /**
     * Return the depth of a term.
     */
    public int getDepthOfTerm(String term) {
        return term.split("\\.").length - 1;

    }

    /**
     * Get the mesh tree term at a given depth.
     */
    public String getMeshTreeAtDepth(String allTrees, int meshtreedepth) {

        String[] branches = allTrees.split("\\.");
        if (branches.length < meshtreedepth) {
            return allTrees;
        }

        String branch = "FIRST";
        for (int p = 0; p < meshtreedepth; p++) {
            if (branch.equals("FIRST")) {
                branch = branches[p];
            } else {
                branch = branch.trim() + "." + branches[p];
            }
        }
        return branch;
    }

    /**
     * Returns the depth at which a mapping was achieved.
     *
     * @return
     */
    public int getShortestPathLength() {
        return shortestPathLength;
    }

}
