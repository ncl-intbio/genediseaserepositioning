/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 *
 * Class extracts all MeSH tree terms from the D MeSH records and the
 * accompanying MeSHUI. Doesn't use the MeSH supplementary records [C] as these
 * don't come with MeSH tree terms.
 *
 */
public class MeSHTree2UI {

    private final static Logger LOGGER = Logger.getLogger(MeSHTree2UI.class.getName());
    private File meshFile;
    private Map<String, String> meshtree2UI;
    private FileManager fm;

    public MeSHTree2UI() {

        this.fm = new FileManager();
        this.meshFile = fm.getMeSHDFile();
        this.meshtree2UI = new HashMap<String, String>();
        extractMeSHTreeTerms2UI();

    }

    public void extractMeSHTreeTerms2UI() {

        BufferedReader br = null;
        String thisLine;
        boolean firstDone = false;
        try {
            //Open the file for reading
            br = new BufferedReader(new FileReader(meshFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MeSHTree2UI.class.getName()).log(Level.SEVERE, null, ex);
        }

        //variables
        String MeSHMainHeader = null;
        String MeSHUI = null;
        Set<String> MeSHTreeTerms = new HashSet<String>();
        try {
            while ((thisLine = br.readLine()) != null) {
                if (thisLine.equals("*NEWRECORD")) {
                    //create new node and reset the variables

                    if (firstDone) {
                        int c = 1;
                        //we are only interested in diseases [begin with C in the hierarchy ] 
                        // and the three concepts of the F branch
                        boolean disease = false;
                        for (String tt : MeSHTreeTerms) {
                            if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                disease = true;
                            }
                        }

                        //if the term is a disease then add it
                        if (disease) {
                            List<String> treeterms = new ArrayList<String>();
                            for (String tt : MeSHTreeTerms) {
                                if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                    treeterms.add(tt);
                                }
                            }

                            Collections.sort(treeterms);
                            meshtree2UI.put(treeterms.toString(), MeSHUI);
                        }
                        MeSHTreeTerms = new HashSet<String>();
                    }
                }
                if (thisLine.startsWith("MH =")) {
                    MeSHMainHeader = split(thisLine);
                }

                if (thisLine.startsWith("UI =")) {
                    MeSHUI = split(thisLine);
                    firstDone = true;
                }

                if (thisLine.startsWith("MN =")) {
                    MeSHTreeTerms.add(split(thisLine));
                }
            }


        } catch (IOException ex) {
            Logger.getLogger(MeSHTree2UI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Splits the MeSH line (aaaa = bbbbb) on the "=" and returns the second
     * element.
     *
     * @return
     */
    public String split(String meshLine) {

        String[] split = meshLine.split("=");
        return split[1].trim();

    }

    /**
     * Method finds the MeSHUI for a disease using a set of meshtree terms.
     *
     */
    public String getMeshUI(List<String> treeTerms) {

        if (meshtree2UI.containsKey(treeTerms.toString())) {
            return meshtree2UI.get(treeTerms.toString());
        } else {
            return "NO_MESH_UI";
        }
    }
}
