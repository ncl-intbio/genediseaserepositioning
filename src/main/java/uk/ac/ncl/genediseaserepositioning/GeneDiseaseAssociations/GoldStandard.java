/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager.DataSource;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class GoldStandard {

    private Set<Pair> truePositives;
    private Serialize ser;
    private Set<String> allDiseases;
    private Set<String> allGenes;
    private DataManager dm;
    private final static Logger LOGGER = Logger.getLogger(GoldStandard.class.getName());
    private String gsName;
    private FileManager fman;
    
    
    public static void main (String [] args){
        GoldStandard gs = new GoldStandard(DataSource.UNIPROT);
    
    }

    public GoldStandard(DataManager.DataSource goldDataSource) {
        this.ser = new Serialize();
        this.dm = new DataManager();
        this.fman = new FileManager();
        this.truePositives = dm.getAssociations(goldDataSource);
        //get the genes from the text file created during the UniProt parse
        this.allGenes = new HashSet<String>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fman.getSuppFolder() + fman.getFileSeparator() + "UniProt_GeneIDs.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoldStandard.class.getName()).log(Level.SEVERE, null, ex);
        }
        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                allGenes.add(thisLine.trim());
            }
        } catch (IOException ex) {
            Logger.getLogger(GoldStandard.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(GoldStandard.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, "Have {0} geneIDs from UniProt", allGenes.size());
        this.allDiseases = ser.deserializeSetString(fman.getSuppFolder() + fman.getFileSeparator() + "MeSH_meshuis.ser");   
        LOGGER.log(Level.INFO, "Have {0} MeSHUIs from MeSH", allDiseases.size());
        this.gsName = goldDataSource.getText();
    }

    /**
     * Constructor.
     *
     * @return
     */
    public Set<Pair> getTruePositives() {
        return truePositives;
    }

    public String getGSName() {

        return gsName;
    }

    /**
     * Get the number of genes that are covered in the gold standard.
     *
     * @return
     */
    public int getNumberOfGSGenes() {
        Set<String> genes = new HashSet<String>();
        for (Pair p : truePositives) {
            genes.add(p.getEntrezGeneSymbol());

        }
        return genes.size();
    }

    /**
     * Get the of genes that are covered in the gold standard.
     *
     * @return
     */
    public Set<String> getGSGenes() {
        Set<String> genes = new HashSet<String>();
        for (Pair p : truePositives) {
            genes.add(p.getEntrezGeneSymbol());

        }
        return genes;
    }

    /**
     * Get the number of diseases that are covered in the gold standard.
     *
     * @return
     */
    public int getNumberOfGSDiseases() {
        Set<String> diseases = new HashSet<String>();
        for (Pair p : truePositives) {
            diseases.add(p.getMeSHUI());
        }
        return diseases.size();
    }

    /**
     * Get the number of diseases that are covered in the gold standard.
     *
     * @return
     */
    public Set<String> getGSDiseases() {
        Set<String> diseases = new HashSet<String>();
        for (Pair p : truePositives) {
            diseases.add(p.getMeSHUI());
        }
        return diseases;
    }

    /**
     * Returns the number of possible gene-disease pairs between MeSH diseases
     * and EntrezGeneSymbols that are not captured in Pharma.
     *
     * @return
     */
    public int getNumberOfTrueNegatives() {

        //get all the MeSH terms
        int MeSH_Diseases = allDiseases.size();
        //get all the Gene symbols
        int UniProt_genes = allGenes.size();
        //multiply mesh terms by the power of gene symbols
        int possible = (int) Math.pow(getNumberOfGSGenes() * 1.0, getNumberOfGSDiseases() * 1.0);
        //extract the number of the true positives
        return (possible - truePositives.size());
    }
}
