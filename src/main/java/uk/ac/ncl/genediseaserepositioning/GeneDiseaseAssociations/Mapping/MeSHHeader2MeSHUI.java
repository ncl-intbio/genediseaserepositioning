/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joe
 *
 * Class takes a MeSHTreeterm and returns the corresponding MeSHTree terms
 * (C535325)
 *
 */
public class MeSHHeader2MeSHUI {

    private String DFile = "/Volumes/MyPassport/Datasets/MeSH/d2015.bin";
    private Map<String, String> MeSHHeader2MeSHUI;
    private boolean outPutDiseaseUIs;
    private BufferedWriter outPut;

    public static void main(String[] args) {
        MeSHHeader2MeSHUI gm = new MeSHHeader2MeSHUI(true);
        //System.out.println(gm.getDUI("C04"));
        // System.out.println(gm.getTreeTerms("C535325"));


    }

    public MeSHHeader2MeSHUI(boolean outPutUis) {
        this.outPutDiseaseUIs = outPutUis;
        if (outPutDiseaseUIs) {
            try {
                outPut = new BufferedWriter(new FileWriter(new File("MeSHUIs.txt")));
            } catch (IOException ex) {
                Logger.getLogger(MeSHHeader2MeSHUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.MeSHHeader2MeSHUI = new HashMap<String, String>();
        getallUIs();
    }

    public void getallUIs() {
        try {
            //Set<String> meshuis = new HashSet<String>();
            int maxDepth = 0;
            String longest = "";


            BufferedReader br = null;
            String thisLine;
            boolean firstDone = false;
            //Open the file for reading
            br = new BufferedReader(new FileReader(DFile));

            //variables
            String MeSHMainHeader = null;
            String MeSHUI = null;
            Set<String> MeSHTreeTerms = new HashSet<String>();
            try {
                while ((thisLine = br.readLine()) != null) {

                    // System.out.println(thisLine);
                    if (thisLine.equals("*NEWRECORD")) {
                        //create new node and reset the variables

                        if (firstDone) {
                            //we are only interested in diseases [begin with C in the hierarchy ] 
                            // and the three concepts of the F branch
                            boolean disease = false;
                            for (String tt : MeSHTreeTerms) {
                                if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                    disease = true;
                                }
                            }
                            MeSHTreeTerms = new HashSet<String>();
                        }

                        if (MeSHMainHeader != null && MeSHUI != null) {
                            if (outPutDiseaseUIs) {
                                outPut.append(MeSHUI + "\n");
                            }
                            MeSHHeader2MeSHUI.put(MeSHMainHeader, MeSHUI);
                        }

                    }
                    if (thisLine.startsWith("UI =")) {
                        MeSHUI = split(thisLine);
                        firstDone = true;
                    }

                    if (thisLine.startsWith("MN =")) {
                        MeSHTreeTerms.add(split(thisLine));
                    }

                    if (thisLine.startsWith("MH =")) {
                        MeSHMainHeader = (split(thisLine).toLowerCase());
                    }

                }
                if (outPutDiseaseUIs) {
                    outPut.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(MeSHHeader2MeSHUI.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(MeSHHeader2MeSHUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Splits the MeSH line (aaaa = bbbbb) on the "=" and returns the second
     * element.
     *
     * @return
     */
    public String split(String meshLine) {

        String[] split = meshLine.split("=");
        return split[1].trim();

    }

    /**
     * Method returns the MeSH UI from a corresponding MeSH header.
     *
     */
    public String getMeshUI(String MeSHHeader) {

        if (MeSHHeader2MeSHUI.containsKey(MeSHHeader.toLowerCase())) {
            return MeSHHeader2MeSHUI.get(MeSHHeader.toLowerCase());
        } else {
            return "NO_MESH_UI";
        }
    }
}
