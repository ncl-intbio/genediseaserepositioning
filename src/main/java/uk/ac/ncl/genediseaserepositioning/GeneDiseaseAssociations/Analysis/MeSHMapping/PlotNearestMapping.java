///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.MeSHMapping;
//
//import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData;
//import uk.ac.ncl.genediseaserepositioning.FileManager;
//import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DataSourceKNOWNS;
//import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DiseaseCat;
//import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DiseaseType;
//import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow;
//
//
///**
// *
// * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
// *
// * Class goes through all the predicted interactions and outputs the closest
// * score for the closest mapping to the each known using the Leacock and
// * Chodorow distance measure.
// *
// */
//public class PlotNearestMapping {
//
//    private Set<Pair> goldStandard;
//    private String scoredAssociationsFile;
//    private String outPut;
//    private int assFromPos;
//    private int assToPos;
//    private int assScorePos;
//    private int truePositives;
//    private int falsePositives;
//    private final static Logger LOGGER = Logger.getLogger(PlotNearestMapping.class.getName());
//    private BufferedWriter bw;
//    private Map<Pair, Double> scoredAssociations;
//    private List<Double> scores;
//    private int check;
//    private FileManager fm;
//    private DataSourceKNOWNS datasource;
//    private DiseaseCat diseaseCat;
//    private DiseaseType diseaseType;
//    private Set<String> DBIDs;
//    private LeacockAndChodorow mesh;
//    private DrugDiseaseData indicationInfo;
//
//    public static void main(String[] args) {
//        String dir = "/Volumes/MyPassport/SearchOP/";
//        String fileNameStart = "_minusSEs_0.463_";
//        String fileNameEnd = "dis_MINUSADME_UNIQUE.txt";
//        String project = "UniProtGS3_DB42";
//        DataSourceKNOWNS[] datasources = DataSourceKNOWNS.values();
//        DiseaseCat[] diseaseCat = DiseaseCat.values();
//        DiseaseType[] diseaseType = DiseaseType.values();
//        for (int ds = 0; ds < datasources.length; ds++) {
//            for (int dcat = 0; dcat < diseaseCat.length; dcat++) {
//                for (int dtype = 0; dtype < diseaseType.length; dtype++) { 
//                    //System.out.println(datasources[ds].getText()+"\t"+diseaseCat[dcat].getText()+"\t"+diseaseType[dtype].getText());
//                    String predicted = dir+project+"/"+(diseaseCat[dcat].getText()+fileNameStart+diseaseType[dtype].getText()+fileNameEnd);
//                    PlotNearestMapping plot = new PlotNearestMapping(predicted, 0, 1, 2, project, datasources[ds], diseaseCat[dcat], diseaseType[dtype]);
//                    plot.getGS();
//                    plot.getInferredAssociations();
//                    plot.goScore();
//                }
//            }
//
//        }
//    }
//
//    public PlotNearestMapping(String associationsFile, int from, int to, int score, String project, DataSourceKNOWNS goldStandard, DiseaseCat category, DiseaseType type) {
//
//        String meshD = "/Volumes/MyPassport/Datasets/MeSH/d2015.bin";
//        String meshC = "/Volumes/MyPassport/Datasets/MeSH/c2015.bin";
//        this.mesh = new LeacockAndChodorow(true, meshD, meshC);
//        this.indicationInfo = new DrugDiseaseData();
//        this.DBIDs = new HashSet<>();
//        this.datasource = goldStandard;
//        this.diseaseCat = category;
//        this.diseaseType = type;
//        this.scoredAssociationsFile = associationsFile;
//        this.fm = new FileManager();
//        String output = fm.getLeacockFile() + project;
//        File f = new File(output);
//        // if the directory does not exist, create it
//        if (!f.exists()) {
//            LOGGER.log(Level.INFO, "Creating directory... {0}", output);
//            boolean result = false;
//            try {
//                f.mkdir();
//                result = true;
//            } catch (SecurityException se) {
//                //handle it
//            }
//            if (result) {
//                LOGGER.log(Level.INFO, "Created directory... {0}", output);
//            }
//        }
//        File in = new File(scoredAssociationsFile);
//        this.outPut = output + "/" + datasource.getText() + "_" + in.getName().replace(".txt", "") + "_LLS.txt";
//        this.assFromPos = from;
//        this.assToPos = to;
//        this.goldStandard = new HashSet<>();
//        this.assScorePos = score;
//        this.scoredAssociations = new HashMap<>();
//        this.check = 0;
//        try {
//            bw = new BufferedWriter(new FileWriter(outPut));
//        } catch (IOException ex) {
//            LOGGER.log(Level.SEVERE, null, ex);
//        }
//    }
//
//    /**
//     * Get all the known associations from the data source as specified.
//     */
//    public void getGS() {
//        this.goldStandard = indicationInfo.getAllKnown(datasource);
//        this.goldStandard = indicationInfo.prune(goldStandard, datasource, diseaseType, diseaseCat);
//        this.DBIDs = indicationInfo.getDrugName2DBID();
//    }
//
//    /**
//     * First of all get all the true positives of the score associations
//     */
//    public void getInferredAssociations() {
//
//        BufferedReader br = null;
//        int count = 0;
//        //variables
//        String from;
//        String fromID;
//        String to;
//        double score;
//        this.truePositives = 0;
//        this.falsePositives = 0;
//        try {
//            //BufferedReader br;
//            br = new BufferedReader(new FileReader(new File(scoredAssociationsFile)));
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(PlotNearestMapping.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        String thisLine;
//        try {
//            while ((thisLine = br.readLine()) != null) {
//                String[] split = thisLine.split("\t");
//                from = split[assFromPos];
//                to = split[assToPos];
//                score = Double.parseDouble(split[assScorePos]);
//                count++;
//                Pair pair = new Pair(from, to);
//                scoredAssociations.put(pair, score);
//            }
//        } catch (IOException ex) {
//            LOGGER.log(Level.SEVERE, null, ex);
//        }
//        LOGGER.log(Level.INFO,
//                "We have {0} different scored associations ...", scoredAssociations.size());
//    }
//
//    /**
//     * For each of the gold standard
//     */
//    public void goScore() {
//        LOGGER.log(Level.INFO, "Mapping scored associations from to the knowns...{0}", scoredAssociationsFile);
//        for (Pair gs : goldStandard) {
//            double highest = 0.0;
//            String drugGS = gs.getEntrezGeneSymbol();
//            Pair predicted = new Pair("null", "null");
//            for (Pair pred : scoredAssociations.keySet()) {
//                String drugPRED = pred.getEntrezGeneSymbol();
//                if (highest == 1.0) {
//                    break;
//                }
//                if (drugGS.equals(drugPRED)) {
//                    double score = getBestMapToGS(gs.getMeSHUI(), pred.getMeSHUI());
//                    if (score > highest) {
//                        highest = score;
//                        predicted = pred;
//                    }
//                }
//            }
//            try {
//                bw.append(gs.toString() + "\t" + highest + "\t" + predicted.toString() + "\t" + scoredAssociations.get(predicted) + "\n");
//            } catch (IOException ex) {
//                Logger.getLogger(PlotNearestMapping.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//        }
//        try {
//            bw.close();
//        } catch (IOException ex) {
//            Logger.getLogger(PlotNearestMapping.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    /**
//     * Get the mapping score using the Leacock and Chodorow scoring metric.
//     *
//     * @param gs Gold standard MeSH UI.
//     * @param inferred Inferred MeSH UI.
//     * @return Score
//     */
//    public double getBestMapToGS(String gs, String inferred) {
//        if (inferred.equals("NO_MESH")) {
//            return 0.0;
//        } else {
//            return mesh.getDistance(gs, inferred);
//        }
//    }
//}
