/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.MeSHTree2UI;

/**
 * Parser takes the GOF LOF interactions created by Peter and integrates them
 * into the network provided. Ideally should be using the hc (high confidence)
 * files. Genes [HUGO id]--> Disease [MeSH id]
 *
 * CHECK; do we index on the MeSH disease UIs? CHECK; do we need to know disease
 * type at this stage?
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class GOF_LOF implements GeneDiseaseParser {

    private File GOFFile;
    private File LOFFile;
    private Set<String> allGenes;
    private FileManager fm;
    // private GetMeSHUI getMeSHUI;
    private Map<String, String> alreadyMappedUIs;
    private MeSHTree2UI getMeSHUI;
    private String GOF_rel;
    private String LOF_rel;
    private String gene;
    private int GOFint = 0;
    private int LOFint = 0;
    private Serialize ser;
    private String filenames;
    private Set<Pair> CuratedAssociations = new HashSet<Pair>();
    private Set<Pair> Unmappable = new HashSet<Pair>();
    private final static Logger LOGGER = Logger.getLogger(GOF_LOF.class.getName());
    private HashMap<String, String> geneSymbol2Id;

    public static void main(String[] args) {
        GOF_LOF gof = new GOF_LOF();
        gof.Parse();

    }

    /**
     * Constructor
     */
    public GOF_LOF() {
        this.fm = new FileManager();
        this.GOFFile = fm.getGOFFile();
        this.LOFFile = fm.getLOFFile();
        this.ser = new Serialize();
        this.filenames = "GOFLOF";
        this.CuratedAssociations = new HashSet<Pair>();
        this.Unmappable = new HashSet<Pair>();
        this.allGenes = new HashSet<String>();
        this.getMeSHUI = new MeSHTree2UI();
        this.alreadyMappedUIs = new HashMap<>();
        this.geneSymbol2Id = ser.deserializeHashMapString(fm.getSuppFolder() + fm.getFileSeparator() + "UniProt_geneSym2ID.ser");

    }

    /**
     * Run the parser.
     */
    @Override
    public void Parse() {

        getAssociations(GOFFile);
        getAssociations(LOFFile);
        writeToFile();

    }

    /**
     * Populate HashMaps with the relations.
     */
    public void getAssociations(File file) {

        BufferedReader br = null;
        int localSucc = CuratedAssociations.size();
        int localUnmappable = Unmappable.size();

        try {
            LOGGER.log(Level.INFO, "Parsing GOF/LOF file {0} ....", new Object[]{file});
            int count = 0;

            br = new BufferedReader(new FileReader(file));
            String line;

            String hugoID = null;
            String geneSymbol = null;
            String diseaseMeSHTree = null;
            String diseaseMeSHUI = null;
            while ((line = br.readLine()) != null) {
                if (!line.isEmpty()) {
                    String[] split = line.split("\t");
                    diseaseMeSHTree = split[1];
                    List<String> treeterms = new ArrayList<String>();
                    treeterms.add(diseaseMeSHTree);
                    if (alreadyMappedUIs.containsKey(diseaseMeSHTree)) {
                        diseaseMeSHUI = alreadyMappedUIs.get(diseaseMeSHTree);
                    } else {
                        diseaseMeSHUI = getMeSHUI.getMeshUI(treeterms);
                        alreadyMappedUIs.put(diseaseMeSHTree, diseaseMeSHUI);
                    }
                    hugoID = split[0];
                    if (geneSymbol2Id.containsValue(hugoID)) {
                        for (String symbol : geneSymbol2Id.keySet()) {
                            if (geneSymbol2Id.get(symbol).equals(hugoID)) {
                                geneSymbol = symbol;
                            }
                        }
                    }

                    Pair pair = new Pair(geneSymbol, diseaseMeSHUI);
                    if (diseaseMeSHUI.length() != 7 || diseaseMeSHUI.contains(".") || geneSymbol == null) {
                        Unmappable.add(pair);
                    } else {
                        CuratedAssociations.add(pair);
                    }
                }
            }
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(GOF_LOF.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Mapped {0} associations from {1} ...", new Object[]{CuratedAssociations.size() - localSucc, file});
        LOGGER.log(Level.INFO, "Unsuccesful {0} associations from {1} ...", new Object[]{Unmappable.size() - localUnmappable, file});


    }

    public void writeToFile() {

        BufferedWriter bw = null;

        try {
            bw = new BufferedWriter(new FileWriter(new File(fm.getStandardisedFolder().getPath() + fm.getFileSeparator() + filenames + ".txt")));
        } catch (IOException ex) {
            Logger.getLogger(MGD.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Pair p : CuratedAssociations) {
            try {
                bw.append(p.getEntrezGeneSymbol() + "\t" + p.getMeSHUI() + "\n");
            } catch (IOException ex) {
                Logger.getLogger(GOF_LOF.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(GOF_LOF.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO,
                "Parsed {0} GOFLOF relations", CuratedAssociations.size());
        LOGGER.log(Level.INFO,
                "{0} associations containing diseases unmappable MeSH terms", Unmappable.size());
        ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() + fm.getFileSeparator() + filenames + ".ser");


        logInfo();

    }

    @Override
    public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(CuratedAssociations.size() + Unmappable.size()).append("\t").append(dateFormat.format(date)).append("\n");

        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true)));
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
