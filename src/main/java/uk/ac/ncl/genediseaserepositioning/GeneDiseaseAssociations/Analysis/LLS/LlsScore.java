package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.LLS;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.GoldStandard;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/*--------------------------------------------------------------------*\
 |                        Class LlsScore.java                           |
 |               Class which takes the gold standard                    |
 |      scores each datasets confidence using the Lee LLS method        |
 |                                                                      |
 |                    Author: Katherine James                           |
 |                      Commenced: 28/01/08                             |
 |                    Last Edited: 28/04/09                             |
 \*--------------------------------------------------------------------*/
public class LlsScore {

    private GoldStandard goldStandard;
    private final static Logger LOGGER = Logger.getLogger(LlsScore.class.getName());
    private DataManager dm;
    private HashMap<String, Set<Pair>> allSources;
    private FileManager fm;

    public LlsScore(DataManager dm) {
        this.dm = dm;
        this.goldStandard = dm.getGoldStandard();
        this.allSources = dm.getAllSources();
        this.fm = new FileManager();

    }

    public Map<String, Double> scoredDataSet() {
        LOGGER.info("getting lls...");
        Map<String, Double> dataSetToScore = new HashMap<String, Double>();
        double highScore = 0.0;//to find the highest score

        for (String dataSet : allSources.keySet()) {//iterate through the datasets

            LOGGER.log(Level.INFO, "calculating for {0}...", dataSet);
            Double lls = new NonParaLLS().logScore(goldStandard, allSources.get(dataSet));//score

            if (lls > 0 && !lls.isNaN()) {
                LOGGER.log(Level.INFO, "{0} is the LLS for {1}", new Object[]{lls, dataSet});
                //write to file
                dataSetToScore.put(dataSet, lls);

                if (lls > highScore && !lls.isInfinite()) {
                    highScore = lls;//to keep track of the highest score
                }
            } else {
                LOGGER.log(Level.INFO, "{0} is the LLS for {1}", new Object[]{lls, dataSet});
                LOGGER.log(Level.INFO, "{0} will not be used for integration :(", dataSet);
                //we don't use the negative scores
            }
        }

        double finalHighScore = Math.ceil(highScore + 1);//round up the highscore
        LOGGER.info("scoring done");
        LOGGER.log(Level.INFO, "highest score is {0}", highScore);
        LOGGER.log(Level.INFO, "high score to be used {0}", finalHighScore);

        //replace Infinity scores with finalHighScore
        for (String dataSet : new HashSet<String>(dataSetToScore.keySet())) {
            if (dataSetToScore.get(dataSet).isInfinite()) {
                dataSetToScore.put(dataSet, finalHighScore);
            }
        }

        String outFile = fm.getLOSDataSetScoreFile() +fm.getFileSeparator()+ "Scores_LLS_" + dm.getGoldStandard().getGSName() + ".txt";
        LOGGER.log(Level.INFO, "writing to {0}", outFile);

        //write to file
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(outFile));
            for (String s : dataSetToScore.keySet()) {
                out.write(s + "\t" + dataSetToScore.get(s) + "\n");
            }
            out.close();

        } catch (IOException ex) {
            ex.printStackTrace();
            LOGGER.info("Somethings gone horribly wrong, check your file path");
        }
        return dataSetToScore;//the confidence scores
        //END OF GETSCORES
    }
}
