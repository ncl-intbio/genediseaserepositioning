/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.LLS;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.GoldStandard;
import java.io.File;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.Hypergeometric.CalcGeometric;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.LLSMatrix;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.ROC_D_Value;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager.DataSource;

/**
 *
 * Class runs the LLS score for different gene-disease associations.
 *
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Run {

    private FileManager fm;
    private Double[] dvalues = new Double[]{1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0};

    public Run() {
        this.fm = new FileManager();
    }

    public static void main(String[] args) {

        Run r = new Run();

        //Parse the datasources
        //r.parseAllDatasets();
        //get the LLS using each source as the gold standard; results in a matrix of scores
        //r.runAllAsGS();
        //score all of the associations using the GS of choice using differing D-value
        r.differDValueWithGS(DataSource.UNIPROT);
        //we can't create ROC curves to calculate the best D-Value to be used as we aren't using a binary classifier, instead let's use the hypergeometric distribution and look for the distribution with the smoothest line- yes you can; at every 'threhold' we are stating that anything below that value is not a g-d association and everythin above IS a gene-disease association; look at that video again.
        //r.getHyperForEach(DataSource.UNIPROT);
        r.calcROCDValue(DataSource.UNIPROT);


    }

    public void calcROCDValue(DataSource ds) {
        DataManager dm = new DataManager(ds);
        for (int i = 0; i < dvalues.length; i++) {
            ROC_D_Value plot = new ROC_D_Value(fm.getScoredAssociationFolder().getPath() + fm.getFileSeparator() + "scored_Associations_" + dvalues[i] + "_" + dm.getGoldStandard().getGSName() + ".txt", dm, 1000);
            plot.run();
        }
    }

    /**
     * Score associations using different D-values for the same GS.
     */
    public void differDValueWithGS(DataSource ds) {

        for (int i = 0; i < dvalues.length; i++) {
            ScorePairs associationScores = new ScorePairs(new DataManager(ds), new File(fm.getLOSDataSetScoreFile().getPath() + fm.getFileSeparator() + "LLS/Scores_LLS_" + ds.getText() + ".txt"), dvalues[i]);
            associationScores.goScore();
        }
    }

    /**
     * Parse the datasources; required the first time you run the analysis.
     */
    public void parseAllDatasets() {
        DataManager dm = new DataManager();
        dm.ParseAllDataSources();
    }

    /**
     * Score associations using the the LLS from each datasource.
     */
    public void scoreAllAssWithDifferentGS(double score) {
        FileManager fm = new FileManager();
        for (DataSource ds : DataSource.values()) {
            DataManager dmRun = new DataManager(ds);
            ScorePairs associationScores = new ScorePairs(dmRun, new File(fm.getLOSDataSetScoreFile().getPath() + fm.getFileSeparator() + "Scores_LLS_" + dmRun.getGoldStandard().getGSName() + ".txt"), score);
            associationScores.goScore();
        }
    }

    /**
     * Gets the data for plotting the hypergeometric for each datasource. Be
     * sure that the score is correct.
     */
    public void getHyperForEach(DataSource ds) {
        DataManager dm = new DataManager(ds);
        FileManager fm = new FileManager();
        GoldStandard gs = dm.getGoldStandard();

        for (int i = 0; i < dvalues.length; i++) {
            CalcGeometric calc = new CalcGeometric(30, 15, dm, fm.getScoredAssociationFolder().getPath() + fm.getFileSeparator() + "scored_Associations_" + dvalues[i] + "_" + dm.getGoldStandard().getGSName() + ".txt", 0, 1, 2, dvalues[i]);
            calc.getAllAssociations();
            calc.getHyperValues();
            calc.getDistributionValues();
        }
    }

    /**
     * Get the LLS score using each datasource as a gold standard.
     */
    public void runAllAsGS() {
        DataManager dm = new DataManager();
        LLSMatrix lls = new LLSMatrix(dm);
        lls.run();
        lls.printMatrix();
        lls.writeMatrixToFile(true);
    }
}
