/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.GWAS2MeSHHeader;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.MeSHHeader2MeSHUI;

/**
 *
 * This parser was developed to parse gene-disease interactions that are
 * captured in GWAS. Data is initially pulled from GWAS central
 * http://www.gwascentral.org/ (see README of Datasets/gene-disease) and minimal
 * information identified. Here we must map the GWAS traits to MeSH terms.
 *
 * <TODO> Include a method for mapping between traits and MeSH terms.
 *
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class GWAS implements GeneDiseaseParser {

    private FileManager fm;
    private Serialize ser;
    private File GWASFile;
    private String filenames;
    private Set<Pair> CuratedAssociations;
    private GWAS2MeSHHeader gwas;
    private MeSHHeader2MeSHUI meshui;
    private double pValue;
    private final static Logger LOGGER = Logger.getLogger(GWAS.class.getName());
    private int associationsChecked  = 0;

    public static void main(String[] args) {
        GWAS ctd = new GWAS(1e-7);
        ctd.Parse();
    }

    public GWAS(double pValue) {
        this.pValue = pValue;
        this.fm = new FileManager();
        this.ser = new Serialize();
        this.filenames = "GWAS";
        this.GWASFile = fm.getGWASFile();
        this.CuratedAssociations = new HashSet<Pair>();
        this.gwas = new GWAS2MeSHHeader();
        this.meshui = new MeSHHeader2MeSHUI(false);
    }

    @Override
    public void Parse() {
        int GWAS = 0;
        /**
         * Fields: # Mapped_gene	p-Value	Risk Allele Frequency	PUBMEDID	SNPs
         * Disease/Trait
         */
        String Mapped_gene = null;
        Double p_Value = null;
        String Risk_Allele_Frequency = null;
        String PUBMEDID = null;
        String SNPs = null;
        String Disease_trait = null;

        BufferedReader br = null;
        BufferedWriter bw = null;
        String thisLine;
        try {
            br = new BufferedReader(new FileReader(GWASFile));

        } catch (FileNotFoundException ex) {
            Logger.getLogger(CTD.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bw = new BufferedWriter(new FileWriter(new File(fm.getStandardisedFolder().getPath() + fm.getFileSeparator() + filenames + ".txt")));
        } catch (IOException ex) {
            Logger.getLogger(CTD.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while ((thisLine = br.readLine()) != null) {
                //skip the first line
                br.readLine();

                String[] split = thisLine.split("\t");

                //genes contain 2 types of split either " - " or ";". 
                //What do these mean? ATM we treat every gene ID as a seperate relation
                Mapped_gene = split[0];
                //some of the genes are just blank; if so don't use them
                if (!Mapped_gene.isEmpty() && !Mapped_gene.trim().equals("-")) {

                    String[] geneSplit1 = Mapped_gene.split(";");
                    int count = 0;
                    for (int i = 0; i < geneSplit1.length; i++) {
                        String[] split2 = geneSplit1[i].split(" - ");
                        //-1 as we have already counted it if it is just one
                        count += split2.length - 1;
                    }

                    String[] geneSplit2 = new String[geneSplit1.length + count];
                    int count2 = 0;
                    for (int i = 0; i < geneSplit1.length; i++) {
                        String[] split2 = geneSplit1[i].split(" - ");
                        for (String gene : split2) {
                            geneSplit2[count2] = gene;
                            count2++;
                        }
                    }
                    if (isDouble(split[1])) {
                        p_Value = Double.parseDouble(split[1]);
                    }
                    Risk_Allele_Frequency = split[2];
                    PUBMEDID = split[3];
                    SNPs = split[4];
                    Disease_trait = split[5];
                    //map the disease_trait to MeSH header using the GSK mappings 
                    String meshHeader = gwas.getMeshHeader(Disease_trait);
                    //we then need to map the header to the UI
                    String MeSHUI = meshui.getMeshUI(meshHeader);

                    //also ned to add a check to make sure that the trait was actually mappable to MeSHUI.
                    if (p_Value != null && p_Value >= pValue && !MeSHUI.equals("NO_MESH_UI")) {
                        for (String gene : geneSplit2) {
                            CuratedAssociations.add(new Pair(gene, MeSHUI));
                            bw.append(gene + "\t" + MeSHUI + "\n");
                        }
                        //System.out.println(Mapped_gene +"\t"+ MeSHUI);
                    }
                    associationsChecked = associationsChecked + geneSplit2.length;

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(CTD.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(CTD.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Checked {0} ", associationsChecked);
        LOGGER.log(Level.INFO, "Of which {0} pass pValue threshold of {1} and are mappable to MeSH", new Object[]{CuratedAssociations.size(), pValue});
        ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() + fm.getFileSeparator() + filenames + ".ser");

        logInfo();

    }
    
    @Override
    public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(associationsChecked).append("\t").append(dateFormat.format(date)).append("\n");

        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true)));
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
