/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.OMIM2MeSH;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Reads in CTD gene-disease associations and converts them to EntrezGene ID
 * "\t" MeSHUI form. These are then added to a set and serialized as well as
 * added to text file.
 *
 * <TODO> Map the OMIM diseases to MeSH if possible.
 *
 * http://ctdbase.org/help/diseaseGeneDetailHelp.jsp "CTD contains curated and
 * inferred gene–disease associationsChecked. Curated gene–disease
 * associationsChecked are extracted from the published literature by CTD
 * curators or are derived from the OMIM database using the mim2gene file from
 * the NCBI Gene database." Taken from above.
 *
 * How do we differentiate between the curated associationsChecked and those
 * that only involve human genes? May have to get all genes from UniProt and
 * store them in a set> checking each time to see if they are in.
 *
 */
public class CTD implements GeneDiseaseParser {

    private FileManager fm;
    private Serialize ser;
    private File CTDFile;
    private String filenames;
    private Set<Pair> CuratedAssociations;
    private final static Logger LOGGER = Logger.getLogger(CTD.class.getName());
    private OMIM2MeSH OMIM2MESH;
    private int associationsChecked = 0;

    public static void main(String[] args) {
        CTD ctd = new CTD();
        ctd.Parse();
    }

    public CTD() {
        this.fm = new FileManager();
        this.ser = new Serialize();
        this.filenames = "CTD";
        this.CTDFile = fm.getCTDFile();
        this.CuratedAssociations = new HashSet<Pair>();
        this.OMIM2MESH = new OMIM2MeSH();
    }

    @Override
    public void Parse() {

        int OMIM = 0;
        /**
         * Fields: #
         * GeneSymbol,GeneID,DiseaseName,DiseaseID,DirectEvidence,InferenceChemicalName,InferenceScore,OmimIDs,PubMedIDs
         */
        String GeneSymbol = null;
        String GeneID = null;
        String DiseaseName = null;
        String DiseaseID = null;
        String DirectEvidence = null;
        String InferenceChemicalName = null;
        String InferenceScore = null;
        String OmimIDs = null;
        String PubMedIDs = null;


        BufferedReader br = null;
        BufferedWriter bw = null;
        String thisLine;
        try {
            br = new BufferedReader(new FileReader(CTDFile));

        } catch (FileNotFoundException ex) {
            Logger.getLogger(CTD.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bw = new BufferedWriter(new FileWriter(new File(fm.getStandardisedFolder().getPath() + fm.getFileSeparator() + filenames + ".txt")));
        } catch (IOException ex) {
            Logger.getLogger(CTD.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while ((thisLine = br.readLine()) != null) {
                //System.out.println(thisLine);
                if (!thisLine.startsWith("#")) {
                    /**
                     * Disease name may have a "," in it. In that case we must
                     * first remove this.
                     */
                    String[] diseaseName = thisLine.split("\"");
                    if (diseaseName.length > 1) {
                        DiseaseName = diseaseName[1];
                        //remove the disease name that contained "," before we split based on all the ","
                        thisLine = thisLine.replace(DiseaseName, "");
                    }
                    String[] split = thisLine.split(",");
                    GeneSymbol = split[0];
                    GeneID = split[1];
                    if (diseaseName == null) {
                        DiseaseName = split[2];
                    }
                    DiseaseID = split[3];
                    DirectEvidence = split[4];
                    InferenceChemicalName = split[5];
                    InferenceScore = split[6];
                    OmimIDs = split[7];
                    //sometimes the array isn't long enough to capture the pubmedids
                    boolean MeSH = false;

                    if (split.length > 8) {
                        PubMedIDs = split[8];
                    }
                    if (DiseaseID.startsWith("MESH:")) {
                        DiseaseID = DiseaseID.substring(5);
                        MeSH = true;
                    } else if (DiseaseID.startsWith("OMIM:")) {
                        DiseaseID = OMIM2MESH.getMeSHTerm(DiseaseID.substring(5));
                        if (!DiseaseID.equals("NO_MeSH_TERM")) {
                            MeSH = true;
                        }
                    }
                    if (!DirectEvidence.equals("")) {
                        if (MeSH) {
                            CuratedAssociations.add(new Pair(GeneSymbol, DiseaseID));
                            //  System.out.println(GeneSymbol+"\t"+ DiseaseID);
                            bw.append(GeneSymbol + "\t" + DiseaseID + "\n");

                        } else {
                            LOGGER.log(Level.INFO, "Could not OMIM 2 MeSH {0}", DiseaseID);
                            OMIM++;
                        }
                    }
                    associationsChecked++;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(CTD.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(CTD.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Checked {0}", associationsChecked);
        LOGGER.log(Level.INFO, "Of which {0} are curated", CuratedAssociations.size());
        LOGGER.log(Level.INFO, "Of which {0} are from OMIM", OMIM);
        ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() + fm.getFileSeparator() + filenames + ".ser");

        logInfo();

    }

    @Override
    public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(associationsChecked).append("\t").append(dateFormat.format(date)).append("\n");
        
        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true))); 
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
