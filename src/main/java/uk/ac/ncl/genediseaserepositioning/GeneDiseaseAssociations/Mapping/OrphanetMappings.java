/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 *
 * Class extracts all possible mappings between Orphanet and MeSH using the
 * disorders cross referenced with other nomenclatures (en_product1.xml) from
 * http://www.orphadata.org/cgi-bin/index.php.
 *
 * NOTE: mappings currently cover OMIM, ICD10, MeSH, UMLS and MedDRA. Please
 * specify which nomenclature you wish to map too prior to running.
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class OrphanetMappings {

    public HashMap<String, Set<String>> ORDO2Nomenclature;
    private FileManager fm;
    private Serialize ser;
    private File Orph2NomenclatureFile;
    private String filenames;
    private String datasource;
    private boolean element = false;
    private String ELEMENT = "Disorder";
    private final String ORPHANUMBER_XPATH = "Disorder/OrphaNumber";
    private final String EXTERNAL_REF_XPATH = "Disorder/ExternalReferenceList/ExternalReference";
    private BufferedWriter bw = null;
    private final static Logger LOGGER = Logger.getLogger(OrphanetMappings.class.getName());
    private int mappable = 0;
    private int nonmappable = 0;

    public static void main(String[] args) {
        String data = "MeSH";
        //  MedDRA
        //  CD-10
        //  MLS
        //  MIM
        OrphanetMappings or = new OrphanetMappings(data);
        or.run();
    }

    public OrphanetMappings(String datasource) {
        this.datasource = datasource;
        this.ORDO2Nomenclature = new HashMap<String, Set<String>>();
        this.fm = new FileManager();
        this.ser = new Serialize();
        this.Orph2NomenclatureFile = fm.getORDO2MeSHFile();
        this.filenames = "Orphanet_2_" + datasource;
    }

    public void run() {
        try {

            StringWriter buf = new StringWriter(1024);
            element = false;
            try {
                LOGGER.log(Level.INFO, "Parsing Orphanet disease-gene file {0}", Orph2NomenclatureFile);
                bw = new BufferedWriter(new FileWriter(fm.getSuppFolder() + fm.getFileSeparator()+filenames + ".txt"));

                //Setup XML 
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLEventReader eventReader = factory
                        .createXMLEventReader(new FileReader(Orph2NomenclatureFile));

                while (eventReader.hasNext()) {
                    // Get the next event
                    XMLEvent event = eventReader.nextEvent();
                    //if we have passed the start tag append the string with the event
                    if (element) {
                        event.writeAsEncodedUnicode(buf);

                    }
                    //if we reach the endelement create XML document and extract data
                    if (event.isEndElement()) {
                        EndElement e = event.asEndElement();
                        String name = e.getName().toString();
                        if (name.equals(ELEMENT)) {
                            element = false;
                            // System.out.println(buf.toString());
                            Document doc = getXML(buf.toString());
                            extractData(doc);
                            // System.out.println("-----------------");
                            buf = new StringWriter(1024);
                        }
                    }
                    //if we reach the start element start wrinting to StringWriter
                    if (event.isStartElement()) {
                        StartElement s = event.asStartElement();
                        String name = s.getName().toString();
                        if (name.equals(ELEMENT)) {
                            element = true;
                            event.writeAsEncodedUnicode(buf);
                        }
                    }
                }
            } catch (FileNotFoundException | XMLStreamException e) {
            }

            bw.close();
            ser.SerializeHashMap(ORDO2Nomenclature, fm.getSuppFolder()+ fm.getFileSeparator() + filenames + ".ser");
            LOGGER.log(Level.INFO, "Successfully mapped {0} orphanet disorders to " + datasource, mappable);
            LOGGER.log(Level.INFO, "Failed to map {0} orphanet disorders to " + datasource, nonmappable);
        } catch (IOException ex) {
            Logger.getLogger(OrphanetMappings.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Takes and XML element and extracts all concept classes and relation types
     * required using XMLPath
     *
     * @param document
     */
    public void extractData(Document document) {
        try {
            //create an XpathFactory to mine the document
            //this is where we extract the relevant mappings
            XPathFactory xpf = XPathFactory.newInstance();
            XPath xpath = xpf.newXPath();

            //get the Orphanet number
            String OrphaNumber = ((String) xpath.evaluate(ORPHANUMBER_XPATH, document, XPathConstants.STRING));
            //get all of the mappings
            NodeList diseaseMappedIds = (NodeList) xpath.evaluate(EXTERNAL_REF_XPATH, document, XPathConstants.NODESET);

            boolean mapped = false;
            for (int i = 0; i < diseaseMappedIds.getLength(); i++) {
                String source = ((String) xpath.evaluate("Source", diseaseMappedIds.item(i), XPathConstants.STRING));
                String reference = ((String) xpath.evaluate("Reference", diseaseMappedIds.item(i), XPathConstants.STRING));
                if (source.equals(datasource)) {
                    mapped = true;
                    try {
                        bw.append(OrphaNumber + "\t" + reference + "\n");
                        if (ORDO2Nomenclature.containsKey(OrphaNumber)) {
                            Set<String> mesh = ORDO2Nomenclature.get(OrphaNumber);
                            mesh.add(reference);
                            ORDO2Nomenclature.put(OrphaNumber, mesh);
                        } else {
                            Set<String> mesh = new HashSet<String>();
                            mesh.add(reference);
                            ORDO2Nomenclature.put(OrphaNumber, mesh);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(OrphanetMappings.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

            if (mapped) {
                mappable++;
            } else {
                nonmappable++;
            }

        } catch (XPathExpressionException ex) {
            Logger.getLogger(OrphanetMappings.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Convert a String to an XML document
     *
     * @param convert
     */
    public Document getXML(String convert) {
        //System.out.println(convert);
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            Document document = null;
            try {
                document = builder.parse(new InputSource(new StringReader(convert)));
            } catch (SAXException ex) {
                Logger.getLogger(OrphanetMappings.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(OrphanetMappings.class.getName()).log(Level.SEVERE, null, ex);
            }
            return document;
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(OrphanetMappings.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;


    }
}
