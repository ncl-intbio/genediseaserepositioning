/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class OMIM2MeSH {

    private Map<String, String> OMIM2MESH;
    private FileManager fm;
    private Serialize ser;
    private final static Logger LOGGER = Logger.getLogger(OMIM2MeSH.class.getName());
    
    public static void main (String [] args){
    
            OMIM2MeSH om = new OMIM2MeSH();
            
            
    
    }

    public OMIM2MeSH() {
        this.fm = new FileManager();
        this.ser = new Serialize();
        this.OMIM2MESH = new HashMap<String, String>();
        //do the GSK ones second as we trust these more than those in Orphanet
        getOMIM2MESH(fm.getOMIM2MeSHORDO());
        getOMIM2MESH(fm.getOMIM2MeSHGSK());
        
        Logger.getLogger(OMIM2MeSH.class.getName()).log(Level.INFO, "Parsed {0} mappings between OMIM and MeSH", OMIM2MESH.size());

    }

    public String getMeSHTerm(String OMIMid) {
        String MESHID = "NO_MeSH_TERM";
        if (OMIM2MESH.containsKey(OMIMid)) {
            return OMIM2MESH.get(OMIMid);
        }
        return MESHID;
    }

    private void getOMIM2MESH(File file) {
        int count =0;
        try {
            BufferedReader br = null;
            String thisLine;
            //Open the file for reading
            br = new BufferedReader(new FileReader(file));
            try {
                //variables
                while ((thisLine = br.readLine()) != null) {
                    String[] split = thisLine.split("\t");
                    OMIM2MESH.put(split[0].trim(), split[1].trim());
                    count ++;
                }
            } catch (IOException ex) {
                Logger.getLogger(OMIM2MeSH.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OMIM2MeSH.class.getName()).log(Level.SEVERE, null, ex);
        }     
        LOGGER.log(Level.INFO, "Parsed ... {0} MIM->MeSH mappings from {1}", new Object[]{count, file});      
    }
    
}
