/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations;

import uk.ac.ncl.genediseaserepositioning.Serialize;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.BeFree;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.CTD;
//import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.DisGENet;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.GOF_LOF;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.GWAS;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.MGD;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.OMIM;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.Orphanet;
//import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.RGD;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.SemRep;
//import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.UniProt;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.RGD;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.UniProt;


/**
 *
 * Allows access to serialized Set<String> geneDisease associations for
 * analysis. Also provides a few methods for these sets to be combined or
 * analysed.
 *
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class DataManager {

    private Serialize ser;
    private final static Logger LOGGER = Logger.getLogger(DataManager.class.getName());
    private GoldStandard goldStandard;
    private DataSource gold;
    private FileManager fman;

    public static void main(String[] args) {
        DataManager dm = new DataManager();
        dm.getAllSources();

    }

    public DataManager() {
        this.ser = new Serialize();
        this.fman = new FileManager();
    }

    public DataManager(DataSource gold) {
        this.ser = new Serialize();
        this.goldStandard = new GoldStandard(gold);
        this.gold = gold;
        this.fman = new FileManager();
    }

    public GoldStandard getGoldStandard() {
        return goldStandard;
    }
    
    public Set<DataSource> getAllEnums(){
        HashSet<DataSource> allEnums = new HashSet<DataSource>();
        allEnums.add(DataSource.BEFREE);
        allEnums.add(DataSource.CTD);
        allEnums.add(DataSource.GOFLOF);
        allEnums.add(DataSource.GWAS);
        allEnums.add(DataSource.MGD);
        allEnums.add(DataSource.OMIM);
        allEnums.add(DataSource.ORPHANET);
        allEnums.add(DataSource.RGD);
        allEnums.add(DataSource.SEMREP);
        allEnums.add(DataSource.UNIPROT);
        return allEnums;
    }
    

    public enum DataSource {

        CTD("CTD"),
        GWAS("GWAS"),
        OMIM("OMIM"),
        ORPHANET("ORPHANET"),
        UNIPROT("UNIPROT"),
        BEFREE("BEFREE"),
        RGD("RGD"),
        GOFLOF("GOFLOF"),
        SEMREP("SEMREP"),
        MGD("MGD");
        //DISGENET("DISGENET");
        private String text;

        DataSource(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public static DataSource fromString(String text) {
            if (text != null) {
                for (DataSource b : DataSource.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static DataSource fromValue(String value) throws IllegalArgumentException {
            try {
                return DataSource.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    /**
     * Returns a hashmap containing a string identifier and a set of
     * associations for each dataset being looked at.
     *
     * @return
     */
    public HashMap<String, Set<Pair>> getAllSources() {

        HashMap<String, Set<Pair>> allSources = new HashMap<String, Set<Pair>>();
        if (gold != DataManager.DataSource.CTD) {
            allSources.put(DataManager.DataSource.CTD.getText(), getAssociations(DataManager.DataSource.CTD));
        }
        if (gold != DataManager.DataSource.GWAS) {
            allSources.put(DataManager.DataSource.GWAS.getText(), getAssociations(DataManager.DataSource.GWAS));
        }
        if (gold != DataManager.DataSource.OMIM) {
            allSources.put(DataManager.DataSource.OMIM.getText(), getAssociations(DataManager.DataSource.OMIM));
        }
        if (gold != DataManager.DataSource.ORPHANET) {
            allSources.put(DataManager.DataSource.ORPHANET.getText(), getAssociations(DataManager.DataSource.ORPHANET));
        }
        if (gold != DataManager.DataSource.BEFREE) {
            allSources.put(DataManager.DataSource.BEFREE.getText(), getAssociations(DataManager.DataSource.BEFREE));
        }
        if (gold != DataManager.DataSource.RGD) {
            allSources.put(DataManager.DataSource.RGD.getText(), getAssociations(DataManager.DataSource.RGD));
        }
        if (gold != DataManager.DataSource.GOFLOF) {
            allSources.put(DataManager.DataSource.GOFLOF.getText(), getAssociations(DataManager.DataSource.GOFLOF));
        }
        if (gold != DataManager.DataSource.MGD) {
            allSources.put(DataManager.DataSource.MGD.getText(), getAssociations(DataManager.DataSource.MGD));
        }
        if (gold != DataManager.DataSource.UNIPROT) {
            allSources.put(DataManager.DataSource.UNIPROT.getText(), getAssociations(DataManager.DataSource.UNIPROT));
        }
        if (gold != DataManager.DataSource.SEMREP) {
            allSources.put(DataManager.DataSource.SEMREP.getText(), getAssociations(DataManager.DataSource.SEMREP));
        }
        
        return allSources;

    }

    public Set<Pair> getAssociations(DataSource dat) {

        switch (dat.getText()) {
            case "CTD":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"CTD.ser");
            case "GOFLOF":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"GOFLOF.ser");
            case "GWAS":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"GWAS.ser");
            case "OMIM":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"OMIM.ser");
            case "ORPHANET":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"Orphanet.ser");
            case "UNIPROT":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"UniProt.ser");
            case "DISGENET":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"DisGENet.ser");
            case "SEMREP":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"SemRep.ser");
            case "BEFREE":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"BeFree.ser");
            case "RGD":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"RGD.ser");
            case "MGD":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"MGD.ser");
            case "PHARMA":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"PharmaPipeline.ser");
            case "CURATEDALL":
                return ser.deserializeSetPair(fman.getSerializedFolder()+fman.getFileSeparator()+"Curated_ALL.ser");
        }
        return null;

    }

    /**
     * Allows for multiple sets of associations to be merged into one set.
     *
     * @param merge
     * @return
     */
    public Set<String> mergeSets(List<Set<String>> merge) {
        Set<String> merged = new HashSet<String>();
        for (Set<String> merging : merge) {
            LOGGER.log(Level.INFO, "Merged set with {0} gene-Disease associations", merging.size());
            merged.addAll(merging);
        }
        LOGGER.info("New set contains " + merged.size() + " unique gene-Disease associations");
        return merged;
    }

    /**
     * Method allows for multiple sets to be provided and a summary of cross
     * over provided.
     */
    public void compareSets(HashMap<String, Set<String>> allSets) {
        for (String datasource : allSets.keySet()) {
            Set<String> associations = allSets.get(datasource);
            System.out.println("DATASOURCE: " + datasource + "(" + associations.size() + ")");
            for (String datasourceComp : allSets.keySet()) {
                //don't need to compare to the same source
                if (!datasourceComp.equals(datasource)) {
                    Set<String> associationsComp = allSets.get(datasourceComp);
                    int common = 0;
                    for (String ass : associations) {
                        if (associationsComp.contains(ass)) {
                            common++;
                        }
                    }
                    System.out.println("Compared_to: " + datasourceComp + " ::" + common + " common associatons");
                }
            }
        }
    }

    public void ParseAllDataSources() {

        BeFree dis = new BeFree(0.0);
        dis.Parse();
        CTD ctd = new CTD();
        ctd.Parse();
        GOF_LOF gof = new GOF_LOF();
        gof.Parse();
        GWAS gwas = new GWAS(0.0);
        gwas.Parse();
        MGD mgd = new MGD();
        mgd.Parse();
        OMIM omim = new OMIM();
        omim.Parse();
        Orphanet orph = new Orphanet();
        orph.Parse();
        RGD rgd = new RGD();
        rgd.Parse();
        SemRep SemRep = new SemRep();
        SemRep.Parse();
        UniProt uni = new UniProt();
        uni.Parse();
    }
}
