package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.LLS;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.GoldStandard;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.BeFree;

/*----------------------------------------------------------------*\
 |                         Class NonParaLLS.java                    |
 |        	    Calculate the LSS for a dataset                |
 |                                                                  |
 |                        Author: Olly Shaw                         |
 |                        Author: Jen Hallinan                      |
 |                      Author: Katherine James                     |
 |                       Commenced: 07/08/07                        |
 |                      Last edited: 16/03/09                       |
 \*----------------------------------------------------------------*/
public class NonParaLLS {

    private final static Logger LOGGER = Logger.getLogger(NonParaLLS.class.getName());

    //**********Method to score the dataset****************
    //this is olly lls code adapted for the pipeline
    public double logScore(GoldStandard pharma, Set<Pair> data) {
        //get the goldstandards
        Set<Pair> pharmaPairs = pharma.getTruePositives();
        int truePosData = pairsInGoldStandard(pharmaPairs, data);//true positives
        int totalGenesPharma = pharma.getNumberOfGSGenes();//number of genes in pharma
        int totalDiseasesPharma = pharma.getNumberOfGSDiseases(); //number of diseases in pharma        
        int truePosPharma = pharmaPairs.size();//number pos pairs
        int possiblePairsPharma = (int) Math.pow(totalGenesPharma * 1.0, totalDiseasesPharma * 1.0);
        int trueNegPharma = pharma.getNumberOfTrueNegatives();//number negative pairs
        int falsePosData = pairsNotInGoldStandard(pharma, data);//false positives

        //calculate lls
        double PLE = 1.0 * truePosData / data.size();
        double notPLE = 1.0 * falsePosData / data.size();
        double PL = 1.0 * truePosPharma / possiblePairsPharma;
        double notPL = 1.0 * trueNegPharma / possiblePairsPharma;
        double PLE_over_notPLE = PLE / notPLE;
        double PL_over_notPL = PL / notPL;//priors
        double LS = PLE_over_notPLE / PL_over_notPL;
        double LLS = Math.log(LS);

        return LLS;//the score for the dataset
    }
    //END OF LOGSCORE METHOD

    //**********Method to count the number of pairs present in the GS********
    private static int pairsInGoldStandard(Set<Pair> goldList, Set<Pair> data) {

        LOGGER.log(Level.INFO, "Data Size:: {0}", data.size());
        LOGGER.log(Level.INFO, "GoldList Size:: {0}", goldList.size());
        int counter = 0;
        for (Pair pair : data)//the test list
        {
            if (goldList.contains(pair))//compare to the gold standard
            {
                counter++;
            }
        }
        return counter;
    }

    /*
     * We only include genes and diseases that are in the gold standard.
     */
    private static int pairsNotInGoldStandard(GoldStandard pharma, Set<Pair> data) {

        FileManager fm = new FileManager();
        StringBuilder string = new StringBuilder();

        int counter = 0;
        for (Pair pair : data) {
            if (!pharma.getTruePositives().contains(pair)
                    && pharma.getGSGenes().contains(pair.getEntrezGeneSymbol())
                    && pharma.getGSDiseases().contains(pair.getMeSHUI())) {
                counter++;
            }
            if (!pharma.getGSGenes().contains(pair.getEntrezGeneSymbol())) {
                string.append(pharma.getGSName()).append("\t").append(pair.toString()).append("\t").append("GENE").append("\n");
            }
            if (!pharma.getGSDiseases().contains(pair.getMeSHUI())) {
                string.append(pharma.getGSName()).append("\t").append(pair.toString()).append("\t").append("DISEASE").append("\n");
                //LOGGER.log(Level.INFO, "Pair {0} contains elements not from MeSH or Genes", pair.toString());
            }
        }
        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter(fm.getGD_OPFolder() + fm.getFileSeparator() + "invalid_Associations.txt", true)));
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
        return counter;
    }
}
