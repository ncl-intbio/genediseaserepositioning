/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.MeSHTreeTerms;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes two mesh terms and returns a mapping score using the Leacock and
 * Chodorow distance.
 *
 */
public class LeacockAndChodorow implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private MeSHTreeTerms mapping;
    public int shortestPathLength;
    private double MAX_DEPTH;
    private double MAX_CHILD_DEPTH;
    private boolean child = false;
    private boolean breadthOnly;
    private final static Logger LOGGER = Logger.getLogger(LeacockAndChodorow.class.getName());

    public static void main(String args[]) {

        LeacockAndChodorow leacock = new LeacockAndChodorow(true);
        System.out.println(leacock.getDistance("C536282", "D020529"));
        double [] scores = leacock.getAllScores();
        for(int i=0; i< scores.length; i++){
            System.out.println(i+"\t"+scores[i]);
        }
    }

    public LeacockAndChodorow(boolean breadthONLY) {
        this.breadthOnly = breadthONLY;
        this.mapping = new MeSHTreeTerms();
        mapping.parseTerms();
        this.MAX_DEPTH = mapping.getMaxDepth();
        this.MAX_CHILD_DEPTH = (2.0 / MAX_DEPTH);
    }

    public MeSHTreeTerms getMeSHTreeTerms() {
        return mapping;
    }

    public double getDistance(String mesh1, String mesh2) {

        Set<String> mesh1Terms = mapping.getTreeTerms(mesh1);
        Set<String> mesh2Terms = mapping.getTreeTerms(mesh2);
        if (mesh1Terms == null || mesh2Terms == null || mesh1Terms.size() < 1 || mesh2Terms.size() < 1) {
            // LOGGER.log(Level.INFO, "Cannot get MeSHTree Terms for {0} or {1}", new Object[]{mesh1, mesh2});
            return 0.0;
        }
        return calcLeacAndChod(mesh1Terms, mesh2Terms);
    }

    public double calcLeacAndChod(Set<String> termsFrom, Set<String> termsTo) {

        double maxMappingScore = -Math.log((1) / (2 * MAX_DEPTH));
        double score = 0.0;
        if (map(termsFrom, termsTo)) {
            double len = getShortestPathLength();
            //method uses node count not edgecount and so we add 1 
            if (breadthOnly) {
                len += 1;
                score = -Math.log((len) / (2 * MAX_DEPTH));
                //normalize the score
                score = 1 / maxMappingScore * score;
            } else {
                if (child) {
                    len = (len * MAX_CHILD_DEPTH) + 1;
                } else {
                    len += 1;
                }
                score = -Math.log((len) / (2 * MAX_DEPTH));
                //normalize the score
                score = 1 / maxMappingScore * score;
            }
        }
        return Math.sqrt(score*score);
    }

    /**
     * Tries to map two terms at any level/path length. Returns true if mapping
     * possible at any depth. Assigns the lowest level mapping of mapping to
     * mappedAt.
     *
     * @param termsFrom
     * @param termsToo
     * @return
     */
    public boolean map(Set<String> termsFrom, Set<String> termsTo) {

        /**
         * See if the terms are exacts
         */
        for (String from : termsFrom) {
            for (String to : termsTo) {
                if (from.equals(to)) {
                    shortestPathLength = 0;
                    return true;
                }
            }
        }

        /**
         * Next we test to see if any of the terms are children of any of the
         * others
         */
        //change to
        for (int i = 0; i < getMaxDepthOfSet(termsTo); i++) {
            for (String from : termsFrom) {
                for (String to : termsTo) {
                    String prunedTo = getMeshTreeAtDepth(to, (getDepthOfTerm(to) - (i)));
                    if (from.equals(prunedTo)) {
                        shortestPathLength = i + 1;
                        child = true;
                        return true;
                    }
                }
            }
        }

        //change from
        for (int i = 0; i < getMaxDepthOfSet(termsFrom); i++) {
            for (String from : termsFrom) {
                String prunedFrom = getMeshTreeAtDepth(from, (getDepthOfTerm(from) - (i)));
                for (String to : termsTo) {
                    //String prunedTo = getMeshTreeAtDepth(to, (i));
                    if (prunedFrom.equals(to)) {
                        shortestPathLength = i + 1;
                        child = true;
                        return true;
                    }
                }
            }
        }

        /**
         * If the terms are not exacts and are neither parents or children of
         * each other then we will try and identify their least common subsumer
         * (LCS)
         */
        int count = 0;
        String fromBEST = "";
        String toBEST = "";

        for (int i = 0; i < getMaxDepthOfSet(termsFrom) + 1; i++) {
            for (String from : termsFrom) {
                String prunedFrom = getMeshTreeAtDepth(from, (i));
                for (String to : termsTo) {
                    String prunedTo = getMeshTreeAtDepth(to, (i));
                    if (prunedFrom.equals(prunedTo)) {
                        if (i >= count) {

                            if (count == 0) {
                                count = i;
                                fromBEST = from;
                                toBEST = to;
                            } else if (getDepthOfTerm(from) + getDepthOfTerm(to) <= getDepthOfTerm(fromBEST) + getDepthOfTerm(toBEST)) {
                                count = i;
                                fromBEST = from;
                                toBEST = to;
                            }
                        }

                    }
                }
            }
        }

        /**
         * Knowing the LCS we can then calculate the path length
         */
        int pathLength = 0;

        if (count > 0) {
            pathLength += (getDepthOfTerm(fromBEST)) - (count - 1);
            pathLength += getDepthOfTerm(toBEST) - (count - 1);
            shortestPathLength = pathLength;
            child = false;
            return true;
        }

        return false;
    }

    /**
     * Returns a Set representation of a String of MeSH tree terms.
     *
     * @param treeTerms
     * @return
     */
    public Set<String> getSetFromString(String treeTerms) {
        Set<String> terms = new HashSet<String>();
        String[] split = treeTerms.split("\\|");
        terms.addAll(Arrays.asList(split));
        return terms;
    }

    /**
     * Return the max depth of a set of terms term.
     */
    public int getMaxDepthOfSet(Set<String> term) {

        int max = 0;
        for (String terms : term) {
            String[] splitt = terms.split("\\.");
            if (splitt.length > max) {
                max = splitt.length;
            }
        }
        return max - 1;
    }

    /**
     * Return the depth of a term.
     */
    public int getDepthOfTerm(String term) {
        return term.split("\\.").length - 1;

    }

    /**
     * Get the mesh tree term at a given depth.
     */
    public String getMeshTreeAtDepth(String allTrees, int meshtreedepth) {

        String[] branches = allTrees.split("\\.");
        if (branches.length < meshtreedepth) {
            return allTrees;
        }
        String branch = "FIRST";
        for (int p = 0; p < meshtreedepth; p++) {
            if (branch.equals("FIRST")) {
                branch = branches[p];
            } else {
                branch = branch.trim() + "." + branches[p];
            }
        }
        return branch;
    }

    /**
     * Returns the depth at which a mapping was achieved.
     *
     * @return
     */
    public int getShortestPathLength() {
        return shortestPathLength;
    }

    public double[] getAllScores() {

        double[] scores = new double[(int)MAX_DEPTH*2];
        double maxMappingScore = -Math.log((1) / (2 * MAX_DEPTH));

        for (int i = 1; i < (int)MAX_DEPTH*2; i++) {
            double score = -Math.log((i) / (2 * MAX_DEPTH));
            //normalize the score
            score = 1 / maxMappingScore * score;
            scores[i-1] = Math.sqrt(score*score);
        }
        return scores;
    }
}
