/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.LLSMatrix;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.MeSHTreeTerms;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class creates an p by k matrix with all the scores of each the associations
 * (p= predicted, k = known)
 *
 */
public class CreateScoredMatrices implements java.io.Serializable {

    private final static Logger LOGGER = Logger.getLogger(CreateScoredMatrices.class.getName());
    private static final long serialVersionUID = 1L;
    private FileManager fman;
    private LeacockAndChodorow leacock;
    private Set<Pair> allpredictedPairs;
    private Set<Pair> allKnownPairs;

    private Set<String> allDBIDs;
    private LeacockMatrixDrug mat;
    private Serialize ser;

    public CreateScoredMatrices() {
        this.leacock = new LeacockAndChodorow(true);
        this.fman = new FileManager();
        this.allDBIDs = new HashSet<String>();
        this.ser = new Serialize();
        getDBIDS();
        getAllKnownPairs();
        getAllPredictedPairs();


    }

    public void getDBIDS() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fman.getSuppFolder() + fman.getFileSeparator() + "DBIDs_after_prune.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CreateScoredMatrices.class.getName()).log(Level.SEVERE, null, ex);
        }

        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                allDBIDs.add(thisLine.trim());
            }
        } catch (IOException ex) {
            Logger.getLogger(CreateScoredMatrices.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "We have ... {0} DBIDs", allDBIDs.size());

    }

    public void getAllKnownPairs() {
        DrugDiseaseData dat = new DrugDiseaseData(DrugDiseaseData.DataSourceKNOWNS.ALL, DrugDiseaseData.DiseaseType.ALL, DrugDiseaseData.DiseaseCat.ALL);
        this.allKnownPairs = dat.getGS();
        LOGGER.log(Level.INFO, "There are {0} knowns", allKnownPairs.size());
    }

    public void getAllPredictedPairs() {
        Set<Pair> inferred = new HashSet<Pair>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(new File(fman.getFilteredMappingsFolder() + "/" + "ALL_minusSEs_0.768_ALLdis_MINUSADME_UNIQUE.txt")));
        } catch (FileNotFoundException ex) {
        }

        String line;
        try {
            while ((line = br.readLine()) != null) {
                inferred.add(new Pair(line.split("\t")[0], line.split("\t")[1]));
            }
        } catch (IOException ex) {
        }

        this.allpredictedPairs = inferred;
        LOGGER.log(Level.INFO, "Added {0} predicted associations", new Object[]{allpredictedPairs.size()});

    }

    public static void main(String[] args) {
        CreateScoredMatrices c = new CreateScoredMatrices();
        c.run();
           
    }

    public void run() {

        Set<Pair> dbidKnown = new HashSet<Pair>();
        Set<Pair> dbidPredicted = new HashSet<Pair>();

        int count = 0;
        for (String dbid : allDBIDs) {

            //get all knowns for dbid
            for (Pair p : allKnownPairs) {
                if (p.getEntrezGeneSymbol().equals(dbid)) {
                    dbidKnown.add(p);
                }
            }
            //get all predicted for dbid
            for (Pair p : allpredictedPairs) {
                if (p.getEntrezGeneSymbol().equals(dbid)) {
                    dbidPredicted.add(p);
                }
            }

            LOGGER.log(Level.INFO, "For DBID {0} ...", dbid);
            LOGGER.log(Level.INFO, "We have {0} predicted pairs", dbidPredicted.size());
            LOGGER.log(Level.INFO, "We have {0} known pairs", dbidKnown.size());

            count++;
            createDBMatrix(dbid, dbidKnown, dbidPredicted);
            dbidKnown = new HashSet<Pair>();
            dbidPredicted = new HashSet<Pair>();

        }
    }

    public void createDBMatrix(String DBID, Set<Pair> known, Set<Pair> predicted) {

        LeacockMatrixDrug leac = new LeacockMatrixDrug(DBID, known.size() + 1, predicted.size() + 1);

        //sort knowns pairs
        Set<String> knowns = new HashSet<String>();
        for (Pair p : known) {
            knowns.add(p.toString());
        }
        ArrayList<String> sortedKnowns = new ArrayList<String>();
        sortedKnowns.addAll(knowns);
        Collections.sort(sortedKnowns);
        //sort predicted pairs
        Set<String> pred = new HashSet<String>();
        for (Pair p : predicted) {
            pred.add(p.toString());
        }
        ArrayList<String> sortedPred = new ArrayList<String>();
        sortedPred.addAll(pred);
        Collections.sort(sortedPred);
        //set these
        leac.setKnownsOrdered(sortedKnowns);
        leac.setPredictedOrdered(sortedPred);

        //name of the matrix
        leac.addValue(DBID, 0, 0);
        //populates the actual axis of the matrix
        int count = 1;
        for (String name : sortedKnowns) {
            leac.addValue(name, count, 0);
            //matrix[count][0] = name;
            count++;
        }
        count = 1;
        for (String name : sortedPred) {
            // matrix[0][count] = name;
            leac.addValue(name, 0, count);
             //matrix[count][0] = name;
            count++;
        }

       // LOGGER.log(Level.INFO, "Created empty matrix [{0}] by [{1}]", new Object[]{x, y});
        LOGGER.info("Now populating matrix with leacock scores...");
        DecimalFormat df = new DecimalFormat("#.####");
        //now to populate the scores
        for (int i = 1; i < sortedKnowns.size()+1; i++) {
            for (int p = 1; p < sortedPred.size()+1; p++) {
                if (sortedKnowns.get(i - 1).equals(sortedPred.get(p - 1))) {
                    leac.addValue(Double.toString(1.0), i, p);//matrix[i][p] = ;
                } else {
                    leac.addValue(df.format(leacock.getDistance(sortedKnowns.get(i - 1).split("\t")[1], sortedPred.get(p - 1).split("\t")[1])), i,p );
                }
            }
        }

        leac.printMatrix();
        //serialise the matrix
        try {
            ser.SerializeLeacockMatrix(leac, fman.getLeacockScores_OPFolder() + fman.getFileSeparator() + DBID + ".ser");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(CreateScoredMatrices.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
