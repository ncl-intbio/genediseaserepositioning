/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.MeSHMapping;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;

/**
 *
 * @author joe
 *
 * Class takes a MeSHTreeterm and returns the corresponding MeSHUI terms
 * (C535325)
 *
 */
public class GetMeSHUI {

    private String MeSHTreeTerm;
    private FileManager fman;
    private File Dfile;

    public static void main(String[] args) {
        GetMeSHUI gm = new GetMeSHUI();
        System.out.println(gm.getDUI("C04"));
        // System.out.println(gm.getTreeTerms("C535325"));
    }

    public GetMeSHUI() {
        this.fman = new FileManager();
        this.Dfile = fman.getMeSHDFile();
    }

    public Set<String> getDUI(String MeSHTree) {
        Set<String> meshuis = new HashSet<String>();
        int maxDepth = 0;
        String longest = "";


        BufferedReader br = null;
        String thisLine;
        boolean firstDone = false;
        try {
            //Open the file for reading
            br = new BufferedReader(new FileReader(Dfile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GetMeSHUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        //variables
        String MeSHMainHeader = null;
        String MeSHUI = null;
        Set<String> MeSHTreeTerms = new HashSet<String>();
        try {
            while ((thisLine = br.readLine()) != null) {

                if (thisLine.equals("*NEWRECORD")) {
                    //create new node and reset the variables

                    if (firstDone) {
                        int c = 1;
                        //we are only interested in diseases [begin with C in the hierarchy ] 
                        // and the three concepts of the F branch
                        boolean disease = false;
                        for (String tt : MeSHTreeTerms) {
                            if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                disease = true;
                            }
                        }

                        //if the term is a disease then add it
                        if (disease) {
                            Set<String> treeterms = new HashSet<String>();
                            for (String tt : MeSHTreeTerms) {
                                if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                    if (tt.equals(MeSHTree)) {
                                        //System.out.println(MeSHTree);
                                        meshuis.add(MeSHUI);

                                    }

                                }
                            }

                        }
                        MeSHTreeTerms = new HashSet<String>();
                    }
                }
                if (thisLine.startsWith("UI =")) {
                    MeSHUI = split(thisLine);
                    firstDone = true;
                }

                if (thisLine.startsWith("MN =")) {
                    MeSHTreeTerms.add(split(thisLine));

                }

            }
        } catch (IOException ex) {
            Logger.getLogger(GetMeSHUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return meshuis;
    }

    /**
     * Splits the MeSH line (aaaa = bbbbb) on the "=" and returns the second
     * element.
     *
     * @return
     */
    public String split(String meshLine) {

        String[] split = meshLine.split("=");
        return split[1].trim();

    }
}
