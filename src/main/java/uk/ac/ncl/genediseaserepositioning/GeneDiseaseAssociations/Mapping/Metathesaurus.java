/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author joe
 *
 *
 * Maps a UMLS to MeSH using the Metathesaurus.
 *
 */
public class Metathesaurus {

    private Map<String, String> unMappable;
    private Map<String, String> alreadyMapped;
    private FileManager fman;
    private Serialize ser;
    private final static Logger LOGGER = Logger.getLogger(Metathesaurus.class.getName());
    private File cantMap;
    private File alreadyMappedMap;

    public static void main(String[] args) {
        Metathesaurus met = new Metathesaurus();
        System.out.println(met.getMeSHfromUMLS("C0015973"));
    }

    /**
     * Return all the already mapped UMLS-> MeSH diseases.
     */
    public Map<String, String> getAlreadyMapped() {
        return alreadyMapped;
    }

    //need to store the unsuccesful searches so that we do not repeat them!!!!
    public Metathesaurus() {

        this.ser = new Serialize();
        this.alreadyMapped = new HashMap<>();
        fman = new FileManager();
        alreadyMappedMap = new File(fman.getUMLS2MeSHFolder() + "UMLS2MeSH.ser");
        if (alreadyMappedMap.exists()) {
            this.alreadyMapped = ser.deserializeHashMapString(alreadyMappedMap.getAbsolutePath());
            LOGGER.log(Level.INFO, "Getting already mapped UMLS2MeSH ids from {0}", alreadyMappedMap.getName());
        } else {
            this.alreadyMapped = new HashMap<>();
            LOGGER.log(Level.INFO, "No stored UMLS2MeSH mappings; creating a new map to be stored in {0}", alreadyMappedMap.getName());
        }
        cantMap = new File(fman.getUMLS2MeSHFolder() + "cantMap.ser");
        if (cantMap.exists()) {
            this.unMappable = ser.deserializeHashMapString(cantMap.getAbsolutePath());
            LOGGER.log(Level.INFO, "Getting already known unmappable UMLS2MeSH ids from {0}", cantMap.getName());
        } else {
            this.unMappable = new HashMap<>();
            LOGGER.log(Level.INFO, "No stored unmappable UMLS2MeSH ids; creating a new map to be stored in {0}", cantMap.getName());
        }

    }

    public String getMeSHfromUMLS(String UMLS) {

        String MeSH = null;
        String file = fman.getMetatheaurusFile();
        BufferedReader br = null;
        //if we have already check for them then simply return the knowne mapping or if we already know that the UMLS cannot be mapped to a MeSH term
        if (alreadyMapped.containsKey(UMLS)) {
            return alreadyMapped.get(UMLS);
        } else if (unMappable.containsKey(UMLS)) {
            return null;
        } else {
            try {
                String thisLine;
                boolean firstDone = false;
                br = new BufferedReader(new FileReader(file));
                String source = null;
                String UI = null;
                try {
                    while ((thisLine = br.readLine()) != null) {
                        if (thisLine.startsWith(UMLS)) {
                            String[] split = thisLine.split("\\|");
                            source = split[11];
                            UI = split[13];
                            if (source.equals("MSH") && !UI.equals("NOCODE")) {
                                MeSH = UI;
                                break;
                            }
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Metathesaurus.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (MeSH == null) {
                    unMappable.put(UMLS, null);
                    ser.SerializeHashMap(unMappable, cantMap.getAbsolutePath());
                } else {
                    alreadyMapped.put(UMLS, MeSH);
                    ser.SerializeHashMap(alreadyMapped, alreadyMappedMap.getAbsolutePath());
                }
                return MeSH;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Metathesaurus.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
