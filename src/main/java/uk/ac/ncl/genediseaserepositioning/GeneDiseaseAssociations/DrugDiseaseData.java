/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.MeSHTreeTerms;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class DrugDiseaseData {

    private final static Logger LOGGER = Logger.getLogger(DrugDiseaseData.class.getName());
    private Set<String> DBIDsUsed;
    private MeSHTreeTerms mesh;
    private Set<String> rareMeSH;
    private FileManager fm;
    private Serialize ser;
    private Set<Pair> prune;
    private Set<Pair> goldStandard;

    public DrugDiseaseData(DataSourceKNOWNS dataSource, DiseaseType disType, DiseaseCat disCat) {
        this.mesh = new MeSHTreeTerms();
        this.ser = new Serialize();
        mesh.parseTerms();
        this.DBIDsUsed = new HashSet<>();
        this.fm = new FileManager();
        this.rareMeSH = new HashSet<String>();
        getRareMeSHUIs();
        getAllKnown(dataSource);
        prune(dataSource, disType, disCat);
        
    }

    public void getRareMeSHUIs() {
        HashMap<String, Set<String>> disType = ser.deserializeHashMapSet(fm.getSuppFolder() + fm.getFileSeparator() + "ORDO_mesh2diseasetype.ser");
        rareMeSH = disType.get("Rare_Disease");
        LOGGER.log(Level.INFO, "Added {0} MeSHUIs that map to rare diseases", rareMeSH.size());
    }

    public void getAllKnown(DataSourceKNOWNS datasource) {

        Set<Pair> goldStandard = new HashSet<>();
        LOGGER.log(Level.INFO,
                "Reading in known indications ...");
        
        int DBID = 0;
        int MeSH = 1;

        BufferedReader br = null;
        //INDICATIONS
        File NDFRT = new File(fm.getIntegratedIndicationsFile() + fm.getFileSeparator() + "NDFRT.txt");
        File SIDER = new File(fm.getIntegratedIndicationsFile() + fm.getFileSeparator() + "SIDR4.txt");
        File CTD = new File(fm.getIntegratedIndicationsFile() + fm.getFileSeparator() + "CTD.txt");
        File PREDICT = new File(fm.getIntegratedIndicationsFile() + fm.getFileSeparator() + "PREDICT.txt");

        String thisLine;
        if (datasource == DataSourceKNOWNS.NDFRT || datasource == DataSourceKNOWNS.ALL) {
            try {
                br = new BufferedReader(new FileReader(NDFRT));
            } catch (FileNotFoundException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
            try {
                while ((thisLine = br.readLine()) != null) {
                    String[] split = thisLine.split("\t");
                    if (!split[MeSH].equals("NO_MeSH")) {
                        goldStandard.add(new Pair(split[DBID], split[MeSH]));
                    }
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
        if (datasource == DataSourceKNOWNS.SIDER || datasource == DataSourceKNOWNS.ALL) {
            try {
                br = new BufferedReader(new FileReader(SIDER));
            } catch (FileNotFoundException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
            try {
                while ((thisLine = br.readLine()) != null) {
                    String[] split = thisLine.split("\t");
                    if (!split[MeSH].equals("NO_MeSH")) {
                        goldStandard.add(new Pair(split[DBID], split[MeSH]));
                    }
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }

        if (datasource == DataSourceKNOWNS.PREDICT || datasource == DataSourceKNOWNS.ALL) {
            try {
                br = new BufferedReader(new FileReader(PREDICT));
            } catch (FileNotFoundException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
            try {
                while ((thisLine = br.readLine()) != null) {
                    String[] split = thisLine.split("\t");
                    goldStandard.add(new Pair(split[DBID], split[MeSH]));
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }

        if (datasource == DataSourceKNOWNS.CTD || datasource == DataSourceKNOWNS.ALL) {
            try {
                br = new BufferedReader(new FileReader(CTD));
            } catch (FileNotFoundException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
            try {
                while ((thisLine = br.readLine()) != null) {
                    String[] split = thisLine.split("\t");
                    goldStandard.add(new Pair(split[DBID], split[MeSH]));
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
        LOGGER.log(Level.INFO, "Parsed {0} known indications ...", goldStandard.size());
        this.goldStandard =  goldStandard;
    }

    /**
     * ONLY include the associations from the gold source that include drugs
     * that were used for the initial search.
     */
    public void prune( DataSourceKNOWNS datasource, DiseaseType type, DiseaseCat category) {

        LOGGER.log(Level.INFO, "Before pruning we have {0} gold standard drug-disease associations from {1}...", new Object[]{goldStandard.size(), datasource.getText()});
        //LOGGER.log(Level.INFO, "Before pruning we have {0} gs disease-target associations..", goldStandard.size());

        Set<Pair> removed = new HashSet<>();
        Set<String> dbids = new HashSet<>();

        //get all of the drugs that are included in the predictions in HashMap{drugName, DBID}
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fm.getSuppFolder() + fm.getFileSeparator() + "DBIDs_after_prune.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DrugDiseaseData.class.getName()).log(Level.SEVERE, null, ex);
        }
        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                String dbid = thisLine.trim();//.split("\t")[1];
                DBIDsUsed.add(dbid);//.put(drugName, dbid);
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }


        //prune the pairs from the gold standard that are not relevant to the restrictions we are looking at 
        for (Pair p : goldStandard) {
            boolean add = false;
            String MeSH = p.getMeSHUI();
            //prune the disease category
            if (category != DiseaseCat.ALL) {
                Set<String> meshTreeTerms = mesh.getTreeTerms(MeSH);
                if (meshTreeTerms != null) {
                    for (String m : meshTreeTerms) {
                        if (m.startsWith(category.getText())) {
                            add = true;
                        }
                    }
                }
            } else {
                add = true;
            }
            //prune the disease type
            if (type != DiseaseType.ALL && add) {
                if (type == DiseaseType.RARE) {
                    if (rareMeSH.contains(p.getMeSHUI())) {
                        add = true;
                    } else {
                        add = false;
                    }
                } else if (type == DiseaseType.COMMON) {
                    if (!rareMeSH.contains(p.getMeSHUI())) {
                        add = true;
                    } else {
                        add = false;
                    }
                }
            }

            if (add) {
                if (datasource == DataSourceKNOWNS.SIDER || datasource == DataSourceKNOWNS.NDFRT) {
                    if (DBIDsUsed.contains(p.getEntrezGeneSymbol())) {
                        removed.add(new Pair((p.getEntrezGeneSymbol()), p.getMeSHUI()));
                    }
                } else if (DBIDsUsed.contains(p.getEntrezGeneSymbol())) {
                    removed.add(p);
                }
            }
        }


        LOGGER.log(Level.INFO, "After pruning we have {0} gs drug-disease associations from {1} of disease type {2} and of category {3}", new Object[]{removed.size(), datasource.getText(), type.getText(), category.getText()});
        prune = removed;
    }

    public enum DataSourceKNOWNS {

        CTD("CTD"),
        PREDICT("PREDICT"),
        NDFRT("NDFRT"),
        ALL("ALL"),
        SIDER("SIDER");
        private String text;

        DataSourceKNOWNS(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public DataSourceKNOWNS[] getallDataSources() {
            return DataSourceKNOWNS.values();
        }

        public static DataSourceKNOWNS fromString(String text) {
            if (text != null) {
                for (DataSourceKNOWNS b : DataSourceKNOWNS.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static DataSourceKNOWNS fromValue(String value) throws IllegalArgumentException {
            try {
                return DataSourceKNOWNS.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    public enum DiseaseType {

        RARE("Rare_Disease"),
        COMMON("Common_Disease"),
        ALL("ALL");
        private String text;

        DiseaseType(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public DiseaseType[] getallDataSources() {
            return DiseaseType.values();
        }

        public static DiseaseType fromString(String text) {
            if (text != null) {
                for (DiseaseType b : DiseaseType.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static DiseaseType fromValue(String value) throws IllegalArgumentException {
            try {
                return DiseaseType.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    public enum DiseaseCat {

        C10("C10"),
        C04("C04"),
        ALL("ALL"),
        C20("C20");
        private String text;

        DiseaseCat(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public DiseaseCat[] getallDataSources() {
            return DiseaseCat.values();
        }

        public static DiseaseCat fromString(String text) {
            if (text != null) {
                for (DiseaseCat b : DiseaseCat.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static DiseaseCat fromValue(String value) throws IllegalArgumentException {
            try {
                return DiseaseCat.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    public Set<Pair> getPruned(){
        return prune;
    }
    
    public Set<Pair> getGS(){
        return goldStandard;
    }
    
    public Set<String> getDrugName2DBID() {
        return DBIDsUsed;
    }
}
