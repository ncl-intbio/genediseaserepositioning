/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class MeSHUI2MeSHTree {

    private FileManager fm;
    private Serialize ser;
    private File meshFileD;
    private File meshFileC;
    private final static Logger LOGGER = Logger.getLogger(MeSHUI2MeSHTree.class.getName());
    private HashMap<String, Set<String>> MeSHUI2MeshTreeTerms;
    private Map<String, Set<String>> headerterms2MeshTree;

    public MeSHUI2MeSHTree() {
        this.fm = new FileManager();
        this.meshFileD = fm.getMeSHDFile();
        this.meshFileC = fm.getMeSHCFile();
        this.MeSHUI2MeshTreeTerms = new HashMap<String, Set<String>>();
        this.headerterms2MeshTree = new HashMap<String, Set<String>>();
        run();
    }

    public static void main(String[] args) {

        MeSHUI2MeSHTree map = new MeSHUI2MeSHTree();
        System.out.println(map.getTreeTerms("C535325").toString());
        System.out.println(map.getTreeTerms("D003920").toString());

    }

    public void run() {

        LOGGER.info("Started to extract MeSH UIs to MeSH Treeterms...");
        getParentTreeTerms();
        getDRecords();
        getCRecords();
        LOGGER.info("Finished extracting MeSH UIs to MeSH Treeterms...");


    }

    public void getParentTreeTerms() {
        try {
            BufferedReader br = null;
            String thisLine;
            boolean firstDone = false;
            //Open the file for reading
            br = new BufferedReader(new FileReader(meshFileD));

            //variables
            String MeSHMainHeader = null;
            String MeSHUI = null;
            Set<String> MeSHTreeTerms = new HashSet<String>();
            try {
                while ((thisLine = br.readLine()) != null) {
                    if (thisLine.equals("*NEWRECORD")) {
                        //create new node and reset the variables

                        if (firstDone) {
                            int c = 1;
                            //we are only interested in diseases [begin with C in the hierarchy ] 
                            // and the three concepts of the F branch
                            boolean disease = false;
                            for (String tt : MeSHTreeTerms) {
                                if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                    disease = true;
                                }
                            }

                            //if the term is a disease then add it
                            if (disease) {
                                Set<String> treeterms = new HashSet<String>();
                                for (String tt : MeSHTreeTerms) {
                                    if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {

                                        treeterms.add(tt);

                                        c++;
                                    }
                                }
                                headerterms2MeshTree.put(MeSHMainHeader, treeterms);
                            }

                            MeSHTreeTerms = new HashSet<String>();
                        }
                    }

                    if (thisLine.startsWith("MH =")) {
                        MeSHMainHeader = split(thisLine);
                    }

                    if (thisLine.startsWith("UI =")) {
                        MeSHUI = split(thisLine);
                        firstDone = true;
                    }

                    if (thisLine.startsWith("MN =")) {
                        MeSHTreeTerms.add(split(thisLine));

                    }

                }
            } catch (IOException ex) {
                Logger.getLogger(MeSHUI2MeSHTree.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MeSHUI2MeSHTree.class.getName()).log(Level.SEVERE, null, ex);
        }


        LOGGER.info("Retrieved MeSHTreeTerms for all parent tree terms");
    }

    public void getCRecords() {
        try {
            BufferedReader br = null;
            String thisLine;
            boolean firstDone = false;

            //Open the file for reading
            br = new BufferedReader(new FileReader(meshFileC));

            //variables
            String MeSHMainHeader = null;
            String MeSHUI = null;
            //could be more than one
            Set<String> headingMappedTo = new HashSet<String>();
            Set<String> allMeSHTrees = new HashSet<String>();
            boolean add = false;
            try {
                while ((thisLine = br.readLine()) != null) {

                    //create new node and reset the variables
                    if (thisLine.startsWith("NM =")) {
                        MeSHMainHeader = split(thisLine);
                    }

                    if (thisLine.startsWith("HM =")) {
                        headingMappedTo.add(split(thisLine));
                    }

                    if (thisLine.startsWith("UI =")) {
                        MeSHUI = split(thisLine);
                        for (String hm : headingMappedTo) {
                            if (headerterms2MeshTree.containsKey(hm)) {
                                Set<String> treeterms = headerterms2MeshTree.get(hm);
                                for (String tt : treeterms) {
                                    if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                        allMeSHTrees.add(tt + ".supp");
                                    }
                                }
                            }

                        }

                        MeSHUI2MeshTreeTerms.put(MeSHUI, allMeSHTrees);
                        firstDone = true;

                        allMeSHTrees = new HashSet<String>();
                        headingMappedTo = new HashSet<String>();
                    }

                }

                //return null;
            } catch (IOException ex) {
                Logger.getLogger(MeSHUI2MeSHTree.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MeSHUI2MeSHTree.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.info("Retrieved MeSHTreeTerms for all C records");

    }

    public void getDRecords() {
        try {
            int maxDepth = 0;
            String longest = "";


            BufferedReader br = null;
            String thisLine;
            boolean firstDone = false;
            //Open the file for reading
            br = new BufferedReader(new FileReader(meshFileD));

            //variables
            String MeSHMainHeader = null;
            String MeSHUI = null;
            Set<String> MeSHTreeTerms = new HashSet<String>();
            try {
                while ((thisLine = br.readLine()) != null) {
                    if (thisLine.equals("*NEWRECORD")) {
                        //create new node and reset the variables

                        if (firstDone) {
                            int c = 1;
                            //create new node and reset the variables
                            // and the three concepts of the F branch
                            boolean disease = false;
                            for (String tt : MeSHTreeTerms) {
                                if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                    disease = true;
                                }
                            }

                            //if the term is a disease then add it
                            if (disease) {
                                Set<String> treeterms = new HashSet<String>();
                                for (String tt : MeSHTreeTerms) {
                                    if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                        String[] split = tt.split("\\.");
                                        if ((split.length - 1) > maxDepth) {
                                            maxDepth = split.length - 1;
                                            longest = tt;
                                        }
                                        treeterms.add(tt);
                                        c++;
                                    }
                                }

                                MeSHUI2MeshTreeTerms.put(MeSHUI, MeSHTreeTerms);
                                //System.out.println(MeSHTreeTerms.toString());
                                MeSHTreeTerms = new HashSet<String>();
                            }
                        }
                    }
                    if (thisLine.startsWith("UI =")) {
                        MeSHUI = split(thisLine);
                        firstDone = true;
                    }
                    if (thisLine.startsWith("MN =")) {
                        MeSHTreeTerms.add(split(thisLine));
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(MeSHUI2MeSHTree.class.getName()).log(Level.SEVERE, null, ex);
            }

            // return null;

            // return null;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MeSHUI2MeSHTree.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.info("Retrieved MeSHTreeTerms for all D records");
    }

    /**
     * Splits the MeSH line (aaaa = bbbbb) on the "=" and returns the second
     * element.
     *
     * @return
     */
    public String split(String meshLine) {

        String[] split = meshLine.split("=");
        return split[1].trim();

    }

    public Set<String> getTreeTerms(String MeSHUI) {

        Set<String> treeTerms;
        if (MeSHUI2MeshTreeTerms.containsKey(MeSHUI)) {
            treeTerms = MeSHUI2MeshTreeTerms.get(MeSHUI);
        } else {
            treeTerms = new HashSet<String>();
            treeTerms.add("NO_TREE_TERMS");
        }
        return treeTerms;
    }
}
