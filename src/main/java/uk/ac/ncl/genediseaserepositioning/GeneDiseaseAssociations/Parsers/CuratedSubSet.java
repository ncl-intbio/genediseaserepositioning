/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * This class creates a "Gold Standard" using all of the curated gene-disease
 * associations.
 *
 *
 */
public class CuratedSubSet implements GeneDiseaseParser {

    private FileManager fm;
    private Serialize ser;
    private String filenames;
    private Set<Pair> CuratedAssociations;
    private DataManager dm;
    private final static Logger LOGGER = Logger.getLogger(CuratedSubSet.class.getName());

    public static void main(String[] args) {
        CuratedSubSet curated = new CuratedSubSet();
        curated.Parse();
    }

    public CuratedSubSet() {
        this.fm = new FileManager();
        this.ser = new Serialize();
        this.filenames = "Curated_ALL";
        this.CuratedAssociations = new HashSet<Pair>();
        this.dm = new DataManager();
    }

    @Override
    public void Parse() {
        //add the CTD associations
        CuratedAssociations.addAll(dm.getAssociations(DataManager.DataSource.CTD));
        //add the UNIPROT associations
        CuratedAssociations.addAll(dm.getAssociations(DataManager.DataSource.UNIPROT));
        //add the OMIM associations
        CuratedAssociations.addAll(dm.getAssociations(DataManager.DataSource.OMIM));
        //add the ORPHANET associations
        CuratedAssociations.addAll(dm.getAssociations(DataManager.DataSource.ORPHANET));

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(new File(fm.getStandardisedFolder().getPath() + fm.getFileSeparator() + filenames + ".txt")));
        } catch (IOException ex) {
            Logger.getLogger(CTD.class.getName()).log(Level.SEVERE, null, ex);
        }

        //write to file
        for (Pair p : CuratedAssociations) {
            try {
                bw.append(p.getEntrezGeneSymbol() + "\t" + p.getMeSHUI() + "\n");
            } catch (IOException ex) {
                Logger.getLogger(CuratedSubSet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(CuratedSubSet.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "We have {0} curated associations", CuratedAssociations.size());
        ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() + fm.getFileSeparator() + filenames + ".ser");

        logInfo();

    }

    @Override
    public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(dateFormat.format(date)).append("\n");

        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true)));
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
