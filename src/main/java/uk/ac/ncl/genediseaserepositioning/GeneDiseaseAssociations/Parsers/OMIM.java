/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.OMIM2MeSH;

/**
 *
 * @author joe
 *
 * This parser is for the FlatFile version of the OMIM database which is
 * available for download from: ftp://ftp.omim.org/OMIM/
 *
 * <TODO>
 * Fix the parser- seems to have problems at; "Nail disorder, nonsyndromic
 * congenital, 7"
 *
 */
public class OMIM implements GeneDiseaseParser {

    private FileManager fm;
    private Serialize ser;
    private File OMIMFile;
    private String filenames;
    private Set<Pair> CuratedAssociations;
    private final static Logger LOGGER = Logger.getLogger(OMIM.class.getName());
    private int unmappable = 0;
    private int malformed = 0;
    private int GSKrels = 0;
    private BufferedWriter bw = null;
    private OMIM2MeSH OMIM2MESH;

    public static void main(String[] args) {
        OMIM omim = new OMIM();
        omim.Parse();
    }

    public OMIM() {
        this.fm = new FileManager();
        this.ser = new Serialize();
        this.OMIMFile = fm.getOMIMFile();
        this.filenames = "OMIM";
        this.OMIM2MESH = new OMIM2MeSH();
        this.CuratedAssociations = new HashSet<Pair>();

    }

    @Override
    public void Parse() {
        try {

            bw = new BufferedWriter(new FileWriter(fm.getStandardisedFolder().getPath() + fm.getFileSeparator() + filenames + ".txt"));

            BufferedReader br = null;
            String thisLine;//        backend.commitTransaction();
            boolean firstDone = false;
            //Open the file for reading
            br = new BufferedReader(new FileReader(OMIMFile));

            //variables
            while ((thisLine = br.readLine()) != null) {
                extractRelation(thisLine);
            }

            bw.close();

            ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() + fm.getFileSeparator() + filenames + ".ser");
            LOGGER.log(Level.INFO, "{0} associations are mapped to MeSH", CuratedAssociations.size());
            LOGGER.log(Level.INFO, "[{0}] gene>>disease associations were unmappable [OMIMdisID->MeSHID]", unmappable);
            LOGGER.log(Level.INFO, "[{0}] gene>>disease associations moleuclar basis unknown", malformed);
        } catch (IOException ex) {
            Logger.getLogger(OMIM.class.getName()).log(Level.SEVERE, null, ex);
        }
        logInfo();

    }

    @Override
    public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(CuratedAssociations.size() + unmappable).append("\t").append(dateFormat.format(date)).append("\n");

        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true)));
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void lineCount(String file) {
        try {
            BufferedReader br = null;
            String thisLine;//        backend.commitTransaction();
            //Open the file for reading
            br = new BufferedReader(new FileReader(file));
            try {
                //variables
                while ((thisLine = br.readLine()) != null) {
                    GSKrels++;
                }
            } catch (IOException ex) {
                Logger.getLogger(OMIM.class.getName()).log(Level.SEVERE, null, ex);
            }
            LOGGER.log(Level.INFO, "Counted [{0}] GSK gene>>disease associations", GSKrels);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OMIM.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void extractRelation(String line) {
        String rel = "";

        String OMIMdiseaseID = null;
        String OMIMdiseaseName = null;
        String MESHID = null;
        String OMIMgeneID = null;
        String ENTREZSYMBOL = null;

        String[] split = line.split("\\|");

        String[] disSplit = split[0].split(",");

        if (disSplit[disSplit.length - 1].length() - 4 > 0) {
            OMIMdiseaseID = disSplit[disSplit.length - 1].substring(0, disSplit[disSplit.length - 1].length() - 4).trim();
        } else {
            OMIMdiseaseID = "why";
        }

        OMIMdiseaseName = "";
        String[] disSplit2 = split[0].split(",");
        for (int i = 0; i < disSplit2.length - 1; i++) {
            OMIMdiseaseName = OMIMdiseaseName.trim() + disSplit2[i].toUpperCase() + ",";
        }
        if (!isInteger(OMIMdiseaseID)) {
            malformed++;
        } else {
            MESHID = OMIM2MESH.getMeSHTerm(OMIMdiseaseID);
            if (MESHID.equals("NO_MeSH_TERM")) {
                unmappable++;
                LOGGER.log(Level.INFO, "No map between OMIM diseaseID and MeSH diseaseID: {0}", line);
            } else {
                try {
                    OMIMdiseaseName = OMIMdiseaseName.substring(0, OMIMdiseaseName.length() - 1);
                    OMIMgeneID = split[2];
                    ENTREZSYMBOL = split[1].split(",")[0].trim();
                    bw.append(ENTREZSYMBOL + "\t" + MESHID + "\n");
                    CuratedAssociations.add(new Pair(ENTREZSYMBOL, MESHID));
                    System.out.println(ENTREZSYMBOL + "\t" + MESHID);
                } catch (IOException ex) {
                    Logger.getLogger(OMIM.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}
