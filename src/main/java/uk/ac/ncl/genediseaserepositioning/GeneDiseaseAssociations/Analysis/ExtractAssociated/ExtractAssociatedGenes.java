/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.ExtractAssociated;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager.DataSource;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes a MeSH UI and returns all genes that are associated to that
 * disease.
 */
public class ExtractAssociatedGenes {

    private DataManager dm;
    private FileManager fm;

    public static void main(String[] args) {

//        DataManager dm = new DataManager(DataSource.PHARMA);
//        ExtractAssociatedGenes extract = new ExtractAssociatedGenes(dm);
//        //extract.goSearch("C537243");
//        Set<String> uis = new HashSet<>();
//        uis.add("C537243");
//        uis.add("D011471");
//        uis.add("D008527");
//        uis.add("D018242");
//        uis.add("D015470");
//        uis.add("D054198");
//        uis.add("C538231");
//        uis.add("D008654");
//        uis.add("D000230");
//        uis.add("D016403");
//        uis.add("D008224");
//        uis.add("D001943");
//        uis.add("D003110");
//        extract.goSearch(uis);

    }

    public ExtractAssociatedGenes(DataManager dm) {
        this.dm = dm;
        this.fm = new FileManager();
    }

    public void goSearch(Set<String> meshuis) {
        for (String dis : meshuis) {
            goSearch(dis);
        }

    }

    public void goSearch(String MeSHUI) {
        HashMap<String, Set<String>> datasources = new HashMap<>();
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(fm.getGDQuery().getPath() + MeSHUI + ".txt"));
        } catch (IOException ex) {
            Logger.getLogger(ExtractAssociatedGenes.class.getName()).log(Level.SEVERE, null, ex);
        }
        HashMap<String, Set<Pair>> associations = dm.getAllSources();
        for (String source : associations.keySet()) {
            //if (!source.equals("GOFLOF")) {
            Set<Pair> pairs = associations.get(source);
            for (Pair p : pairs) {
                if (p.getMeSHUI().equals(MeSHUI)) {
                    if (datasources.containsKey(p.getEntrezGeneSymbol())) {
                        Set<String> sources = datasources.get(p.getEntrezGeneSymbol());
                        sources.add(source);
                        datasources.put(p.getEntrezGeneSymbol(), sources);
                    } else {
                        Set<String> sources = new HashSet<String>();
                        sources.add(source);
                        datasources.put(p.getEntrezGeneSymbol(), sources);
                    }
                }
                //   }
            }
        }

        for (String gene : datasources.keySet()) {
            try {
                bw.append(gene + "\t" + datasources.get(gene).size() + "\t" + datasources.get(gene).toString() + "\n");
            } catch (IOException ex) {
                Logger.getLogger(ExtractAssociatedGenes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(ExtractAssociatedGenes.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
