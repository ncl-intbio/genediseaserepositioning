/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.LLS.LlsScore;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class creates a matrix containing all the scores of using each dataset as the
 * gold standard during the LLS scoring of associations.
 */
public class LLSMatrix {

    private final static Logger LOGGER = Logger.getLogger(LLSMatrix.class.getName());
    private DataManager dm;
    private ArrayList<String> sortedNames;
    private HashMap<String, Set<Pair>> allSources;
    private String[][] matrix;
    private FileManager fman;

    public static void main(String[] args) {
        DataManager dm = new DataManager();
        LLSMatrix lls = new LLSMatrix(dm);
        lls.run();
        lls.printMatrix();
        lls.writeMatrixToFile(true);

    }

    public LLSMatrix(DataManager dm) {
        this.dm = dm;
        this.allSources = dm.getAllSources();
        this.matrix = new String[allSources.size() + 1][allSources.size() + 1];
        this.fman = new FileManager();
    }

    public void run() {
        //sort the names into alphabetical order
        Set<String> names = allSources.keySet();
        ArrayList<String> sortedNames = new ArrayList<String>();
        sortedNames.addAll(names);
        Collections.sort(sortedNames);
        this.sortedNames = sortedNames;
        //System.out.println(sortedNames.indexOf("GOFLOF"));

        matrix[0][0] = "LLS_SCO_MATR";
        int count = 1;
        for (String name : sortedNames) {
            System.out.println(name);
            matrix[count][0] = name;
            matrix[0][count] = name;
            count++;
        }
        for (DataManager.DataSource d : dm.getAllEnums()) {
            DecimalFormat df = new DecimalFormat("#.##");
            LOGGER.log(Level.INFO, "Scoring using {0} as the gold standard", d.getText());
            DataManager data = new DataManager(d);
            LlsScore llsdata = new LlsScore(data);
            Map<String, Double> scores = llsdata.scoredDataSet();
            Map<String, Double> trimmed = new HashMap<>();
            for (String name : scores.keySet()) {
                trimmed.put(name, Double.parseDouble(df.format(scores.get(name))));
            }
            addToMatrix(d.getText(), trimmed);
        }
    }

    public void addToMatrix(String goldStandard, Map<String, Double> scores) {
        int gs = getMatrixPos(goldStandard);
        for (String name : scores.keySet()) {
            int from = getMatrixPos(name);
            matrix[gs][from] = (scores.get(name).toString());
        }
    }

    public int getMatrixPos(String name) {
        return sortedNames.indexOf(name) + 1;
    }

    public void printMatrix() {
        for (int i = 0; i < matrix.length; i++) {
            for (int p = 0; p < matrix.length; p++) {
                System.out.print(matrix[i][p] + "\t");
            }
            System.out.println("\n");
        }
    }

    public void writeMatrixToFile(boolean latex) {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(fman.getGD_OPFolder() + fman.getFileSeparator() + "LLS_Matrix" + System.currentTimeMillis() + ".txt"));
        } catch (IOException ex) {
            Logger.getLogger(LLSMatrix.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < matrix.length; i++) {
            StringBuilder line = new StringBuilder();
            for (int p = 0; p < matrix.length; p++) {

                if (latex) {
                    line.append(matrix[i][p] + "&");
                } else {
                    line.append(matrix[i][p] + "\t");
                }
            }

            if (latex) {
                line.append("\\");
            }

            try {
                bw.append(line + "\n");
            } catch (IOException ex) {
                Logger.getLogger(LLSMatrix.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(LLSMatrix.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
