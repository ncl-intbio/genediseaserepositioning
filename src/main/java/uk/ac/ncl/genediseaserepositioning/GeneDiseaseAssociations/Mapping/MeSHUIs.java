/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author joe
 *
 * Class takes a MeSHTreeterm and returns the corresponding MeSHTree terms
 * (C535325)
 *
 */
public class MeSHUIs {

    private String CFile = "/Volumes/MyPassport/Datasets/MeSH/c2015.bin";
    private String DFile = "/Volumes/MyPassport/Datasets/MeSH/d2015.bin";
    private final static Logger LOGGER = Logger.getLogger(MeSHUIs.class.getName());
    private FileManager fm;
    private Serialize ser;
    private String filenames;
    private BufferedWriter bw;
    private Set<String> MeSHUIs;
    private Map<String, Set<String>> headerterms2MeshTree;
    private Map<String, String> headerTerms;

    //private String CFile = "/Users/joemullen/Desktop/Datasets_GSK/MeSH/c2015.bin";
    public static void main(String[] args) {
        MeSHUIs m = new MeSHUIs();
        m.run();
    }

    public MeSHUIs() {
        this.fm = new FileManager();
        this.ser = new Serialize();
        this.filenames = "MeSH";
        this.MeSHUIs = new HashSet<String>();
        this.headerTerms = new HashMap<String, String>();
        this.headerterms2MeshTree = new HashMap<String, Set<String>>();
    }

    public void run() {

        try {
            bw = new BufferedWriter(new FileWriter(fm.getStandardisedFolder().getPath()+fm.getFileSeparator() + filenames + ".txt"));
        } catch (IOException ex) {
            Logger.getLogger(MeSHUIs.class.getName()).log(Level.SEVERE, null, ex);
        }

        extractDFile();
        int d = MeSHUIs.size();
        LOGGER.log(Level.INFO, "{0} MeSHUIs were extracted from {1}", new Object[]{d, DFile});
        extractCFile();
        LOGGER.log(Level.INFO, "{0} MeSHUIs were extracted from {1}", new Object[]{(MeSHUIs.size() - d), CFile});
        LOGGER.log(Level.INFO, "{0} MeSHUIs were extracted in total", MeSHUIs.size());
        ser.SerializeSetString(MeSHUIs, fm.getSerializedFolder().getPath() +fm.getFileSeparator() + filenames + ".ser");
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(MeSHUIs.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void extractDFile() {

        BufferedReader br = null;
        String thisLine;
        boolean firstDone = false;
        try {
            //Open the file for reading
            br = new BufferedReader(new FileReader(DFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MeSHUIs.class.getName()).log(Level.SEVERE, null, ex);
        }
        //variables
        String MeSHMainHeader = null;
        String MeSHUI = null;
        Set<String> MeSHTreeTerms = new HashSet<String>();
        try {
            while ((thisLine = br.readLine()) != null) {
                if (thisLine.equals("*NEWRECORD")) {
                    //create new node and reset the variables

                    if (firstDone) {
                        int c = 1;
                        //we are only interested in diseases [begin with C in the hierarchy ] 
                        // and the three concepts of the F branch
                        boolean disease = false;
                        for (String tt : MeSHTreeTerms) {
                            if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                disease = true;
                            }
                        }
                        //if the term is a disease then add it
                        if (disease) {
                            Set<String> treeterms = new HashSet<String>();
                            for (String tt : MeSHTreeTerms) {
                                if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                    headerTerms.put(tt, MeSHUI);
                                    treeterms.add(tt);
                                    MeSHUIs.add(MeSHUI);
                                    bw.append(MeSHUI + "\t");
                                }
                            }
                            headerterms2MeshTree.put(MeSHMainHeader, treeterms);
                        }
                        MeSHTreeTerms = new HashSet<String>();
                    }
                }

                if (thisLine.startsWith("MH =")) {
                    MeSHMainHeader = split(thisLine);
                }

                if (thisLine.startsWith("UI =")) {
                    MeSHUI = split(thisLine);
                    firstDone = true;
                }

                if (thisLine.startsWith("MN =")) {
                    MeSHTreeTerms.add(split(thisLine));
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(MeSHUIs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void extractCFile() {
        try {
            BufferedReader br = null;
            String thisLine;
            boolean firstDone = false;
            //Open the file for reading
            br = new BufferedReader(new FileReader(CFile));

            //variables
            String MeSHMainHeader = null;
            String MeSHUI = null;
            //could be more than one
            Set<String> headingMappedTo = new HashSet<String>();
            boolean add = false;
            try {
                while ((thisLine = br.readLine()) != null) {
                    if (thisLine.equals("*NEWRECORD")) {
                        //create new node and reset the variables

                        if (firstDone) {
                            if (!headerTerms.isEmpty()) {

                                for (String hm : headingMappedTo) {
                                    if (headerTerms.containsKey(hm)) {
                                        add = true;
                                    }
                                    if (headerterms2MeshTree.containsKey(hm)) {
                                        Set<String> treeterms = headerterms2MeshTree.get(hm);
                                        for (String tt : treeterms) {
                                            int c = 1;
                                            if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                                MeSHUIs.add(MeSHUI);
                                                bw.append(MeSHUI + "\t");
                                            }
                                        }
                                    }
                                }
                                if (add) {
                                }
                            }
                            add = false;
                            headingMappedTo = new HashSet<String>();
                        }
                    }

                    if (thisLine.startsWith("NM =")) {
                        MeSHMainHeader = split(thisLine);
                    }

                    if (thisLine.startsWith("HM =")) {
                        headingMappedTo.add(split(thisLine));
                    }

                    if (thisLine.startsWith("UI =")) {
                        MeSHUI = split(thisLine);
                        firstDone = true;
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(MeSHUIs.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MeSHUIs.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Splits the MeSH line (aaaa = bbbbb) on the "=" and returns the second
     * element.
     *
     * @return
     */
    public String split(String meshLine) {

        String[] split = meshLine.split("=");
        return split[1].trim();

    }
}
