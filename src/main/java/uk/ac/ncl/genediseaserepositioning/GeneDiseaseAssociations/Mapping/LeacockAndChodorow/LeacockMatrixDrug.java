/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class LeacockMatrixDrug implements Serializable {

    public String[][] getMatrix() {
        return matrix;
    }

    public String getDBID() {
        return DBID;
    }

    public ArrayList<String> getKnownsOrdered() {
        return knownsOrdered;
    }

    public ArrayList<String> getPredictedOrdered() {
        return predictedOrdered;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    private String[][] matrix;
    private String DBID;
    private ArrayList<String> knownsOrdered;
    private ArrayList<String> predictedOrdered;
    private int x;
    private int y;

    public LeacockMatrixDrug(String DBID, int x, int y) {

        this.x = x;
        this.y = y;
        this.matrix = new String[x][y];
        this.DBID = DBID;

    }

    public String getScore(int x, int y) {
        return matrix[x][y];
    }

    public void addValue(String value, int x, int y) {
        matrix[x][y] = value;
    }

    public void printMatrix() {
        for (int i = 0; i < x; i++) {
            for (int p = 0; p < y; p++) {
                System.out.print(matrix[i][p] + "\t");
            }
            System.out.println("\n");
        }
    }

    /**
     * Check to see if any of the known pairs map to the predicted pair with a certain threshold.
     * @param threshold
     * @param predPos
     * @return 
     */
    public boolean testMapPredicted(double threshold, int predPos) {
        for (int i = 1; i < knownsOrdered.size() + 1; i++) {
            if (Double.parseDouble(getScore(i, predPos)) >= threshold) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Check to see if any of the predicted pairs map to the known pair with a certain threshold.
     * @param threshold
     * @param predPos
     * @return 
     */
    public boolean testMapKnown(double threshold, int knownPos) {
        for (int i = 1; i < predictedOrdered.size() + 1; i++) {
            if (Double.parseDouble(getScore(knownPos, i)) >= threshold) {
                return true;
            }
        }
        return false;
    }

    public int getKnownPos(String pair) {
        return knownsOrdered.indexOf(pair) + 1;
    }

    public int getPredPos(String pair) {
        return predictedOrdered.indexOf(pair) + 1;
    }

    public void setMatrix(String[][] matrix) {
        this.matrix = matrix;
    }

    public void setDBID(String DBID) {
        this.DBID = DBID;
    }

    public void setKnownsOrdered(ArrayList<String> knownsOrdered) {
        this.knownsOrdered = knownsOrdered;
    }

    public void setPredictedOrdered(ArrayList<String> predictedOrdered) {
        this.predictedOrdered = predictedOrdered;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
