/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.MeSHUI2MeSHTree;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes all sources from the disease-gene files and outputs the
 * percentage of associations from each source that is in each therapeutic area
 * of the MeSH branch.
 *
 *
 */
public class DiseaseDistribution {

    private FileManager fm;
    private Serialize ser;
    private final static Logger LOGGER = Logger.getLogger(DiseaseDistribution.class.getName());
    private DataManager dm;
    private HashMap<String, HashMap<String, Integer>> count;
    private MeSHUI2MeSHTree getMeSHTree;
    private int depth;
    private FileManager fman;

    public static void main(String[] args) {
        DiseaseDistribution dd = new DiseaseDistribution(1);
        dd.run();
    }

    public DiseaseDistribution(int depth) {
        this.dm = new DataManager();
        this.count = new HashMap<String, HashMap<String, Integer>>();
        this.getMeSHTree = new MeSHUI2MeSHTree();
        this.depth = depth;
        this.fman = new FileManager();
    }

    public void run() {
        //do for all
        HashMap<String, Set<Pair>> allData = dm.getAllSources();
        for (String datasource : allData.keySet()) {
            getDistributionOfDiseases(datasource, allData.get(datasource));
        }
        //then do for the gold standard
        //getDistributionOfDiseases("PHARMA", dm.getGoldStandard().getTruePositives());
        //write the output to file to be plotted
        writeToFile();
    }

    public void getDistributionOfDiseases(String database, Set<Pair> associations) {
        HashMap<String, Integer> check = new HashMap<String, Integer>();
        for (Pair p : associations) {
            Set<String> treeTerms = getMeSHTree.getTreeTerms(p.getMeSHUI());
            for (String tt : treeTerms) {
                if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                    String add = getTreeAtDepth(tt);
                    if (check.containsKey(add)) {
                        int freq = check.get(add) + 1;
                        check.put(add, freq);
                    } else {
                        int freq = 1;
                        check.put(add, freq);
                    }
                }
            }
        }
        count.put(database, check);
        LOGGER.log(Level.INFO, "Calculated diseases distribution of associations from {0}", database);
    }

    public String getTreeAtDepth(String tree) {

        String[] split = tree.split("\\.");
        String newTree = "";
        for (int i = 0; i < split.length; i++) {
            if (i < depth) {
                if (i == 0) {
                    newTree = split[i];
                } else {
                    newTree = newTree + "." + split[i];
                }
            } else {
                break;
            }
        }
        return newTree;

    }

    public void writeToFile() {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(new File(fman.getGD_OPFolder()+fman.getFileSeparator()+"disease_Distributions.txt")));
        } catch (IOException ex) {
            Logger.getLogger(DiseaseDistribution.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (String datasource : count.keySet()) {
            HashMap<String, Integer> deets = count.get(datasource);
            try {
                bw.append(datasource + "\n");
            } catch (IOException ex) {
                Logger.getLogger(DiseaseDistribution.class.getName()).log(Level.SEVERE, null, ex);
            }


            //check that all the disease areas have been added
            String [] areas = new String("C01,C02,C03,C04,C05,C06,C07,C08,C09,C10,C11,C12,C13,C14,C15,C16,C17,C18,C19,C20,C21,C22,C23,C24,C25,C26,F01,F02,F03").split(",");
            for (String therarea: areas){
                if (!deets.keySet().contains(therarea)){
                    deets.put(therarea, 0);   
                }    
            }
            
            
            List<String> sortedKeys = new ArrayList(deets.keySet());
            Collections.sort(sortedKeys);
            String therArea = "";
            String freq = "";

            int count = 0;
            for (String area : sortedKeys) {
                if (count == 0) {
                    therArea = area;
                    freq = deets.get(area).toString();
                } else {
                    therArea = therArea + "," + area;
                    freq = freq + "," + deets.get(area).toString();
                }
                count++;
            }
            try {
                bw.append(therArea + "\n");
                bw.append(freq + "\n");
                bw.append("-------------------");
            } catch (IOException ex) {
                Logger.getLogger(DiseaseDistribution.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(DiseaseDistribution.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
