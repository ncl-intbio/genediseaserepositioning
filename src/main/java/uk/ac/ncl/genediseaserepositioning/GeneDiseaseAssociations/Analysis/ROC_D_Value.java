/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class calculates the values to be used during the production of ROC curves to
 * identify the d_value to be used during scoring of gene-disease associations.
 *
 * ANYTHING ABOVE THE THREHOLD WE ARE CLASSIFYING AS A GENE-DISEASE ASSOCIATION;
 * ANYTHING BELOW WE ARE SAYING IS NOT.
 *
 * DO THIS AGAIN---- EEDIOT.
 *
 *
 */
public class ROC_D_Value {

    private BufferedReader br;
    private FileManager fm;
    private DataManager dm;
    private String file;
    private int points;
    //required values
    private int possibleRels;
    private int possibleFalseNeg;
    private int truePositive;
    //ranges for the points
    private double[] thresholds;
    //results
    private int[][] matchesTruePositives;
    private double highestScore;
    private double lowestScore;
    private final static Logger LOGGER = Logger.getLogger(ROC_D_Value.class.getName());

    /**
     * Get all values that are required to plot a ROC curve from the input file
     * using the Gold Standard.
     *
     * @param file
     * @param dm
     */
    public ROC_D_Value(String file, DataManager dm, int points) {
        this.dm = dm;
        this.possibleRels = dm.getGoldStandard().getNumberOfGSDiseases() * dm.getGoldStandard().getNumberOfGSGenes();
        this.possibleFalseNeg = possibleRels - dm.getGoldStandard().getTruePositives().size();
        this.file = file;
        this.points = points;
        this.highestScore = 0.0;
        this.fm = new FileManager();
    }

    public void run() {
        createRange();
        calculatePlots();
        print();
    }

    /**
     * Add pair to correct category and check whether it is a true positive.
     *
     * @param p
     * @param score
     */
    public void recordPair(Pair p, double score) {
        //find the index of the threshold at which the pair has been scored
        int index = 0;
        for (int i = 0; i < thresholds.length; i++) {
            if (i == thresholds.length - 1) {
                if (score > thresholds[i] && score <= highestScore) {
                    index = i;
                }
            } else {
                if (score > thresholds[i] && score <= thresholds[i + 1]) {
                    index = i;
                }
            }
        }

        int checked = matchesTruePositives[index][0];
        int tp = matchesTruePositives[index][1];

        if (dm.getGoldStandard().getTruePositives().contains(p)) {
            //it is a true positive
            tp++;
        }
        checked++;

        matchesTruePositives[index][0] = checked;
        matchesTruePositives[index][1] = tp;

    }

    /**
     * Calculate the values required to plot a ROC curve.
     */
    public void calculatePlots() {

        //initialize data structure to store data to be plotted
        this.matchesTruePositives = new int[thresholds.length][2];

        try {
            this.br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            String line;
            while ((line = br.readLine()) != null) {
                String[] split = line.split("\t");
                Pair p = new Pair(split[0], split[1]);
                double pairScore = Double.parseDouble(split[2]);
                recordPair(p, pairScore);
            }
        } catch (IOException ex) {
            Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void print() {
        LOGGER.info("Binned results...");
        int[][] matchesTruePositivesCUM = new int[matchesTruePositives.length][2];
        for (int i = 0; i < matchesTruePositives.length; i++) {
            for (int y = 0; y < matchesTruePositives.length; y++) {
                if (y <= i) {
                    int mappings = matchesTruePositivesCUM[y][0];
                    int tps = matchesTruePositivesCUM[y][1];
                    mappings += matchesTruePositives[i][0];
                    tps += matchesTruePositives[i][1];
                    matchesTruePositivesCUM[y][0] = mappings;
                    matchesTruePositivesCUM[y][1] = tps;
                }
            }
        }
        LOGGER.info("Cumulative results...");

        //output to file
        BufferedWriter bw = null;
        String opfile = fm.getLLSToPlotFolder() + fm.getFileSeparator() + file.split("/")[file.split("/").length - 1];
        try {
            bw = new BufferedWriter(new FileWriter(opfile));
        } catch (IOException ex) {
            Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
        }

        int allAssociations = matchesTruePositivesCUM[0][0];
        System.out.println(allAssociations);
        System.out.println(dm.getGoldStandard().getTruePositives().size());

        for (int i = 0; i < matchesTruePositives.length; i++) {

            int negClassed = allAssociations -matchesTruePositivesCUM[i][0];
            int allNegative = allAssociations - dm.getGoldStandard().getTruePositives().size();
            int tp= matchesTruePositivesCUM[i][1];
            
            double sensitivity = tp/(dm.getGoldStandard().getTruePositives().size()*1.0);       
            //look here again!
            
            double specificity = (negClassed-(dm.getGoldStandard().getTruePositives().size()-tp))/(allNegative*1.0);                
                    
                    //(allAssociations--(dm.getGoldStandard().getTruePositives().size()-allAssociations-matchesTruePositivesCUM[i][1]))/(allAssociations-(dm.getGoldStandard().getTruePositives().size()*1.0));

            try {
                bw.append(matchesTruePositivesCUM[i][0] + "\t" + matchesTruePositivesCUM[i][1] + "\t" + thresholds[i] + "\t"+sensitivity+"\t"+specificity+"\n");
            } catch (IOException ex) {
                Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Cumulative results written to {0} ready for plot", opfile);
        LOGGER.log(Level.INFO, "Possible rels {0}", possibleRels);
        LOGGER.log(Level.INFO, "Possible false negative {0}", possibleFalseNeg);

    }

    /**
     * Creates the ranges that will be used to create the points on the ROC
     * curve.
     */
    public void createRange() {
        try {
            this.br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            String line;
            int count = 0;
            while ((line = br.readLine()) != null) {
                String[] split = line.split("\t");
                if (count == 0) {
                    highestScore = Double.parseDouble(split[2]);
                }
                lowestScore = Double.parseDouble(split[2]);
                count++;
            }
        } catch (IOException ex) {
            Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
        }

        double[] ranges = new double[points];
        for (int i = 0; i < ranges.length - 1; i++) {
            ranges[i] = (((highestScore - lowestScore) / (points - 1.0)) * (i * 1.0)) + lowestScore;
        }
        ranges[points - 1] = highestScore;
        System.out.println(highestScore + "\t" + lowestScore);
        thresholds = ranges;
    }
}
