/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations;

import java.io.Serializable;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Pair implements Serializable{
    private static final long serialVersionUID = 1L;

    private String EntrezGeneSymbol;
    private String MeSHUI;

    public Pair(String Gene, String MeSHUI) {
        this.MeSHUI = MeSHUI;
        this.EntrezGeneSymbol = Gene;
    }

    public String getMeSHUI() {
        return MeSHUI;
    }

    public String getEntrezGeneSymbol() {
        return EntrezGeneSymbol;
    }

    public void setMeSHUI(String MeSHUI) {
        this.MeSHUI = MeSHUI;
    }

    public void setEntrezGeneSymbol(String EntrezGeneSymbol) {
        this.EntrezGeneSymbol = EntrezGeneSymbol;
    }

    @Override
    public String toString() {
        return EntrezGeneSymbol + "\t" + MeSHUI;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((EntrezGeneSymbol == null) ? 0 : EntrezGeneSymbol.hashCode());
        result = prime * result + ((MeSHUI == null) ? 0 : MeSHUI.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Pair other = (Pair) obj;

        return (this.EntrezGeneSymbol == other.EntrezGeneSymbol || (this.EntrezGeneSymbol != null && this.EntrezGeneSymbol.equals(other.EntrezGeneSymbol))) && (this.MeSHUI == other.MeSHUI || (this.MeSHUI != null && this.MeSHUI.equals(other.MeSHUI)));

    }
}
