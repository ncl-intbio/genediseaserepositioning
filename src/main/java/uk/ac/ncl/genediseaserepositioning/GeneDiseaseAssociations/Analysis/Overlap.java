/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * This method takes associations at random from data-sources and sees what the
 * overlap for this association is amongst all the data-sources. E.g. pick one
 * association at random from each source (CTD) and see how many data-sources
 * capture this association.
 *
 */
public class Overlap {

    private HashMap<String, Set<Pair>> test;
    private DataManager dm;
    private FileManager fm;
    private int GSSIZE;
    private int repeats = 1000;

    public static void main(String[] alan) {
        Overlap pr = new Overlap();
        pr.run();
    }

    public Overlap() { 
            this.dm = new DataManager();
            this.fm = new FileManager();
            this.test = dm.getAllSources(); 
    }

    public void run() {
        try {
            BufferedWriter bw = null;
            try {
                bw = new BufferedWriter(new FileWriter(new File(fm.getGD_OPFolder()+fm.getFileSeparator() + "overlap_Random.txt")));
            } catch (IOException ex) {
                Logger.getLogger(Overlap.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (String source : test.keySet()) {
                Set<Pair> ass = test.get(source);
                List<Pair> assoc = new ArrayList<Pair>();
                assoc.addAll(ass);
                Set<Pair> randomAss = new HashSet<Pair>();
                Random random = new Random(); //Create random class object
                int randomNumber = 0;
                int count = 0;
                //pick a repeats # of random relations from a datasource
                while (count < repeats) {
                    if (count == assoc.size()) {
                        break;
                    }
                    randomNumber = random.nextInt(assoc.size());
                    Pair association = assoc.get(randomNumber);
                    randomAss.add(association);
                    assoc.remove(association);
                    count++;
                }
                double total = 0.0;
                int[] overlaps = new int[repeats];
                int check = 0;
                for (Pair randomgd : randomAss) {
                    int overlap = 0;
                    for (String source2 : test.keySet()) {
                        if (!source.equals(source2)) {
                            Set<Pair> ass2 = test.get(source2);
                            if (ass2.contains(randomgd)) {
                                overlap++;
                            }
                        }

                    }

                    total += overlap;
                    overlaps[check] = overlap;
                    check++;
                }

               // System.out.println(source + "\t" + total / repeats);
                String toPlot = "";
                for (double d : overlaps) {
                    bw.append(source + "\t" + d + "\n");
                }
            }
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(Overlap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
