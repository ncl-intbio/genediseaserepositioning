/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.Metathesaurus;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.OMIM2MeSH;


/**
 *
 * @author joemullen download the human gene-disease associations file from:
 *
 *
 *
 */
public class MGD implements GeneDiseaseParser{

    private FileManager fm;
    private Serialize ser;
    private String filenames;
    private Set<Pair> CuratedAssociations;
    private String line;
    private double scoreThresh;
    private Set<String> unmapped;
    private File filepath;
    private final static Logger LOGGER = Logger.getLogger(MGD.class.getName());
    private Map<String, String> alreadyMapped;
    private OMIM2MeSH OMIM2MESH;
    private Metathesaurus met;
    private int unmappable = 0;

    public static void main(String[] args) {
        MGD dis = new MGD();
        dis.Parse();
    }

    public MGD() {
        this.met = new Metathesaurus();
        this.ser = new Serialize();
        this.filenames = "MGD";
        this.CuratedAssociations = new HashSet<Pair>();
        this.fm = new FileManager();
        this.filepath = fm.getMGDFile();
        this.alreadyMapped = new HashMap<>();
        this.OMIM2MESH = new OMIM2MeSH();
        this.unmapped = new HashSet<String>();
    }

    @Override
    public void Parse() {
        try {
            LOGGER.log(Level.INFO, "Parsing MGD file {0} ....", new Object[]{filepath});

            BufferedReader br = null;
            BufferedWriter bw = null;

            String line;
            try {
                br = new BufferedReader(new FileReader(filepath));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MGD.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                bw = new BufferedWriter(new FileWriter(new File(fm.getStandardisedFolder().getPath()+fm.getFileSeparator() + filenames + ".txt")));
            } catch (IOException ex) {
                Logger.getLogger(MGD.class.getName()).log(Level.SEVERE, null, ex);
            }

            String OMIM_Disease_ID = null;
            String OMIM_Disease_Name = null;
            String HomoloGene_ID = null;
            String Common_Organism_Name = null;
            String NCBI_Taxon_ID = null;
            String Symbol = null;
            String EntrezGene_ID = null;
            String Mouse_MGI_ID = null;

            boolean skipFirst = true;
            while ((line = br.readLine()) != null) {
                //do something
                if (!line.startsWith("#") && !line.isEmpty()) {
                    //System.out.println(line);
                    if (skipFirst) {
                        skipFirst = false;
                    } else {
                        String[] values = line.split("\\t", -1); // don't truncate empty fields

                        OMIM_Disease_ID = values[0];
                        OMIM_Disease_Name = values[1];
                        HomoloGene_ID = values[2];
                        Common_Organism_Name = values[3];
                        NCBI_Taxon_ID = values[4];
                        Symbol = values[5];
                        EntrezGene_ID = values[6];
                        Mouse_MGI_ID = values[7];

                        String MESH_ID = OMIM2MESH.getMeSHTerm(OMIM_Disease_ID);
                        Pair p = new Pair(Symbol.toUpperCase(), MESH_ID);

                       // System.out.println(p.toString());
                        if (Common_Organism_Name.contains("mouse") && !Symbol.isEmpty() && !OMIM_Disease_ID.isEmpty() && !Symbol.contains("(")) {
                            if (MESH_ID.equals("NO_MeSH_TERM")) {
                                unmappable++;
                            } else {
                                CuratedAssociations.add(p);
                               // System.out.println(p.toString());
                            }
                        }
                    }
                }
            }

            for (Pair p : CuratedAssociations) {
                bw.append(p.getEntrezGeneSymbol() + "\t" + p.getMeSHUI() + "\n");
            }

            br.close();
            bw.close();
            LOGGER.log(Level.INFO,
                    "Parsed {0} MGD relations", CuratedAssociations.size());
            LOGGER.log(Level.INFO,
                    "{0} dassociations containing diseases unmappable MeSH terms", unmappable);
            ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() +fm.getFileSeparator() + filenames + ".ser");
        } catch (IOException ex) {
            Logger.getLogger(MGD.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
     logInfo();

    }
    
    @Override
    public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(CuratedAssociations.size()+unmappable).append("\t").append(dateFormat.format(date)).append("\n");

        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true)));
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
