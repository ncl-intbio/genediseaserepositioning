/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.OMIM2MeSH;

/**
 *
 * @author joemullen download the human gene-disease associations file from: v1.
 * ftp://rgd.mcw.edu/pub/data_release/ v2.
 * ftp://rgd.mcw.edu/pub/data_release/annotated_rgd_objects_by_ontology/with_terms/rattus_terms_rdo
 *
 * Some diseases are mapped to MeSH whereas others are mapped to OMIM.
 *
 * In this file, we kept the associations that were not labeled as “resistance”,
 * “induced”, or “no association”, nor the ones annotated with the following
 * evidence codes “Inferred from electronic annotation”, “Inferred from sequence
 * or structural similarity” and “Non-traceable author statement”.
 *
 *
 * After contacting the RGD I was told to use the file from:
 *
 * 1	DB 2	DB Object ID 3	DB Object Symbol 4	Qualifier 5	RDO ID 6	DB:Reference
 * (|DB:Reference) 7	Evidence Code 8	With (or) From 9	Aspect 10	DB Object Name
 * 11	ONTOLOGY TERM SYNONYM (MESH:xxx OR OMIM:xxx) 12	DB Object Type 13
 * Taxon(|taxon) 14	Date 15	Assigned By 16	Annotation Extension (not used at
 * RGD) 17	Gene Product Form ID (not used at RGD)
 * 
* ftp://rgd.mcw.edu/pub/data_release/annotated_rgd_objects_by_ontology/homo_genes_rdo
 *
 */
public class RGD implements GeneDiseaseParser {

    private FileManager fm;
    private Serialize ser;
    private String filenames;
    private Set<Pair> CuratedAssociations;
    private double scoreThresh;
    private File filepath;
    private final static Logger LOGGER = Logger.getLogger(RGD.class.getName());
    private Map<String, String> alreadyMapped;
    private OMIM2MeSH OMIM2MESH;
    private int unmappable = 0;

    public static void main(String[] args) {
        RGD dis = new RGD();
        dis.Parse();
    }

    public RGD() {

        this.ser = new Serialize();
        this.filenames = "RGD";
        this.CuratedAssociations = new HashSet<Pair>();
        this.fm = new FileManager();
        this.filepath = fm.getRGDFile();
        this.alreadyMapped = new HashMap<>();
        this.OMIM2MESH = new OMIM2MeSH();

    }

    @Override
    public void Parse() {
        try {
            LOGGER.log(Level.INFO, "Parsing RGD file {0} ....", new Object[]{filepath});

            Set<String> types = new HashSet<String>();
            BufferedReader br = null;
            BufferedWriter bw = null;

            // In this file, we kept the associations that were not labeled as “resistance”, “induced”, or “no association”, nor the ones annotated with the following evidence codes “Inferred from electronic annotation”, “Inferred from sequence or structural similarity” and “Non-traceable author statement”.

            Set<String> notWantedQualifier = new HashSet<String>() {
                {
                    add("resistance");
                    add("induced");
                    add("no association");
                }
            };

            Set<String> notWantedEvidence = new HashSet<String>() {
                {
                    add("IEA"); //“Inferred from electronic annotation”
                    add("ISS"); //“Inferred from sequence or structural similarity”
                    add("NAS"); //“Non-traceable author statement”
                }
            };

            /*
             * Evidence code definitions are (http://rgd.mcw.edu/wg/help3/genes/rgd-gene-report-pages/evidence-codes-guide):
             * 
             * EXP: Inferred by EXPeriment
             IAGP: Inferred by Association of Genotype with Phenotype
             IDA: Inferred from Direct Assay
             IED: Inferred from Experimental Data
             IEP: Inferred from Expression Pattern
             IGI: Inferred from Genetic Interaction
             IMP: Inferred from Mutant Phenotype
             IPI: Inferred from Physical Interaction
             IPM: Inferred from Phenotype Manipulation
             */

            /**
             * #1 RGD_ID unique RGD_ID of the annotated object #2 OBJECT_SYMBOL
             * official symbol of the annotated object #3 OBJECT_NAME official
             * name of the annotated object #4 OBJECT_TYPE annotated object data
             * type: one of ['gene','qtl','strain'] #5 TERM_ACC_ID ontology term
             * accession id #6 TERM_NAME ontology term name #7 QUALIFIER
             * optional qualifier #8 EVIDENCE evidence #9 WITH with info #10
             * ASPECT aspect #11 REFERENCES db references (Reference
             * RGDID|PUBMED ID) #12 CREATED_DATE created date #13 ASSIGNED_BY
             * assigned by #14 MESH_OMIM_ID MESH:xxx or OMIM:xxx id
             * corresponding to RDO:xxx id found in TERM_ACC_ID column (RGD/CTD
             * Disease Ontology annotations only) #15 CURATION_NOTES curation
             * notes provided by RGD curators #16 ORIGINAL_REFERENCE original
             * reference VERSION WITH THE FILE TOLD BY DisGeNET- NOT THE SAME
             * FILE TOLD TO USE BY RGD THEMSELVES!!
             */
            String SPECIES = null;
            String CANDIDATE_GENE_RGD_IDS = null;
            double P_VALUE = 0.0;
            String ASSOCIATED_DISEASES = null;
            String PHENOTYPES = null;
            String OMIM_ID = null;
            //variables in file
            String line;
            try {
                br = new BufferedReader(new FileReader(filepath));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(RGD.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                bw = new BufferedWriter(new FileWriter(new File(fm.getStandardisedFolder().getPath() + fm.getFileSeparator() + filenames + ".txt")));
            } catch (IOException ex) {
                Logger.getLogger(RGD.class.getName()).log(Level.SEVERE, null, ex);
            }


            String OBJECT_SYMBOL = null;
            String OBJECT_TYPE = null;
            String QUALIFIER = null;
            String EVIDENCE = null;
            String MESH_OMIM_ID = null;


            boolean skipFirst = true;
            while ((line = br.readLine()) != null) {
                //do something
                if (!line.startsWith("#") && !line.isEmpty()) {
                    //System.out.println(line);
                    if (skipFirst) {
                        skipFirst = false;
                    } else {
                        String[] values = line.split("\\t", -1); // don't truncate empty fields
                        OBJECT_SYMBOL = values[1];
                        OBJECT_TYPE = values[3];
                        QUALIFIER = values[6];
                        EVIDENCE = values[7];
                        MESH_OMIM_ID = values[13];

                        if (OBJECT_TYPE.equals("gene") && !MESH_OMIM_ID.isEmpty() && !notWantedEvidence.contains(EVIDENCE) && !notWantedQualifier.contains(QUALIFIER.toLowerCase())) {
                            if (MESH_OMIM_ID.startsWith("OMIM")) {
                                LOGGER.info("You have not accounted for OMIM disease ids");
                            }
                            CuratedAssociations.add(new Pair(OBJECT_SYMBOL.toUpperCase(), MESH_OMIM_ID.substring(5)));
                            //System.out.println(OBJECT_SYMBOL.toUpperCase() +"\t"+ MESH_OMIM_ID.substring(5));
                        }
                    }
                }
            }

            for (Pair p : CuratedAssociations) {
                bw.append(p.getEntrezGeneSymbol() + "\t" + p.getMeSHUI() + "\n");
            }

            br.close();
            bw.close();

            LOGGER.log(Level.INFO,
                    "Parsed {0} RGD relations", CuratedAssociations.size());
            LOGGER.log(Level.INFO,
                    "{0} associations contained disease terms unmappable to MeSH terms", unmappable);

            ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() + fm.getFileSeparator() + filenames + ".ser");
        } catch (IOException ex) {
            Logger.getLogger(RGD.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        logInfo();

    }

    @Override
    public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(CuratedAssociations.size() + unmappable).append("\t").append(dateFormat.format(date)).append("\n");

        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true)));
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
