/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.OMIM2MeSH;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author joe
 *
 * Please note that the file used during this parser is created by the
 * UniProtSwissProtXML parser in the ssip project.
 *
 *
 */
public class UniProt implements GeneDiseaseParser {

    private FileManager fm;
    private Serialize ser;
    private File UniProtFile;
    private String filenames;
    private Set<Pair> CuratedAssociations;
    private final static Logger LOGGER = Logger.getLogger(UniProt.class.getName());
    private int unmappable = 0;
    private int malformed = 0;
    private BufferedWriter bw = null;
    private OMIM2MeSH OMIM2MESH;

    public static void main(String[] args) {
        UniProt uni = new UniProt();
        uni.Parse();
    }

    public UniProt() {
        this.fm = new FileManager();
        this.ser = new Serialize();
        this.UniProtFile = fm.getUniGeneDiseases();
        this.filenames = "UniProt";
        this.OMIM2MESH = new OMIM2MeSH();
        this.CuratedAssociations = new HashSet<Pair>();

    }

    @Override
    public void Parse() {
        try {


            bw = new BufferedWriter(new FileWriter(fm.getStandardisedFolder().getPath() + fm.getFileSeparator() + filenames + ".txt"));

            BufferedReader br = null;
            String thisLine;//        backend.commitTransaction();
            boolean firstDone = false;
            //Open the file for reading
            br = new BufferedReader(new FileReader(UniProtFile));

            //variables
            while ((thisLine = br.readLine()) != null) {
                extractRelation(thisLine);
            }

            bw.close();

            ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() + fm.getFileSeparator() + filenames + ".ser");
            LOGGER.log(Level.INFO, "{0} associations are mapped to MeSH", CuratedAssociations.size());
            LOGGER.log(Level.INFO, "[{0}] gene>>disease associations were unmappable [OMIMdisID->MeSHID]", unmappable);
        } catch (IOException ex) {
            Logger.getLogger(UniProt.class.getName()).log(Level.SEVERE, null, ex);
        }
        logInfo();

    }

    @Override
    public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(CuratedAssociations.size() + unmappable).append("\t").append(dateFormat.format(date)).append("\n");

        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true)));
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void extractRelation(String line) {
        String[] split = line.split("\t");

        String ENTREZSYMBOL = split[0];
        String OMIMdiseaseID = split[1];
        String DISSOURCE = split[2];
        String MESHID = null;


        if (DISSOURCE.equals("MIM")) {
            if (!OMIM2MESH.getMeSHTerm(OMIMdiseaseID).equals("NO_MeSH_TERM")) {
                MESHID = OMIM2MESH.getMeSHTerm(OMIMdiseaseID);
            }
            if (!isInteger(OMIMdiseaseID)) {
                malformed++;
            } else if (MESHID == null) {
                unmappable++;
                LOGGER.log(Level.INFO, "No map between OMIM diseaseID and MeSH diseaseID: {0}", line);
            } else {
                try {
                    bw.append(ENTREZSYMBOL + "\t" + MESHID + "\n");
                    System.out.println(ENTREZSYMBOL + "\t" + MESHID);
                    CuratedAssociations.add(new Pair(ENTREZSYMBOL, MESHID));
                } catch (IOException ex) {
                    Logger.getLogger(OMIM.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            System.out.println("ERRRRR NAAAAAA " + line);

        }
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}
