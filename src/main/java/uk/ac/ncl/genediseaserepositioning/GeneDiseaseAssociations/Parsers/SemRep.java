/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.Metathesaurus;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes the SemRep associations extracted from the SQL database in the
 * format
 *
 * "geneCUI"+"geneTYPE"+"genePREFERRED_NAME"+"geneSEMTYPE"+"disCUI"+"disTYPE"+"disSEMTYPE+"disPREFERRED_NAME"
 *
 * Diseases are mapped to MeSH terms.
 *
 */
public class SemRep implements GeneDiseaseParser {

    private FileManager fm;
    private File semRepFile;
    private Serialize ser;
    private String filenames;
    private HashSet<Pair> CuratedAssociations;
    private HashSet<Pair> Unmappable;
    private HashMap<String, String> alreadyMappedUIs;
    private final static Logger LOGGER = Logger.getLogger(SemRep.class.getName());
    private Metathesaurus met;

    public static void main(String[] args) {
        SemRep sem = new SemRep();
        sem.Parse();
    }

    public SemRep() {
        this.fm = new FileManager();
        this.semRepFile = fm.getSemRepFile();
        this.ser = new Serialize();
        this.filenames = "SemRep";
        this.CuratedAssociations = new HashSet<Pair>();
        this.Unmappable = new HashSet<Pair>();
        this.alreadyMappedUIs = new HashMap<>();
        this.met = new Metathesaurus();
    }

    @Override
    public void Parse() {
        int notIn = 0;
        int SemRep = 0;
        /**
         * Fields: #
         * "geneCUI"+"geneTYPE"+"genePREFERRED_NAME"+"geneSEMTYPE"+"disCUI"+"disTYPE"+"disSEMTYPE+"disPREFERRED_NAME"
         */
        String geneCUI = null;
        String geneTYPE = null;
        String genePREFERRED_NAME = null;
        String geneSEMTYPE = null;
        String disCUI = null;
        String disTYPE = null;
        String disSEMTYPE = null;
        String disPREFERRED_NAME = null;

        BufferedReader br = null;
        BufferedWriter bw = null;
        String thisLine;
        try {
            br = new BufferedReader(new FileReader(semRepFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SemRep.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bw = new BufferedWriter(new FileWriter(new File(fm.getStandardisedFolder().getPath() + fm.getFileSeparator() + filenames + ".txt")));
        } catch (IOException ex) {
            Logger.getLogger(SemRep.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while ((thisLine = br.readLine()) != null) {
                String[] values = thisLine.split("\\t", -1);
                geneCUI = values[0];
                geneTYPE = values[1];
                genePREFERRED_NAME = values[2];
                geneSEMTYPE = values[3];
                disCUI = values[4];
                disTYPE = values[5];
                disSEMTYPE = values[6];
                disPREFERRED_NAME = values[7];
                //get the mesh id for the disease UMLS
                String MeSH = met.getMeSHfromUMLS(disCUI);
                if (MeSH != null) {
                    CuratedAssociations.add(new Pair(genePREFERRED_NAME, MeSH));
                    System.out.println(genePREFERRED_NAME + "\t" + MeSH);
                    SemRep++;
                } else {
                    Unmappable.add(new Pair(genePREFERRED_NAME, disCUI));
                    notIn++;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(SemRep.class.getName()).log(Level.SEVERE, null, ex);
        }


        for (Pair p : CuratedAssociations) {
            try {
                bw.append(p.getEntrezGeneSymbol() + "\t" + p.getMeSHUI() + "\n");

            } catch (IOException ex) {
                Logger.getLogger(SemRep.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(SemRep.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Checked {0}", (SemRep + notIn));
        LOGGER.log(Level.INFO, "Associations {0} map to MeSH", CuratedAssociations.size());
        LOGGER.log(Level.INFO, "Associations {0} do NOT map to MeSH", Unmappable.size());
        ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() + fm.getFileSeparator() + filenames + ".ser");
        logInfo();

    }

    @Override
    public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(CuratedAssociations.size() + Unmappable.size()).append("\t").append(dateFormat.format(date)).append("\n");

        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true)));
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
