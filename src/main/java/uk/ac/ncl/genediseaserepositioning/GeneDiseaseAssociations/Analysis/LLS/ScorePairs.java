/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.LLS;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 *
 * Class goes through each association that is provided by all the datasets and
 * scores them based on the LLS score calculated for each dataset.
 *
 *
 */
public class ScorePairs {

    private BufferedWriter bw;
    private DataManager dm;
    private FileManager fm;
    private Map<String, Double> datasetScores;
    private Map<Pair, Double> scoredPairs;
    private Map<Pair, Integer> numberOfSourcesPairs;
    private double dValue;
    private final static Logger LOGGER = Logger.getLogger(ScorePairs.class.getName());
    private HashMap<String, Set<Pair>> allPairs;
    private File datasetScoresFile;

    /**
     * Empty constructor.
     */
    public ScorePairs() {
    }

    /**
     *
     * Constructor.
     *
     * @param dm
     * @param datasetScores
     * @param dValue
     */
    public ScorePairs(DataManager dm, Map<String, Double> datasetScores, Double dValue) {

        this.dm = dm;
        this.datasetScores = datasetScores;
        this.dValue = dValue;
        this.allPairs = dm.getAllSources();
        this.scoredPairs = new HashMap<Pair, Double>();
        this.numberOfSourcesPairs = new HashMap<Pair, Integer>();
        this.fm = new FileManager();
    }

    /**
     *
     * Constructor.
     *
     * @param dm
     * @param datasetScores
     * @param dValue
     */
    public ScorePairs(DataManager dm, File datasetScores, Double dValue) {

        this.dm = dm;
        this.datasetScoresFile = datasetScores;
        this.datasetScores = new HashMap<String, Double>();
        this.dValue = dValue;
        this.allPairs = dm.getAllSources();
        this.scoredPairs = new HashMap<Pair, Double>();
        this.numberOfSourcesPairs = new HashMap<Pair, Integer>();
        this.fm = new FileManager();
        getLLSScoresFromFile();

    }

    public void getLLSScoresFromFile() {

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(datasetScoresFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScorePairs.class.getName()).log(Level.SEVERE, null, ex);
        }
        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                datasetScores.put(thisLine.split("\t")[0], Double.parseDouble(thisLine.split("\t")[1]));
            }
        } catch (IOException ex) {
            Logger.getLogger(ScorePairs.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Retrieved LLS scores from ...{0}", datasetScoresFile.getAbsolutePath());
    }

    /**
     * Go score all associations captured by all datasets.
     */
    public void goScore() {

        LOGGER.info("Sorting datasources...");
        HashMap<String, Double> sorted = (HashMap<String, Double>) sortByValue(datasetScores);

        //order the datasets with the highest scoring dataset to be used first
        int i = 1;
        for (String dataSet
                : sorted.keySet()) {//iterate through the datasets
            LOGGER.log(Level.INFO, "Scoring associations from {0}", dataSet);
            Set<Pair> pairs = allPairs.get(dataSet);
            for (Pair pair : pairs) {
                scorePair(pair, dataSet);
            }
            i++;
        }
        writeToFile();
    }

    /**
     * Score an individual pair for that dataset only- using the equation
     * described by Lee et al.
     *
     * @param pair
     * @param dataset
     * @param number
     */
    public void scorePair(Pair pair, String dataset) {

        boolean print = false;
        if (pair.getMeSHUI().equals("D010505") && pair.getEntrezGeneSymbol().equals("MEFV")) {
            print = true;
        }
        int scored = 1;
        if (numberOfSourcesPairs.containsKey(pair)) {
            scored = numberOfSourcesPairs.get(pair);
        }
        //this is where we get the score for the pair as well as the add it to the data structure
        double datasetScore_Li = datasetScores.get(dataset);
        double pairScore_WS = 0.0;

        if (scoredPairs.containsKey(pair)) {
            pairScore_WS = scoredPairs.get(pair);
        }
        double localDValue_D = Math.pow(dValue, (scored - 1));
        double scoreToAdd_WS = datasetScore_Li / localDValue_D;
        pairScore_WS += scoreToAdd_WS;
        if(print){
            System.out.println("\n"+dataset+"\t"+datasetScore_Li+"\t"+(scored - 1)+"\t"+localDValue_D +"\t"+scoreToAdd_WS+"\n");
        }
        //put the pair and the score back in the hashmap
        scoredPairs.put(pair, pairScore_WS);
        numberOfSourcesPairs.put(pair, (scored + 1));
    }

    /**
     * Write the scored associations to file.
     */
    public void writeToFile() {
        String outPutFile = fm.getScoredAssociationFolder() + fm.getFileSeparator() + "scored_Associations_" + dValue + "_" + dm.getGoldStandard().getGSName() + ".txt";
        //can we order the gene-disease associations somehow??
        try {
            this.bw = new BufferedWriter(new FileWriter(outPutFile));
        } catch (IOException ex) {
            Logger.getLogger(ScorePairs.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.info("Sorting scored pairs...");
        HashMap<Pair, Double> sortedPairs = (HashMap<Pair, Double>) sortByValue(scoredPairs);

        for (Pair p : sortedPairs.keySet()) {
            try {
                bw.append(p.getEntrezGeneSymbol() + "\t" + p.getMeSHUI() + "\t" + scoredPairs.get(p) + "\n");
            } catch (IOException ex) {
                Logger.getLogger(ScorePairs.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(ScorePairs.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Succesfully scored {0} gene-disease associations and output to {1}, using a D_Value of {2}", new Object[]{sortedPairs.size(), outPutFile, dValue});

    }

    /**
     * Allows for a hashmap to be ordered on the value. Used when we order the
     * datasets by their LLS score.
     *
     * @param <K>
     * @param <V>
     * @param map
     * @return
     */
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list =
                new LinkedList<>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
