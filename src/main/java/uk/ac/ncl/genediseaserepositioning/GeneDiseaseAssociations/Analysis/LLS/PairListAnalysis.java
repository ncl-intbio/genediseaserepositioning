package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.LLS;


import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.util.*;
/*----------------------------------------------------------------*\
|                   Class PairListAnalysis.java    	           |
|         Class to analyse and compare pair lists                  |
|                                                                  |
|                     Author: Olly Shaw                            |
|                   Author: Katherine James                        |
|                     Commenced: 07/08/07                          |
|                    Last edited: 16/03/09                         |
\*----------------------------------------------------------------*/

public class PairListAnalysis {

    public PairListAnalysis() {
    }

    //*********Method to count the number of unique genes in a list************
    public int uniqueGenesInList(Set<Pair> list) {
        List<String> uniqueGenes = new ArrayList<String>();//the unique genes
        String gene;
        for (Pair pair : list) {
            gene = pair.getEntrezGeneSymbol();
            if (!uniqueGenes.contains(gene))//check the list
            {
                uniqueGenes.add(gene);
            }
        }
        return uniqueGenes.size();//the final list size
    }
    //END OF UNIQUEGENESINLIST METHOD
    
     //*********Method to count the number of unique diseases in a list************
    public int uniqueDiseasesInList(Set<Pair> list) {
        List<String> uniqueDiseases = new ArrayList<String>();//the unique genes
        String disease;
        for (Pair pair : list) {
            disease = pair.getMeSHUI();

            if (!uniqueDiseases.contains(disease))//check the list
            {
                uniqueDiseases.add(disease);
            }

            if (!uniqueDiseases.contains(disease)) {
                uniqueDiseases.add(disease);
            }
        }
        return uniqueDiseases.size();//the final list size
    }
    //END OF UNIQUEGENESINLIST METHOD

    //**********Method to count the number of pairs present in the GS********
    public int pairsInGoldStandard(Set<Pair> goldList, Set<Pair> dataList) {

        int counter = 0;
        for (Pair pair : dataList)//the test list
        {
            if (goldList.contains(pair))//compare to the gold standard
            {
                counter++;
            }
        }
        return counter;
    }
    //END OF PAIRS IN GOLD STANDARD METHOD
}
