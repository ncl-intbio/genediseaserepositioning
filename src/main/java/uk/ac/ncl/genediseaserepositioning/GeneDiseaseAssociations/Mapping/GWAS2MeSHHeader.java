/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 *
 * Class extracts all MeSH tree terms from the D MeSH records and the
 * accompanying MeSHUI. Doesn't use the MeSH supplementary records [C] as these
 * don't come with MeSH tree terms.
 *
 */
public class GWAS2MeSHHeader {

    private final static Logger LOGGER = Logger.getLogger(GWAS2MeSHHeader.class.getName());
    private String meshFile;
    private Map<String, String> GWAS2MeSHHeader;
    private FileManager fm;

    public GWAS2MeSHHeader() {

        this.fm = new FileManager();
        this.meshFile = fm.getGWAS2MeSHHeaderFile();
        this.GWAS2MeSHHeader = new HashMap<String, String>();
        extractMeSHTreeTerms2UI();
       

    }

    public static void main(String[] args) {
        GWAS2MeSHHeader g = new GWAS2MeSHHeader();

    }

    public void extractMeSHTreeTerms2UI() {

        BufferedReader br = null;
        String thisLine;
        boolean firstDone = false;
        try {
            //Open the file for reading
            br = new BufferedReader(new FileReader(meshFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GWAS2MeSHHeader.class.getName()).log(Level.SEVERE, null, ex);
        }

        //variables
        String disease = null;
        String MSH_Final = null;
        String Category = null;
        String Prior_MSH = null;
        String Comments = null;


        try {
            while ((thisLine = br.readLine()) != null) {
                //skip first line
                br.readLine();
                String[] split = thisLine.split("\t");
                disease = split[0];
                if (split.length > 1) {
                    MSH_Final = split[1];
                    GWAS2MeSHHeader.put(disease.toLowerCase(), MSH_Final.toLowerCase().replace("\"", ""));
                }
                if (split.length > 2) {
                    Category = split[2];
                }
                if (split.length > 3) {
                    Prior_MSH = split[3];
                }
                if (split.length > 4) {
                    Comments = split[4];
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GWAS2MeSHHeader.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, "Successfully extracted mappings between {0} GWAS diseases and MeSH headers.", GWAS2MeSHHeader.size());
    }

    /**
     * Method returns the MeSH header from a corresponding GWAS trait.
     *
     */
    public String getMeshHeader(String GWAStrait) {

        if (GWAS2MeSHHeader.containsKey(GWAStrait.toLowerCase())) {
            return GWAS2MeSHHeader.get(GWAStrait.toLowerCase());
        } else {
            return "NO_MESH_HEADER";
        }
    }
}
