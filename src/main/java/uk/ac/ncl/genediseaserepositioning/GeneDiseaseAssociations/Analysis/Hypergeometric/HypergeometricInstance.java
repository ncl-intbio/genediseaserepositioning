/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.Hypergeometric;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Holds all information needed for a point in the hypergeometric plot.
 *
 */
public class HypergeometricInstance {

    //number of interaction ins the sample that are known to be correct
    private int k;
    //sample size
    private int N;
    //number of interactions in the dataset that are NOT correct 
    private int m;
    //number of interactions in the dataset that are correct 
    private int n;
    //the score on the x axis
    private double xScore;

    public static void main(String[] args) {
        HypergeometricInstance hyp = new HypergeometricInstance(10, 5445, 355000, 100, 56.0);
        System.out.println(hyp.getPoints());
    }

    /**
     * Constructor
     *
     * @param k
     * @param N
     * @param m
     * @param n
     * @param xscore
     */
    public HypergeometricInstance(int k, int N, int m, int n, double xscore) {
        this.k = k;
        this.N = N;
        this.m = m;
        this.n = n;
        this.xScore = xscore;
    }

    /**
     * Return the x and y values to plot
     *
     * @return
     */
    public String getPoints() {
        return Double.toString(k) + '\t' + N + '\t' + m + '\t' + n + '\t' + xScore;
    }
}
