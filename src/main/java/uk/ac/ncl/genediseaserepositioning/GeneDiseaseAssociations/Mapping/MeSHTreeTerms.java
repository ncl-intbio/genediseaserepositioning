/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class gets all the tree terms associated with a MeSH term.
 *
 */
public class MeSHTreeTerms implements java.io.Serializable{
    private static final long serialVersionUID = 1L;

    private HashMap<String, Set<String>> allTerms;
    private File MeSHC;
    private File MeSHD;
    private Map<String, Set<String>> headerterms2MeshTree;
    private Map<String, String> headerTerm2MeSHUI;
    private int MAXDEPTH;
    private final static Logger LOGGER = Logger.getLogger(MeSHTreeTerms.class.getName());
    private FileManager fm;

    public static void main(String args[]) {

//        String meshD = "/Volumes/MyPassport/Datasets/MeSH/d2015.bin";
//        String mesC = "/Volumes/MyPassport/Datasets/MeSH/c2015.bin";
//        MeSHTreeTerms mesh = new MeSHTreeTerms(meshD, mesC);
//        mesh.parseTerms();
//        System.out.println(mesh.getMeSHUIFromHeader("Pulmonary Disease, Chronic Obstructive"));

    }

    public MeSHTreeTerms() {
        this.fm = new FileManager();
        this.MeSHD = fm.getMeSHDFile();
        this.MeSHC = fm.getMeSHCFile();
        this.headerterms2MeshTree = new HashMap<String, Set<String>>();
        this.headerTerm2MeSHUI = new HashMap<String,String>();
        this.allTerms = new HashMap<String, Set<String>>();
    }

    
    public HashMap<String, Set<String>> getAllTerms(){
        return allTerms;     
    }
    
    
    public void parseTerms() {
        getDRecord();
        getCRecord();
        LOGGER.log(Level.INFO, "Added total {0} MeSH DISEASE records with corresponding tree terms", allTerms.size());
    }

    public Set<String> getTreeTerms(String mesh) {
        if (allTerms.containsKey(mesh)) {
            return allTerms.get(mesh);
        } else {
            return null;
        }
    }

    
    public String getMeSHUIFromHeader(String name) {
        if (headerTerm2MeSHUI.containsKey(name)) {
            return headerTerm2MeSHUI.get(name);
        } else {
            return null;
        }
    }
    
    public int getMaxDepth() {
        return MAXDEPTH;
    }

    public String getDRecord() {

        int count = 0;
        String longest = "";
        BufferedReader br = null;
        String thisLine;
        boolean firstDone = false;

        try {
            //Open the file for reading
            br = new BufferedReader(new FileReader(MeSHD));
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        //variables
        String MeSHMainHeader = null;
        String MeSHUI = null;
        Set<String> MeSHTreeTerms = new HashSet<String>();
        try {
            while ((thisLine = br.readLine()) != null) {
                if (thisLine.equals("*NEWRECORD")) {
                    if (firstDone) {
                        int c = 1;
                        boolean disease = false;
                        for (String tt : MeSHTreeTerms) {
                            if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                disease = true;
                            }
                        }
                        //if the term is a disease then add it
                        if (disease) {
                            Set<String> treeterms = new HashSet<String>();
                            for (String tt : MeSHTreeTerms) {
                                if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                    String[] split = tt.split("\\.");
                                    if ((split.length - 1) > MAXDEPTH) {
                                        MAXDEPTH = split.length - 1;
                                        longest = tt;
                                    }
                                    treeterms.add(tt);
                                    c++;
                                }
                            }
                            headerterms2MeshTree.put(MeSHMainHeader, treeterms);
                            allTerms.put(MeSHUI, MeSHTreeTerms);
                            count++;
                        }
                        
                        headerTerm2MeSHUI.put(MeSHMainHeader, MeSHUI);
                        MeSHTreeTerms = new HashSet<String>();
                        MeSHMainHeader = null;
                        MeSHUI = null;
                        
                    }
                }
                if (thisLine.startsWith("UI =")) {
                    MeSHUI = split(thisLine);
                    firstDone = true;
                }
                if (thisLine.startsWith("MH =")) {
                    MeSHMainHeader = split(thisLine);
                }
                if (thisLine.startsWith("MN =")) {
                    MeSHTreeTerms.add(split(thisLine));
                }
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Added {0} MeSH DISEASE records with corresponding tree terms", count);
        LOGGER.log(Level.INFO, "{0}\t{1}\t{2}", new Object[]{"MaxDepth", MAXDEPTH, longest});

        return null;
    }

    public String getCRecord() {

        BufferedReader br = null;
        String thisLine;
        String longest = "";
        boolean firstDone = false;
        boolean disease = false;
        int count = 0;
        try {
            //Open the file for reading
            br = new BufferedReader(new FileReader(MeSHC));
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        //variables
        String MeSHMainHeader = null;
        String MeSHUI = null;
        //could be more than one
        Set<String> headingMappedTo = new HashSet<String>();
        Set<String> MeSHTreeTerms = new HashSet<String>();
        boolean add = false;
        try {
            while ((thisLine = br.readLine()) != null) {
                //create new node and reset the variables
                if (thisLine.startsWith("NM =")) {
                    MeSHMainHeader = split(thisLine);
                }
                if (thisLine.startsWith("HM =")) {
                    headingMappedTo.add(split(thisLine));
                }
                if (thisLine.startsWith("UI =")) {
                               
                    for (String hm : headingMappedTo) {

                        if (headerterms2MeshTree.containsKey(hm)) {
                            Set<String> treeterms = headerterms2MeshTree.get(hm);
                            for (String tt : treeterms) {
                                if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                    String newTerm = tt + ".supp";
                                    String[] split = newTerm.split("\\.");
                                    if ((split.length - 1) > MAXDEPTH) {
                                        MAXDEPTH = split.length - 1;
                                        longest = newTerm;
                                    }
                                    MeSHTreeTerms.add(newTerm);
                                    disease = true;
                                }
                            }
                        }
                    }

                    if (disease) {
                        headerTerm2MeSHUI.put(MeSHMainHeader, MeSHUI);
                        allTerms.put(MeSHUI, MeSHTreeTerms);
                        count++;
                    }
                    MeSHUI = split(thisLine);
                    firstDone = true;
                    MeSHTreeTerms = new HashSet<String>();
                    headingMappedTo = new HashSet<String>();
                    disease = false;
                }
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, "Added {0} supplementary MeSH DISEASE records with corresponding tree terms", count);
        LOGGER.log(Level.INFO, "{0}\t{1}\t{2}", new Object[]{"MaxDepth", MAXDEPTH, longest});
        return null;
    }

    /**
     * Splits the MeSH line (aaaa = bbbbb) on the "=" and returns the second
     * element.
     *
     * @return
     */
    public String split(String meshLine) {

        String[] split = meshLine.split("=");
        return split[1].trim();

    }
}
