/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.ExtractAssociated;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes a HUGO disease id and returns all MeSH UI diseases that are
 * associated to that disease.
 */
public class ExtractAssociatedDiseases {

    private DataManager dm;
    private FileManager fm;

    public static void main(String[] args) {
    }

    public ExtractAssociatedDiseases(DataManager dm) {
        this.dm = dm;
        this.fm = new FileManager();
    }

    public void goSearch(Set<String> geneids) {
        for (String gene : geneids) {
            goSearch(gene);
        }

    }

    public void goSearch(String HUGO) {
        HashMap<String, Set<String>> datasources = new HashMap<>();
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(fm.getGDQuery().getPath()+HUGO + ".txt"));
        } catch (IOException ex) {
            Logger.getLogger(ExtractAssociatedGenes.class.getName()).log(Level.SEVERE, null, ex);
        }
        HashMap<String, Set<Pair>> associations = dm.getAllSources();
        for (String source : associations.keySet()) {
            Set<Pair> pairs = associations.get(source);
            for (Pair p : pairs) {
                if (p.getEntrezGeneSymbol().equals(HUGO)) {
                    if (datasources.containsKey(p.getMeSHUI())) {
                        Set<String> sources = datasources.get(p.getMeSHUI());
                        sources.add(source);
                        datasources.put(p.getMeSHUI(), sources);
                    } else {
                        Set<String> sources = new HashSet<String>();
                        sources.add(source);
                        datasources.put(p.getMeSHUI(), sources);
                    }
                }
            }

        }

        for (String disease : datasources.keySet()) {
            try {
                bw.append(disease + "\t" + datasources.get(disease).size() + "\t" + datasources.get(disease).toString() + "\n");
            } catch (IOException ex) {
                Logger.getLogger(ExtractAssociatedGenes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(ExtractAssociatedGenes.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
