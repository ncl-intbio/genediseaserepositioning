/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.Metathesaurus;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Downloaded from: http://www.disgenet.org/web/DisGeNET/v2.1/dbinfo
 *
 * BeFree genes:13402 diseases:10557 associations:330888
 *
 *
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
public class BeFree implements GeneDiseaseParser {

    private FileManager fm;
    private Serialize ser;
    private String filenames;
    private Set<Pair> CuratedAssociations;
    private String line;
    private Set<Pair> unsuccesful;
    private Map<String, List<String>> UMLSrels;
    private Map<String, List<String>> OMIMSrels;
    private Map<String, List<String>> ALLrels;
    private double scoreThresh;
    private Set<String> unmapped;
    private File filepath;
    private final static Logger LOGGER = Logger.getLogger(BeFree.class.getName());
    private Map<String, String> alreadyMapped;
    private Metathesaurus met;

    public static void main(String[] args) {

        BeFree dis = new BeFree(0.7);
        dis.Parse();

    }

    public BeFree(double score) {
        this.met = new Metathesaurus();
        this.ser = new Serialize();
        this.scoreThresh = score;
        this.filenames = "BeFree";
        this.CuratedAssociations = new HashSet<Pair>();
        this.fm = new FileManager();
        this.filepath = fm.getBeFreeFile();
        this.alreadyMapped = new HashMap<>();
        this.ALLrels = new HashMap<>();
        this.OMIMSrels = new HashMap<>();
        this.UMLSrels = new HashMap<>();
        this.unmapped = new HashSet<String>();
        this.unsuccesful = new HashSet<Pair>();
    }

    @Override
    public void Parse() {

        LOGGER.log(Level.INFO, "Parsing BeFree file {0} with a threshold of {1}", new Object[]{filepath, scoreThresh});

        Set<String> types = new HashSet<String>();
        BufferedReader br = null;
        BufferedWriter bw = null;
        int test = 50;
        int count = 0;

        //variables in file
        String geneId = null;
        String geneSymbol = null;
        String geneName = null;
        String diseaseId = null;
        String diseaseName = null;
        double score = 0.000000000;
        int NumberOfPubmeds = 0;
        Set<String> associationTypes = new HashSet<String>();
        String source = null;

        String previousLine = "FIRST";
        String line;
        try {
            br = new BufferedReader(new FileReader(filepath));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bw = new BufferedWriter(new FileWriter(new File(fm.getStandardisedFolder().getPath() + fm.getFileSeparator() + filenames + ".txt")));
        } catch (IOException ex) {
            Logger.getLogger(CTD.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            //skip the first line
            br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((line = br.readLine()) != null) {
                count++;
                String[] split = line.split("\t");
                geneId = split[0];
                geneSymbol = split[1];
                geneName = split[2];
                diseaseId = split[3];
                if (!(geneId + diseaseId).equals(previousLine)) {
                    previousLine = geneId + diseaseId;
                    if (diseaseId.startsWith("umls:")) {
                        diseaseId = diseaseId.substring(5);
                    } else {
                        LOGGER.log(Level.INFO, "This is not a disease UMLS: {0}", diseaseId);
                    }
                    diseaseName = split[4];
                    score = Double.parseDouble(split[5]);
                    NumberOfPubmeds = Integer.parseInt(split[6]);
                    associationTypes.add(split[7]);
                    types.add(split[7]);
                    source = split[8];
                    //map the diseases to mesh if we can 
                    String disTest = met.getMeSHfromUMLS(diseaseId);
                    Pair p = new Pair(geneSymbol, disTest);
                    if (disTest != null && score >= scoreThresh) {
                        alreadyMapped.put(diseaseId, disTest);
                        diseaseId = disTest;
                        CuratedAssociations.add(new Pair(geneSymbol, disTest));
                        System.out.println(geneSymbol + "\t" + disTest);

                    } else {
                        p = new Pair(geneSymbol, diseaseId);
                        unmapped.add(diseaseId);
                        unsuccesful.add(new Pair(geneSymbol, diseaseId));
                    }

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Pair p
                : CuratedAssociations) {
            try {
                bw.append(p.getEntrezGeneSymbol() + "\t" + p.getMeSHUI() + "\n");
            } catch (IOException ex) {
                Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        try {
            br.close();
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO,
                "Parsed {0} BeFree relations", CuratedAssociations.size());
        LOGGER.log(Level.INFO,
                "{0} relations have a UMLS disease as disease unmappable to MeSH terms", unsuccesful.size());
        LOGGER.log(Level.INFO,
                "{0} disease terms unmappable to MeSH terms", unmapped.size());
        LOGGER.log(Level.INFO,
                "{0} disease terms mapped to MeSH terms", alreadyMapped.size());
        ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() + fm.getFileSeparator() + filenames + ".ser");
        
        logInfo();
    }
    
     public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(CuratedAssociations.size() + unmapped.size()).append("\t").append(dateFormat.format(date)).append("\n");
        
        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true))); 
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
