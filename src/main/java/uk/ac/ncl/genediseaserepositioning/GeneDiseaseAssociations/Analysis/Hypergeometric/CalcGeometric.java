/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.Hypergeometric;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.GoldStandard;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager.DataSource;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes a file of scored associations and returns a plottable
 * hypergeometric set of values.
 */
public class CalcGeometric {

    private int sampleSize;
    private int sampleOverlap;
    private Set goldStandard;
    private String scoredAssociationsFile;
    private String outPutHyper;
    private String outPutDistribution;
    private int assFromPos;
    private int assToPos;
    private int assScorePos;
    private int truePositives;
    private int falsePositives;
    private final static Logger LOGGER = Logger.getLogger(CalcGeometric.class.getName());
    private BufferedWriter bwHyper;
    private BufferedWriter bwDistribution;
    private Map<Double, Set<Pair>> scoredAssociations;
    private List<Double> scores;
    private Set<Pair> sample;
    private double[] window;
    private int check;
    private FileManager fm;
    private double dValue;

    public static void main(String[] args) {
//        DataManager dm = new DataManager(DataSource.CTD);
//        FileManager fm = new FileManager();
//        GoldStandard gs = dm.getGoldStandard();
//        CalcGeometric calc = new CalcGeometric(30, 15, dm, fm.getScoredAssociationFolder() + "scored_Associations_3.0_BEFREE.txt", 0, 1, 2);
//        calc.getAllAssociations();
//        calc.getHyperValues();
//        calc.getDistributionValues();
    }

    public CalcGeometric(int slidingSize, int sampleOverlap, DataManager dm, String associations, int from, int to, int scorePosition, double dValue) {
        this.dValue = dValue;
        this.scoredAssociationsFile = associations;
        this.goldStandard = dm.getGoldStandard().getTruePositives();
        this.sampleSize = slidingSize;
        this.fm = new FileManager();
        this.sampleOverlap = sampleOverlap;
        String UID = UUID.randomUUID().toString().substring(0, 6);
        //this.outPutHyper = fm.getHyperFile().getPath() +fm.getFileSeparator()+ dm.getGoldStandard().getGSName() + "_" + dValue+"_"+ UID + ".txt";
        //this.outPutDistribution = fm.getScoredAssociationDistributionFolder().toString()+fm.getFileSeparator() + dm.getGoldStandard().getGSName() + "_" + dValue+"_"+ UID + ".txt";
        this.outPutHyper = fm.getHyperFile().getPath() +fm.getFileSeparator()+ dm.getGoldStandard().getGSName() + "_" + dValue+ ".txt";
        this.outPutDistribution = fm.getScoredAssociationDistributionFolder().toString()+fm.getFileSeparator() + dm.getGoldStandard().getGSName() + "_" + dValue+ ".txt";
        this.assFromPos = from;
        this.assToPos = to;
        this.assScorePos = scorePosition;
        this.scoredAssociations = new HashMap<Double, Set<Pair>>();
        this.sample = new HashSet<Pair>();
        this.window = new double[sampleSize];
        this.check = 0;

    }

    /**
     * First of all get all the true positives of the score associations
     */
    public void getAllAssociations() {
        LOGGER.info("Getting scored associations...");
        BufferedReader br = null;
        String thisLine;
        int count = 0;
        //variables
        String from;
        String to;
        double score;
        this.truePositives = 0;
        this.falsePositives = 0;
        try {
            br = new BufferedReader(new FileReader(scoredAssociationsFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CalcGeometric.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((thisLine = br.readLine()) != null) {
                String[] split = thisLine.split("\t");
                from = split[assFromPos];
                to = split[assToPos];
                score = Double.parseDouble(split[assScorePos]);
                count++;
                Pair pair = new Pair(from, to);

                if (goldStandard.contains(pair)) {
                    truePositives++;
                } else {
                    falsePositives++;
                }

                if (scoredAssociations.containsKey(score)) {
                    Set<Pair> pairs = scoredAssociations.get(score);
                    pairs.add(pair);
                    scoredAssociations.put(score, pairs);
                } else {
                    Set<Pair> pairs = new HashSet<Pair>();
                    pairs.add(pair);
                    scoredAssociations.put(score, pairs);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, "We have {0} different scored associations ...", count);
        LOGGER.log(Level.INFO, "We have {0} different scores for associations ...", scoredAssociations.keySet().size());

        Set<Double> allScores = scoredAssociations.keySet();
        List<Double> sortedScores = new ArrayList<>();
        for (double scored : allScores) {
            sortedScores.add(scored);
        }
        Collections.sort(sortedScores);
        this.scores = sortedScores;

    }

    /**
     * Export the values to allow for plotting in r
     */
    public void getHyperValues() {
        LOGGER.info("Calculating hyper values to plot...");

        try {
            bwHyper = new BufferedWriter(new FileWriter(outPutHyper));
        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < scores.size(); i++) {
            updateWindow(i, scores.get(i));
        }
        try {
            bwHyper.close();
        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Plot values for hyper have been stored in {0}", outPutHyper);
    }

    /**
     * Export the values ready to be plotted in a distribution graph.
     */
    public void getDistributionValues() {
        LOGGER.info("Calculating distribution values to plot...");

        try {
            bwDistribution = new BufferedWriter(new FileWriter(outPutDistribution));
        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.info("Disribution of scores if needed....");
        //int i = 0;
        for (Double scored : scores) {
            try {
                for (int i = 0; i < scoredAssociations.get(scored).size(); i++) {
                    bwDistribution.append(Double.toString(scored) + "\n");        
                }
               // bwDistribution.append(Double.toString(scored) + "\t" + Integer.toString(scoredAssociations.get(scored).size()) + "\n");
                //bwDistribution.append(Double.toString(scored) + "\t" + Integer.toString(scoredAssociations.get(scored).size())+"\n");
            } catch (IOException ex) {
                Logger.getLogger(CalcGeometric.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bwDistribution.close();
        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, "Plot values for distribution have been stored in {0}", outPutDistribution);
    }

    /**
     * Update the latest view of the window.
     *
     * @param pair
     * @param score
     * @param count
     */
    public void updateWindow(int count, double score) {
        if (count < sampleSize) {
            window[count] = score;
        } else {
            double[] window2 = new double[sampleSize];
            for (int i = 1; i < window.length; i++) {
                window2[i - 1] = window[i];
            }
            window2[sampleSize - 1] = score;
            window = window2;
            if ((check - sampleSize) % (sampleSize - sampleOverlap) == 0) {
                calculatePoint();
            }
        }
        check++;
    }

    /**
     * Calculate the points to be plotted.
     */
    public void calculatePoint() {

        //number of interaction ins the sample that are known to be correct
        int k = 0;
        double averageScore = 0.0;
        //get all the associations for the window
        Set<Pair> pairWindow = new HashSet<>();

        for (int i = 0; i < sampleSize; i++) {
            averageScore = averageScore + window[i];
            Set<Pair> localPair = scoredAssociations.get(window[i]);
            pairWindow.addAll(localPair);
        }

        //check for any mappings
        for (Pair p : pairWindow) {
            if (goldStandard.contains(p)) {
                k++;
            }
        }

        HypergeometricInstance hyper = new HypergeometricInstance(k, pairWindow.size(), falsePositives, truePositives, averageScore / sampleSize);
        try {
            bwHyper.append(hyper.getPoints() + "\n");
        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
