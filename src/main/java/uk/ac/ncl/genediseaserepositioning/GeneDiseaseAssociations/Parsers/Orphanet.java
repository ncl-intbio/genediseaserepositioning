/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.*;
import org.w3c.dom.NodeList;

/**
 *
 * @author joemullen
 *
 * This parser was developed to parse all interactions between rare diseases and
 * their associated genes from ORPHANET. Dataset is available for download from
 * http://www.orphadata.org/cgi-bin/inc/product6.inc.php
 *
 * <TODO>
 * Look at this in a bit more detail; are we parsing the data properly?
 *
 */
public class Orphanet implements GeneDiseaseParser {

    private FileManager fm;
    private Serialize ser;
    private File OrphanetFile;
    private String filenames;
    private Set<Pair> CuratedAssociations;
    private HashMap<String, String> Orphanet2MeSH;
    private Scanner sc;
    private String ELEMENT = "Disorder";
    private String NAMESPACE = "";
    private String characters;
    private BufferedWriter bw = null;
    private boolean element = false;
    private String GENEID = "UNIPROTKB/SWISSPROT";
    private final String DISEASE_XPATH = "/Disorder/OrphaNumber";
    private final String GENES_XPATH = "/Disorder/DisorderGeneAssociationList/DisorderGeneAssociation/Gene";
    private final String GENE_NAMES_EXREF_XPATH = GENES_XPATH + "/ExternalReferenceList/ExternalReference";
    private final String GENE_NAMES_EXSOURCE_XPATH = GENE_NAMES_EXREF_XPATH + "/Source";
    private HashMap<String, Set<String>> ORDO2MeSH;
    private int mapped = 0;
    private int unmappable = 0;
    private final static Logger LOGGER = Logger.getLogger(Orphanet.class.getName());

    public static void main(String[] args) {
        Orphanet orph = new Orphanet();
        orph.Parse();

    }

    public Orphanet() {
        this.fm = new FileManager();
        this.ser = new Serialize();
        this.OrphanetFile = fm.getOrphanetFile();
        this.ORDO2MeSH = ser.deserializeHashMapSet(fm.getSuppFolder() + fm.getFileSeparator() + "Orphanet_2_MeSH.ser");
        this.filenames = "Orphanet";
        this.CuratedAssociations = new HashSet<Pair>();
        this.Orphanet2MeSH = new HashMap<String, String>();
    }

    @Override
    public void Parse() {
        try {

            StringWriter buf = new StringWriter(1024);
            element = false;
            try {
                LOGGER.log(Level.INFO, "Parsing Orphanet disease-gene file {0}", OrphanetFile);
                bw = new BufferedWriter(new FileWriter(fm.getStandardisedFolder().getPath() + fm.getFileSeparator() + filenames + ".txt"));

                //Setup XML 
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLEventReader eventReader = factory
                        .createXMLEventReader(new FileReader(OrphanetFile));

                while (eventReader.hasNext()) {
                    // Get the next event
                    XMLEvent event = eventReader.nextEvent();
                    //if we have passed the start tag append the string with the event
                    if (element) {
                        event.writeAsEncodedUnicode(buf);

                    }
                    //if we reach the endelement create XML document and extract data
                    if (event.isEndElement()) {
                        EndElement e = event.asEndElement();
                        String name = e.getName().toString();
                        if (name.equals(ELEMENT)) {
                            element = false;
                            // System.out.println(buf.toString());
                            Document doc = getXML(buf.toString());
                            extractData(doc);
                            // System.out.println("-----------------");
                            buf = new StringWriter(1024);
                        }
                    }
                    //if we reach the start element start wrinting to StringWriter
                    if (event.isStartElement()) {
                        StartElement s = event.asStartElement();
                        String name = s.getName().toString();
                        if (name.equals(ELEMENT)) {
                            element = true;
                            event.writeAsEncodedUnicode(buf);
                        }
                    }
                }
            } catch (FileNotFoundException | XMLStreamException e) {
            }

            bw.close();

            ser.SerializeSet(CuratedAssociations, fm.getSerializedFolder().getPath() + fm.getFileSeparator() + filenames + ".ser");
            LOGGER.log(Level.INFO, "Successfully parsed {0} disease-gene relations", CuratedAssociations.size());
            LOGGER.log(Level.INFO, "{0} of the Orphanet disease-gene relations could be mapped to one or more MeSH terms", mapped);
            LOGGER.log(Level.INFO, "{0} of the Orphhanet disease-gene relations could NOT be mapped to a MeSH term", unmappable);
        } catch (IOException ex) {
            Logger.getLogger(Orphanet.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        logInfo();

    }

    /**
     * Takes and XML element and extracts all concept classes and relation types
     * required using XMLPath
     *
     * @param document
     */
    public void extractData(Document document) {
        //create an XpathFactory to mine the document
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        String disease = getOrphanetDisease(document, xpath);
        Set<String> associatedGenes = new HashSet<>();
        try {
            associatedGenes = getAssociatedGeneSymbols(document, xpath);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(Orphanet.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (String gen : associatedGenes) {

            if (ORDO2MeSH.containsKey(disease)) {
                mapped++;
                Set<String> mesh = ORDO2MeSH.get(disease);
                for (String mes : mesh) {
                    try {
                        CuratedAssociations.add(new Pair(gen, mes));
                        bw.append(gen + "\t" + mes + "\n");
                    } catch (IOException ex) {
                        Logger.getLogger(Orphanet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                unmappable++;
                //do we want to check that it is possible to map to OMIM and then to MeSH??
            }

        }

    }

    @Override
    public void logInfo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder string = new StringBuilder();
        string.append(filenames).append("\t").append(CuratedAssociations.size()).append("\t").append(CuratedAssociations.size() + unmappable).append("\t").append(dateFormat.format(date)).append("\n");

        PrintWriter bw = null;
        try {
            bw = new PrintWriter(new BufferedWriter(new FileWriter("SuppFiles/GeneDiseaseLOG.txt", true)));
            bw.println(string.toString());
            bw.close();
            //To change body of generated methods, choose Tools | Templates.
        } catch (IOException ex) {
            Logger.getLogger(BeFree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Convert a String to an XML document
     *
     * @param convert
     */
    public Document getXML(String convert) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            Document document = null;
            try {
                document = builder.parse(new InputSource(new StringReader(convert)));

            } catch (SAXException ex) {
                Logger.getLogger(Orphanet.class
                        .getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Orphanet.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
            return document;
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Orphanet.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;


    }

    /**
     * Retrieve the diseases ID
     *
     * @param doc
     * @param xpath
     * @return
     */
    public String getOrphanetDisease(Document doc, XPath xpath) {
        try {
            String id = null;
            id = (String) xpath.evaluate(DISEASE_XPATH, doc, XPathConstants.STRING);
            return id;
        } catch (XPathExpressionException ex) {
            Logger.getLogger(Orphanet.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Returns the Gene IDs for all associated genes
     *
     * @param doc
     * @param xpath
     * @return
     * @throws XPathExpressionException
     */
    public Set<String> getAssociatedGeneSymbols(Document doc, XPath xpath) throws XPathExpressionException {

        Set<String> associatedGenes = new HashSet<String>();
        //get all associated genes
        NodeList genes = (NodeList) xpath.evaluate(GENES_XPATH, doc, XPathConstants.NODESET);

        for (int i = 0; i < genes.getLength(); i++) {
            String name = null;
            String geneSymbol = null;
            associatedGenes.add((String) xpath.evaluate("Symbol", genes.item(i), XPathConstants.STRING));
        }
        return associatedGenes;
    }
}
