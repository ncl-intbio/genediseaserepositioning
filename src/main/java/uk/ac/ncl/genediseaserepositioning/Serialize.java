/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow.CreateScoredMatrices;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow.LeacockMatrixDrug;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;

/**
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * This class provides the ability to serialize maps that may be used for
 * mapping ids in during integration.
 */
public class Serialize {

    private final static Logger LOGGER = Logger.getLogger(Serialize.class.getName());

    public static void main(String[] args) {
        try {
            Set<String> test = new HashSet<String>();
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader("StandardisedAssociations/UniProt_GeneIDs.txt"));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
            }
            String thisLine;
            while ((thisLine = br.readLine()) != null) {
                if (!thisLine.isEmpty() && !thisLine.trim().equals("")) {
                    test.add(thisLine.trim());
                }
            }

            Serialize ser = new Serialize();
            ser.SerializeSetString(test, "SerializedAssociations/UniProt_genes.ser");

            Set<String> test2 = ser.deserializeSetString("SerializedAssociations/UniProt_genes.ser");
            System.out.println(test2.toString());
        } catch (IOException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Serialize() {
    }

    /**
     * Serialize a map to a file of choice.
     *
     * @param toser
     * @param file
     */
    public void SerializeLeacockMatrix(LeacockMatrixDrug mat, String file) throws FileNotFoundException {

        FileOutputStream f_out = new FileOutputStream(file);
        ObjectOutputStream obj_out = null;
        try {
            obj_out = new ObjectOutputStream(f_out);
        } catch (IOException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            obj_out.writeObject(mat);
        } catch (IOException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            obj_out.close();
        } catch (IOException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * De-serialize a map form file and return it.
     *
     * @param file
     * @return
     */
    public LeacockMatrixDrug deserializeLeacockMatrix(String file) {
        FileInputStream f_in = null;
        try {
            LeacockMatrixDrug map = null;
            f_in = new FileInputStream(file);
            ObjectInputStream obj_in = null;
            try {
                obj_in = new ObjectInputStream(f_in);
            } catch (IOException ex) {
                Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                map= (LeacockMatrixDrug) obj_in.readObject();
            } catch (IOException ex) {
                Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                f_in.close();
            } catch (IOException ex) {
                Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                obj_in.close();
            } catch (IOException ex) {
                Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
            }
//            LOGGER.log(
//                    Level.INFO, "Deserialized CreateScoredMatrix from {0}", file);
            return map;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                f_in.close();
            } catch (IOException ex) {
                Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * Serialize a map to a file of choice.
     *
     * @param toser
     * @param file
     */
    public void SerializeSetPair(Set toser, String file) {

        try {
            try (FileOutputStream fos = new FileOutputStream(file); ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(toser);
            }
            LOGGER.log(
                    Level.INFO, "Serialized Set<Pair> data is saved to {0}", file);
        } catch (IOException ioe) {
        }
    }

    /**
     * Serialize a map to a file of choice.
     *
     * @param toser
     * @param file
     */
    public void SerializeHashMap(Map toser, String file) {

        try {
            try (FileOutputStream fos = new FileOutputStream(file); ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(toser);
            }
            LOGGER.log(
                    Level.INFO, "Serialized HashMap data is saved to {0}", file);
        } catch (IOException ioe) {
        }
    }

    /**
     * De-serialize a map form file and return it.
     *
     * @param file
     * @return
     */
    public HashMap<String, String> deserializeHashMap(String file) {
        HashMap<String, String> map = null;
        try {
            try (FileInputStream fis = new FileInputStream(file); ObjectInputStream ois = new ObjectInputStream(fis)) {
                map = (HashMap) ois.readObject();
            }
        } catch (IOException | ClassNotFoundException ioe) {
        }

        LOGGER.log(
                Level.INFO, "Deserialized HashMap from {0}", file);

        return map;
    }

    public HashMap<String, Set<String>> deserializeHashMapSet(String file) {
        LOGGER.log(
                Level.INFO, "Deserializing HashMap from {0}", file);

        HashMap<String, Set<String>> map = null;
        try {
            try (FileInputStream fis = new FileInputStream(file)) {
                ObjectInputStream ois = new ObjectInputStream(fis);
                map = (HashMap) ois.readObject();
                ois.close();
            }
        } catch (IOException | ClassNotFoundException ioe) {
        }

        LOGGER.log(
                Level.INFO, "Deserialized HashMap from {0}", file);

        return map;
    }

    public Set<String> deserializeSet(String file) {
        LOGGER.log(
                Level.INFO, "Deserializing HashSet from {0}", file);

        Set<String> map = null;
        try {
            try (FileInputStream fis = new FileInputStream(file)) {
                ObjectInputStream ois = new ObjectInputStream(fis);
                map = (HashSet) ois.readObject();
                ois.close();
            }
        } catch (IOException | ClassNotFoundException ioe) {
        }

        LOGGER.log(
                Level.INFO, "Deserialized HashMap from {0}", file);

        return map;
    }

    public Set<Pair> deserializeSetPair(String file) {
        LOGGER.log(
                Level.INFO, "Deserializing HashSet from {0}", file);

        Set<Pair> map = null;
        try {
            try (FileInputStream fis = new FileInputStream(file)) {
                ObjectInputStream ois = new ObjectInputStream(fis);
                map = (HashSet) ois.readObject();
                ois.close();
            }
        } catch (IOException | ClassNotFoundException ioe) {
        }

        LOGGER.log(
                Level.INFO, "Deserialized HashMap from {0}", file);

        return map;
    }

    /**
     * Serialize a map to a file of choice.
     *
     * @param toser
     * @param file
     */
    public void SerializeSet(Set<Pair> toser, String file) {

        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(toser);

            LOGGER.log(
                    Level.INFO, "Serialized HashSet data is saved to {0}", file);
        } catch (IOException ioe) {
        }
    }

    /**
     * Serialize a map to a file of choice.
     *
     * @param toser
     * @param file
     */
    public void SerializeSetString(Set<String> toser, String file) {

        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(toser);

            LOGGER.log(
                    Level.INFO, "Serialized HashSet data is saved to {0}", file);
        } catch (IOException ioe) {
        }
    }

    /**
     * De-serialize a map form file and return it.
     *
     * @param file
     * @return
     */
    public Set<String> deserializeSetString(String file) {
        ObjectInputStream ois = null;
        try {
            HashSet<String> map = null;
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
            }
            ois = new ObjectInputStream(fis);
            try {
                map = (HashSet) ois.readObject();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
            }
            LOGGER.log(
                    Level.INFO, "Deserialized HashSet from {0}", file);
            return map;
        } catch (IOException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ois.close();
            } catch (IOException ex) {
                Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * De-serialize a map form file and return it.
     *
     * @param file
     * @return
     */
    public HashMap<String, String> deserializeHashMapString(String file) {
        HashMap<String, String> map = null;
        try {
            try (FileInputStream fis = new FileInputStream(file); ObjectInputStream ois = new ObjectInputStream(fis)) {
                map = (HashMap) ois.readObject();
            }
        } catch (IOException | ClassNotFoundException ioe) {
        }

        LOGGER.log(
                Level.INFO, "Deserialized HashMap from {0}", file);

        return map;
    }
}
