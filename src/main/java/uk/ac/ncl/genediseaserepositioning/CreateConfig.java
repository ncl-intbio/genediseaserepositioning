/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class is used for the creation of the config file which is read using the
 * File Manager of the project.
 */
public class CreateConfig {

    public static void main(String[] args) {
        XMLConfiguration configCreate = new XMLConfiguration();
        configCreate.setFileName("fileSettings.xml");
        //1. folder paths
        configCreate.addProperty("Folder.Graph", "/Users/joemullen/Desktop/test");
        configCreate.addProperty("Folder.GeneDisease_OP", "GeneDisease_OP");
        configCreate.addProperty("Folder.Mappings_OP", "Mappings_OP");
        configCreate.addProperty("Folder.Mappings", "Mappings_OP/Mappings");
        configCreate.addProperty("Folder.FilteredMappings", "Mappings_OP/FilteredMappings");
        configCreate.addProperty("Folder.DS_Visualisation", "Mappings_OP/DS_vis");
        configCreate.addProperty("Folder.SerializedAssociations", "GeneDisease_OP/SerializedAssociations");
        configCreate.addProperty("Folder.StandardisedAssociations", "GeneDisease_OP/StandardisedAssociations");
        configCreate.addProperty("Folder.SuppFiles", "SuppFiles");
        configCreate.addProperty("Folder.IntegratedIndications", "SuppFiles/IntegratedIndications");
        configCreate.addProperty("Folder.HyperGeometric_OP", "GeneDisease_OP/HyperGeometric_OP");
        configCreate.addProperty("Folder.ROC_OP", "GeneDisease_OP/ROC_OP");
        configCreate.addProperty("Folder.HyperGeometricDistribution", "GeneDisease_OP/HyperGeometric_OP/Distribution");
        configCreate.addProperty("Folder.LLS_OP", "GeneDisease_OP/LLS_OP");
        configCreate.addProperty("Folder.LLS_ScoredAssociations", "GeneDisease_OP/LLS_OP/ScoredAssociations");
        configCreate.addProperty("Folder.LLS_toPlot", "GeneDisease_OP/LLS_OP/Plot");
        configCreate.addProperty("Folder.Query_OP", "GeneDisease_OP/Query_OP");
        configCreate.addProperty("Folder.Leacock", "GeneDisease_OP/Leacock");
        configCreate.addProperty("Folder.Leacock_Scores", "LeacockScores");
        configCreate.addProperty("Folder.SE_Analysis", "SE_Mapping");

        //2. dateset paths
        //2.1 G_D associations 
        configCreate.addProperty("GeneDiseaseDatasets.BeFree_TXT", "/Volumes/MyPassport/Datasets/Gene-Disease/befree_gene_disease_associations.txt");
        configCreate.addProperty("GeneDiseaseDatasets.CTD_CSV", "/Volumes/MyPassport/Datasets/Gene-Disease/CTD_genes_diseases.csv");
        configCreate.addProperty("GeneDiseaseDatasets.DisGeNet_TXT", "/Volumes/MyPassport/Datasets_GSK/DisGENet/all_gene_disease_associations.txt");
        configCreate.addProperty("GeneDiseaseDatasets.GOF_TXT", "/Volumes/MyPassport/Datasets_DReNIn_v1/GOFLOF/GOF_hc.txt");
        configCreate.addProperty("GeneDiseaseDatasets.LOF_TXT", "/Volumes/MyPassport/Datasets_DReNIn_v1/GOFLOF/LOF_hc.txt");
        configCreate.addProperty("GeneDiseaseDatasets.GWAS_TXT", "/Volumes/MyPassport/Datasets/Gene-Disease/gwascatalog-minimal.txt");
        configCreate.addProperty("GeneDiseaseDatasets.MGD_TXT", "/Volumes/MyPassport/Datasets/Gene-Disease/MGI_OMIM.txt");
        configCreate.addProperty("GeneDiseaseDatasets.OMIM_TXT", "/Volumes/MyPassport/Datasets/Gene-Disease/morbidmap");
        configCreate.addProperty("GeneDiseaseDatasets.Orphanet_XML", "/Volumes/MyPassport/Datasets/Gene-Disease/en_product6.xml");
        configCreate.addProperty("GeneDiseaseDatasets.RGD_TXT", "/Volumes/MyPassport/Datasets/Gene-Disease/rattus_terms_rdo.txt");
        configCreate.addProperty("GeneDiseaseDatasets.SemRep_TXT", "/Volumes/MyPassport/Datasets/Gene-Disease/SemRep/DISTINCT_geneDisease.txt");
        configCreate.addProperty("GeneDiseaseDatasets.UniProt", "SuppFiles/UniProt_GeneDisease.txt");
        //2.2 Other datasets for graph
        configCreate.addProperty("GraphDatasets.MeSH_D", "/Volumes/MyPassport/Datasets/MeSH/d2015.bin");
        configCreate.addProperty("GraphDatasets.MeSH_C", "/Volumes/MyPassport/Datasets/MeSH/c2015.bin");
        configCreate.addProperty("GraphDatasets.DrugBank", "/Volumes/MyPassport/Datasets_DReNIn_v1/DrugBank/drugbank.xml");
        configCreate.addProperty("GraphDatasets.ORDO", "/Volumes/MyPassport/Datasets_DReNIn_v1/Orphanet/ORDO.csv");
        configCreate.addProperty("GraphDatasets.ChEMBL_DRUGMECH", "/Volumes/MyPassport/Datasets_DReNIn_v1/ChEMBL/activityTypes.txt");
        configCreate.addProperty("GraphDatasets.ChEMBL_BindsTo", "/Volumes/MyPassport/Datasets_DReNIn_v1/ChEMBL/ChEMBL_Associations");
        configCreate.addProperty("GraphDatasets.SIDER_sideEffects", "/Volumes/MyPassport/Datasets_DReNIn_v1/SIDER/meddra_all_se.tsv");
        configCreate.addProperty("GraphDatasets.SIDER_indications", "/Volumes/MyPassport/Datasets_DReNIn_v1/SIDER/meddra_all_indications.tsv");
        configCreate.addProperty("GraphDatasets.NDFRT_indications", "/Volumes/MyPassport/Datasets_DReNIn_v1/NDFRT/NDFRTrelations.txt");
        configCreate.addProperty("GraphDatasets.UniProt_XML", "/Volumes/MyPassport/Datasets_DReNIn_v1/UniProt/uniprot_sprot.xml");
        configCreate.addProperty("GraphDatasets.PREDICT_GD", "/Volumes/MyPassport/Datasets_DReNIn_v1/PREDICT/associations_Drug_Disease.txt");
        configCreate.addProperty("GraphDatasets.PREDIC_UMLS", "/Volumes/MyPassport/Datasets_DReNIn_v1/PREDICT/disease_UMLS.txt");
        configCreate.addProperty("GraphDatasets.CTD_Indications", "/Volumes/MyPassport/Datasets_DReNIn_v1/CTD/CTD_chemicals_diseases.tsv");
        configCreate.addProperty("GraphDatasets.CTD_Chemicals", "/Volumes/MyPassport/Datasets_DReNIn_v1/CTD/CTD_chemicals.tsv");
        configCreate.addProperty("GraphDatasets.ADME_CORE", "/Volumes/MyPassport/Datasets/ADME_Genes/ADMEcoreGENES.txt");
        configCreate.addProperty("GraphDatasets.ADME_EXTENDED", "/Volumes/MyPassport/Datasets/ADME_Genes/ADMEextendedGENES.txt");
        
        //2.3 Mapping datasets
        configCreate.addProperty("Mapping.Metathesurus_MRCONSO", "MRCONSO.RRF_Location");
        configCreate.addProperty("Mapping.UniChem_src1src2", "/Volumes/MyPassport/Datasets_DReNIn_v1/ChEMBL/ChEMBL_UniChem/src1src2.txt");
        configCreate.addProperty("Mapping.PharmGKB_Drug", "/Volumes/MyPassport/Datasets_DReNIn_v1/PharmaGKB/drugs/drugs.tsv");
        configCreate.addProperty("Mapping.GSK_OMIM2MeSH", "SuppFiles/GSK_omim2mesh.txt");
        configCreate.addProperty("Mapping.ORDO_OMIM2MeSH", "SuppFiles/ORDO_omim2mesh.txt");
        configCreate.addProperty("Mapping.ORDO_OMIM2MeSH_File", "/Volumes/MyPassport/Datasets/Gene-Disease/en_product1.xml");

        try {
            configCreate.save();
        } catch (ConfigurationException ex) {
            Logger.getLogger(CreateConfig.class.getName()).log(Level.SEVERE, null, ex);
        }

        XMLConfiguration configRead = null;
        try {
            configRead = new XMLConfiguration("fileSettings.xml");
        } catch (ConfigurationException ex) {
            Logger.getLogger(CreateConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        String settingValue = configRead.getString("Mapping.Metathesurus_MRCONSO");
        System.out.println(settingValue);

    }
}
