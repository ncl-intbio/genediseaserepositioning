/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.MeSHUI2MeSHTree;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.SSSStepDescription;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.MetaDataInterface;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphMine.PropComparator.Comparator;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.Fetch;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode.ApprovedSmallMolecule;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode.RuleNode;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * Class identifies all diseases that are have gene-disease associations
 * involving them.
 *
 * @author joe
 */
public class MeSH_CalcGeneDiseases {

    private Neo4J backend;
    private Map<String, String> drugIds;
    private Set<Pair> geneDisease;
    private String type;
    private FileManager fman;
    private HashMap<String, Set<String>> toPlot;
    private HashMap<String, Set<String>> diseaseFreq;
    private final static Logger LOGGER = Logger.getLogger(MeSH_CalcTreatableDiseases.class.getName());
    private int gdCount = 0;
    private int parentterms = 0;
    private Set<String> startsWith;
    private int depth;
    private File opFile;
    private File scoredGeneDisease;
    private MeSHUI2MeSHTree meshMap;
    private Set<String> MeSHUIs;
    private Serialize ser;

    public static void main(String[] args) {

        Set<String> highLevel = new HashSet<String>(Arrays.asList("C", "F01", "F02", "F03"));
        FileManager fm = new FileManager();
        Neo4J backend = new Neo4J(fm.getGraphLocation().toString());

        MeSH_CalcGeneDiseases eg = new MeSH_CalcGeneDiseases(backend, 1, highLevel, new File("gene_disease_distribution.txt"), new File(fm.getScoredAssociationFolder() + fm.getFileSeparator() + "scored_Associations_5.0_UNIPROT.txt"));
        eg.getAllGeneDiseaseAssociations();
        eg.getNumberOfDiseases();
        eg.calc();
        try {
            eg.writeToFile();
        } catch (IOException ex) {
            Logger.getLogger(MeSH_CalcGeneDiseases.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public MeSH_CalcGeneDiseases(Neo4J handler, int depth, Set<String> startsWith, File opFile, File scoredGD) {
        this.ser = new Serialize();
        this.scoredGeneDisease = scoredGD;
        this.geneDisease = new HashSet<Pair>();
        this.startsWith = startsWith;
        this.fman = new FileManager();
        this.backend = handler;
        this.toPlot = new HashMap<String, Set<String>>();
        this.diseaseFreq = new HashMap<String, Set<String>>();
        this.depth = depth;
        this.opFile = opFile;
        this.meshMap = new MeSHUI2MeSHTree();
        this.MeSHUIs = ser.deserializeSetString(fman.getSuppFolder() + fman.getFileSeparator() + "MeSH_meshuis.ser");
    }

    /**
     * Get all drugs from the network.
     */
    public void getAllGeneDiseaseAssociations() {

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(scoredGeneDisease));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MeSH_CalcGeneDiseases.class.getName()).log(Level.SEVERE, null, ex);
        }
        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                geneDisease.add(new Pair(thisLine.split("\t")[0], thisLine.split("\t")[1]));
            }
        } catch (IOException ex) {
            Logger.getLogger(MeSH_CalcGeneDiseases.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Added {0} gene-disease associations...", geneDisease.size());
    }

    /**
     * For each drug go through all the diseases that it is marketed to treat.
     *
     */
    public void calc() {

        String gene = null;
        String MeSHUI = null;

        for (Pair p : geneDisease) {
            gene = p.getEntrezGeneSymbol();
            MeSHUI = p.getMeSHUI();
            Set<String> treeTerms = meshMap.getTreeTerms(MeSHUI);
            for (String tree : treeTerms) {
                if (diseaseCheck(tree)) {
                    gdCount++;
                    String parentterm = getMeshTreeForClass(tree);
                    //System.out.println(parentterm + "\t" + UI);
                    addMeshTerm(parentterm, MeSHUI);
                    parentterms++;
                }
            }
        }

        LOGGER.log(Level.INFO,
                "Checked {0} unique indications ", gdCount);
        LOGGER.log(Level.INFO,
                "Which mapped to {0} MeSH parent terms", parentterms);

    }

    /**
     * Go through all diseases in the graph and see how many there are for each
     * category.
     */
    private void getNumberOfDiseases() {
        for (String UI : MeSHUIs) {
            Set<String> meshtrees = meshMap.getTreeTerms(UI);
            for (String tree : meshtrees) {
                if (diseaseCheck(tree)) {
                    String parentterm = getMeshTreeForClass(tree);
                    //System.out.println(parentterm + "\t" + UI);
                    diseaseFreq(parentterm, UI);
                    parentterms++;
                }
            }
        }
    }

    /**
     * Check to see if the disease starts with the desired ID.
     *
     * @param treeterm
     * @return
     */
    private boolean diseaseCheck(String treeterm) {

        boolean include = false;
        for (String s : startsWith) {
            if (treeterm.startsWith(s)) {
                return true;
            }
        }
        return include;

    }

    /**
     * Add the mesh term to the data count for that branch of the hierarchy.
     *
     * @param meshTerm
     */
    public void addMeshTerm(String meshTerm, String meshUI) {

        if (toPlot.containsKey(meshTerm)) {
            Set<String> treatedDis = toPlot.get(meshTerm);
            treatedDis.add(meshUI);
            toPlot.put(meshTerm, treatedDis);
        } else {
            Set<String> treatedDis = new HashSet<String>();
            treatedDis.add(meshUI);
            toPlot.put(meshTerm, treatedDis);
        }
    }

    /**
     * Add the mesh term to the data count for that branch of the hierarchy.
     *
     * @param meshTerm
     */
    public void diseaseFreq(String meshTerm, String meshUI) {

        if (diseaseFreq.containsKey(meshTerm)) {
            Set<String> treatedDis = diseaseFreq.get(meshTerm);
            treatedDis.add(meshUI);
            diseaseFreq.put(meshTerm, treatedDis);
        } else {
            Set<String> treatedDis = new HashSet<String>();
            treatedDis.add(meshUI);
            diseaseFreq.put(meshTerm, treatedDis);
        }
    }

    /**
     * Get the mesh tree term.
     */
    public String getMeshTreeForClass(String allTrees) {

        String[] branches = allTrees.split("\\.");
        if (branches.length < depth) {
            return allTrees;
        }
        String branch = "FIRST";
        for (int p = 0; p < depth; p++) {
            if (branch.equals("FIRST")) {
                branch = branches[p];
            } else {
                branch = branch.trim() + "." + branches[p];
            }
        }
        return branch;
    }

    /**
     * Write all of the counts to file.
     */
    private void writeToFile() throws IOException {

        DecimalFormat df = new DecimalFormat("#.##");
        //System.out.println("DISFREQ::" + diseaseFreq.toString());

        // System.out.println("TOPLOT::" + toPlot.toString());

        BufferedWriter bw = new BufferedWriter(new FileWriter(opFile));
        for (String name : toPlot.keySet()) {
            Set<String> count = toPlot.get(name);
            Set<String> freqcount = diseaseFreq.get(name);
            for (String dis : count) {
                if (!freqcount.contains(dis)) {
                    System.out.println(name + "\t" + dis);
                }
            }
            double percent = (100.0 / freqcount.size()) * (count.size() * 1.0);
            bw.append(name + "\t" + count.size() + "\t" + freqcount.size() + "\t" + df.format(percent) + "\t" + (freqcount.size() - count.size()) + "\n");
        }

        bw.close();
    }
}
