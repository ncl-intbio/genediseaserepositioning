/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.Hypergeometric.CalcGeometric;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.Hypergeometric.HypergeometricInstance;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DataSourceKNOWNS;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes a directory of scored associations and returns a plottable
 * hypergeometric set of values.
 */
public class ValidateAssociations_Hyper {

    private int sampleSize;
    private int sampleOverlap;
    private Set<Pair> goldStandard;
    private String scoredAssociationsFile;
    private String outPut;
    private int assFromPos;
    private int assToPos;
    private int assScorePos;
    private int truePositives;
    private int falsePositives;
    private final static Logger LOGGER = Logger.getLogger(ValidateAssociations_Hyper.class.getName());
    private BufferedWriter bw;
    private Map<Double, Set<Pair>> scoredAssociations;
    private List<Double> scores;
    private Set<Pair> sample;
    private double[] window;
    private int check;
    private FileManager fm;
    private DataSourceKNOWNS datasource;
    private Set<String> DBID;
    private DrugDiseaseData indicationInfo;

    public ValidateAssociations_Hyper(int slidingSize, int sampleOverlap, String associationsFile, int from, int to, int score, String project, DataSourceKNOWNS dataSource, DrugDiseaseData.DiseaseType disType, DrugDiseaseData.DiseaseCat disCat) {

        this.DBID = new HashSet<>();
        this.datasource = dataSource;
        this.scoredAssociationsFile = associationsFile;
        this.sampleSize = slidingSize;
        this.fm = new FileManager();
        this.sampleOverlap = sampleOverlap;
        String output = fm.getHyperFile().getPath() + fm.getFileSeparator() + project;
        File f = new File(output);
        // if the directory does not exist, create it
        if (!f.exists()) {
            LOGGER.log(Level.INFO, "Creating directory... {0}", output);
            boolean result = false;

            try {
                f.mkdir();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                LOGGER.log(Level.INFO, "Created directory... {0}", output);
            }
        }

        File in = new File(scoredAssociationsFile);

        this.outPut = output + "/" + datasource.getText() + "_" + in.getName().replace(".txt", "") + "_HYPER.txt";
        this.assFromPos = from;
        this.assToPos = to;
        this.goldStandard = new HashSet<>();
        this.assScorePos = score;
        this.scoredAssociations = new HashMap<>();
        this.sample = new HashSet<>();
        this.window = new double[sampleSize];
        this.check = 0;
        this.indicationInfo = new DrugDiseaseData(dataSource, disType, disCat);
    }

    public void getKnowns() {
        this.goldStandard = indicationInfo.getPruned();
        System.out.println("Gold standard size --------" + goldStandard.size());
        this.DBID = indicationInfo.getDrugName2DBID();
        System.out.println("DBID size --------" + DBID.size());
    }

    public void calcSlidingWindow() {
        //was previously 9
        this.sampleSize = (scoredAssociations.keySet().size() / 8) * 2;
        LOGGER.log(Level.INFO, "SampleSize {0}", sampleSize);
        this.sampleOverlap = (scoredAssociations.keySet().size() / 8);
        LOGGER.log(Level.INFO, "SampleOverlap {0}", sampleOverlap);
        this.window = new double[sampleSize];
    }

    /**
     * First of all get all the true positives of the score associations
     */
    public void getAllAssociations() {
        LOGGER.log(Level.INFO, "Getting scored associations from ...{0}", scoredAssociationsFile);
        BufferedReader br = null;
        int count = 0;
        //variables
        String from;
        String fromID;
        String to;
        double score;
        this.truePositives = 0;
        this.falsePositives = 0;
        try {
            //BufferedReader br;
            br = new BufferedReader(new FileReader(new File(scoredAssociationsFile)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ValidateAssociations_Hyper.class.getName()).log(Level.SEVERE, null, ex);
        }

        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                String[] split = thisLine.split("\t");
                from = split[assFromPos];
                to = split[assToPos];
                score = Double.parseDouble(split[assScorePos]);
                count++;
                Pair pair = new Pair(from, to);
                //System.out.println("Checking pair:: "+ pair.toString());
                if (goldStandard.contains(pair)) {
                    truePositives++;
                } else {
                    falsePositives++;
                }

                if (scoredAssociations.containsKey(score)) {
                    Set<Pair> pairs = scoredAssociations.get(score);
                    pairs.add(pair);
                    scoredAssociations.put(score, pairs);
                } else {
                    Set<Pair> pairs = new HashSet<>();
                    pairs.add(pair);
                    scoredAssociations.put(score, pairs);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ValidateAssociations_Hyper.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO,
                "We have {0} different scored associations ...", count);
        LOGGER.log(Level.INFO,
                "We have {0} different scores for associations ...", scoredAssociations.keySet().size());

        Set<Double> allScores = scoredAssociations.keySet();
        List<Double> sortedScores = new ArrayList<>();
        for (double scored : allScores) {
            sortedScores.add(scored);
        }

        Collections.sort(sortedScores);
        this.scores = sortedScores;
    }

    /**
     * Export the values to allow for plotting in r
     */
    public void calculateValues() {
        LOGGER.info("Calculating values to plot...");

        try {
            bw = new BufferedWriter(new FileWriter(outPut));


        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < scores.size(); i++) {
            updateWindow(i, scores.get(i));
        }
        try {
            bw.close();

        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Plot values have been stored in {0}", outPut);
    }

    /**
     * Update the latest view of the window.
     *
     * @param pair
     * @param score
     * @param count
     */
    public void updateWindow(int count, double score) {
        if (count < sampleSize) {
            window[count] = score;
        } else {
            double[] window2 = new double[sampleSize];
            //System.out.println("w2 length: "+window2.length);
            //System.out.println("w length: "+window.length);
            for (int i = 1; i < window.length; i++) {
                window2[i - 1] = window[i];
            }
            window2[sampleSize - 1] = score;
            window = window2;
            if ((check - sampleSize) % (sampleSize - sampleOverlap) == 0) {
                calculatePoint();
            }
        }
        check++;
    }

    /**
     * Calculate the points to be plotted.
     */
    public void calculatePoint() {

        //number of interaction ins the sample that are known to be correct
        int k = 0;
        double averageScore = 0.0;
        //get all the associations for the window
        Set<Pair> pairWindow = new HashSet<>();

        for (int i = 0; i < sampleSize; i++) {
            averageScore = averageScore + window[i];
            Set<Pair> localPair = scoredAssociations.get(window[i]);
            pairWindow.addAll(localPair);
        }

        //check for any mappings
        for (Pair p : pairWindow) {
            if (goldStandard.contains(p)) {
                k++;
            }
        }

        HypergeometricInstance hyper = new HypergeometricInstance(k, pairWindow.size(), falsePositives, truePositives, averageScore / sampleSize);
        try {
            bw.append(hyper.getPoints() + "\n");


        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
}
