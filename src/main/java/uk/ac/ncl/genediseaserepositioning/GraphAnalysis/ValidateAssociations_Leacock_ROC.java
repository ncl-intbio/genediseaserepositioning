/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.Hypergeometric.CalcGeometric;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.ROC_D_Value;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DataSourceKNOWNS;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DiseaseCat;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow.CreateScoredMatrices;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow.LeacockAndChodorow;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow.LeacockMatrixDrug;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * Class takes the filtered associations and a gold standard, a disease category
 * and a disease type and produces the output for a ROC curve. We want a point
 * at every score.
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class ValidateAssociations_Leacock_ROC {

    private Set<Pair> goldStandard;
    private String scoredAssociationsFile;
    private String outPut;
    private int truePositives;
    private int falsePositives;
    private final static Logger LOGGER = Logger.getLogger(ValidateAssociations_Hyper.class.getName());
    private BufferedWriter bw;
    private Map<Double, Set<Pair>> scoredAssociations;
    private List<Double> scores;
    private Set<Pair> sample;
    private int check;
    private FileManager fm;
    private DrugDiseaseData.DataSourceKNOWNS datasource;
    private DrugDiseaseData indicationInfo;
    private double[] scoresToPLot;
    private double threshold;
    private final int POINTS;
    private LeacockAndChodorow leac;
    private LeacockMatrixDrug local;
    private Serialize ser;
    private Set<String> allDBIDs;

    public static void main(String[] args) {

        FileManager fm = new FileManager();
        DrugDiseaseData.DataSourceKNOWNS[] dats = new DrugDiseaseData.DataSourceKNOWNS[1];

        LeacockAndChodorow leac = new LeacockAndChodorow(true);
        double[] thresholds = leac.getAllScores();
        for (int i = 0; i < thresholds.length; i++) {
            //for (int i = 0; i < 1; i++) {//dats.length; i++) {
            File dir = fm.getFilteredMappingsFolder();
            File[] directoryListing = dir.listFiles();
            if (directoryListing != null) {
                for (File child : directoryListing) {
                    if (!child.getName().contains("MeSH") && !child.getName().contains("FILTERED") && !child.getName().contains(".DS_") && child.getAbsolutePath().contains("UNIQUE") && !child.getName().contains("filterSE_")) {
                        //get the disease cat and the disease type from the file names of the filtered associations.
                        DrugDiseaseData.DiseaseCat disCat = DrugDiseaseData.DiseaseCat.fromValue(child.getName().split("_")[0]);
                        DrugDiseaseData.DiseaseType disType = null;
                        String t = child.getName().split("_")[3];
                        if (t.contains("RARE")) {
                            disType = DrugDiseaseData.DiseaseType.RARE;
                        } else if (t.contains("COMMON")) {
                            disType = DrugDiseaseData.DiseaseType.COMMON;
                        } else if (t.contains("ALL")) {
                            disType = DrugDiseaseData.DiseaseType.ALL;
                        }
                        if (t.contains("ALL") && disCat.equals(DiseaseCat.ALL)) {
                            ValidateAssociations_Leacock_ROC roc = new ValidateAssociations_Leacock_ROC(thresholds[i], child.getAbsolutePath().toString(), "Leacock", DrugDiseaseData.DataSourceKNOWNS.ALL, disType, disCat, 20);
                            roc.getKnowns();
                            roc.getAllAssociations();
                            roc.getScoresToPlot();
                            roc.calculateValues();
                        }
                    }
                }
            }
        }
    }

    public ValidateAssociations_Leacock_ROC(double threshold, String associationsFile, String project, DataSourceKNOWNS dataSource, DrugDiseaseData.DiseaseType disType, DrugDiseaseData.DiseaseCat disCat, int points) {

        this.POINTS = points;
        this.allDBIDs = new HashSet<String>();
        this.leac = new LeacockAndChodorow(true);
        this.ser = new Serialize();
        this.threshold = threshold;
        this.datasource = dataSource;
        this.scoredAssociationsFile = associationsFile;
        this.fm = new FileManager();
        String output = fm.getROC_OPFolder().getPath() + fm.getFileSeparator() + project;
        File f = new File(output);
        File in = new File(scoredAssociationsFile);
        this.outPut = output + "/" + datasource.getText() + "_" + in.getName().replace(".txt", "") + "_" + threshold + "_ROC.txt";
        this.goldStandard = new HashSet<>();
        this.scoredAssociations = new HashMap<>();
        this.sample = new HashSet<>();
        this.check = 0;
        this.indicationInfo = new DrugDiseaseData(dataSource, disType, disCat);
        this.scoresToPLot = new double[POINTS];
        getDBIDS();
    }

    public void getDBIDS() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fm.getSuppFolder() + fm.getFileSeparator() + "DBIDs_after_prune.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CreateScoredMatrices.class.getName()).log(Level.SEVERE, null, ex);
        }
        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                allDBIDs.add(thisLine.trim());
            }
        } catch (IOException ex) {
            Logger.getLogger(CreateScoredMatrices.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "We have ... {0} DBIDs", allDBIDs.size());

    }

    public void getKnowns() {
        this.goldStandard = indicationInfo.getPruned();
    }

    /**
     * First of all get all the true positives of the score associations
     */
    public void getAllAssociations() {
        LOGGER.log(Level.INFO, "Getting scored associations from ...{0}", scoredAssociationsFile);
        BufferedReader br = null;
        int count = 0;
        //variables
        String from;
        String fromID;
        String to;
        double score;
        this.truePositives = 0;
        this.falsePositives = 0;
        try {
            //BufferedReader br;
            br = new BufferedReader(new FileReader(new File(scoredAssociationsFile)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(uk.ac.ncl.genediseaserepositioning.GraphAnalysis.ValidateAssociations_Hyper.class.getName()).log(Level.SEVERE, null, ex);
        }

        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                String[] split = thisLine.split("\t");
                from = split[0];
                to = split[1];
                score = Double.parseDouble(split[2]);
                count++;
                Pair pair = new Pair(from, to);
                if (goldStandard.contains(pair)) {
                    truePositives++;
                } else {
                    falsePositives++;
                }

                if (scoredAssociations.containsKey(score)) {
                    Set<Pair> pairs = scoredAssociations.get(score);
                    pairs.add(pair);
                    scoredAssociations.put(score, pairs);
                } else {
                    Set<Pair> pairs = new HashSet<>();
                    pairs.add(pair);
                    scoredAssociations.put(score, pairs);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(uk.ac.ncl.genediseaserepositioning.GraphAnalysis.ValidateAssociations_Hyper.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO,
                "We have {0} different scored associations ...", count);
        LOGGER.log(Level.INFO,
                "We have {0} different scores for associations ...", scoredAssociations.keySet().size());

        Set<Double> allScores = scoredAssociations.keySet();
        List<Double> sortedScores = new ArrayList<>();
        for (double scored : allScores) {
            sortedScores.add(scored);
        }

        Collections.sort(sortedScores);
        this.scores = sortedScores;

    }

    public void getScoresToPlot() {

        double size = scores.get(scores.size() - 1) / (scoresToPLot.length * 1.0);

        for (int i = 0; i < scoresToPLot.length; i++) {
            scoresToPLot[i] = size * (i + 1);
        }

        for (int i = 0; i < scoresToPLot.length; i++) {
            System.out.println(scoresToPLot[i] + "\t" + i);
        }
    }

    /**
     * Export the values to allow for plotting in r
     */
    public void calculateValues() {
        LOGGER.info("Calculating values to plot...");

        try {
            bw = new BufferedWriter(new FileWriter(outPut));

        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < scoresToPLot.length; i++) {
            //System.out.println(i);
            calculatePoint(getAllPairsLessThanScore(i), getAllPairsGreaterThanScore(i), i);
        }
        try {
            bw.close();

        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Plot values have been stored in {0}", outPut);
    }

    /**
     * @return The set of Pairs that are all less than or equal to the score
     * being looked at.
     */
    public Set<Pair> getAllPairsLessThanScore(int p) {
        Set<Pair> allPairs = new HashSet<Pair>();
        for (int i = 0; i < scores.size(); i++) {
            if (scores.get(i) <= scoresToPLot[p]) {
                allPairs.addAll(scoredAssociations.get(scores.get(i)));
            }

        }
        return allPairs;
    }

    /**
     * @return The set of Pairs that are all less than or equal to the score
     * being looked at.
     */
    public Set<Pair> getAllPairsGreaterThanScore(int p) {
        Set<Pair> allPairs = new HashSet<Pair>();
        for (int i = 0; i < scores.size(); i++) {
            if (scores.get(i) > scoresToPLot[p]) {
                allPairs.addAll(scoredAssociations.get(scores.get(i)));
            }

        }
        return allPairs;
    }

    public void calculatePoint(Set<Pair> allPairsLess, Set<Pair> allPairsGreater, double score) {


        int falseNegative = 0;
        int trueNegative = 0;
        int truePositive = 0;
        int falsePositive = 0;

        int KNOWNSnotIdentified = 0;
        int noMappingsInferredForDrug = 0;
        int noDrugsNotInferred = 0;

        //use the matrices for the drug
        for (String dbid : allDBIDs) {
            int mappingsForDrugPred = 0;
            int mappingsForDrugKnown = 0;
            local = ser.deserializeLeacockMatrix(fm.getLeacockScores_OPFolder() + fm.getFileSeparator() + dbid + ".ser");

            for (Pair p : goldStandard) {

                boolean tp = false;
                boolean fn = false;
                if (p.getEntrezGeneSymbol().equals(dbid)) {
                    mappingsForDrugKnown++;
                   // System.out.println(p.toString());
                    for (Pair pinf : allPairsGreater) {
                        if (pinf.getEntrezGeneSymbol().equals(dbid)) {
                            mappingsForDrugPred++;
                      //      System.out.println("\t" + pinf.toString() + "\t" + local.getScore(local.getKnownPos(p.toString()), local.getPredPos(pinf.toString())));
                            if (Double.parseDouble(local.getScore(local.getKnownPos(p.toString()), local.getPredPos(pinf.toString()))) >= threshold) {
                      //          System.out.println("tp!");
                                tp = true;
                                break;
                            }
                        }
                    }
                    if (tp == false) {
                        for (Pair pinf : allPairsLess) {
                            if (pinf.getEntrezGeneSymbol().equals(dbid)) {
                                mappingsForDrugPred++;
                                if (Double.parseDouble(local.getScore(local.getKnownPos(p.toString()), local.getPredPos(pinf.toString()))) >= threshold) {
                                    fn = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                //record
                if (tp) {
                    truePositive++;
                } else if (tp == false && fn) {
                    falseNegative++;
                } else if (tp == false && fn == false && (p.getEntrezGeneSymbol().equals(dbid))) {
                    KNOWNSnotIdentified++;
                }
            }
            //we have inferred no mappings for the drug!
            if (mappingsForDrugKnown > 0 && mappingsForDrugPred == 0) {
                noDrugsNotInferred++;
                noMappingsInferredForDrug += mappingsForDrugKnown;
            }

        }

        falsePositive = allPairsGreater.size() - truePositive;
        trueNegative = allPairsLess.size() - falseNegative;

        double sensitivity = truePositive / (truePositive + (falseNegative * 1.0));
        double specificity = trueNegative / (trueNegative + (falsePositive * 1.0));

        System.out.println(score + "\t" + sensitivity + "\t" + specificity + "\t" + truePositive + "\t" + falsePositive + "\t" + trueNegative + "\t" + falseNegative + "\t" + allPairsLess.size() + "/" + allPairsGreater.size() + "=" + (allPairsLess.size() + allPairsGreater.size()) + "\tNOT_IDd:" + KNOWNSnotIdentified+"\t"+noDrugsNotInferred+"/"+noMappingsInferredForDrug);

        try {
            bw.append(sensitivity + "\t" + specificity + "\t" + truePositive + "\t" + falsePositive + "\t" + trueNegative + "\t" + falseNegative + "\t" + allPairsLess.size() + "/" + allPairsGreater.size() + "=" + (allPairsLess.size() + allPairsGreater.size()) + "\n");
        } catch (IOException ex) {
            Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
