/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import java.util.HashMap;

/**
 *
 * Class allows for the MeSH name to be returned when a parent code is provided-
 * useful for plotting.
 *
 * @author joe
 */
public class MeSHNames {

    public HashMap<String, String> meshCodeNamePARENTS;
    public HashMap<String, String> meshCodeNameNSDLevel2;
    public HashMap<String, String> meshCodeNamesImmunologyLevel2;

    public MeSHNames() {

        HashMap<String, String> meshCodeNames = new HashMap<String, String>();

        meshCodeNames.put("C01", "bacterial infections and mycoses");
        meshCodeNames.put("C02", "virus diseases");
        meshCodeNames.put("F02", "psychological phenomena and processes");
        meshCodeNames.put("F01", "behavior and behavior mechanisms");
        meshCodeNames.put("F03", "mental disorders");
        meshCodeNames.put("C23", "pathological conditions, signs and symptoms");
        meshCodeNames.put("C22", "animal diseases");
        meshCodeNames.put("C21", "disorders of environmental origin");
        meshCodeNames.put("C20", "immune system diseases");
        meshCodeNames.put("C19", "endocrine system diseases");
        meshCodeNames.put("C18", "nutritional and metabolic diseases");
        meshCodeNames.put("C17", "skin and connective tissue diseases");
        meshCodeNames.put("C16", "congenital, hereditary, and neonatal diseases and abnormalities");
        meshCodeNames.put("C15", "hemic and lymphatic diseases");
        meshCodeNames.put("C14", "cardiovascular diseases");
        meshCodeNames.put("C24", "occupational diseases");
        meshCodeNames.put("C25", "chemically-induced disorders");
        meshCodeNames.put("C26", "wounds and injuries");
        meshCodeNames.put("C13", "female genital diseases and pregnancy complications");
        meshCodeNames.put("C12", "urologic and male genital diseases");
        meshCodeNames.put("C11", "eye diseases");
        meshCodeNames.put("C10", "nervous system diseases");
        meshCodeNames.put("C09", "otorhinolaryngologic diseases");
        meshCodeNames.put("C08", "respiratory tract diseases");
        meshCodeNames.put("C07", "stomatognathic diseases");
        meshCodeNames.put("C06", "digestive system diseases");
        meshCodeNames.put("C05", "musculoskeletal diseases");
        meshCodeNames.put("C04", "neoplasms");
        meshCodeNames.put("C03", "parasitic diseases");

        meshCodeNamePARENTS = meshCodeNames;

        meshCodeNames = new HashMap<String, String>();
        meshCodeNames.put("C10.114", "autoimmune diseases of the nervous system");
        meshCodeNames.put("C10.177", "autonomic nervous system diseases");
        meshCodeNames.put("C10.803", "restless legs syndrome");
        meshCodeNames.put("C10.228", "central nervous system diseases");
        meshCodeNames.put("C10.281", "chronobiology disorders");
        meshCodeNames.put("C10.292", "cranial nerve diseases");
        meshCodeNames.put("C10.314", "demyelinating diseases");
        meshCodeNames.put("C10.500", "nervous system malformations");
        meshCodeNames.put("C10.551", "nervous system neoplasms");
        meshCodeNames.put("C10.562", "neurocutaneous syndromes");
        meshCodeNames.put("C10.574", "neurodegenerative diseases");
        meshCodeNames.put("C10.597", "neurologic manifestations");
        meshCodeNames.put("C10.668", "neuromuscular diseases");
        meshCodeNames.put("C10.720", "neurotoxicity syndromes");
        meshCodeNames.put("C10.886", "sleep disorders");
        meshCodeNames.put("C10.900", "trauma, nervous system");

        meshCodeNameNSDLevel2 = meshCodeNames;

        meshCodeNames = new HashMap<String, String>();
        meshCodeNames.put("C20.673", "immunologic deficiency syndromes");
        meshCodeNames.put("C20.683", "immunoproliferative disorders");
        meshCodeNames.put("C20.452", "graft vs host disease");
        meshCodeNames.put("C20.111", "autoimmune disease");
        meshCodeNames.put("C20.543", "hypersensitivity");
        meshCodeNames.put("C20.841", "purpura, thrombocytopenic, idiopathic");
        meshCodeNames.put("C20.306", "erythroblastosis, fetal");
        meshCodeNames.put("C20.425", "glomerulonephritis, membranoproliferative");
        meshCodeNames.put("C20.608", "immune reconstruction inflammatory syndrome");
        meshCodeNames.put("C20", "immune system diseases");
        meshCodeNames.put("C20.920", "transfusion reaction");

        meshCodeNamesImmunologyLevel2 = meshCodeNames;

    }

    public String getName(String code) {

        if (meshCodeNamePARENTS.containsKey(code)) {
            return meshCodeNamePARENTS.get(code);
        } else if (meshCodeNameNSDLevel2.containsKey(code)) {
            return meshCodeNameNSDLevel2.get(code);
        } else if (meshCodeNamesImmunologyLevel2.containsKey(code)) {
            return meshCodeNamesImmunologyLevel2.get(code);
        } else {
            return code;

        }
    }

}
