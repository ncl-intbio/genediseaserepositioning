/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.SSSStepDescription;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.MetaDataInterface;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.GraphMine.PropComparator.Comparator;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.Fetch;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode.ApprovedSmallMolecule;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode.RuleNode;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.SubgraphManager;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SimpleTraversalExamples;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class extracts all has_indications from the network. For rare_diseases the
 * MeSH UI used is the 'MESH1'; the same that is used for the mapping.
 *
 * Only the 1,188 drugs that are used during the search are analysed.
 *
 */
public class ExtractHasIndications {

    private FileManager fm;
    private HashMap<String, Set<Pair>> allAssociations;
    private Neo4J handler;
    private final static Logger LOGGER = Logger.getLogger(ExtractHasIndications.class.getName());

    public static void main(String[] args) {

        FileManager fm = new FileManager();
        ExtractHasIndications getInds = new ExtractHasIndications(new Neo4J(fm.getGraphLocation().toString() + "_GD"));
        getInds.run();

    }

    public ExtractHasIndications(Neo4J handler) {
        this.fm = new FileManager();
        this.handler = handler;
        this.allAssociations = new HashMap<String, Set<Pair>>();
    }

    public void run() {

        LOGGER.info("Starting extraction of an initial candidate set");

        //1. extract the drugs of interest.    
        String startingClass = DReNInMetadata.NodeType.SMALL_MOLECULE.getText();
        SubgraphManager sub = new SubgraphManager(SubgraphManager.Query.GeneDisease, startingClass, false);
        RuleNode smallMol = new ApprovedSmallMolecule();
        smallMol.createRuleNode();
        Fetch initSet = new Fetch(smallMol, handler);
        initSet.goFetch();
        HashMap<String, String> ids = initSet.getSuccIDs();

        //2. extract all the has_indication associations with the correct provenenance.
        handler.initialiseDatabaseConnection();
        int count = 0;
        for (String name : ids.keySet()) {
            count++;
//            if (count > 10) {
//                break;
//            }
            SSIPNode ssip = handler.getInitialSSIPNodeFromDB(new SSIPNode(ids.get(name), name));
            Set<SSIPNode> sideEffects = getSideEffects(ssip);
            for (MetaDataInterface se : ssip.getRelations().keySet()) {
                SSIPRelationType rel = ssip.getRelations().get(se);
                if (rel.getType().contains(DReNInMetadata.RelTypes.HAS_INDICATION.getText())) {
                    Map<String, Object> relAtts = rel.getAttributes();
                    if (relAtts.containsKey("Relation_Prov")) {
                        String source = (String) relAtts.get("Relation_Prov");
                        source = source.split("\t")[0];
                        String MeSHUI = "";
                        //then we want to get the MESHUI1 as we have done in the mappings
                        if (se.getId().startsWith("Orphanet_")) {                         
                            MeSHUI = getMeSHID(handler.getInitialSSIPNodeFromDB(new SSIPNode("Rare_Disease", se.getId())));            
                        } else {
                            MeSHUI = se.getId();
                        }
                        if (allAssociations.containsKey(source)) {
                            Set<Pair> rels = allAssociations.get(source);
                            rels.add(new Pair(name, MeSHUI));
                            allAssociations.put(source, rels);
                        } else {
                            final Pair p = new Pair(name, MeSHUI);
                            allAssociations.put(source, new HashSet<Pair>() {
                                {
                                    add(p);
                                }
                            });
                        }
                    }
                }
            }

        }

        handler.finaliseDatabaseConnectionNoUPDATES();
        //3. Add all to file.
        
        BufferedWriter br = null;
        for (String source : allAssociations.keySet()) {
            try {
                br = new BufferedWriter(new FileWriter(fm.getIntegratedIndicationsFile() + fm.getFileSeparator() + source + ".txt"));
            } catch (IOException ex) {
                Logger.getLogger(ExtractHasIndications.class.getName()).log(Level.SEVERE, null, ex);
            }
            Set<Pair> indications = allAssociations.get(source);
            for (Pair p : indications) {
                try {
                    br.append(p.toString() + "\n");
                } catch (IOException ex) {
                    Logger.getLogger(ExtractHasIndications.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                br.close();
                LOGGER.log(Level.INFO, "Extracted {0} has_indications from {1} involving the {2} drugs of interest...", new Object[]{indications.size(), source, ids.size()});
            } catch (IOException ex) {
                Logger.getLogger(ExtractHasIndications.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public Set<SSIPNode> getSideEffects(SSIPNode drug) {

        SimpleTraversalExamples dm = new SimpleTraversalExamples();
        SSSStepDescription[] steps = null;
        steps = dm.getIndications();
        Map<String, MetaDataInterface> ses = handler.traversal(drug, steps);

        Set<SSIPNode> seNodes = new HashSet<SSIPNode>();

        for (String d : ses.keySet()) {
            seNodes.add((SSIPNode) ses.get(d));
        }
        return seNodes;
    }
    
      /**
     * Get the MeSHUi of a disease if it has one.
     *
     * @param disease
     * @return
     */
    public String getMeSHID(SSIPNode disease) {
        String MeSH = "NO_MESH";
        if (disease.getId().length() > 7) {
            Map<String, Object> props = disease.getAttributes();
                if (props.containsKey("MESH1")) {
                    MeSH = (String) props.get("MESH1");
                }
        } else {
            MeSH = disease.getId();
        }
        if(MeSH.equals("NO_MESH")){
            System.out.println(disease.getAttributes().toString());
        }
        return MeSH;
    }
}
