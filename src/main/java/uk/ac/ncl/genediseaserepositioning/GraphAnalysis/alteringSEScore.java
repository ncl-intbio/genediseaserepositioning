/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow.LeacockAndChodorow;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;

/**
 *
 * Class filters the mappings using each of the possible SE scores and outputs
 * to file.
 *
 * Also provides the ability to calculate precision/threshold and f-measure
 * score for each threshold- which is OP to a latex table for Supp Material.
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class alteringSEScore {

    private FileManager fman;
    private BufferedWriter bw;
    private LeacockAndChodorow leacock;
    private double[] thresholds;
    private Set<Pair> allKnownPairs;
    private Set<Pair> unfilteredPairs;
    private HashMap<Pair, Integer> unfilteredPairsPos;
    private final static Logger LOGGER = Logger.getLogger(alteringSEScore.class.getName());
    private int totalPos;
    private int totalInferred;

    public static void main(String[] args) {
        alteringSEScore a = new alteringSEScore();
        a.run();
    }

    public alteringSEScore() {

        this.unfilteredPairs = new HashSet<Pair>();
        this.unfilteredPairsPos = new HashMap<Pair, Integer>();
        this.fman = new FileManager();
        this.leacock = new LeacockAndChodorow(true);
        this.thresholds = leacock.getAllScores();
        try {
            this.bw = new BufferedWriter(new FileWriter(fman.getSEMapping_OPFolder() + fman.getFileSeparator() + "latexTable.txt"));
        } catch (IOException ex) {
            Logger.getLogger(alteringSEScore.class.getName()).log(Level.SEVERE, null, ex);
        }
        DrugDiseaseData dat = new DrugDiseaseData(DrugDiseaseData.DataSourceKNOWNS.ALL, DrugDiseaseData.DiseaseType.ALL, DrugDiseaseData.DiseaseCat.ALL);
        this.allKnownPairs = dat.getGS();
        LOGGER.log(Level.INFO, "There are {0} knowns", allKnownPairs.size());

    }

    public void run() {

        //get the unfiltered association
        getUnFiltered();

        //go through each threshold and do your thing- only up to 11 as it just stays the same after that   
        int count = 0;
        for (double score : thresholds) {
            count++;
            if (count > 11) {
                break;
            }
            filterAssociations(score);
            validateAssociations(score);
        }

        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(alteringSEScore.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void filterAssociations(double threshold) {
        FilterMappings filt = new FilterMappings(fman.getSEMapping_OPFolder().getPath(), "filterSE_" + threshold, null, ADMEGenes.ADMEGeneSet.EXTENDED, null, null, true, false, threshold, true);
        filt.extractAssociations();
    }

    public void getUnFiltered() {
        FilterMappings filt = new FilterMappings(fman.getSEMapping_OPFolder().getPath(), "filterSE_Unfiltered", null, ADMEGenes.ADMEGeneSet.EXTENDED, null, null, true, false, 20.0, true);
        filt.extractAssociations();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fman.getFilteredMappingsFolder().getPath() + fman.getFileSeparator() + "filterSE_Unfiltered" + "_UNIQUE.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(alteringSEScore.class.getName()).log(Level.SEVERE, null, ex);
        }
        String thisLine;
        int lineNumber = 0;
        Pair inferred = null;
        try {
            while ((thisLine = br.readLine()) != null) {
                lineNumber++;
                inferred = (new Pair(thisLine.split("\t")[0], thisLine.split("\t")[1]));
                unfilteredPairs.add(inferred);
                unfilteredPairsPos.put(inferred, lineNumber);
            }
        } catch (IOException ex) {
            Logger.getLogger(alteringSEScore.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void validateAssociations(double threshold) {

        int totalPruned = 0;
        int truePositive = 0;
        int falseNegative = 0;
        int falsePositive = 0;

        long rankPosTotal = 0;
        int prunedSEs = 0;

        Set<Pair> prunedAss = new HashSet<Pair>();
        HashMap<Pair, Integer> pAssPos = new HashMap<Pair, Integer>();

        Pair inferred = null;

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fman.getFilteredMappingsFolder().getPath() + fman.getFileSeparator() + "filterSE_" + threshold + "_UNIQUE.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(alteringSEScore.class.getName()).log(Level.SEVERE, null, ex);
        }
        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                inferred = (new Pair(thisLine.split("\t")[0], thisLine.split("\t")[1]));
                prunedAss.add(inferred);
            }
        } catch (IOException ex) {
            Logger.getLogger(alteringSEScore.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Pair p : allKnownPairs) {
            if (prunedAss.contains(p)) {
                truePositive++;
            }
        }

        //get the ranking pos of the side efffects pruned
        for (Pair se : unfilteredPairs) {
            if (!prunedAss.contains(se)) {
                int pos = unfilteredPairsPos.get(se);
                System.out.println(pos);
                rankPosTotal = rankPosTotal + pos;
                prunedSEs++;
            }
        }

        if (threshold == 1.0) {
            this.totalInferred = prunedAss.size();
            this.totalPos = truePositive;
        }

        falseNegative = totalPos - truePositive;
        falsePositive = prunedAss.size() - truePositive;

        double precision = (truePositive * 1.0) / prunedAss.size();
        double recall = (truePositive * 1.0) / (truePositive + falseNegative);

        DecimalFormat df = new DecimalFormat("#.####");
        try {
            bw.append(df.format(threshold) + "&" + prunedAss.size() + "&" + truePositive + "&" + falsePositive + "&" + falseNegative + "&" + df.format(precision) + "&" + df.format(recall) + "&" + df.format(fMeasure(precision, recall)) + "&" + rankPosTotal + "/" + prunedSEs + "(" + df.format(rankPosTotal / (prunedSEs * 1.0)) + ")\\\\n");


            System.out.println(df.format(threshold) + "&" + prunedAss.size() + "&" + truePositive + "&" + falsePositive + "&" + falseNegative + "&" + df.format(precision) + "&" + df.format(recall) + "&" + df.format(fMeasure(precision, recall)) + "&" + rankPosTotal + "/" + prunedSEs + "(" + (rankPosTotal / (prunedSEs * 1.0)) + ")\\\\n");
        } catch (IOException ex) {
            Logger.getLogger(alteringSEScore.class.getName()).log(Level.SEVERE, null, ex);
        }
        //delete the file
        File f = new File(fman.getFilteredMappingsFolder().getPath() + fman.getFileSeparator() + "filterSE_" + threshold + "_UNIQUE.txt");
        f.delete();
    }

    public double fMeasure(double precision, double recall) {
        return 2 * ((precision * recall) / (precision + recall));
    }
}
