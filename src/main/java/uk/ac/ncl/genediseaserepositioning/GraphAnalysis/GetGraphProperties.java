/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.MetaDataInterface;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.GraphMine.PropComparator.Comparator;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.Fetch;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class gets |V| and |G| of graph as well as connectivity.
 *
 */
public class GetGraphProperties {

    private Neo4J handler;
    private final static Logger LOGGER = Logger.getLogger(GetGraphProperties.class.getName());
    private HashMap<String, Integer> typeCount;
    private FileManager fman;
    private HashMap<String, Set<String>> nullNodesRels;

    public GetGraphProperties(Neo4J handler) {
        this.handler = handler;
        this.typeCount = new HashMap<String, Integer>();
        this.fman = new FileManager();
        this.nullNodesRels = new HashMap<String, Set<String>>();
    }

    public void traverse() {

        Set<String> nullNodes = new HashSet<String>();

        int nodes = 0;
        int edges = 0;

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(new File(fman.getSuppFolder() + fman.getFileSeparator() + "nodesWithNULLtype.txt")));
        } catch (IOException ex) {
            Logger.getLogger(GetGraphProperties.class.getName()).log(Level.SEVERE, null, ex);
        }


        handler.initialiseDatabaseConnection();
        Map<String, MetaDataInterface> node = handler.returnAllNodesMap();
        nodes = node.size();

        for (String name : node.keySet()) {
            SSIPNode ssip = handler.getInitialSSIPNodeFromDB(new SSIPNode(node.get(name).getType(), name.replaceFirst("UID", "")));
            String type = (String) ssip.getAttributes().get("type");
            if (type == null) {

                nullNodes.add(name.replaceFirst("UID", ""));
//                try {
//                    bw.append(ssip.getId()+"\t"+ssip.getAttributes().toString());
//                } catch (IOException ex) {
//                    Logger.getLogger(GetGraphProperties.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }
            if (typeCount.containsKey(type)) {
                int count = typeCount.get(type);
                count++;
                typeCount.put(type, count);
            } else {
                typeCount.put(type, 1);
            }

            Map<MetaDataInterface, SSIPRelationType> rels = ssip.getRelations();
            edges = edges + rels.size();
        }

        for (String name : node.keySet()) {
            SSIPNode ssip = handler.getInitialSSIPNodeFromDB(new SSIPNode(node.get(name).getType(), name.replaceFirst("UID", "")));
            Map<MetaDataInterface, SSIPRelationType> rels = ssip.getRelations();
            for (MetaDataInterface relNode : rels.keySet()) {
                for (String nul : nullNodes) {
                    if (relNode.getId().contains(nul)) {
                        if (nullNodesRels.containsKey(nul)) {//rels.get(relNode))          
                            Set<String> relTypes = nullNodesRels.get(nul);
                            relTypes.add(rels.get(relNode).getType()+"\t"+ssip.getId()+"\t"+ssip.getType());
                            nullNodesRels.put(nul, relTypes);
                        } else {
                            Set<String> relTypes = new HashSet<String>();
                            relTypes.add(rels.get(relNode).getType()+"\t"+ssip.getId()+"\t"+ssip.getType());
                            nullNodesRels.put(nul, relTypes);
                        }
                    }
                }

            }
        }

        for (String nod : nullNodesRels.keySet()) {
            try {
                bw.append("\n"+nod + "\t" + nullNodesRels.get(nod)+"\n\n");
            } catch (IOException ex) {
                Logger.getLogger(GetGraphProperties.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(GetGraphProperties.class.getName()).log(Level.SEVERE, null, ex);
        }


        handler.finaliseDatabaseConnectionNoUPDATES();
        LOGGER.log(Level.INFO, "All types count :\n{0}", typeCount.toString());
        LOGGER.log(Level.INFO, "Graph has {0} nodes", nodes);
        LOGGER.log(Level.INFO, "Graph has {0} edges", edges);

    }
}
