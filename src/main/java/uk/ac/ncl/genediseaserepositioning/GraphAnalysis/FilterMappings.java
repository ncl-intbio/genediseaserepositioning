/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.MeSHMapping.GetMeSHUI;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DiseaseCat;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DiseaseType;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.GraphAnalysis.ADMEGenes.ADMEGeneSet;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.MappingTextSummary.GSKPhaseIII;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes a disease type (i.e. rare or common) a disease category (i.e. C10
 * or C20) and a side effect threshold and outputs associations to file for
 * further analysis.
 *
 * ONLY take the highest scoring example of the association.
 */
public class FilterMappings {

    private DiseaseCat diseaseCat;
    private DiseaseType diseaseType;
    private ADMEGeneSet geneSet;
    private Set<String> ADMEgenes;
    private String gene;
    private BufferedWriter opfileALL;
    private BufferedWriter opfileUNIQUE;
    private BufferedWriter unmappedDiseases;
    private final static Logger LOGGER = Logger.getLogger(FilterMappings.class.getName());
    private String opFileName;
    private HashMap<Pair, Double> uniqueAss_HS;
    private double seScore;
    private boolean removeDirection;
    private Set<Pair> goF;
    private Set<Pair> loF;
    private GetMeSHUI getMeSHUI;
    private Map<String, Set<String>> alreadyMapped;
    private Serialize ser;
    private FileManager fman;
    private HashMap<String, Integer> MeSHCount;
    private boolean MeShDistr;
    private HashMap<Pair, GSKPhaseIII> uniqueMappings;
    private boolean recordUnMappedDiseases;
    private boolean justUnique;

    public static void main(String[] args) {

        //get the MeSH distribution
        FileManager fm = new FileManager();
        FilterMappings ex2 = new FilterMappings(fm.getFilteredMappingsFolder().getPath(), "MeSH_distr.txt", null, ADMEGeneSet.EXTENDED, null, null, true, true, 0.768, true);
        //null, ADMEGeneSet.EXTENDED, disease[i], null, true, false, threshold, true
        ex2.extractAssociations();
    }

    public FilterMappings(String opDir, String opFileName, DiseaseCat cat, ADMEGeneSet admegeneSet, DiseaseType type, String gene, boolean removeDirection, boolean MeSHDistr, double threshold, boolean justUnique) {
        this.justUnique = justUnique;
        this.uniqueMappings = new HashMap<Pair, GSKPhaseIII>();
        this.ser = new Serialize();
        this.fman = new FileManager();
        this.MeShDistr = MeSHDistr;
        this.geneSet = admegeneSet;
        if (admegeneSet != null) {
            ADMEgenes = geneSet.getGeneSet();
        }
        this.opFileName = opFileName;
        this.seScore = threshold;
        this.uniqueAss_HS = new HashMap<>();
        this.diseaseCat = cat;
        this.diseaseType = type;
        this.removeDirection = removeDirection;
        this.alreadyMapped = new HashMap<>();
        this.getMeSHUI = new GetMeSHUI();
        this.goF = new HashSet<>();
        this.loF = new HashSet<>();
        if (removeDirection) {
            getGoFLoFAssociations(fman.getGOFFile());
            getGoFLoFAssociations(fman.getLOFFile());
            LOGGER.log(Level.INFO, "We have {0} GOF associations...", goF.size());
            LOGGER.log(Level.INFO, "We have {0} LOF associations...", loF.size());
        }
        this.gene = gene;
        if (MeShDistr == false) {
            if (justUnique == false) {
                try {
                    this.opfileALL = new BufferedWriter(new FileWriter(new File(fman.getFilteredMappingsFolder() + fman.getFileSeparator() + opFileName + "_ALL.txt")));
                } catch (IOException ex) {
                    Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                this.opfileUNIQUE = new BufferedWriter(new FileWriter(new File(fman.getFilteredMappingsFolder() + fman.getFileSeparator() + opFileName + "_UNIQUE.txt")));
            } catch (IOException ex) {
                Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        this.MeSHCount = new HashMap<>();

        if (diseaseType == null && diseaseCat == null) {
            recordUnMappedDiseases = true;
        }

        if (recordUnMappedDiseases) {
            try {
                unmappedDiseases = new BufferedWriter(new FileWriter(fman.getFilteredMappingsFolder() + fman.getFileSeparator() + "FILTERED_unmappedDiseases.txt"));
            } catch (IOException ex) {
                Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void extractAssociations() {

        int adme = 0;
        int goflof = 0;
        int se = 0;

        int added = 0;

        File[] files = fman.getMappingsFolder().listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                File[] files2 = file.listFiles();
                for (File file2 : files2) {

                    if (file2.getAbsoluteFile().getAbsolutePath().endsWith(".txt")) {
                        // LOGGER.log(Level.INFO, "Checking file {0} ...", file);
                        BufferedReader br = null;
                        try {
                            br = new BufferedReader(new FileReader(file2));
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        String thisLine;
                        try {
                            while ((thisLine = br.readLine()) != null) {
                                GSKPhaseIII mapping = new GSKPhaseIII(thisLine);
                                boolean add = true;
                                if (diseaseCat != null && add) {
                                    boolean yep = false;
                                    Set<String> cats = mapping.getDiseaseMeshTree();
                                    for (String c : cats) {
                                        if (c.startsWith(diseaseCat.getText())) {
                                            yep = true;
                                            break;
                                        }
                                    }
                                    add = yep;
                                }
                                if (diseaseType != null && add) {
                                    if (!mapping.getDiseaseType().equals(diseaseType.getText())) {
                                        add = false;
                                    }
                                }
                                //if the disease type = null then there has been a problem mapping- dp not include
                                if (mapping.getDiseaseType().equals("null")) {
                                    add = false;
                                    if (recordUnMappedDiseases) {
                                        unmappedDiseases.append(mapping.getDrugName() + "\t" + mapping.getGeneName() + "\t" + mapping.getDiseaseMeSH() + "\n");
                                    }
                                }
                                if (gene != null && add) {
                                    if (!mapping.getGeneName().equals(gene)) {
                                        add = false;
                                    }

                                }
                                if (geneSet != null) {
                                    if (ADMEgenes.contains(mapping.getGeneName())) {
                                        adme++;
                                        add = false;
                                    }

                                }
                                if (removeDirection) {

                                    if (checkDir(mapping) == false) {
                                        add = false;
                                        goflof++;
                                    }
                                }

                                if ((seScore < 10.0) && (mapping.getMapSEScore() >= seScore)) {
                                    se++;
                                    add = false;
                                }

                                if (add) {
                                    Pair p = new Pair(mapping.getDrugName(), mapping.getDiseaseMeSH());
                                    if (uniqueAss_HS.containsKey(p)) {
                                        if (mapping.getSubScore() > uniqueAss_HS.get(p)) {
                                            uniqueAss_HS.put(p, mapping.getSubScore());
                                        } else {
                                            //nowt
                                        }
                                    } else {
                                        uniqueAss_HS.put(p, mapping.getSubScore());
                                    }

                                    if ((MeShDistr == false)) {
                                        if (justUnique == false) {
                                            try {
                                                opfileALL.append(thisLine + "\n");
                                            } catch (IOException ex) {
                                                Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                        }
                                        added++;
                                    }
                                    uniqueMappings.put(p, mapping);
                                }
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }

                }
            }
        }

        LOGGER.info("Parsed all mappings...  now identifiying unique drug-disease associations and ranking...");
        //System.out.println(uniqueAss_HS.toString());
        List<Double> allScores = new ArrayList<>();
        for (Pair p : uniqueAss_HS.keySet()) {
            if (!allScores.contains(uniqueAss_HS.get(p))) {
                allScores.add(uniqueAss_HS.get(p));
            }
        }
        Collections.sort(allScores);
        if (MeShDistr == false) {
            for (int i = allScores.size() - 1; i > -1; i--) {
                for (Pair p : uniqueAss_HS.keySet()) {
                    if (Double.compare(allScores.get(i), uniqueAss_HS.get(p)) == 0) {
                        try {
                            opfileUNIQUE.append(p.getEntrezGeneSymbol() + "\t" + p.getMeSHUI() + "\t" + uniqueAss_HS.get(p) + "\n");
                        } catch (IOException ex) {
                            Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }

        if (MeShDistr) {
            for (Pair p : uniqueMappings.keySet()) {
                Set<String> disAreas = new HashSet<String>();
                for (String meshtreeterm : uniqueMappings.get(p).getDiseaseMeshTree()) {
                    String[] split = meshtreeterm.split("\\.");
                    String mesharea = meshtreeterm;
                    if (split.length > 1) {
                        mesharea = split[0].trim();
                        disAreas.add(mesharea);
                    }
                }
                for (String dis : disAreas) {
                    if (MeSHCount.containsKey(dis)) {
                        int count = MeSHCount.get(dis);
                        MeSHCount.put(dis, count + 1);
                    } else {
                        MeSHCount.put(dis, 1);
                    }
                }
            }

            LOGGER.info("Writing associations to file...");
            BufferedWriter br = null;
            try {
                br = new BufferedWriter(new FileWriter(new File(fman.getFilteredMappingsFolder() + fman.getFileSeparator() + "MeSH_Distribution_Mappings")));
            } catch (IOException ex) {
                Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
            }
            Map<String, Integer> map = new TreeMap<String, Integer>(MeSHCount);
            for (String mesh : map.keySet()) {
                try {
                    br.append(mesh + "\t" + MeSHCount.get(mesh) + "\n");
                } catch (IOException ex) {
                    Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            if (justUnique == false) {
                try {
                    opfileALL.close();
                } catch (IOException ex) {
                    Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                opfileUNIQUE.close();
            } catch (IOException ex) {
                Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (recordUnMappedDiseases) {
                try {
                    unmappedDiseases.close();
                } catch (IOException ex) {
                    Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        LOGGER.log(Level.INFO, "{0} associations filtered due to involving ADME genes", adme);
        LOGGER.log(Level.INFO, "{0} associations filtered due to involving GOFLOF functionality ", goflof);
        LOGGER.log(Level.INFO, "{0} associations filtered due to high possiblility of being a SE ", se);

        LOGGER.log(Level.INFO, "Added {0} associations to {1} of which {2} are unique", new Object[]{added, opFileName, uniqueAss_HS.size()});
    }

    public boolean checkDir(GSKPhaseIII mapping) {

        String drugMech = mapping.getDrugmech();
        String DBdrugKnown = mapping.getDBknownAction();
        String DBdrugAction = mapping.getDBaction();
        String gene = mapping.getGeneName();
        String diseaseMeSH = mapping.getDiseaseMeSH();
        String diseaseName = mapping.getDiseaseName();

        boolean safe = true;
        if (goF.contains(new Pair(gene, diseaseMeSH))) {
            //System.out.println("GOF ASSOCIATION--\t"+drugMech+"\t"+DBdrugKnown+"\t"+DBdrugAction);
            //if any of the drugMechanisms from ChEMBL match the funtion mutation return false         
            switch (drugMech.toLowerCase()) {
                case "agonist":
                    return false;
                case "activator":
                    return false;
                case "partial agonist":
                    return false;
                case "positive allosteric modulator":
                    return false;
                case "opener":
                    return false;
                case "positive modulator":
                    return false;
            }
            //if any of the DBassociations macth the function mutation return false
            switch (DBdrugAction.toLowerCase()) {
                case "agonist":
                    return false;
                case "stimulator":
                    return false;
                case "activator":
                    return false;
                case "partial agonist":
                    return false;
                case "positive modulator":
                    return false;
                case "allosteric modulator":
                    return false;
            }
        } else if (loF.contains(new Pair(gene, diseaseMeSH)) || diseaseName.toLowerCase().contains("deficiency")) {
            //System.out.println("LOF ASSOCIATION--\t"+drugMech+"\t"+DBdrugKnown+"\t"+DBdrugAction);
            //if any of the drugMechanisms from ChEMBL contradict the funtion mutation return false 
            switch (drugMech.toLowerCase()) {
                case "antagonist":
                    return false;
                case "inhibitor":
                    return false;
                case "blocker":
                    return false;
                case "negative allosteric modulator":
                    return false;
            }

            //if any of the DBassociations macth the function mutation return false
            switch (DBdrugAction.toLowerCase()) {
                case "antagonist":
                    return false;
                case "inhibitor":
                    return false;
                case "inverse agonist":
                    return false;
                case "negative modulator":
                    return false;
                case "allosteric modulator":
                    return false;
                case "partial antagonist":
                    return false;
                case "inhibitor, competitive":
                    return false;
                case "suppressor":
                    return false;
            }

        } else {
            return true;
        }
        return safe;

    }

    public void getGoFLoFAssociations(File file) {

        if (file.getName().contains("GOF_hc")) {
            if (new File(fman.getSerializedFolder() + fman.getFileSeparator() + "GOF.ser").exists()) {
                goF = ser.deserializeSetPair(fman.getSerializedFolder() + fman.getFileSeparator() + "GOF.ser");
            } else {
                int count = 0;

                BufferedReader br = null;
                try {
                    br = new BufferedReader(new FileReader(file));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                }
                String line;

                //br.readLine();
                String geneHUGO = null;
                String diseaseMeSHTree = null;
                Set<String> diseaseMeSHUI = null;
                try {
                    while ((line = br.readLine()) != null) {
                        count++;
                        if (count % 1000 == 0) {
                            System.out.println(count);
                        }
                        if (!line.isEmpty()) {
                            //System.out.println(line);
                            String[] split = line.split("\t");
                            diseaseMeSHTree = split[2];
                            //String diseaseMeSHUI = "blah";
                            // System.out.println(diseaseMeSHTree);
                            if (alreadyMapped.containsKey(diseaseMeSHTree)) {
                                diseaseMeSHUI = alreadyMapped.get(diseaseMeSHTree);
                            } else {
                                diseaseMeSHUI = getMeSHUI.getDUI(diseaseMeSHTree);
                                alreadyMapped.put(diseaseMeSHTree, diseaseMeSHUI);
                            }
                            geneHUGO = split[4];
                            for (String m : diseaseMeSHUI) {
                                Pair p = new Pair(geneHUGO, m);
                                if (file.getName().contains("GOF_hc")) {
                                    goF.add(p);
                                } else {
                                    loF.add(p);
                                }
                            }

                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    br.close();
                } catch (IOException ex) {
                    Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
                }

                ser.SerializeSetPair(goF, fman.getSerializedFolder() + fman.getFileSeparator() + "GOF.ser");
            }

        } else if (new File(fman.getSerializedFolder() + fman.getFileSeparator() + "LOF.ser").exists()) {
            loF = ser.deserializeSetPair(fman.getSerializedFolder() + fman.getFileSeparator() + "LOF.ser");
        } else {
            int count = 0;

            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(file));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
            }
            String line;

            //br.readLine();
            String geneHUGO = null;
            String diseaseMeSHTree = null;
            Set<String> diseaseMeSHUI = null;
            try {
                while ((line = br.readLine()) != null) {
                    count++;
                    if (count % 1000 == 0) {
                        System.out.println(count);
                    }
                    if (!line.isEmpty()) {
                        //System.out.println(line);
                        String[] split = line.split("\t");
                        diseaseMeSHTree = split[2];
                        //String diseaseMeSHUI = "blah";
                        // System.out.println(diseaseMeSHTree);
                        if (alreadyMapped.containsKey(diseaseMeSHTree)) {
                            diseaseMeSHUI = alreadyMapped.get(diseaseMeSHTree);
                        } else {
                            diseaseMeSHUI = getMeSHUI.getDUI(diseaseMeSHTree);
                            alreadyMapped.put(diseaseMeSHTree, diseaseMeSHUI);
                        }
                        geneHUGO = split[4];
                        for (String m : diseaseMeSHUI) {
                            Pair p = new Pair(geneHUGO, m);
                            if (file.getName().contains("GOF_hc")) {
                                goF.add(p);
                            } else {
                                loF.add(p);
                            }
                        }

                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(FilterMappings.class.getName()).log(Level.SEVERE, null, ex);
            }
            ser.SerializeSetPair(loF, fman.getSerializedFolder() + fman.getFileSeparator() + "LOF.ser");
        }
    }
}
