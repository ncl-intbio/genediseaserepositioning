/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import com.google.common.collect.Sets;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class ADMEGenes {

    private static FileManager fman = new FileManager();

    public static void main(String[] args) {

        Set<String> core = ADMEGeneSet.CORE.getGeneSet();

        System.out.println(core.size());
        System.out.println(core.toString());

        Set<String> extended = ADMEGeneSet.EXTENDED.getGeneSet();

        System.out.println(extended.size());
        System.out.println(extended.toString());

    }

    public ADMEGenes() {
    }

    public enum ADMEGeneSet {

        //data taken from pharmadme.org (accessed 27th May 2015) 
        CORE(parseGenes(fman.getADMECore())),
        EXTENDED(parseGenesEXTENDED());
        private Set<String> ADMEGeneSet;

        ADMEGeneSet(Set<String> text) {
            this.ADMEGeneSet = text;
        }

        public Set<String> getGeneSet() {
            return this.ADMEGeneSet;
        }

        public static HashSet<String> parseGenesEXTENDED() {
            HashSet<String> allGenes = new HashSet<>();
            allGenes.addAll(parseGenes(fman.getADMECore()));
            allGenes.addAll(parseGenes(fman.getADMEExtended()));
            return allGenes;
        }

        public static HashSet<String> parseGenes(File file) {
            HashSet<String> genes = new HashSet<>();
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(file));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ADMEGenes.class.getName()).log(Level.SEVERE, null, ex);
            }
            String line;
            try {
                while ((line = br.readLine()) != null) {
                    genes.add(line.trim());
                }
            } catch (IOException ex) {
                Logger.getLogger(ADMEGenes.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(ADMEGenes.class.getName()).log(Level.SEVERE, null, ex);
            }
            return genes;
        }
    }
}
