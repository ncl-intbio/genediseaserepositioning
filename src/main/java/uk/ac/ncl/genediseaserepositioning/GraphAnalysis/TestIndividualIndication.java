/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow.LeacockMatrixDrug;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Test to see if a single inferred association maps to any of the knowns above
 * a given threshold.
 */
public class TestIndividualIndication {

    private LeacockMatrixDrug local;
    private FileManager fm;
    private Serialize ser;

    public static void main(String[] args) {

        TestIndividualIndication t = new TestIndividualIndication();
        Set<Pair> c10 = new HashSet<Pair>();

        Pair p = new Pair("DB01054", "D020514");
        c10.add(p);

        Pair p1 = new Pair("DB01068", "D020190");
        c10.add(p1);

        Pair p2 = new Pair("DB00834", "C537017");
        c10.add(p2);

        Pair p3 = new Pair("DB01043", "D018887");
        c10.add(p3);

        Pair p4 = new Pair("DB01200", "C536096");
        c10.add(p4);

        Pair p6 = new Pair("DB01656", "C538179");
        c10.add(p6);

        Pair p7 = new Pair("DB00722", "D000544");
        c10.add(p7);

        Pair p8 = new Pair("DB01656", "D020521");
        c10.add(p8);

        Pair p9 = new Pair("DB00897", "C565811");
        c10.add(p9);

        for (Pair ps : c10) {
            System.out.println(ps.toString()+"\t"+t.getScoreToKnown(ps));
        }
    }

    public TestIndividualIndication() {

        this.fm = new FileManager();
        this.ser = new Serialize();
    }

    public boolean getScoreToKnown(Pair p) {
        local = ser.deserializeLeacockMatrix(fm.getLeacockScores_OPFolder() + fm.getFileSeparator() + p.getEntrezGeneSymbol() + ".ser");
        return local.testMapPredicted(0.7, local.getPredPos(p.toString()));
        //getScore(local.getPredPos(p.toString()), local.getPredPos(pinf.toString())));
    }
}
