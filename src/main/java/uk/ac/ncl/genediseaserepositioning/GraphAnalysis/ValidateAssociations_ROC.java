/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.Hypergeometric.CalcGeometric;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.ROC_D_Value;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DataSourceKNOWNS;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData;

/**
 *
 * Class takes the filtered associations and a gold standard, a disease category
 * and a disease type and produces the output for a ROC curve. We want a point
 * at every score.
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class ValidateAssociations_ROC {

    private Set<Pair> goldStandard;
    private String scoredAssociationsFile;
    private String outPut;
    private int truePositives;
    private int falsePositives;
    private final static Logger LOGGER = Logger.getLogger(ValidateAssociations_Hyper.class.getName());
    private BufferedWriter bw;
    private Map<Double, Set<Pair>> scoredAssociations;
    private List<Double> scores;
    private Set<Pair> sample;
    private int check;
    private FileManager fm;
    private DrugDiseaseData.DataSourceKNOWNS datasource;
    private DrugDiseaseData indicationInfo;
    private double[] scoresToPLot;
    
    private final int POINTS= 2000;

    public static void main(String[] args) {
    
    }

    public ValidateAssociations_ROC(String associationsFile, String project, DataSourceKNOWNS dataSource, DrugDiseaseData.DiseaseType disType, DrugDiseaseData.DiseaseCat disCat) {


        this.datasource = dataSource;
        this.scoredAssociationsFile = associationsFile;
        this.fm = new FileManager();
        String output = fm.getROC_OPFolder().getPath() + fm.getFileSeparator() + project;
        File f = new File(output);
        // if the directory does not exist, create it
        if (!f.exists()) {
            LOGGER.log(Level.INFO, "Creating directory... {0}", output);
            boolean result = false;

            try {
                f.mkdir();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                LOGGER.log(Level.INFO, "Created directory... {0}", output);
            }
        }

        File in = new File(scoredAssociationsFile);

        this.outPut = output + "/" + datasource.getText() + "_" + in.getName().replace(".txt", "") + "_ROC.txt";
        this.goldStandard = new HashSet<>();
        this.scoredAssociations = new HashMap<>();
        this.sample = new HashSet<>();
        this.check = 0;
        this.indicationInfo = new DrugDiseaseData(dataSource, disType, disCat);
        this.scoresToPLot = new double[POINTS];
    }

    public void getKnowns() {
        this.goldStandard = indicationInfo.getPruned();
    }

    /**
     * First of all get all the true positives of the score associations
     */
    public void getAllAssociations() {
        LOGGER.log(Level.INFO, "Getting scored associations from ...{0}", scoredAssociationsFile);
        BufferedReader br = null;
        int count = 0;
        //variables
        String from;
        String fromID;
        String to;
        double score;
        this.truePositives = 0;
        this.falsePositives = 0;
        try {
            //BufferedReader br;
            br = new BufferedReader(new FileReader(new File(scoredAssociationsFile)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(uk.ac.ncl.genediseaserepositioning.GraphAnalysis.ValidateAssociations_Hyper.class.getName()).log(Level.SEVERE, null, ex);
        }

        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                String[] split = thisLine.split("\t");
                from = split[0];
                to = split[1];
                score = Double.parseDouble(split[2]);
                count++;
                Pair pair = new Pair(from, to);
                //System.out.println("Checking pair:: "+ pair.toString());
                if (goldStandard.contains(pair)) {
                    truePositives++;
                } else {
                    falsePositives++;
                }

                if (scoredAssociations.containsKey(score)) {
                    Set<Pair> pairs = scoredAssociations.get(score);
                    pairs.add(pair);
                    scoredAssociations.put(score, pairs);
                } else {
                    Set<Pair> pairs = new HashSet<>();
                    pairs.add(pair);
                    scoredAssociations.put(score, pairs);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(uk.ac.ncl.genediseaserepositioning.GraphAnalysis.ValidateAssociations_Hyper.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO,
                "We have {0} different scored associations ...", count);
        LOGGER.log(Level.INFO,
                "We have {0} different scores for associations ...", scoredAssociations.keySet().size());

        Set<Double> allScores = scoredAssociations.keySet();
        List<Double> sortedScores = new ArrayList<>();
        for (double scored : allScores) {
            sortedScores.add(scored);
        }

        Collections.sort(sortedScores);
        this.scores = sortedScores;
       
    }

    public void getScoresToPlot() {

        double size = scores.get(scores.size() - 1) / (scoresToPLot.length * 1.0);

        for (int i = 0; i < scoresToPLot.length; i++) {
            scoresToPLot[i] = size * (i + 1);
        }

        for (int i = 0; i < scoresToPLot.length; i++) {
            System.out.println(scoresToPLot[i] + "\t" + i);
        }
    }

    /**
     * Export the values to allow for plotting in r
     */
    public void calculateValues() {
        LOGGER.info("Calculating values to plot...");

        try {
            bw = new BufferedWriter(new FileWriter(outPut));

        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < scoresToPLot.length; i++) {
            calculatePoint(getAllPairsLessThanScore(i),getAllPairsGreaterThanScore(i), i);
        }
        try {
            bw.close();

        } catch (IOException ex) {
            Logger.getLogger(CalcGeometric.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Plot values have been stored in {0}", outPut);
    }

    /**
     * @return The set of Pairs that are all less than or equal to the score
     * being looked at.
     */
    public Set<Pair> getAllPairsLessThanScore(int p) {
        Set<Pair> allPairs = new HashSet<Pair>();
        for (int i = 0; i < scores.size(); i++) {
            if (scores.get(i) <= scoresToPLot[p]) {
                allPairs.addAll(scoredAssociations.get(scores.get(i)));
            }

        }
        return allPairs;
    }
    
    /**
     * @return The set of Pairs that are all less than or equal to the score
     * being looked at.
     */
    public Set<Pair> getAllPairsGreaterThanScore(int p) {
        Set<Pair> allPairs = new HashSet<Pair>();
        for (int i = 0; i < scores.size(); i++) {
            if (scores.get(i) > scoresToPLot[p]) {
                allPairs.addAll(scoredAssociations.get(scores.get(i)));
            }

        }
        return allPairs;
    }

    public void calculatePoint(Set<Pair> allPairsLess, Set<Pair> allPairsGreater, double score) {
        
        int falseNegative = 0;
        int trueNegative = 0;
        int truePositive = 0;
        int falsePositive =0;

        for (Pair p : allPairsLess) {
            if (goldStandard.contains(p)) {
                falseNegative++;
            } else {
                trueNegative++;      
            }
        }
        
        for (Pair p : allPairsGreater) {
            if (goldStandard.contains(p)) {
                truePositive++;
            } else {
                falsePositive++;      
            }
        }

        double sensitivity = truePositive/ (truePositive+(falseNegative*1.0));//tp / (dm.getGoldStandard().getTruePositives().size() * 1.0);
        double specificity = trueNegative/ (trueNegative+(falsePositive*1.0));//(negClassed - (dm.getGoldStandard().getTruePositives().size() - tp)
        try {
            bw.append(sensitivity+"\t"+specificity+"\n");                   
                   //falseNegative+ "\t" + trueNegative+ "\t" + truePositive+ "\t" + falsePositive+ "\t"+ score + "\t" +(allPairsLess.size()+allPairsGreater.size())+"\n");
            //System.out.println(allPairs.size() + "\t" + score);
        } catch (IOException ex) {
            Logger.getLogger(ROC_D_Value.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
