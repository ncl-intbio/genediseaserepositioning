/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphAnalysis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.MeSHUI2MeSHTree;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.SSSStepDescription;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.MetaDataInterface;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphMine.PropComparator.Comparator;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.Fetch;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode.ApprovedSmallMolecule;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode.RuleNode;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * Class identifies all diseases that are treatable with approved
 * small_molecules.
 *
 * @author joe
 */
public class MeSH_CalcTreatableDiseases {

    private Neo4J backend;
    private Map<String, String> drugIds;
    private String type;
    private FileManager fman;
    private HashMap<String, Set<String>> toPlot;
    private HashMap<String, Set<String>> diseaseFreq;
    private final static Logger LOGGER = Logger.getLogger(MeSH_CalcTreatableDiseases.class.getName());
    private int indicationCount = 0;
    private int parentterms = 0;
    private Set<String> startsWith;
    private int depth;
    private File opFile;
    private MeSHUI2MeSHTree meshMap;
    private Set<String> MeSHUIs;
    private Serialize ser;

    public static void main(String[] args) {

        Set<String> highLevel = new HashSet<String>(Arrays.asList("C", "F01", "F02", "F03"));
        FileManager fm = new FileManager();
        Neo4J backend = new Neo4J(fm.getGraphLocation().toString());

        MeSH_CalcTreatableDiseases eg = new MeSH_CalcTreatableDiseases(backend, 1, highLevel, new File("druggable_disease.txt"));
        eg.getAllDrugs();
        eg.getAllIndications();
        eg.getNumberOfDiseases();
        try {
            eg.writeToFile();
        } catch (IOException ex) {
            Logger.getLogger(MeSH_CalcTreatableDiseases.class.getName()).log(Level.SEVERE, null, ex);
        }

        //        Set<String> highNSD = new HashSet<String>(Arrays.asList("C10"));
        //        Set<String> immunology = new HashSet<String>(Arrays.asList("C20"));
        //
        //        String opFile = "C20_Treatable.txt";
    }

    public MeSH_CalcTreatableDiseases(Neo4J handler, int depth, Set<String> startsWith, File opFile) {
        this.startsWith = startsWith;
        //this.meshName = new MeSHNames();
        this.fman = new FileManager();
        this.backend = handler;
        this.toPlot = new HashMap<String, Set<String>>();
        this.diseaseFreq = new HashMap<String, Set<String>>();
        this.depth = depth;
        this.opFile = opFile;
        this.ser = new Serialize();
        this.meshMap = new MeSHUI2MeSHTree();
        this.MeSHUIs = ser.deserializeSetString(fman.getSuppFolder() + fman.getFileSeparator() + "MeSH_meshuis.ser");
    }

    /**
     * Get all drugs from the network.
     */
    public void getAllDrugs() {
        RuleNode smallMol = new ApprovedSmallMolecule();
        smallMol.createRuleNode();
        Fetch initSet = new Fetch(smallMol, backend);
        initSet.goFetch();
        drugIds = initSet.getSuccIDs();
        LOGGER.log(Level.INFO, "Found {0} approved small_molecules in the graph", drugIds.size());
    }

    /**
     * For each drug go through all the diseases that it is marketed to treat.
     *
     */
    public void getAllIndications() {

        backend.initialiseDatabaseConnection();

        for (String name : drugIds.keySet()) {
            String drugtype = drugIds.get(name);
            //System.out.println("Drug:: " + drugtype + "\t" + name);
            SSSStepDescription[] steps = traversalDef();
            Map<String, MetaDataInterface> diseases = backend.traversal(new SSIPNode(drugtype, name), steps);
            //System.out.println("Indicated to treat ... " + diseases.size());
            for (String s : diseases.keySet()) {
                if (!diseases.get(s).getId().equals(name)) {
                    indicationCount++;
                    Map<String, Object> atts = diseases.get(s).getAttributes();
                    String UI = "NONE";
                    if (atts.containsKey("MeSH")) {
                        UI = (String) atts.get("MeSH");
                    }
                    for (String attname : atts.keySet()) {
                        if (attname.startsWith("MeSHTree")) {
                            String tree = (String) atts.get(attname);
                            if (diseaseCheck(tree)) {
                                String parentterm = getMeshTreeForClass(tree);
                                //System.out.println(parentterm + "\t" + UI);
                                addMeshTerm(parentterm, UI);
                                parentterms++;
                            }
                        }
                    }
                }

            }
        }

        backend.finaliseDatabaseConnectionNoUPDATES();

        LOGGER.log(Level.INFO, "Checked {0} unique indications ", indicationCount);
        LOGGER.log(Level.INFO, "Which mapped to {0} MeSH parent terms", parentterms);

    }

    /**
     * Go through all diseases in the graph and see how many there are for each
     * category.
     */
    private void getNumberOfDiseases() {
        for (String UI : MeSHUIs) {
            Set<String> meshtrees = meshMap.getTreeTerms(UI);
            for (String tree : meshtrees) {
                if (diseaseCheck(tree)) {
                    String parentterm = getMeshTreeForClass(tree);
                    diseaseFreq(parentterm, UI);
                    parentterms++;
                }
            }
        }
    }

    /**
     * Check to see if the disease starts with the desired ID.
     *
     * @param treeterm
     * @return
     */
    private boolean diseaseCheck(String treeterm) {

        boolean include = false;
        for (String s : startsWith) {
            if (treeterm.startsWith(s)) {
                return true;
            }
        }
        return include;

    }

    /**
     * Add the mesh term to the data count for that branch of the hierarchy.
     *
     * @param meshTerm
     */
    public void addMeshTerm(String meshTerm, String meshUI) {

        if (toPlot.containsKey(meshTerm)) {
            Set<String> treatedDis = toPlot.get(meshTerm);
            treatedDis.add(meshUI);
            toPlot.put(meshTerm, treatedDis);
        } else {
            Set<String> treatedDis = new HashSet<String>();
            treatedDis.add(meshUI);
            toPlot.put(meshTerm, treatedDis);
        }
    }

    /**
     * Add the mesh term to the data count for that branch of the hierarchy.
     *
     * @param meshTerm
     */
    public void diseaseFreq(String meshTerm, String meshUI) {

        if (diseaseFreq.containsKey(meshTerm)) {
            Set<String> treatedDis = diseaseFreq.get(meshTerm);
            treatedDis.add(meshUI);
            diseaseFreq.put(meshTerm, treatedDis);
        } else {
            Set<String> treatedDis = new HashSet<String>();
            treatedDis.add(meshUI);
            diseaseFreq.put(meshTerm, treatedDis);
        }
    }

    /**
     * Get the mesh tree term.
     */
    public String getMeshTreeForClass(String allTrees) {

        String[] branches = allTrees.split("\\.");
        if (branches.length < depth) {
            return allTrees;
        }
        String branch = "FIRST";
        for (int p = 0; p < depth; p++) {
            if (branch.equals("FIRST")) {
                branch = branches[p];
            } else {
                branch = branch.trim() + "." + branches[p];
            }
        }
        return branch;
    }

    /**
     * Write all of the counts to file.
     */
    private void writeToFile() throws IOException {

        DecimalFormat df = new DecimalFormat("#.##");

        BufferedWriter bw = new BufferedWriter(new FileWriter(opFile));
        for (String name : toPlot.keySet()) {
            Set<String> count = toPlot.get(name);
            Set<String> freqcount = diseaseFreq.get(name);
            double percent = (100.0 / freqcount.size()) * (count.size() * 1.0);
            bw.append(name + "\t" + count.size() + "\t" + freqcount.size() + "\t" + df.format(percent) + "\t" + (freqcount.size() - count.size()) + "\n");
        }

        bw.close();
    }

    /**
     * Traversal definition- step out from a drug on all has_indication
     * associations with a depth of 1.
     *
     * @return
     */
    public SSSStepDescription[] traversalDef() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.HAS_INDICATION.getText());
        return steps;
    }
}
