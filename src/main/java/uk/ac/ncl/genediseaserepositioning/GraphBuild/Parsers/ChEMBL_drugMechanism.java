/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.Serialize;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Parser integrates activity type data about a drug. This data was extracted
 * from the SQL dump of chembl using the VM set-up on the windows machine.
 *
 */
public class ChEMBL_drugMechanism {

    private String smallMoleculeName;
    private String bioTechName;
    private String bindsToRel;
    private Map<String, String> DBID2Type;
    private Map<String, String> chembl2db;
    private File unichemMapping;
    private Set<String> allMechanisms;

    private FileManager fmap;
    private Serialize ser;
    private File filePath;
    private Neo4J handler;
    

    private final static Logger LOGGER = Logger.getLogger(ChEMBL_drugMechanism.class.getName());

    public ChEMBL_drugMechanism(Neo4J handler) {

        this.handler = handler;
        this.bioTechName = DReNInMetadata.NodeType.BIOTECH.getText();;
        this.smallMoleculeName = DReNInMetadata.NodeType.SMALL_MOLECULE.getText();
        this.allMechanisms = new HashSet<>();
        this.fmap = new FileManager();
        this.ser = new Serialize();
        this.filePath = fmap.getChEMBLDrugMech();
        this.DBID2Type = ser.deserializeHashMap(fmap.getSuppFolder() + fmap.getFileSeparator()+ "DrugBank_DBID2Type.ser");
        this.unichemMapping = fmap.getUniChemsrc1src2();
        this.chembl2db = new HashMap<>();

    }

    public void parseFile() {
        getChEMBLtoDBID();
        
        int count =0;
        SSIPNode drug = null;
        String activityType = null;
        String ChEMBLID = null;
        String DBID = null;
        String drugType = null;

        BufferedReader br = null;

        String line;
        try {
            br = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while ((line = br.readLine()) != null) {
                String[] split = line.split("\t", -1);
                ChEMBLID = split[0];
                activityType = split[1].toLowerCase();
                allMechanisms.add(activityType);
                DBID = chembl2db.get(ChEMBLID);
                //System.out.println(ChEMBLID+"\t"+DBID);
                drugType = DBID2Type.get(DBID);
                drug = new SSIPNode(drugType, DBID);
                drug.addProperty("Drug_Mechanism", activityType);
                handler.addNode(drug);
                count ++;
                
            }
        } catch (IOException ex) {
            Logger.getLogger(ChEMBL_drugMechanism.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        LOGGER.log(Level.INFO, "Added {0} drug mechanisms to drugs", count);
        LOGGER.log(Level.INFO, "We have {0} different drug mechanisms >> {1}", new Object[]{allMechanisms.size(), allMechanisms.toString()});

    }

    public void getChEMBLtoDBID() {
        BufferedReader br = null;

        String line;
        try {
            br = new BufferedReader(new FileReader(unichemMapping));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            //skip first line
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] split = line.split("\t", -1);
                chembl2db.put(split[0], split[1]);
            }
        } catch (IOException ex) {
            Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
