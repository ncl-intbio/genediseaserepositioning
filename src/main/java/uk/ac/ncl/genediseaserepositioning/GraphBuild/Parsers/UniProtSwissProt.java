/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class UniProtSwissProt {

    private File filepath;
    private FileManager fman;
    private Serialize ser;
    private Scanner sc;
    private Neo4J handler;
    private final static Logger LOGGER = Logger.getLogger(UniProtSwissProt.class.getName());
    private final String ELEMENT = "entry";
    private final String NAMESPACE = "{http://uniprot.org/uniprot}";
    private String characters;
    private boolean parseEntry = false;
    boolean element = false;
    //Xpath queries
    private final String ORGANISM_XPATH = "/entry/organism/dbReference/@id";
    private final String PROTEIN_UNIPROTACCESSIONS_XPATH = "/entry/accession";
    private final String PROTEIN_SEQ = "/entry/sequence";
    private final String PROTEIN_UNIPROTUID_XPATH = "/entry/name";
    private final String PROTEIN_NAME_XPATH = "/entry/protein/recommendedName/fullName";
    private final String GENE_NAME_XPATH = "/entry/gene/name[@type=\"primary\"]";
    private final String DBREFS_XPATH = "/entry/dbReference";
    private final String COMMENT_XPATH = "/entry/comment";
    private final int count = 0;
    //counts
    private int proteins = 0;
    private int genes = 0;
    private int geneDisease = 0;
    private int encoded = 0;
    //boolean
    private boolean includeGenes;
    private boolean includeProteins;
    //node names
    private String geneName;
    private String proteinName;
    //reltypes
    private String encodedByRel;
    private BufferedWriter outPutFileGeneNames;
    private BufferedWriter outPutFileGD;
    private Set<String> uniProtIDs;
    private Set<String> stringIDs;
    private HashMap<String, String> geneSymbol2Id;
    
    
    public UniProtSwissProt(Neo4J handler, boolean proteins, boolean genes) {
        this.fman = new FileManager();
        this.filepath = fman.getUniProtFile();
        this.handler = handler;
        this.ser = new Serialize();
        this.includeProteins = proteins;
        this.includeGenes = genes;
        this.geneName = DReNInMetadata.NodeType.GENE.getText();
        this.proteinName = DReNInMetadata.NodeType.PROTEIN.getText();
        this.encodedByRel = DReNInMetadata.RelTypes.IS_ENCODED_BY.getText();
        try {
            this.outPutFileGeneNames = new BufferedWriter(new FileWriter(new File(fman.getSuppFolder().getPath() + fman.getFileSeparator() + "UNIPROT_GeneIDs.txt")));
        } catch (IOException ex) {
            Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            this.outPutFileGD = new BufferedWriter(new FileWriter(new File(fman.getSuppFolder().getPath() + fman.getFileSeparator() + "UNIPROT_GeneDisease.txt")));
        } catch (IOException ex) {
            Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.uniProtIDs = new HashSet<String>();
        this.stringIDs = new HashSet<String>();
        this.geneSymbol2Id = new HashMap<String,String>();

    }

    /**
     * Parse Proteins and Genes from UniProt
     *
     * @throws IOException
     */
    public void parseFile() {

        String content = "";
        BufferedReader br = null;

        LOGGER.info("Parsing uniprot file "
                + filepath);
        
        
        String line;
        try {
            br = new BufferedReader(new FileReader(filepath));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((line = br.readLine()) != null) {
                if (line.contains("/"+ELEMENT)) {
                    content = content + line;
                    Document drugDoc = getXML(content);
                    extractData(drugDoc);             
                    content = "";
                    parseEntry = false;
                }
                if (parseEntry) {
                    content = content + line;
                }
                if (line.startsWith("<"+ELEMENT)) {
                    content = line;
                    parseEntry = true;
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            outPutFileGeneNames.close();
        } catch (IOException ex) {
            Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            outPutFileGD.close();
        } catch (IOException ex) {
            Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
        }

        ser.SerializeSetPair(uniProtIDs, fman.getSuppFolder() + fman.getFileSeparator()+"UniProtIDs.ser");
        ser.SerializeSetPair(stringIDs, fman.getSuppFolder() + fman.getFileSeparator()+ "STRINGIDs.ser");
        ser.SerializeHashMap(geneSymbol2Id, fman.getSuppFolder() + fman.getFileSeparator()+ "UniProt_geneSym2ID.ser");

        LOGGER.log(Level.INFO, "Parsed {0} proteins", proteins);
        LOGGER.log(Level.INFO, "Parsed {0} genes", genes);
        LOGGER.log(Level.INFO, "Parsed {0} is_encoded_by associations", encoded);
        LOGGER.log(Level.INFO, "Parsed {0} gene-disease associations", geneDisease);
    }

    /**
     * Takes and XML element and extracts all concept classes and relation types
     * required using XMLPath
     *
     * @param document
     */
    public void extractData(Document document) {

        Map<String, Object> attributes = new HashMap<>();

        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();

        //initialize nodes
        SSIPNode protein = null;
        SSIPNode gene = null;
        //testing for homo sapiens
        String organism = null;
        //protein attributes
        String proteinUID = null;
        String primaryUniProt = null;
        Set<String> uniprotAccessions = new HashSet<>();
        String uniprotFullName = null;
        String stringAcc = null;
        //String sequence = null;

        //gene attributes
        String entrezGeneName = null;
        String entrezGeneID = null;
        try {
            //check the protein comes from a human
            organism = (String) xpath.evaluate(ORGANISM_XPATH, document, XPathConstants.STRING);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (organism.equals("9606")) {
            try {
                //get the gene-disease associations
                uniprotFullName = (String) xpath.evaluate(PROTEIN_NAME_XPATH, document, XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                proteinUID = (String) xpath.evaluate(PROTEIN_UNIPROTUID_XPATH, document, XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
            }
//            try {
//                sequence = (String) xpath.evaluate(PROTEIN_SEQ, document, XPathConstants.STRING);
//            } catch (XPathExpressionException ex) {
//                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
//            }

            NodeList accs = null;
            try {
                accs = (NodeList) xpath.evaluate(PROTEIN_UNIPROTACCESSIONS_XPATH, document, XPathConstants.NODESET);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (int i = 0; i < accs.getLength(); i++) {
                if (i == 0) {
                    primaryUniProt = accs.item(i).getTextContent();
                }
                uniprotAccessions.add(accs.item(i).getTextContent());
            }

            NodeList dbrefs = null;
            try {
                dbrefs = (NodeList) xpath.evaluate(DBREFS_XPATH, document, XPathConstants.NODESET);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                stringAcc = getSTRINGAcc(dbrefs, "STRING", xpath);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                entrezGeneID = getSTRINGAcc(dbrefs, "GeneID", xpath);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                entrezGeneName = (String) xpath.evaluate(GENE_NAME_XPATH, document, XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (entrezGeneName != null && !entrezGeneName.isEmpty()) {
                //create gene node
                //System.out.println("Gene Name:: " + geneName + "\t" + sourceURIHGNC + entrezGeneName);
                gene = new SSIPNode(geneName, entrezGeneName);
            }

            NodeList comments = null;
            try {
                comments = (NodeList) xpath.evaluate(COMMENT_XPATH, document, XPathConstants.NODESET);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
            }

            for (int i = 0; i < comments.getLength(); i++) {
                String type = null;
                try {
                    type = (String) xpath.evaluate("@type", comments.item(i), XPathConstants.STRING);
                } catch (XPathExpressionException ex) {
                    Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (type.equals("disease")) {
                    String disSource = null;
                    try {
                        disSource = (String) xpath.evaluate("disease/dbReference/@type", comments.item(i), XPathConstants.STRING);
                    } catch (XPathExpressionException ex) {
                        Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    String disID = null;
                    try {
                        disID = (String) xpath.evaluate("disease/dbReference/@id", comments.item(i), XPathConstants.STRING);
                    } catch (XPathExpressionException ex) {
                        Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (!disID.isEmpty()) {
                        try {
                            outPutFileGD.append(entrezGeneName + "\t" + disID + "\t" + disSource + "\n");
                        } catch (IOException ex) {
                            Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        geneDisease++;

                    }
                }
            }
            //create protein node
            protein = new SSIPNode(proteinName, primaryUniProt);
            uniProtIDs.add(primaryUniProt);
            SSIPRelationType s = new SSIPRelationType(encodedByRel);
            if (entrezGeneName != null && !entrezGeneName.isEmpty()) {

                protein.addRelation(gene, s);
                encoded++;
                try {
                    outPutFileGeneNames.append(entrezGeneName + "\n");
                } catch (IOException ex) {
                    Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            //add all the uniprot acessions as individual properties as they will be indexed on these
            //for integration
            int count = 1;
            for (String uniacc : uniprotAccessions) {
                uniProtIDs.add(uniacc);
                protein.addProperty("UniProtID" + count, uniacc);
                count++;
            }
            protein.addProperty("Name", uniprotFullName);
            protein.addProperty("UniProtUID", proteinUID);
            //protein.addProperty("AASeq", sequence);
//            if (stringAcc != null) {
//                protein.addProperty("STRING", stringAcc);
//                stringIDs.add(stringAcc);
//            }
            //can get other gene IDs
            if (gene != null) {
                gene.addProperty("EntrezGeneSymbol", entrezGeneName);
                if (entrezGeneID != null) {
                    gene.addProperty("EntrezGeneID", entrezGeneID);
                    geneSymbol2Id.put(entrezGeneName, entrezGeneID);
                }
            }

            if (includeGenes && entrezGeneName != null && !entrezGeneName.isEmpty()) {
                handler.addNode(gene);
                genes++;
            }
            if (includeProteins && proteinUID != null) {

                handler.addNode(protein);
                proteins++;
            }

        }
    }

    /**
     * Given a list of XML nodes return the value (accession) of the db of
     * interest.
     *
     * @param nodes XML Nodelist
     * @param dbref Reference database identifier
     * @param xpath Xpath instance to query
     * @return the accession of the reference database
     * @throws XPathExpressionException
     */
    public String getSTRINGAcc(NodeList nodes, String dbref, XPath xpath) throws XPathExpressionException {
        String STRINGacc = null;
        for (int i = 0; i < nodes.getLength(); i++) {
            NamedNodeMap dbsource = nodes.item(i).getAttributes();
            Node datasource = dbsource.getNamedItem("type");
            String ds = datasource.getNodeValue();
            if (ds.equals(dbref)) {
                STRINGacc = (dbsource.getNamedItem("id").getNodeValue());
            }
        }
        return STRINGacc;
    }

    /**
     * Convert a String to an XML document
     *
     * @param convert
     * @return
     */
    public Document getXML(String convert) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            Document document = null;

            try {
                document = builder.parse(new InputSource(new StringReader(convert)));
            } catch (SAXException ex) {
                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
            }

            return document;

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(UniProtSwissProt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
