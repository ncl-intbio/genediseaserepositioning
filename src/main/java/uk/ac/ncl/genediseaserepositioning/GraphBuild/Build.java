/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild;

import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.CTD_Indications;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.ChEMBL_bindsTo;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.ChEMBL_drugMechanism;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.DrugBank;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.MeSHCRecords;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.MeSHDRecords;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.NDFRT_Indications;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.ORDO;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.PREDICT_Indications;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.SIDER4_Indications;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.SIDER4_SideEffects;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers.UniProtSwissProt;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Build {

    private FileManager fman;
    private Neo4J backend;

    public Build(String backendLocation) {
        this.fman = new FileManager();
        this.backend = new Neo4J(backendLocation);

    }

    public void createGraph() {

  //      >>>>>>>>>>>>>>>>>> STEP_1 START      
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(100);
        backend.createIndex();
        for (int i = 1; i < 11; i++) {
            backend.createIndex("UniProtID" + i);
        }
        backend.createIndex("EntrezGeneID");
        backend.createIndex("EntrezGeneSymbol");
        backend.syncIndexes(10);
        UniProtSwissProt uni = new UniProtSwissProt(backend, true, true);
        uni.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
        //       >>>>>>>>>>>>>>>>>> STEP_2 START
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(100);
        //backend.createIndex();
        backend.syncIndexes(10);
        backend.createIndex("DBID");
        backend.createIndex("CHEMBL");
        DrugBank db = new DrugBank( backend, true);
        db.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
        //     >>>>>>>>>>>>>>>>>> STEP_3 START  Rare Diseases Ontology
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(1000);
        backend.syncIndexes(10);
        backend.createIndex("ORDO_ID");
        for (int i = 1; i < 20; i++) {
            backend.createIndex("UMLS_CUI" + i);
        }
        for (int i = 1; i < 4; i++) {
            backend.createIndex("MESH" + i);
        }
        ORDO orph = new ORDO(backend);
        orph.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
        //>>>>>>>>>>>>>>>>>> STEP_4 START  MeSH
        backend.setCommitSize(1000);
        backend.initialiseDatabaseConnection();
        //backend.createIndex();
        backend.syncIndexes(10);
        backend.createIndex("MeSHHeader");
        backend.createIndex("MeSHUI");
        for (int i = 1; i < 11; i++) {
            backend.createIndex("MeSHTree" + i);
        }
        MeSHDRecords mesh = new MeSHDRecords(backend);
        mesh.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
        //>>>>>>>>>>>>>>>>>> STEP_5 START  MeSH
        backend.setCommitSize(1000);
        backend.initialiseDatabaseConnection();
        backend.syncIndexes(10);
        MeSHCRecords meshRec = new MeSHCRecords(backend);
        meshRec.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
        //>>>>>>>>>>>>>>>>>> STEP_13 START
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(1000);
        backend.syncIndexes(10);
        ChEMBL_drugMechanism chemdm = new ChEMBL_drugMechanism(backend);
        chemdm.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
//        //        >>>>>>>>>>>>>>>>>> STEP_13 START
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(1000);
        backend.syncIndexes(10);
        ChEMBL_bindsTo chembt = new ChEMBL_bindsTo(backend);
        chembt.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
        //  >>>>>>>>>>>>>>>>>> STEP_13 FILES
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(1000);
        backend.syncIndexes(10);
        SIDER4_Indications sidr = new SIDER4_Indications(backend);
        sidr.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
        //  >>>>>>>>>>>>>>>>>> STEP_14 FILES
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(1000);
        backend.syncIndexes(10);
        SIDER4_SideEffects siderSE = new SIDER4_SideEffects(backend);
        siderSE.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
        //  >>>>>>>>>>>>>>>>>> STEP_14 FILES 
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(1000);
        backend.syncIndexes(10);
        NDFRT_Indications ndfrt = new NDFRT_Indications(backend);
        ndfrt.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
       //   >>>>>>>>>>>>>>>>>> STEP_14 FILES 
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(1000);
        backend.syncIndexes(10);
        PREDICT_Indications predict = new PREDICT_Indications(backend);
        predict.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
        //  >>>>>>>>>>>>>>>>>> STEP_14 FILES 
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(1000);
        backend.syncIndexes(10);
        CTD_Indications ctd = new CTD_Indications(backend);
        ctd.parseFile();
        backend.commitTransaction();
        backend.finaliseDatabaseConnection();
    }
}
