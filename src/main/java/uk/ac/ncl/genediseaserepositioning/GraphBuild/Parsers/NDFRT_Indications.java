/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Integrates indication information retrieved from NDF-RT.
 *
 *
 */
public class NDFRT_Indications   {

    private final static Logger LOGGER = Logger.getLogger(NDFRT_Indications.class.getName());
    private int indicationRelCount = 0;
    private File filePath;
    private Neo4J handler;
    private Set<String> notMap;
    private FileManager fmap;
    private Set<String> uniqueRels;
    private Serialize ser;
    private Map<String, Set<String>> mesh2type;
    private Map<String, String> DBID2Type;

    private String diseaseName;
    private String rareDiseaseName;
    private String indicationRelation;
    private String smallMolName;
    private String biotechName;
    
    private final DReNInMetadata.DataSource prov;
    private DReNInMetadata provenance;


    public NDFRT_Indications(Neo4J handler) {

        this.handler = handler;
        this.diseaseName = DReNInMetadata.NodeType.COMMON_DISEASE.getText();
        this.rareDiseaseName = DReNInMetadata.NodeType.RARE_DISEASE.getText();
        this.indicationRelation = DReNInMetadata.RelTypes.HAS_INDICATION.getText();
        this.smallMolName = DReNInMetadata.NodeType.SMALL_MOLECULE.getText();
        this.biotechName = DReNInMetadata.NodeType.BIOTECH.getText();
        this.uniqueRels = new HashSet<String>();
        this.notMap = new HashSet<>();
        this.fmap = new FileManager();
        this.ser = new Serialize();
        this.filePath = fmap.getNDFRTIndications();
        this.mesh2type = ser.deserializeHashMapSet(fmap.getSuppFolder() + fmap.getFileSeparator()+ "ORDO_mesh2diseasetype.ser");
        this.DBID2Type = ser.deserializeHashMap(fmap.getSuppFolder() + fmap.getFileSeparator()+ "DrugBank_DBID2Type.ser");

        this.prov = DReNInMetadata.DataSource.NDFRT;
        this.provenance = new DReNInMetadata();
    }

    public void parseFile() {
        int relations = 0;
        
        SSIPNode drug = null;
        SSIPRelationType indication = null;

        String DBID = null;
        String drugType = null;
        String MeSHUI = null;
        String diseaseType = null;

        String thisLine;
        //Open the file for reading

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NDFRT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((thisLine = br.readLine()) != null) {
                String[] split = thisLine.split("\t", -1);
                DBID = split[0];
                drugType = DBID2Type.get(DBID);
                MeSHUI = split[3];
                diseaseType = getDisType(MeSHUI);
                if (drugType == null) {
                    LOGGER.log(Level.INFO, "No drugType for drug {0}", DBID);
                } else {
                    drug = new SSIPNode(drugType, DBID);
                    indication = new SSIPRelationType(indicationRelation);
                    indication.addAttribute(provenance.addProv("Relation"), prov.toString());
                    drug.addRelation(new SSIPNode(diseaseType, MeSHUI), indication);
                    handler.addNode(drug);
                    relations ++;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(NDFRT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        LOGGER.log(Level.INFO, "Added {0} has_indication relations from NDFRT", relations);
        
    }

    /**
     * Method takes a MeSH id. if the term is covered in the OD [disease] or the
     * ORDO [rare_disease] then the type is returned; if it is not then MESHTerm
     * is returned.
     *
     * @param mesh
     * @return
     */
    public String getDisType(String mesh) {
        //System.out.println("Checking for: "+ mesh);
        for (String keys : mesh2type.keySet()) {
            Set<String> ids = mesh2type.get(keys);
            if (ids.contains(mesh)) {
                //System.out.println(keys);
                return keys;
            }
        }

        //System.out.println("MeSHTerm");
        return diseaseName;
    }

}
