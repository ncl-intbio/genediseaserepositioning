/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 *
 * Parser takes data from SIDER http://sideeffects.embl.de/se/ and integrates
 * Side Effect and Indication information.
 *
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.Metathesaurus;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author joemullen
 */
/**
 * This class parsers in the contents of SIDER4_Indications indications from a
 * flatfile. SIDER4_Indications database can be found at :
 * http://sideeffects.embl.de/download/ . At the moment only extracting
 * relations between single compounds.
 *
 * Uses indications_raw.txt.
 *
 * WARNING: TAKES ABOUT 6 hours to run the extractRawIndicationsToFile method.
 *
 * @author joemullen
 */
public class SIDER4_SideEffects {

    private final static Logger LOGGER = Logger.getLogger(SIDER4_SideEffects.class.getName());
    private int sideeffectRelCount = 0;
    private Map<String, String> GENERICNAME2DB;
    private Map<String, String> MEDDRA2MESH;
    private File pharmGKBDrugs;
    private File filePath;
    private Neo4J handler;
    private Set<String> notMap;
    private Metathesaurus met;
    private BufferedWriter bwsideEffects;
    private FileManager fmap;
    private Set<String> uniqueRels;
    private Serialize ser;
    private Map<String, String> DBID2Type;
    private Map<String, Set<String>> mesh2type;
    private String diseaseName;
    private String rareDiseaseName;
    private String sideRelation;
    private String smallMolName;
    private String biotechName;

    public SIDER4_SideEffects(Neo4J handler) {

        this.handler = handler;
        this.diseaseName = DReNInMetadata.NodeType.COMMON_DISEASE.getText();
        this.rareDiseaseName = DReNInMetadata.NodeType.RARE_DISEASE.getText();
        this.sideRelation = DReNInMetadata.RelTypes.SIDE_EFFECT.getText();
        this.smallMolName = DReNInMetadata.NodeType.SMALL_MOLECULE.getText();
        this.biotechName = DReNInMetadata.NodeType.BIOTECH.getText();

        this.fmap = new FileManager();
        this.met = new Metathesaurus();
        this.ser = new Serialize();
        this.GENERICNAME2DB = new HashMap<>();
        this.uniqueRels = new HashSet<String>();
        this.pharmGKBDrugs = fmap.getPharmGKBDrug();
        this.filePath = fmap.getSIDERSideEffects();
        this.MEDDRA2MESH = new HashMap<>();
        this.notMap = new HashSet<>();

        try {
            this.bwsideEffects = new BufferedWriter(new FileWriter(fmap.getSuppFolder() + fmap.getFileSeparator() + "SIDER_sideEffects.txt"));
        } catch (IOException ex) {
            Logger.getLogger(SIDER4_SideEffects.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.DBID2Type = ser.deserializeHashMap(fmap.getSuppFolder() + fmap.getFileSeparator() + "DrugBank_DBID2Type.ser");
        this.mesh2type = ser.deserializeHashMapSet(fmap.getSuppFolder() + fmap.getFileSeparator() + "ORDO_mesh2diseasetype.ser");

    }


    public void getStats() {
        int notMapped = 0;
        int mapped = 0;
        Set<String> relats = new HashSet<>();
        Set<String> drugs = new HashSet<>();

        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(new File(fmap.getSuppFolder() + fmap.getFileSeparator() + "SIDER_indications.txt")));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SIDER4_SideEffects.class.getName()).log(Level.SEVERE, null, ex);
        }

        String thisLine;
        try {
            while ((thisLine = br.readLine()) != null) {
                String[] split = thisLine.split("\t");
                if (split[2].equals("null")) {
                    notMapped++;
                } else {
                    mapped++;
                    relats.add(split[0] + split[2]);
                    drugs.add(split[0]);
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(SIDER4_SideEffects.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Can't map to diseases {0}", notMapped);
        LOGGER.log(Level.INFO, "Can map to diseases {0}", mapped);
        LOGGER.log(Level.INFO, "drugs {0}", drugs.size());
        LOGGER.log(Level.INFO, "nonr rels {0}", relats.size());

    }

    public void parseFile() {

        getPubChemToDb();

        getInfo(filePath);
        try {
            bwsideEffects.close();
        } catch (IOException ex) {
            Logger.getLogger(SIDER4_SideEffects.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Extracts has_indication [indications_raw.tsv]. Produces a file containing
     * a list of interactions in the form [DRUGBANK ID] > [DISEASE UMLS] >
     * [DISEASE MeSH]
     *
     * @param file file location
     */
    public void getInfo(File file) {

        SSIPNode drug = null;
        SSIPRelationType sideEffect = null;

        int count = 0;
        int test = 10;
        String previous = "FIRST";

        String thisLine;
        //Open the file for reading
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((thisLine = br.readLine()) != null) { // while loop begins here                

                //side effects
                String STITCHcompoundIds1 = null; //(flat/stereo, see above)
                String STITCHcompoundIds2 = null; //(flat/stereo, see above)
                String UMLSconceptid = null;
                String MedDRAConceptType = null; //(LLT = lowest level term, PT = preferred term;
                String UMLSconceptidForMedDRAterm = null;
                String MedDRAConceptName = null;

                String[] split = thisLine.split("\t", -1);
                count++;

                //side effects
                STITCHcompoundIds1 = split[0]; //(flat/stereo, see above)
                STITCHcompoundIds2 = split[1]; //(flat/stereo, see above)
                UMLSconceptid = split[2];
                MedDRAConceptType = split[3]; //(LLT = lowest level term, PT = preferred term;
                UMLSconceptidForMedDRAterm = split[4];
                MedDRAConceptName = split[5];

                String dbComp1 = GENERICNAME2DB.get(STITCHcompoundIds1);
                String dbComp2 = GENERICNAME2DB.get(STITCHcompoundIds2);

                if (MedDRAConceptType.equals("PT")) {
                    //create an indication relation
                    String drugName = null; //GENERICNAME2DB.get(STITCHcompoundIds1);
                    if (dbComp1 == null && dbComp2 != null) {
                        drugName = dbComp2;
                    } else if (dbComp2 == null && dbComp1 != null) {
                        drugName = dbComp1;
                    } else if (dbComp1 != null && dbComp2 != null) {
                        if (dbComp1.equals(dbComp2)) {
                            drugName = dbComp1;
                        }
                    } else {
                        drugName = null;
                    }
                    String mesh = null;
                    if (!MEDDRA2MESH.containsKey(UMLSconceptidForMedDRAterm)) {
                        mesh = met.getMeSHfromUMLS(UMLSconceptidForMedDRAterm);
                        if (mesh != null) {
                            MEDDRA2MESH.put(UMLSconceptidForMedDRAterm, mesh);
                        } else {
                            notMap.add(UMLSconceptidForMedDRAterm);
                        }
                    } else {
                        mesh = MEDDRA2MESH.get(UMLSconceptidForMedDRAterm);
                    }
                    bwsideEffects.append(STITCHcompoundIds1 + "/" + STITCHcompoundIds2 + "\t" + UMLSconceptidForMedDRAterm + "\t" + drugName + "\t" + mesh + "\n");

                    if (drugName != null && mesh != null) {

                        if (DBID2Type.containsKey(drugName)) {
                          //  System.out.println(DBID2Type.get(drugName)+"\t"+ drugName);
                            drug = new SSIPNode(DBID2Type.get(drugName), drugName);
                            sideEffect = new SSIPRelationType(sideRelation);
                          //  System.out.println(getDisType(mesh)+"\t"+ mesh);
                            drug.addRelation(new SSIPNode(getDisType(mesh), mesh), sideEffect);
                            handler.addNode(drug);
                        } else {
                            LOGGER.log(Level.INFO, "We have no drug with the DBID {0} in DReNIn", drugName);
                            System.out.println("MESH::" + mesh);
                        }
                    }

                    if (drugName != null && mesh != null) {
                        uniqueRels.add(drugName + "\t" + mesh);
                    }
                    sideeffectRelCount++;
                }
            }
        } catch (IOException e) {
            System.err.println("Error: " + e);
        }

        sideeffectRelCount++;

        LOGGER.log(Level.INFO, "Parsed {0} has_side_effect relations {1}", new Object[]{sideeffectRelCount, file.toPath().toString()});
        LOGGER.log(Level.INFO, "Of which {0} are unique", uniqueRels.size());

    }

    /**
     * Get the mappings of the generic name -> drugBank id from PharmGKB.
     *
     * Within SIDER package inserts contain information about the common and/or
     * brand names of the drugs they describe. Based on this information, labels
     * were mapped to STITCH 4.0 compound identifiers, which in turn are derived
     * from PubChem compound identifiers.
     *
     * STITCH identifiers append pubchem ids with a CID10000 to make it 12 in
     * length and so the PubChem ID for Sildenafil (5212) becomes CID100005212.
     *
     * CID0... - this is a stereo-specific compound, and the suffix is the
     * PubChem compound id.
     *
     * CID1... - this is a "flat" compound, i.e. with merged stereo-isomers The
     * suffix (without the leading "1") is the PubChem compound id.
     *
     */
    public void getPubChemToDb() {

        String STITCHStereo = "CID000000000";
        String STITCHFlat = "CID100000000";

        BufferedReader br = null;
        Map<String, String> PUBCHEM2DBlocal = new HashMap<>();
        String thisLine;

        try {
            //Open the file for reading
            br = new BufferedReader(new FileReader(pharmGKBDrugs));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SIDER4_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while ((thisLine = br.readLine()) != null) {
                String db = null;
                String pubchem = null;
                String[] split = thisLine.split("\t");
                //get the DrugBank ID and the pubchem IDs which are crossRefs
                String[] split2 = split[6].split(",");
                for (String split21 : split2) {
                    String[] split3 = split21.replace("\"", "").split(":");
                    if (split3[0].equals("DrugBank")) {
                        db = split3[1];
                    }
                    if (split3[0].equals("PubChem Compound")) {
                        pubchem = split3[1];
                    }
                }
                if (db != null && pubchem != null) {
                    PUBCHEM2DBlocal.put(STITCHStereo.substring(0, STITCHStereo.length() - pubchem.length()) + pubchem, db);
                    PUBCHEM2DBlocal.put(STITCHFlat.substring(0, STITCHFlat.length() - pubchem.length()) + pubchem, db);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(SIDER4_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.GENERICNAME2DB = PUBCHEM2DBlocal;

    }

    /**
     * Method takes a MeSH id. if the term is covered in the OD [disease] or the
     * ORDO [rare_disease] then the type is returned; if it is not then MESHTerm
     * is returned.
     *
     * @param mesh
     * @return
     */
    public String getDisType(String mesh) {
        //System.out.println("Checking for: "+ mesh);
        for (String keys : mesh2type.keySet()) {
            Set<String> ids = mesh2type.get(keys);
            if (ids.contains(mesh)) {
                //System.out.println(keys);
                return keys;
            }
        }

        //System.out.println("MeSHTerm");
        return diseaseName;
    }
}
