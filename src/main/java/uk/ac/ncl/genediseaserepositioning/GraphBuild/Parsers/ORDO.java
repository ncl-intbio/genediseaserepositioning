/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Parser is for the csv (comma separated) version of the ORDO which can be
 * obtained from:: http://bioportal.bioontology.org/ontologies/ORDO
 *
 */
public class ORDO  {

    private final static Logger LOGGER = Logger.getLogger(ORDO.class.getName());
    private File filePath;
    private Neo4J handler;
    private int terms;
    private int has_parent;
    private int part_of;

    private String rareDisease = DReNInMetadata.NodeType.RARE_DISEASE.getText();
    private String has_parentRel = DReNInMetadata.RelTypes.IS_A_RARE_DISEASE.toString();
    private String part_ofRel = DReNInMetadata.RelTypes.PART_OF_RARE_DISEASE.toString();

    private FileManager fmap;
    private Serialize ser;
    private HashMap<String, Set<String>> MESH2DisType;
    private BufferedWriter omim2Mesh;
    private HashMap<String, Set<String>> ORDO2MeSH;
    


    public ORDO(Neo4J backend) {
        this.handler = backend;
        this.ser = new Serialize();
        this.fmap = new FileManager();
        this.filePath = fmap.getORDOFile();
        this.part_of = 0;
        this.has_parent = 0;
        this.terms = 0;
        
        this.MESH2DisType = new HashMap<String, Set<String>>();
        try {
            this.omim2Mesh = new BufferedWriter(new FileWriter(fmap.getSuppFolder() +fmap.getFileSeparator()+ "ORDO_omim2mesh.txt"));
        } catch (IOException ex) {
            Logger.getLogger(ORDO.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.ORDO2MeSH = new HashMap<String, Set<String>>();
    }

    public void parseFile() {

        Set<String> meshUIs = new HashSet<String>();
        SSIPNode disease = null;
        SSIPRelationType disRel = null;

        //we are only interested in including terms that are subclasses of the phenome branch of the ontology
        Set<String> phenomeIDs = new HashSet<String>();
        //Phenome 
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_C001");
        //Which includes the following "parents"     
        //disease
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_377788");
        //groups of disorders
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_377794");
        //etiological subtype 
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_377795");
        //clinical subtype
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_377796");
        //histopatholgical subtype
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_377797");
        //malformation syndrome
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_377789");
        //clinical syndrome  
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_377792");
        //morpholical anomaly
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_377791");
        //biological anomaly
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_377790");
        //particular clinical situation in the course of a disorder
        phenomeIDs.add("http://www.orpha.net/ORDO/Orphanet_377793");

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ORDO.class.getName()).log(Level.SEVERE, null, ex);
        }
        String thisLine;

        try {
            while ((thisLine = br.readLine()) != null) {

                //split the line on commas and ignore the commas if they occur between 2 ""
                String[] split = thisLine.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
                String ID = split[0];
                String preferredName = split[1];
                String synonyms = split[2];
                String definitions = split[3];
                boolean obsolete = Boolean.parseBoolean(split[4]);
                //Never anything in// String CUI = split[5];
                //Never anything in// String semanticTypes = split[6];
                String parents = split[7];
                //Never anything in// String candGene = split[8];
                //Never anything in// String curatorInference = split[9];
                //Never anything in// String GOF = split[10];
                //Never anything in// String LOF = split[11]; //all the way to 24//
                String part_of = split[25];
                String dbXref = split[33];

                String[] part_ofs = part_of.split("\\|");
                String[] has_parents = parents.split("\\|");
                String[] Xrefs = dbXref.split("\\|");
                
                //System.out.println(dbXref);

                if (obsolete == false && checkDisease(phenomeIDs, has_parents)) {
                    disease = new SSIPNode(rareDisease, ID.replace("http://www.orpha.net/ORDO/", ""));
                    //remove the speech marks from the name
                    disease.addProperty("Name", preferredName.replace("\"", ""));
                    //disease.addProperty("label", ID.replace("http://www.orpha.net/ORDO/", ""));
                    //has_parent relations
                    if (has_parents.length > 0) {
                        for (String parent : has_parents) {
                            if (!parent.isEmpty()) {
                                // System.out.println("\t Has_parent :: " + parent);
                                disRel = new SSIPRelationType(has_parentRel);
                                disease.addRelation(new SSIPNode(rareDisease, parent.replace("http://www.orpha.net/ORDO/", "")), disRel);
                                has_parent++;
                            }
                        }
                    }
                    //part_of relations
                    if (part_ofs.length > 0) {
                        for (String part : part_ofs) {
                            if (!part.isEmpty()) {
                                disRel = new SSIPRelationType(part_ofRel);
                                disease.addRelation(new SSIPNode(rareDisease, part.replace("http://www.orpha.net/ORDO/", "")), disRel);
                                //System.out.println("\t Part_of :: " + part);
                                this.part_of++;
                            }
                        }
                    }
                    //xrefs

                    int UMLS = 1;
                    int MESH = 1;
                    int OMIM = 1;

                    Set<String> OMIMs = new HashSet<String>();
                    Set<String> MeSHs = new HashSet<String>();

                    if (Xrefs.length > 0) {
                        for (String ref : Xrefs) {
                            if (!ref.isEmpty()) {
                                String[] splitref = ref.split(":");
                                if (splitref[0].equals("MeSH")) {
                                    disease.addProperty("MESH" + MESH, splitref[1]);
                                    MESH++;
                                    meshUIs.add(splitref[1]);
                                    MeSHs.add(splitref[1]);
                                }
                                if (splitref[0].equals("OMIM")) {
                                    disease.addProperty("OMIM" + OMIM, splitref[1]);
                                    OMIM++;
                                    OMIMs.add(splitref[1]);
                                }
                                if (splitref[0].equals("UMLS")) {
                                    disease.addProperty("UMLS" + UMLS, splitref[1]);
                                    UMLS++;
                                }

                            }
                        }
                    }
                    for (String om : OMIMs) {
                        for (String me : MeSHs) {
                            omim2Mesh.append(om + "\t" + me + "\n");
                            //System.out.println(om + "\t" + me);
                        }
                    }
                    
                    ORDO2MeSH.put(ID, MeSHs);

                    handler.addNode(disease);
                    terms++;

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ORDO.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        LOGGER.log(Level.INFO, "Added {0} ORDO diseases", terms);
        LOGGER.log(Level.INFO, "Added {0} has_parent relation", has_parent);
        LOGGER.log(Level.INFO, "Added {0} part_of relation", part_of);

        MESH2DisType.put(rareDisease, meshUIs);

        try {
            omim2Mesh.close();
        } catch (IOException ex) {
            Logger.getLogger(ORDO.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        ser.SerializeHashMap(MESH2DisType, fmap.getSuppFolder() +fmap.getFileSeparator()+ "ORDO_mesh2diseasetype.ser");
        ser.SerializeHashMap(ORDO2MeSH, fmap.getSuppFolder() +fmap.getFileSeparator()+ "ORDO_ORDO2MeSH.ser");

    }

    public boolean checkDisease(Set<String> ids, String[] parents) {
        for (String parent : parents) {
            if (ids.contains(parent)) {
                return true;
            }
        }
        return false;
    }
    
}
