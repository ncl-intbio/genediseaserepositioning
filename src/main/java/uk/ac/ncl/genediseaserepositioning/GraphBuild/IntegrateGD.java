/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes a file containing ranked gene-disease associations and integrates
 * them with the rest of the graph.
 *
 *
 */
public class IntegrateGD {

    private String GraphSource;
    private String GraphDest;
    private String GeneDisease;
    private Neo4J backend;
    private BufferedReader br;
    private final static Logger LOGGER = Logger.getLogger(IntegrateGD.class.getName());
    private int countRels = 0;
    private FileManager fman;
    //node name
    private String geneName;
    private String invInRel;
    private HashMap<String, Set<String>> MESH2DisType;
    private Serialize s;


    public IntegrateGD(String source, String destination, String geneDisease) {
        this.GraphSource = source;
        this.GraphDest = destination;
        this.GeneDisease = geneDisease;
    }

    /**
     * Duplicate the graph that has already been created.
     */
    public void duplicateGraph() {
        File source = new File(GraphSource);
        File dest = new File(GraphDest);
        try {
            FileUtils.copyDirectory(source, dest);
        } catch (IOException ex) {
            Logger.getLogger(IntegrateGD.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Add the gene-disease associations.
     */
    public void IntegrateGDAss() {

        this.backend = new Neo4J(GraphDest);
        backend.initialiseDatabaseConnection();
        backend.setCommitSize(100);
        backend.syncIndexes(10);

        this.fman = new FileManager();
        this.s = new Serialize();
        this.MESH2DisType = s.deserializeHashMapSet(fman.getSuppFolder()+ fman.getFileSeparator() + "ORDO_mesh2diseasetype.ser");

        try {
            br = new BufferedReader(new FileReader(GeneDisease));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(IntegrateGD.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.geneName = DReNInMetadata.NodeType.GENE.getText();
        this.invInRel = DReNInMetadata.RelTypes.INVOLVED_IN_DISEASE.getText();
        LOGGER.log(Level.INFO,
                "Integrating gene-disease associations from {0}", GeneDisease);
        String gene = null;
        String MeSHId = null;
        double relationScore;

        Map<String, Set<String>> orderedRels = new HashMap<>();

        String diseaseType = null;
        double max = 0.0;

        /**
         * ONE- order the rels so that we have all relations with each gene
         * added at the same time.
         */
        String line;
        int count = 0;
        try {
            while ((line = br.readLine()) != null) {
                String[] split = line.split("\t");
                MeSHId = split[1];
                gene = split[0];
                relationScore = Double.parseDouble(split[2]);
                if (count == 0) {
                    max = relationScore;
                    count++;
                }
                //normalize relation score
                relationScore = 1.0 / max * relationScore;
                
                if (orderedRels.containsKey(gene)) {
                    Set<String> dis = orderedRels.get(gene);
                    dis.add(MeSHId + "\t" + relationScore);
                    orderedRels.put(gene, dis);
                } else {
                    Set<String> dis = new HashSet<String>();
                    dis.add(MeSHId + "\t" + relationScore);
                    orderedRels.put(gene, dis);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(IntegrateGD.class.getName()).log(Level.SEVERE, null, ex);
        }

//        /**
//         * TWO- add the rels.
//         */
        SSIPNode ssipGene = null;
        for (String entrez : orderedRels.keySet()) {
            ssipGene = new SSIPNode(geneName, entrez);
            for (String rel : orderedRels.get(entrez)) {
                String diseaseUI = rel.split("\t")[0];
                double relationScore2 = Double.parseDouble(rel.split("\t")[1]);
                String diseaseTy = null;
                for (String key : MESH2DisType.keySet()) {
                    Set<String> uis = MESH2DisType.get(key);
                    if (uis.contains(diseaseTy)) {
                        diseaseTy = key;
                    } else {
                        diseaseTy = DReNInMetadata.NodeType.COMMON_DISEASE.getText();
                    }
                }
                SSIPRelationType relation = new SSIPRelationType(invInRel);
                relation.addAttribute("EvidenceScore", relationScore2);
                ssipGene.addRelation(new SSIPNode(diseaseType, diseaseUI), relation);
                countRels++;
            }
            backend.addNode(ssipGene);
        }
        LOGGER.log(Level.INFO,
                "{0} unique gene-disease have been integrated", countRels);

        backend.commitTransaction();
        backend.finaliseDatabaseConnection();

    }
}
