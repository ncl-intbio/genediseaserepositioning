/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.Metathesaurus;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Pair;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes the 1933 experimentally confirmed drug-disease associations from
 * the PREDICT paper and outputs them in DrugBank> MeSH format.
 *
 *
 */
public class PREDICT_Indications {

    private File assocationsFile;// = "/Users/joemullen/Dropbox/LatexPapers/GSKPaper/PREDICT_GS/associations_Drug_Disease.txt";
    private File diseaseUMLS;// = "/Users/joemullen/Dropbox/LatexPapers/GSKPaper/PREDICT_GS/disease_UMLS.txt";
    private Set<Pair> associations;
    private HashMap<String, String> NAME2UMLS;
    private Metathesaurus met;
    private FileManager fman;
    private Neo4J handler;
    private Serialize ser;
    private final static Logger LOGGER = Logger.getLogger(PREDICT_Indications.class.getName());
    private Map<String, Set<String>> mesh2type;
    private Map<String, String> DBID2Type;
    private String diseaseName;
    private String rareDiseaseName;
    private String indicationRelation;
    private String smallMolName;
    private String biotechName;
    private final DReNInMetadata.DataSource prov;
    private DReNInMetadata provenance;

    @SuppressWarnings("unchecked")
    public PREDICT_Indications(Neo4J handler) {
        this.handler = handler;
        this.associations = new HashSet<>();
        this.NAME2UMLS = new HashMap();
        this.met = new Metathesaurus();
        this.fman = new FileManager();
        this.assocationsFile = fman.getPREDICTIndications();
        this.diseaseUMLS = fman.getPREDICTUMLS();
        this.ser = new Serialize();
        this.mesh2type = ser.deserializeHashMapSet(fman.getSuppFolder() + fman.getFileSeparator()+ "ORDO_mesh2diseasetype.ser");
        this.DBID2Type = ser.deserializeHashMap(fman.getSuppFolder() + fman.getFileSeparator()+ "DrugBank_DBID2Type.ser");
        
        this.diseaseName = DReNInMetadata.NodeType.COMMON_DISEASE.getText();
        this.rareDiseaseName = DReNInMetadata.NodeType.RARE_DISEASE.getText();
        this.indicationRelation = DReNInMetadata.RelTypes.HAS_INDICATION.getText();
        this.smallMolName = DReNInMetadata.NodeType.SMALL_MOLECULE.getText();
        this.biotechName = DReNInMetadata.NodeType.BIOTECH.getText();
        this.prov = DReNInMetadata.DataSource.PREDICT;
        this.provenance = new DReNInMetadata();

    }

    public void parseFile() {
        readFiles();
        mapDiseaseToUMLS();
        mapUMLSToMeSH();
        writeToFile();
        addToGraph();


    }

    public void addToGraph() {
        
        int relations = 0;
        
        String thisLine;
        String DBID = null;
        String drugType = null;
        String MeSHUI = null;
        String diseaseType = null;

        SSIPNode drug = null;
        SSIPRelationType indication = null;

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fman.getSuppFolder() + fman.getFileSeparator() + "PREDICT_Indications.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NDFRT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((thisLine = br.readLine()) != null) {
                String[] split = thisLine.split("\t", -1);
                DBID = split[0];
                drugType = DBID2Type.get(DBID);
                MeSHUI = split[1];
                diseaseType = getDisType(MeSHUI);
                if (drugType == null) {
                    LOGGER.log(Level.INFO, "No drugType for drug {0}", DBID);
                } else {
                    drug = new SSIPNode(drugType, DBID);
                    indication = new SSIPRelationType(indicationRelation);
                    indication.addAttribute(provenance.addProv("Relation"), prov.toString());
                    drug.addRelation(new SSIPNode(diseaseType, MeSHUI), indication);
                    handler.addNode(drug);
                    relations++;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(NDFRT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, "Added {0} has_indication relations from PREDICT", relations);
    }

    public void writeToFile() {
        int count = 0;
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(new File(fman.getSuppFolder() + fman.getFileSeparator() + "PREDICT_Indications.txt")));
        } catch (IOException ex) {
            Logger.getLogger(PREDICT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Pair p : associations) {
            try {
                bw.append(p.getEntrezGeneSymbol() + '\t' + p.getMeSHUI() + '\n');
            } catch (IOException ex) {
                Logger.getLogger(PREDICT_Indications.class.getName()).log(Level.SEVERE, null, ex);
            }
            count++;

        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(PREDICT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Added {0} associations to {1}/PREDICT_GS.txt", new Object[]{count, fman.getSuppFolder()});

    }

    public void mapUMLSToMeSH() {

        Set<Pair> mapped = new HashSet<>();
        for (Pair p : associations) {
            String dis = p.getMeSHUI();
            String mesh = met.getMeSHfromUMLS(dis);
            if (mesh != null) {
                mapped.add(new Pair(p.getEntrezGeneSymbol(), mesh));
            }
        }

        associations = mapped;
        LOGGER.log(Level.INFO, "We have {0} associations with MeSH...", associations.size());

    }

    public void mapDiseaseToUMLS() {

        Set<Pair> mapped = new HashSet<Pair>();
        for (Pair p : associations) {
            String dis = "";
            if (p.getMeSHUI().equals("\"Neuropathy, Hereditary Sensory And Autonomic, Type I, With Cough And Gastroesophageal Reflux\"")) {
                dis = "\"Neuropathy, Hereditary Sensory And Autonomic, Type I, With Cough And\"";
            } else {
                dis = p.getMeSHUI();
            }
            if (!NAME2UMLS.containsKey(dis)) {
                System.out.println("No mapping for " + p.getMeSHUI());
            } else {
                mapped.add(new Pair(p.getEntrezGeneSymbol(), NAME2UMLS.get(dis)));
            }
        }

        associations = mapped;
        LOGGER.log(Level.INFO, "We have {0} associations with UMLS...", associations.size());

    }

    public void readFiles() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(assocationsFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PREDICT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }

        String line;
        try {
            br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(PREDICT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((line = br.readLine()) != null) {
                if (line.isEmpty() || line.startsWith(" ") || line.split("\t").length < 2) {
                    break;
                }
                Pair p = new Pair(line.split("\t")[2], line.split("\t")[1]);
                associations.add(p);
            }
        } catch (IOException ex) {
            Logger.getLogger(PREDICT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "We have {0} associations...", associations.size());
        br = null;
        try {
            br = new BufferedReader(new FileReader((diseaseUMLS)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PREDICT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(PREDICT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((line = br.readLine()) != null) {
                if (line.isEmpty() || line.startsWith(" ") || line.split("\t").length < 2) {
                    break;
                }
                NAME2UMLS.put(line.split("\t")[1], line.split("\t")[2]);
            }
        } catch (IOException ex) {
            Logger.getLogger(PREDICT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
      /**
     * Method takes a MeSH id. if the term is covered in the OD [disease] or the
     * ORDO [rare_disease] then the type is returned; if it is not then MESHTerm
     * is returned.
     *
     * @param mesh
     * @return
     */
    public String getDisType(String mesh) {
        //System.out.println("Checking for: "+ mesh);
        for (String keys : mesh2type.keySet()) {
            Set<String> ids = mesh2type.get(keys);
            if (ids.contains(mesh)) {
                //System.out.println(keys);
                return keys;
            }
        }
        //System.out.println("MeSHTerm");
        return diseaseName;
    }
}
