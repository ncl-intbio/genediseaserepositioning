/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Parsers.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Parser integrates binds_to relations data was extracted from the SQL dump of
 * ChEMBL using the VM set-up on the windows machine.
 *
 */
public class ChEMBL_bindsTo {

    private String smallMoleculeName;
    private String bioTechName;
    private String bindsToRel;
    private String proteinName;
    private Map<String, String> DBID2Type;
    private Map<String, String> chembl2db;
    private File unichemMapping;
    private Map<String, ArrayList<Double>> activityTypes;
    private File ChEMBLstats;

    private FileManager fmap;
    private Serialize ser;
    private File filePath;
    private Neo4J handler;
    private int bindsToCount = 0;

    private final static Logger LOGGER = Logger.getLogger(ChEMBL_bindsTo.class.getName());

    public ChEMBL_bindsTo(Neo4J handler) {

        this.bindsToRel = DReNInMetadata.RelTypes.BINDS_TO.getText();
        this.bioTechName = DReNInMetadata.NodeType.BIOTECH.getText();
        this.smallMoleculeName = DReNInMetadata.NodeType.SMALL_MOLECULE.getText();
        this.proteinName = DReNInMetadata.NodeType.PROTEIN.getText();
        this.ser = new Serialize();
        this.fmap = new FileManager();
        this.DBID2Type = ser.deserializeHashMap(fmap.getSuppFolder()+fmap.getFileSeparator() + "DrugBank_DBID2Type.ser");
        this.unichemMapping = fmap.getUniChemsrc1src2();
        this.chembl2db = new HashMap<>();
        this.activityTypes = new HashMap<>();
        this.ChEMBLstats = new File(fmap.getSuppFolder()+fmap.getFileSeparator()+"ChEMBL_stats.txt");
        this.handler =handler;
        this.filePath = fmap.getChEMBLBindsTo();

    }


    public void parseFile() {
        getChEMBLtoDBID();
        //record details about relations and properties
        BufferedWriter bw = null;
        BufferedReader br = null;

        SSIPNode drug = null;
        //relation attributes
        String uniProtID = null;
        String activityType = null;
        String activityValue = null;

        File dir = filePath;
        if (dir.exists()) {
            if (dir.isDirectory()) {
                LOGGER.log(Level.INFO, "Parsing drug-> target interactions from {0}", dir);
                File[] directoryListing = dir.listFiles();
                if (directoryListing != null) {
                    for (File child : directoryListing) {
                        if (!child.getName().startsWith(".DS_Store")) {

                            //createSSIPNode
                            String type = DBID2Type.get(chembl2db.get(child.getName().split("\\.")[0]));
                            if (type == null) {
                                type = "Small_Molecule";
                            }

                            //System.out.println("Checking for "+ child.getName().split("\\.")[0]);
                            //System.out.println("Created drug :: "+type +"\t"+chembl2db.get(child.getName().split("\\.")[0])) ;
                            drug = new SSIPNode(type, chembl2db.get(child.getName().split("\\.")[0]));

                            try {
                                br = new BufferedReader(new FileReader(child.getAbsoluteFile()));
                            } catch (FileNotFoundException ex) {
                                Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            int indivRel = 0;
                            String thisLine;
                            try {
                                //add relations
                                while ((thisLine = br.readLine()) != null) {
                                    String[] split = thisLine.split("\t");
                                    uniProtID = split[6];
                                    activityType = split[1];
                                    activityValue = split[2];
                                    //create the relation
                                    SSIPRelationType bindsTo = new SSIPRelationType(bindsToRel);
                                    bindsTo.addAttribute("ActivityType", activityType);
                                    System.out.println(activityType);
                                    bindsTo.addAttribute("ActivityValue", activityValue);
                                    System.out.println(activityValue);
                                    addActivityType(activityType, Double.parseDouble(activityValue));
                                    drug.addRelation(new SSIPNode(proteinName, uniProtID), bindsTo);
                                    indivRel++;
                                    bindsToCount++;
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            handler.addNode(drug);

                            try {
                                br.close();
                            } catch (IOException ex) {
                                Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }
                    }
                } else {
                    LOGGER.log(Level.INFO, "{0} is empty", dir);
                }

            } else {
                LOGGER.log(Level.INFO, "{0} is a file not a directory", dir);
            }

        } else {
            LOGGER.log(Level.INFO, "{0} does not exist", dir);
        }

        try {
            bw = new BufferedWriter(new FileWriter(ChEMBLstats));
        } catch (IOException ex) {
            Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (String key : activityTypes.keySet()) {

            try {
                bw.append(key + "  " + activityTypes.get(key).size() + "\n");
            } catch (IOException ex) {
                Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        for (String key : activityTypes.keySet()) {

            try {
                bw.append("---------------------------------------------------------------------" + key + "  " + activityTypes.get(key).size() + "\n");
            } catch (IOException ex) {
                Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
            }
            ArrayList<Double> values = activityTypes.get(key);
            for (double v : values) {

                try {
                    bw.append(v + "\n");
                } catch (IOException ex) {
                    Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        }

        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO,
                "Parsed {0} compound . target relations", bindsToCount);
        LOGGER.log(Level.INFO,
                "Further details regarding activity types and frequencies can be found in {0}", ChEMBLstats.getAbsolutePath());
    }


    public void getChEMBLtoDBID() {
        BufferedReader br = null;

        String line;
        try {
            br = new BufferedReader(new FileReader(unichemMapping));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            //skip first line
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] split = line.split("\t", -1);
                chembl2db.put(split[0], split[1]);
            }
        } catch (IOException ex) {
            Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(ChEMBL_bindsTo.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.println(chembl2db.toString());

    }

    /**
     * Stores the frequency of every activity type that is captured in the
     * dataset.
     *
     * @param activityType
     */
    public void addActivityType(String activityType, double value) {
        if (activityTypes.containsKey(activityType)) {
            ArrayList<Double> values = activityTypes.get(activityType);
            values.add(value);
            activityTypes.put(activityType, values);
        } else {
            ArrayList<Double> values = new ArrayList<Double>();
            values.add(value);
            activityTypes.put(activityType, values);
        }
    }

}
