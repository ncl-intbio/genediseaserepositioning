/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template filePath, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;


/**
 *
 * @author joe
 *
 * This class is designed to parse the MeSH (Medical Subject Headings) thesaurus
 * of terms. Contains a set of descriptors in a hierarchical structure. Uses the
 * descriptors filePath [d2015] which can be downloaded from: from:
 * http://www.nlm.nih.gov/mesh/filelist.html
 *
 * Top-level categories in the MeSH descriptor hierarchy are:
 *
 * Anatomy [A] Organisms [B] Diseases [C] Chemicals and Drugs [D] Analytical,
 * Diagnostic and Therapeutic Techniques and Equipment [E] Psychiatry and
 * Psychology [F] Biological Sciences [G] Physical Sciences [H] Anthropology,
 * Education, Sociology and Social Phenomena [I] Technology and Food and
 * Beverages [J] Humanities [K] Information Science [L] Persons [M] Health Care
 * [N] Publication Characteristics [V] Geographic Locations [Z]
 *
 * Note that DisGENet classify diseases according to the MeSH hierarchy using 23
 * upper level concepts of the tree branch [C], HOWEVER they also include three
 * concepts of the [F] branch ("Behavior and Behavior Mechanisms" [F01],
 * "Psychological Phenomena and Processes" [F02] , and "Mental Disorders"
 * [F03]). http://www.disgenet.org/web/DisGeNET/v2.1/dbinfo
 */
public class MeSHDRecords {

    private File filePath;
    private final static Logger LOGGER = Logger.getLogger(MeSHDRecords.class.getName());
    private int terms = 0;
    private int entryCount = 0;
    private int printEntryCount = 0;
    private Map<String, Set<String>> isARelations;
    private int isARelCount = 0;
    private Neo4J handler;
    private Map<String, Set<String>> mesh2type;
    private Map<String, String> treeterm2ID;
    private Map<String, String> headerTerms;
    private Map<String, String> headerTerms2MeSHUI;
    private Map<String, Set<String>> headerterms2MeshTree;
    private FileManager fmap;
    private Serialize ser;
    private String diseaseName;
    private String isADiseaseRel;
    private Set<String> meshUIs;


    public MeSHDRecords(Neo4J handler) {
        this.handler = handler;
        this.ser = new Serialize();
        this.fmap = new FileManager();
        this.filePath = fmap.getMeSHDFile();
        this.headerterms2MeshTree = new HashMap<>();
        this.headerTerms2MeSHUI = new HashMap<>();
        this.isARelations = new HashMap<>();
        this.treeterm2ID = new HashMap<>();
        this.headerTerms = new HashMap<>();
        this.diseaseName = DReNInMetadata.NodeType.COMMON_DISEASE.getText();
        this.isADiseaseRel = DReNInMetadata.RelTypes.IS_A_DISEASE.getText();
        this.fmap = new FileManager();
        this.mesh2type = ser.deserializeHashMapSet(fmap.getSuppFolder() +fmap.getFileSeparator()+ "ORDO_mesh2diseasetype.ser");
        this.meshUIs = new HashSet<String>();
    }

 

    /**
     * Returns a map of the mesh main header and all associated tree terms
     *
     * @return
     */
    public Map<String, Set<String>> getheaderterms2MeshTree() {
        return headerterms2MeshTree;
    }

    public void parseFile() {

        BufferedReader br = null;
        String thisLine;
        boolean firstDone = false;
        try {
            //Open the filePath for reading
            br = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MeSHDRecords.class.getName()).log(Level.SEVERE, null, ex);
        }

        //variables
        String MeSHMainHeader = null;
        String MeSHUI = null;
        Set<String> MeSHTreeTerms = new HashSet<String>();

        try {
            while ((thisLine = br.readLine()) != null) {
                if (thisLine.equals("*NEWRECORD")) {
                    //create new node and reset the variables
                    if (firstDone) {
                        SSIPNode s = new SSIPNode(getDisType(MeSHUI), MeSHUI);
                        s.addProperty("MeSH", MeSHUI);
                        s.addProperty("MeSHHeader", MeSHMainHeader);
                        
                        int c = 1;
                        //we are only interested in diseases [begin with C in the hierarchy ]
                        // and the three concepts of the F branch
                        boolean disease = false;
                        for (String tt : MeSHTreeTerms) {
                            if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                disease = true;
                            }
                        }

                        //if the term is a disease then add it
                        if (disease) {
                            Set<String> treeterms = new HashSet<String>();
                            for (String tt : MeSHTreeTerms) {
                                if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                    addTreeRel(tt);
                                    treeterm2ID.put(tt, MeSHUI);
                                    treeterms.add(tt);
                                    s.addProperty("MeSHTree" + c, tt);
                                    c++;
                                }
                            }

                            headerTerms2MeSHUI.put(MeSHMainHeader, MeSHUI);
                            headerterms2MeshTree.put(MeSHMainHeader, treeterms);
                            headerTerms.put(MeSHMainHeader, getDisType(MeSHUI));
                            handler.addNode(s);
                            meshUIs.add(MeSHUI);
                            terms++;
                        }

                        MeSHTreeTerms = new HashSet<String>();
                    }
                }

                if (thisLine.startsWith("MH =")) {
                    MeSHMainHeader = split(thisLine);
                }

                if (thisLine.startsWith("UI =")) {
                    MeSHUI = split(thisLine);
                    firstDone = true;
                }

                if (thisLine.startsWith("MN =")) {
                    MeSHTreeTerms.add(split(thisLine));

                }

                if (thisLine.startsWith("ENTRY =")) {
                    entryCount++;
                }

                if (thisLine.startsWith("PRINT ENTRY =")) {
                    printEntryCount++;
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(MeSHDRecords.class.getName()).log(Level.SEVERE, null, ex);
        }

        //make sure all nodes are committed before we refer to the id they are indexed on.
        handler.commitTransaction();

        //  System.out.println(treeterm2ID.toString());
        //add relations
        for (String s : isARelations.keySet()) {
            // System.out.println("s:  " + s);
            String MeSHUITo = treeterm2ID.get(s);
            Set<String> from = isARelations.get(s);
            SSIPRelationType rel = new SSIPRelationType(isADiseaseRel);
            for (String f : from) {
                //System.out.println("f:  " + f);
                String MeSHUIFrom = treeterm2ID.get(f);
                if (MeSHUIFrom != null && MeSHUITo != null) {
                    isARelCount++;
                    SSIPNode fromTT = new SSIPNode(getDisType(MeSHUIFrom), MeSHUIFrom);
                    fromTT.addRelation(new SSIPNode(getDisType(MeSHUITo), MeSHUITo), rel);
                    handler.addNode(fromTT);
                }

            }

        }

        handler.commitTransaction();

        Serialize s = new Serialize();
        s.SerializeHashMap(headerterms2MeshTree, fmap.getSuppFolder()+fmap.getFileSeparator() + "MeSH_headerterms2MeshTree.ser");

        s.SerializeHashMap(headerTerms, fmap.getSuppFolder()+fmap.getFileSeparator() + "MeSH_headerTerms.ser");
        s.SerializeHashMap(headerTerms2MeSHUI, fmap.getSuppFolder()+fmap.getFileSeparator() + "MeSH_headerTerms2MeSHUI.ser");

        s.SerializeSetString(meshUIs, fmap.getSuppFolder()+fmap.getFileSeparator() + "MeSH_meshuis.ser");
        LOGGER.log(Level.INFO, "Added {0} MeSHUIs to \"MeSH_meshuis.ser\"", meshUIs.size());
        LOGGER.log(Level.INFO, "Parsed [{0}] MeSH descriptor terms", terms);
        LOGGER.log(Level.INFO, "Parsed [{0}] hierarchical relations", isARelCount);
    }

    /**
     * Splits the MeSH line (aaaa = bbbbb) on the "=" and returns the second
     * element.
     *
     * @return
     */
    public String split(String meshLine) {

        String[] split = meshLine.split("=");
        return split[1].trim();

    }

    /**
     * Tree numbers are provided in the form C19.800.090. This method returns a
     * isARelation, from C19.900.090 >>> C19.900 and stores it in the hashMap to
     * be added after all nodes have been created.
     *
     * @param treeNumber
     */
    public void addTreeRel(String treeNumber) {

        String from = treeNumber;
        String to = null;
        String[] split = treeNumber.split("\\.");
        if (split.length == 1) {
            //do nothing it is a term at the top of the hierarchy
        } else {
            //return the parent name
            String temp = "";
            for (int i = 0; i < split.length - 1; i++) {
                temp = temp.trim() + split[i] + ".";
            }

            //remove the last full stop
            to = temp.substring(0, temp.length() - 1);
            // System.out.println("Adding relation " + from + ">>" + to);

            //store the relation
            if (isARelations.containsKey(to)) {
                Set<String> froms = isARelations.get(to);
                froms.add(from);
                isARelations.put(to, froms);
            } else {
                Set<String> froms = new HashSet<String>();
                froms.add(from);
                isARelations.put(to, froms);
            }
        }
    }

    /**
     * Method takes a MeSH id. if the term is covered in the OD [disease] or the
     * ORDO [rare_disease] then the type is returned; if it is not then MESHTerm
     * is returned.
     *
     * @param mesh
     * @return
     */
    public String getDisType(String mesh) {
        //System.out.println("Checking for: "+ mesh);
        for (String keys : mesh2type.keySet()) {
            Set<String> ids = mesh2type.get(keys);
            if (ids.contains(mesh)) {
                //System.out.println(keys);
                return keys;
            }
        }

        //System.out.println("MeSHTerm");
        return diseaseName;
    }

    /**
     * Returns a set of the header terms for the MeSH descriptors parsed into
     * the graph. This is used to ensure only diseases are integrated from the
     * supplementary records. If the term they are to be mapped too are included
     * in this list then they are a disease;
     *
     * @return
     */
    public Map<String, String> getHeaderTerms() {
        return headerTerms;

    }

}
