/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata.DataSource;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class DrugBank {

    //required
    private File filepath;
    private Serialize ser;
    private FileManager fmap;
    private Scanner sc;
    private Neo4J handler;
    private final static Logger LOGGER = Logger.getLogger(DrugBank.class.getName());
    //specific to this parser
    private String ELEMENT = "drug";
    private final String DRUG_NAME_XPATH = "/drug/name";
    private final String DRUG_DBID_XPATH = "/drug/drugbank-id";
    private final String DRUG_TYPE_XPATH = "/drug/@type";
    private final String DRUG_GROUPS_XPATH = "/drug/groups/group";
    private final String DRUG_SYNONYMS_XPATH = "/drug/synonyms/synonym";
    private final String DRUG_CATEGORIES_XPATH = "/drug/categories/category/category";
    private final String DRUG_AFFORG_XPATH = "/drug/affected-organisms/affected-organism";
    private final String TARGET_XPATH = "/drug/targets/target";
    private final String TRANSPORTER_TARGET_XPATH = "/drug/transporters/transporter";
    private final String ENZYME_TARGET_XPATH = "/drug/enzymes/enzyme";
    private final String INTERACTIONS_XPATH = "/drug/drug-interactions/drug-interaction";
    private final String DRUG_PROPERTIES_XPATH = "/drug/calculated-properties/property";
    private boolean parseDrug = false;
    private Set<String> drugActions;
    private boolean element = false;
    private String content = "";
    private HashMap<String, Integer> mechCount;
    private BufferedWriter drugMech;
    //counts
    private int parsedDrugs = 0;
    private int smallMolecule = 0;
    private int biotech = 0;
    private int nutraceuticals = 0;
    private int drugInteractions = 0;
    private int bindsTo = 0;
    private Set tarNames = new HashSet<String>();
    private int numberOfDrugs = 0;
    private Map drugTypeCache = new HashMap<>();
    private Map drugNameDBIDCache = new HashMap<>();
    private boolean initial = true;
    private int nonHuman = 0;
    int count = 0;
    private boolean getTargets;
    private HashMap<String, String> chem2type;
    //node names
    private String smallMoleculeName;
    private String bioTechName;
    private String proteinName;
    private String bindsToRel;
    private final DataSource prov;
    private DReNInMetadata provenance;

    public DrugBank(Neo4J handler, boolean targets) {
        this.fmap = new FileManager();
        this.filepath = fmap.getDrugBankFile();
        this.handler = handler;
        this.getTargets = targets;
        this.smallMoleculeName = DReNInMetadata.NodeType.SMALL_MOLECULE.getText();
        this.bioTechName = DReNInMetadata.NodeType.BIOTECH.getText();
        this.proteinName = DReNInMetadata.NodeType.PROTEIN.getText();
        this.bindsToRel = DReNInMetadata.RelTypes.BINDS_TO.getText();
        this.chem2type = new HashMap<>();
        this.ser = new Serialize();
        this.drugActions = new HashSet<>();
        this.mechCount = new HashMap<String, Integer>();
        try {
            this.drugMech = new BufferedWriter(new FileWriter(new File(fmap.getSuppFolder() + fmap.getFileSeparator() + "DB_MECH_KNOWNS.txt")));
        } catch (IOException ex) {
            Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.prov = DReNInMetadata.DataSource.DRUGBANKv4;
        this.provenance = new DReNInMetadata();


    }

    /**
     * Parse in the XML database as a stream
     *
     */
    public void parseFile() {
        BufferedReader br = null;
        try {
            String line;
            br = new BufferedReader(new FileReader(filepath));
            while ((line = br.readLine()) != null) {

                if (line.equals("</drug>")) {
                    content = content + line;
                    Document drugDoc = getXML(content);
                    count++;
                    try {
                        getDrugs(drugDoc);
                    } catch (XPathExpressionException ex) {
                        System.out.println("XPath exception error! getDrugs");
                        Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    content = "";
                    parseDrug = false;
                }
                if (parseDrug) {
                    content = content + line;
                }
                if (line.startsWith("<drug ")) {
                    content = line;
                    parseDrug = true;
                }
            }

        } catch (IOException e) {
            System.out.println("IO exception in parseFile error in DrugBankXML");
            e.printStackTrace();

        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                System.out.println("IO exception parseFile error in DrugBankXML");
                ex.printStackTrace();
            }
        }

        handler.commitTransaction();
        //add the drug drug interactions
        // addDrugInteractions();

        // System.out.println(drugTypeCache.toString());
        // System.out.println(drugNameDBIDCache.toString());

        ser.SerializeHashMap(drugTypeCache, fmap.getSuppFolder() + fmap.getFileSeparator() + "DrugBank_DBID2Type.ser");
        ser.SerializeHashMap(drugNameDBIDCache, fmap.getSuppFolder() + fmap.getFileSeparator() + "DrugBank_drugName2DBID.ser");
        try {
            drugMech.close();
        } catch (IOException ex) {
            Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Parsed {0} small molecules", smallMolecule);
        LOGGER.log(Level.INFO, "Parsed {0} biotech", biotech);
        LOGGER.log(Level.INFO, "Parsed {0} nutraceuticals", nutraceuticals);
        LOGGER.log(Level.INFO, "Parsed {0} unique targets", tarNames.size());
        LOGGER.log(Level.INFO, "Parsed {0} interacts_with rels", drugInteractions);
        LOGGER.log(Level.INFO, "Parsed {0} binds_to rels", bindsTo);
    }

    /**
     * Convert a String to an XML document
     *
     * @param convert
     */
    public Document getXML(String convert) {

        //System.out.println(convert);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            System.out.println("Parser config exception error from builder factory.newDocumentBuilder");
            Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
        }

        Document document = null;
        try {
            document = builder.parse(new InputSource(new StringReader(convert)));
        } catch (SAXException ex) {
            System.out.println("SAX exception error from builder.parse");
            Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println("IO exception error from builder.parse");
            Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        return document;

    }

    /**
     * Retrieve all the required information from the XML document
     *
     * @param document
     * @throws XPathExpressionException
     */
    public void getDrugs(Document document) throws XPathExpressionException {
        //initial node for Neo4J
        SSIPNode initialNode = null;
        NodeList nodes = document.getChildNodes();
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        //initialise an empty SSIP Node
        SSIPNode drugNode = null;

        //drug attributes to be extracted     
        String dbID = null;
        String name = null;
        Set<String> accessions = null;
        String type = null;
        // String SMILES = null;
        // String InChI = null;
        Set<String> groups = null;
        Set<String> synonyms = null;
        Set<String> categories = null;
        //not an attribute just a check- only want drugs that affect humans
        Set<String> affectedOrganisms = null;
        boolean affectsHumans = false;
        boolean node = false;
        //cache the drug type

        dbID = (String) xpath.evaluate(DRUG_DBID_XPATH, document, XPathConstants.STRING);
        type = (String) xpath.evaluate(DRUG_TYPE_XPATH, document, XPathConstants.STRING);
        name = (String) xpath.evaluate(DRUG_NAME_XPATH, document, XPathConstants.STRING);

        //get all drug groups
        NodeList groupsNodes = (NodeList) xpath.evaluate(DRUG_GROUPS_XPATH, document, XPathConstants.NODESET);
        groups = getAttributeFromNodeList(groupsNodes, xpath);

        //get all synonyms (we use these to help map the clinical trials data whch only gives string names for drugs :/)
        NodeList synonymNodes = (NodeList) xpath.evaluate(DRUG_SYNONYMS_XPATH, document, XPathConstants.NODESET);
        synonyms = getAttributeFromNodeList(synonymNodes, xpath);
        for (String syn : synonyms) {
            drugNameDBIDCache.put(syn.toLowerCase(), dbID);
        }

        //get all drug categories
        NodeList categoriesNodes = (NodeList) xpath.evaluate(DRUG_CATEGORIES_XPATH, document, XPathConstants.NODESET);
        categories = getAttributeFromNodeList(categoriesNodes, xpath);

        //get all affected organisms- we are only concerned with those that affect humans
        NodeList affectedOrganismNodes = (NodeList) xpath.evaluate(DRUG_AFFORG_XPATH, document, XPathConstants.NODESET);
        affectedOrganisms = getAttributeFromNodeList(affectedOrganismNodes, xpath);

        //get the drug properties; this is how we extract the SMILES string
        NodeList drugProperties = (NodeList) xpath.evaluate(DRUG_PROPERTIES_XPATH, document, XPathConstants.NODESET);
        // SMILES = getProp(drugProperties, xpath, "SMILES");
        // InChI = getProp(drugProperties, xpath, "InChI");

        if (affectedOrganisms.contains("Humans and other mammals")) {
            affectsHumans = true;
        } else {
            nonHuman++;
        }

        //only add the drug if it affects humans
        //if (affectsHumans) {
        if (type.equals("biotech")) { //&& affectsHumans == true) {
            type = bioTechName;
            drugNode = new SSIPNode(type, dbID);
            biotech++;
            node = true;
        }
        if (type.equals("small molecule")) {// && affectsHumans == true) {
            type = smallMoleculeName;
            drugNode = new SSIPNode(type, dbID);
            smallMolecule++;
            node = true;
        }

        HashMap<String, String> targetIds = null;
        if (getTargets) {
            //get  targets
            NodeList targets = (NodeList) xpath.evaluate(TARGET_XPATH, document, XPathConstants.NODESET);
            targetIds = getTargets(xpath, targets);
            // System.out.println("just targets: "+targetIds.toString());
            //get enzyme targets
//            targets = (NodeList) xpath.evaluate(ENZYME_TARGET_XPATH, document, XPathConstants.NODESET);
//            targetIds.addAll(getTargets(xpath, targets));
//            // System.out.println("just targets and enzyme: "+targetIds.toString());
//            //get transporter targets
//            targets = (NodeList) xpath.evaluate(TRANSPORTER_TARGET_XPATH, document, XPathConstants.NODESET);
//            targetIds.addAll(getTargets(xpath, targets));
            // System.out.println("just targets and enzyme and transporter: "+targetIds.toString());
        }

        //get all interactions
        NodeList interactions = (NodeList) xpath.evaluate(INTERACTIONS_XPATH, document, XPathConstants.NODESET);

        //submit node
        if (node) {
            //System.out.println(dbID);
            //    get drug interacitons and cache
            // Set<String> drugInteractions = getDrugInteractions(xpath, interactions);
            //      if we have any targets create corresponding node
            //    get drug interacitons and cache

            //      if we have any targets create corresponding node
            if (targetIds != null && !targetIds.isEmpty()) {
                for (String protein : targetIds.keySet()) {
                    SSIPNode proteinNode = new SSIPNode(proteinName, protein);
                    //System.out.println(dbID+"\t"+ protein);
                    //    handler.addNode(proteinNode);
                    SSIPRelationType s = new SSIPRelationType(bindsToRel);
                    s.addAttribute(provenance.addProv("Relation"), prov.toString());
                    s.addAttribute("DrugBank_knownAction", targetIds.get(protein).split("\t")[0].trim());
                    s.addAttribute("DrugBank_action", targetIds.get(protein).split("\t")[1].trim());
                    if (mechCount.containsKey(targetIds.get(protein))) {
                        int count = mechCount.get(targetIds.get(protein));
                        mechCount.put(targetIds.get(protein), count + 1);
                    } else {
                        mechCount.put(targetIds.get(protein), 1);
                    }
                    drugNode.addRelation(proteinNode, s);
                    bindsTo++;
                }
            }

            StringBuilder mechs = new StringBuilder();
            mechs.append(dbID).append("\t");

            //add actions to the drug mechanism file if not empty
            if (!drugActions.isEmpty()) {
                for (String me : drugActions) {
                    mechs.append(me).append("\t");
                }
                try {
                    drugMech.append(mechs + "\n");
                } catch (IOException ex) {
                    Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            drugActions = new HashSet<String>();

            //druginteraction relations
            drugTypeCache.put(dbID, type);
            drugNameDBIDCache.put(name.toLowerCase(), dbID);
//
//            if (!drugInteractions.isEmpty()) {
//                drugInteractionsCache.put(dbID, drugInteractions);
//            }
            //add properties
            if (dbID != null) {
                drugNode.addProperty("DBID", dbID);
            }
            if (affectedOrganisms != null && !affectedOrganisms.isEmpty()) {
                // System.out.println(affectedOrganisms.toString());
                int count = 0;
                for (String efOr : affectedOrganisms) {
                    if (!efOr.isEmpty()) {
                        drugNode.addProperty("AffectedOrganism" + count, efOr.trim());
                        // System.out.println("AffectedOrganism" + count + " :: " + efOr.trim());
                        count++;
                    }

                }
            }
            if (name != null) {
                drugNode.addProperty("label", name);
            }
            if (groups != null && !groups.isEmpty()) {
                // System.out.println(groups.toString());
                //for(S)
                int count = 0;
                for (String group : groups) {
                    if (!group.isEmpty()) {
                        drugNode.addProperty("Group" + count, group.trim());
                        // System.out.println("Group" + count + " :: " + group.trim());
                        count++;
                    }
                }
            }
            if (categories != null && !categories.isEmpty()) {
                // System.out.println(categories.toString());
                //drugNode.addProperty("Category", categories.toString());
                int count = 0;
                for (String cat : categories) {
                    if (!cat.isEmpty()) {
                        drugNode.addProperty("Category" + count, cat.trim());
                        //System.out.println("Category" + count + "::" + cat.trim());
                        count++;
                    }
                }

            }

            handler.addNode(drugNode);
            numberOfDrugs++;

        }


    }

    /**
     * Create target nodes and interactions from the drug to these
     *
     * @param xpath
     * @param targets
     */
    public HashMap<String, String> getTargets(XPath xpath, NodeList targets) {

        HashMap<String, String> targetIDs = new HashMap<String, String>();
        for (int i = 0; i < targets.getLength(); i++) {
            try {
                //target attributes: we only need all this info if we haven't already got our proteins from another source
                //such as UNIRPOT_SWISSPROT
                String id = null;
                String SwissProtpolypeptideID = null;
                String tarName = null;
                String aminoAcidSequence = null;
                String geneSequence = null;
                String ncbiTaxonomy = null;
                String knownAction = null;
                String action = null;


                knownAction = (String) xpath.evaluate("known-action", targets.item(i), XPathConstants.STRING);
                action = (String) xpath.evaluate("actions/action", targets.item(i), XPathConstants.STRING);
                id = (String) xpath.evaluate("id", targets.item(i), XPathConstants.STRING);
                String polypepSource = (String) xpath.evaluate("polypeptide/@source", targets.item(i), XPathConstants.STRING);
                if (polypepSource.equals("Swiss-Prot")) {
                    SwissProtpolypeptideID = (String) xpath.evaluate("polypeptide/@id", targets.item(i), XPathConstants.STRING);
                }

                tarName = (String) xpath.evaluate("name", targets.item(i), XPathConstants.STRING);
                aminoAcidSequence = (String) xpath.evaluate("polypeptide/amino-acid-sequence", targets.item(i), XPathConstants.STRING);
                geneSequence = (String) xpath.evaluate("polypeptide/gene-sequence", targets.item(i), XPathConstants.STRING);
                ncbiTaxonomy = (String) xpath.evaluate("organism/@ncbi-taxonomy-id", targets.item(i), XPathConstants.STRING);

                if (SwissProtpolypeptideID != null) {
                    if (knownAction.isEmpty()) {
                        knownAction = "NONE";
                    }
                    if (action.isEmpty()) {
                        action = "NONE";
                    }

                    targetIDs.put(SwissProtpolypeptideID, knownAction + "\t" + action);
                }
                if (knownAction.equals("yes") && !action.isEmpty()) {
                    drugActions.add(action);
                }
                tarNames.add(id);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return targetIDs;
    }

    /**
     * Returns the set of attributes from a nodeList, when it is the same tag to
     * be used each time.
     *
     * @param nodes
     * @param xpath
     * @return
     */
    public Set<String> getAttributeFromNodeList(NodeList nodes, XPath xpath) {
        Set<String> attributes = new HashSet<>();
        for (int i = 0; i < nodes.getLength(); i++) {

            try {
                attributes.add((String) xpath.evaluate("text()", nodes.item(i), XPathConstants.STRING));
            } catch (XPathExpressionException ex) {
                System.out.println("XPath attributes exception error! get attributes from node");
                Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return attributes;
    }

    /**
     * Returns the set of attributes from a nodeList, when it is the same tag to
     * be used each time.
     *
     * @param nodes
     * @param tag
     * @param xpath
     * @return
     */
    public String getProp(NodeList nodes, XPath xpath, String propName) {
        String attributes = null;
        for (int i = 0; i < nodes.getLength(); i++) {
            String kind = null;
            try {
                kind = (String) xpath.evaluate("kind", nodes.item(i), XPathConstants.STRING);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (kind.equals(propName)) {
                try {
                    attributes = ((String) xpath.evaluate("value", nodes.item(i), XPathConstants.STRING));
                } catch (XPathExpressionException ex) {
                    System.out.println("XPath attributes exception error!");
                    Logger.getLogger(DrugBank.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
        return attributes;
    }

    @Override
    public String toString() {
        return "Object: " + this.getClass().getName() + "\nFilepath: " + this.filepath;
    }
}
