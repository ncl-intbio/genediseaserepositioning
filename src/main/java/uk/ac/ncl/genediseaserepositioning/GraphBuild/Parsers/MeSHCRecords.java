/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template filePath, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author joe
 *
 * This class is designed to parse the MeSH (Medical Subject Headings)
 * supplementary headings. That is terms that are not captured by the descriptor
 * records (e.g C537505). Uses the supplementary concept records filePath
 * [c2015] which can be downloaded from: from:
 * http://www.nlm.nih.gov/mesh/filelist.html
 *
 * Top-level categories in the MeSH descriptor hierarchy are:
 *
 *
 */
public class MeSHCRecords {

    private File filePath;
    private final static Logger LOGGER = Logger.getLogger(MeSHCRecords.class.getName());
    private int terms = 0;
    private int entryCount = 0;
    private int printEntryCount = 0;
    private Map<String, Set<String>> isARelations;
    private int isARelCount = 0;
    private Neo4J handler;
    private Map<String, Set<String>> mesh2type;
    private Map<String, String> treeterm2ID;
    private Map<String, String> headerTerms;
    private FileManager fmap;
    private Map<String, Set<String>> headerterms2MeshTree;
    private Map<String, String> headerTerms2MeSHUI;
    private Serialize ser;
    private String diseaseName;
    private Set<String> meshUIs;
    private final String isADiseaseRel;

    public MeSHCRecords(Neo4J handler) {


        this.handler = handler;
        this.treeterm2ID = new HashMap<>();
        this.fmap = new FileManager();
        this.filePath = fmap.getMeSHCFile();
        this.ser = new Serialize();
        this.mesh2type = ser.deserializeHashMapSet(fmap.getSuppFolder() + fmap.getFileSeparator() + "ORDO_mesh2diseasetype.ser");
        this.isARelations = new HashMap<>();
        this.isADiseaseRel = DReNInMetadata.RelTypes.IS_A_DISEASE.getText();
        this.diseaseName = DReNInMetadata.NodeType.COMMON_DISEASE.getText();
        this.headerterms2MeshTree = ser.deserializeHashMapSet(fmap.getSuppFolder() + fmap.getFileSeparator() + "MeSH_headerterms2MeshTree.ser");
        this.headerTerms = ser.deserializeHashMap(fmap.getSuppFolder() + fmap.getFileSeparator() + "MeSH_headerTerms.ser");

        this.headerTerms2MeSHUI = ser.deserializeHashMap(fmap.getSuppFolder() + fmap.getFileSeparator() + "MeSH_headerTerms2MeSHUI.ser");

        this.meshUIs = ser.deserializeSetString(fmap.getSuppFolder() + fmap.getFileSeparator() + "MeSH_meshuis.ser");

    }

    public void parseFile() {
        
        int originalMeSHUis =  meshUIs.size();

        BufferedReader br = null;
        String thisLine;
        boolean firstDone = false;
        try {
            //Open the filePath for reading
            br = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MeSHCRecords.class.getName()).log(Level.SEVERE, null, ex);
        }

        SSIPNode suppRec = null;
        //variables
        String MeSHMainHeader = null;
        String MeSHUI = null;
        //could be more than one
        Set<String> headingMappedTo = new HashSet<String>();
        boolean add = false;

        try {
            while ((thisLine = br.readLine()) != null) {
                if (thisLine.equals("*NEWRECORD")) {
                    //create new node and reset the variables

                    if (firstDone) {
                        if (!headerTerms.isEmpty()) {
                            suppRec = new SSIPNode(getDisType(MeSHUI), MeSHUI);
                            //System.out.println(getDisType(MeSHUI)+"\t"+ MeSHUI);
                            suppRec.addProperty("MeSH", MeSHUI);
                            suppRec.addProperty("MeSHHeader", MeSHMainHeader);
                            for (String hm : headingMappedTo) {
                                if (headerTerms.containsKey(hm)) {
                                    suppRec.addRelation(new SSIPNode(headerTerms.get(hm), headerTerms2MeSHUI.get(hm)), new SSIPRelationType(isADiseaseRel));
                                    isARelCount++;
                                    add = true;
                                }
                                if (headerterms2MeshTree.containsKey(hm)) {
                                    Set<String> treeterms = headerterms2MeshTree.get(hm);
                                    for (String tt : treeterms) {
                                        int c = 1;
                                        if (tt.startsWith("C") || tt.startsWith("F01") || tt.startsWith("F02") || tt.startsWith("F03")) {
                                            suppRec.addProperty("MeSHTree" + c, tt + ".supp");
                                            c++;
                                        }
                                    }
                                }

                            }

                            if (add) {
                                handler.addNode(suppRec);
                                meshUIs.add(MeSHUI);
                                terms++;
                            }
                        }
                        add = false;
                        headingMappedTo = new HashSet<String>();
                    }
                }

                if (thisLine.startsWith("NM =")) {
                    MeSHMainHeader = split(thisLine);
                }

                if (thisLine.startsWith("HM =")) {
                    headingMappedTo.add(split(thisLine));
                }

                if (thisLine.startsWith("UI =")) {
                    MeSHUI = split(thisLine);
                    firstDone = true;
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(MeSHCRecords.class.getName()).log(Level.SEVERE, null, ex);
        }

        //make sure all nodes are committed before we refer to the id they are indexed on.
        handler.commitTransaction();
        LOGGER.log(Level.INFO, "Parsed [{0}] MeSH supplementary record terms", terms);
        LOGGER.log(Level.INFO, "Parsed [{0}] relations", isARelCount);
        ser.SerializeSetString(meshUIs, fmap.getSuppFolder() + fmap.getFileSeparator() + "MeSH_meshuis.ser");
        LOGGER.log(Level.INFO, "Added {0} MeSHUIs to \"MeSH_meshuis.ser\", with a total of {1}", new Object[]{meshUIs.size() - originalMeSHUis, meshUIs.size()});
        
        //System.out.println(meshUIs.toString());
    }

    /**
     * Splits the MeSH line (aaaa = bbbbb) on the "=" and returns the second
     * element.
     *
     * @return
     */
    public String split(String meshLine) {
        String[] split = meshLine.split("=");
        return split[1].trim();
    }

    /**
     * Method takes a MeSH id. If the term is covered in the ORDO [rare_disease]
     * then the type is returned; otherwise a [disease] type is returned.
     *
     * @param mesh
     * @return
     */
    public String getDisType(String mesh) {
        for (String type : mesh2type.keySet()) {
            Set<String> ids = mesh2type.get(type);
            if (ids.contains(mesh)) {
                return type;
            }
        }
        return diseaseName;
    }
}
