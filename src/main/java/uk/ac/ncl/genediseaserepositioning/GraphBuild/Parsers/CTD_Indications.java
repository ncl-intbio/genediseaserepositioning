/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphBuild.Parsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.Serialize;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class CTD_Indications {

    /*
     * Done by calling the python script Python/extractCTDIndications.py.
     */
    public File drugDisfile;
    public File CTDChemfile;
    public FileManager fman;
    public Neo4J handler;
    private Map<String, Set<String>> mesh2type;
    private Map<String, String> DBID2Type;
    private Serialize ser;
    private String diseaseName;
    private String rareDiseaseName;
    private String indicationRelation;
    private String smallMolName;
    private String biotechName;
    private final static Logger LOGGER = Logger.getLogger(CTD_Indications.class.getName());
    
    private final DReNInMetadata.DataSource prov;
    private DReNInMetadata provenance;

    public CTD_Indications(Neo4J handler) {
        this.handler = handler;
        this.fman = new FileManager();
        this.ser = new Serialize();
        this.drugDisfile = fman.getCTDIndications();
        this.CTDChemfile = fman.getCTDChemicals();
        this.mesh2type = ser.deserializeHashMapSet(fman.getSuppFolder() + fman.getFileSeparator() + "ORDO_mesh2diseasetype.ser");
        this.DBID2Type = ser.deserializeHashMap(fman.getSuppFolder() + fman.getFileSeparator() + "DrugBank_DBID2Type.ser");
        this.diseaseName = DReNInMetadata.NodeType.COMMON_DISEASE.getText();
        this.rareDiseaseName = DReNInMetadata.NodeType.RARE_DISEASE.getText();
        this.indicationRelation = DReNInMetadata.RelTypes.HAS_INDICATION.getText();
        this.smallMolName = DReNInMetadata.NodeType.SMALL_MOLECULE.getText();
        this.biotechName = DReNInMetadata.NodeType.BIOTECH.getText();
        
        this.prov = DReNInMetadata.DataSource.CTD;
        this.provenance = new DReNInMetadata();
    }

    public void parseFile() {
        callPython();
        addToGraph();
    }

    public void addToGraph() {

        int relations = 0;
        
        String thisLine;
        String DBID = null;
        String drugType = null;
        String MeSHUI = null;
        String diseaseType = null;

        SSIPNode drug = null;
        SSIPRelationType indication = null;

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fman.getSuppFolder().getAbsolutePath() + fman.getFileSeparator() + "CTD_Curated_Indications.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NDFRT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((thisLine = br.readLine()) != null) {
                String[] split = thisLine.split("\t", -1);
                DBID = split[0];
                drugType = DBID2Type.get(DBID);
                MeSHUI = split[1];
                diseaseType = getDisType(MeSHUI);
                if (drugType == null) {
                    LOGGER.log(Level.INFO, "No drugType for drug {0}", DBID);
                } else {
                    drug = new SSIPNode(drugType, DBID);
                    indication = new SSIPRelationType(indicationRelation);
                    indication.addAttribute(provenance.addProv("Relation"), prov.toString());
                    drug.addRelation(new SSIPNode(diseaseType, MeSHUI), indication);
                    handler.addNode(drug);
                    relations++;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(NDFRT_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, "Added {0} has_indication relations from CTD", relations);
    }

    public void callPython() {

        Process p = null;
        try {
            p = Runtime.getRuntime().exec("python Python/extractCTDIndications.py  " + drugDisfile.getAbsolutePath() + "  " + CTDChemfile.getAbsolutePath() + " " + fman.getSuppFolder().getAbsolutePath() + fman.getFileSeparator());
        } catch (IOException ex) {
            Logger.getLogger(CTD_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line = null;
        String re = "";
        try {
            while ((line = in.readLine()) != null) {
                re = re + line + "\n";
            }
        } catch (IOException ex) {
            Logger.getLogger(CTD_Indications.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(re);
    }

    /**
     * Method takes a MeSH id. if the term is covered in the OD [disease] or the
     * ORDO [rare_disease] then the type is returned; if it is not then MESHTerm
     * is returned.
     *
     * @param mesh
     * @return
     */
    public String getDisType(String mesh) {
        //System.out.println("Checking for: "+ mesh);
        for (String keys : mesh2type.keySet()) {
            Set<String> ids = mesh2type.get(keys);
            if (ids.contains(mesh)) {
                //System.out.println(keys);
                return keys;
            }
        }

        //System.out.println("MeSHTerm");
        return diseaseName;
    }
}
