/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphMine.PropComparator;

/**
 *
 * @author joe
 *
 * Class provides a way of comparing values (of type double) of a property; be
 * it on a node or an edge.
 *
 */
public class DoubleComp implements Comparator {

    private checkType c;
    private String key;
    private double value;

    public static void main(String[] args) {

        DoubleComp p = new DoubleComp("IC50", checkType.LESS_THAN, 100.0);
        if (p.Compare("IC50", "1.0")) {
            System.out.println("rue bit");

        }

    }

    /**
     * Constructor- define the type of comparison and provide a key and value to
     * be compared too.
     *
     * @param c
     * @param key
     * @param value
     */
    public DoubleComp(String key, checkType c, double value) {
        this.c = c;
        this.key = key;
        this.value = value;
    }

    /**
     * Actual comparison method.
     *
     * @param checkkey
     * @param checkvalue
     * @return the boolean value returns reflects whether or not a node should
     * be removed during a search i.e. a true means that it will be removed as
     * there was no match and vice versa.
     *
     */
    public boolean Compare(String checkkey, String checkvalue) {
        if (isDouble(checkvalue)) {
            double check = Double.parseDouble(checkvalue);
            if (c.equals(checkType.GREATER_THAN)) {
                if (key.toLowerCase().equals(checkkey.toLowerCase())) {
                    if (check > value) {
                        return false;
                    }
                } else {
                    return true;
                }
            } else if (c.equals(checkType.LESS_THAN)) {
                if (key.toLowerCase().equals(checkkey.toLowerCase())) {
                    if (check < value) {
                        return false;
                    }
                } else {
                    return true;
                }
            }
        }
        return true;
    }

    /**
     * Checks to see if the value we are comparing is actually a double.
     *
     * @param value
     * @return
     */
    public boolean isDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public String getPropKey() {
        return key;
    }

}
