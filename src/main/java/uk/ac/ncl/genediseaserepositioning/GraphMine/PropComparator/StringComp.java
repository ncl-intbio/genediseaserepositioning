/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphMine.PropComparator;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author joe
 */
public class StringComp implements Comparator {

    private checkType c;
    private String key;
    private String value;
    private Set<String> mulValue;

    public static void main(String[] args) {

        Set<String> test = new HashSet<String>();
        test.add("dave");
        test.add("craig");

        StringComp p = new StringComp("name", checkType.EQUALS_EITHER_VALUE, test);
        if (p.Compare("name", "craig")) {
            System.out.println("did not stonatin it");
        } else {
            System.out.println("Matched: will NOT be removed");
        }

    }

    /**
     * Constructor- define the type of comparison and provide a key and value to
     * be compared too.
     *
     * @param c
     * @param key
     * @param value
     */
    public StringComp(String key, checkType c, String value) {
        this.c = c;
        this.key = key;
        this.value = value;
    }

    /**
     * Constructor- define the type of comparison and provide a key and value to
     * be compared too.
     *
     * @param c
     * @param key
     * @param value
     */
    public StringComp(String key, checkType c, Set<String> value) {
        this.c = c;
        this.key = key;
        this.mulValue = value;
    }

    /**
     * Actual comparison method.
     *
     * @param checkkey
     * @param checkvalue
     * @return the boolean value returns reflects whether or not a node should
     * be removed during a search i.e. a true means that it will be removed as
     * there was no match and vice versa.
     *
     */
    public boolean Compare(String checkkey, String checkvalue) {

        if (c.equals(checkType.CONTAINS)) {
            if (checkvalue.toLowerCase().contains(value.toLowerCase())) {
                return false;
            } else {
                return true;
            }
        } else if (c.equals(checkType.EQUALS)) {
            if (checkvalue.toLowerCase().equals(value.toLowerCase())) {

                return false;
            } else {
                return true;
            }
        } else if (c.equals(checkType.EQUALS_EITHER_VALUE)) {

            for (String val : mulValue) {
                if (checkvalue.toLowerCase().equals(val.toLowerCase())) {
                    return false;
                }
            }

        }

        return true;
    }

    @Override
    public String getPropKey() {
        return key;
    }

}
