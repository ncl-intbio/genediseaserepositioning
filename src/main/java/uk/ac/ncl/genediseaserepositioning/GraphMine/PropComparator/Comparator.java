/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphMine.PropComparator;

/**
 *
 * @author joe
 */
public interface Comparator {

    checkType compare = null;
    String key = null;

    
    enum checkType {
        CONTAINS,
        EQUALS_EITHER_VALUE,
        EQUALS,
        GREATER_THAN,
        LESS_THAN;
    }

    /**
     * Does the comparison.
     *
     * @param key Property key being compared
     * @param value Property value being compared
     * @return
     */
    public boolean Compare(String key, String value);
    
    
    public String getPropKey();

}
