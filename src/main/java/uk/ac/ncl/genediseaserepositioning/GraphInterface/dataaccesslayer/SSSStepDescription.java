/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.neo4j.graphdb.Direction;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SSSQuery.Dir;


/**
 *
 * @author Joe Mullen
 */
public class SSSStepDescription {

    private int depth;
    private boolean depthFirst;
    //to do add ability to specify node type as well as relation type
    private List<String> relations;
    private Dir relDir;
    private Map<String, Object> nodeProps;
    private Map<String, Object> relProps;

    public void setNodeProps(Map<String, Object> nodeProps) {
        this.nodeProps = nodeProps;
    }

    public void setRelProps(Map<String, Object> relProps) {
        this.relProps = relProps;
    }

  

    public Map<String, Object> getNodeProps() {
        return nodeProps;
    }

    public Map<String, Object> getRelProps() {
        return relProps;
    }

    public Dir getRelDir() {
        return relDir;
    }


    public SSSStepDescription() {
        this.relations = new ArrayList<String>();

    }

    public SSSStepDescription(int depth, boolean depthFirst) {
        this.relations = new ArrayList<String>();
        this.depth = depth;
        this.depthFirst = depthFirst;
    }

    public SSSStepDescription(int depth, boolean depthFirst, Map<String, Object> nodeProps, Map<String, Object> relProps) {
        this.relations = new ArrayList<String>();
        this.depth = depth;
        this.depthFirst = depthFirst;
        this.relProps = relProps;
        System.out.println("added relprops STEP" + relProps.toString());
        this.nodeProps = nodeProps;
        System.out.println("added nodeprops STEP" + nodeProps.toString());
    }

    public void addRelation(String rel) {
        relations.add(rel);
    }

    public List<String> getRelations() {
        return relations;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public boolean isDepthFirst() {
        return depthFirst;
    }

    public void setDepthFirst(boolean depthFirst) {
        this.depthFirst = depthFirst;
    }

    public void setDirection(Dir d) {
        this.relDir = d;
    }

    public Dir getDirection() {
        return relDir;
    }
}
