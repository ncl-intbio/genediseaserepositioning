/*
 * Copyright(c) 2014 Matthew Collison. 
 */
package uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer;

import it.uniroma1.dis.wsngroup.gexf4j.core.Edge;
import it.uniroma1.dis.wsngroup.gexf4j.core.Gexf;
import it.uniroma1.dis.wsngroup.gexf4j.core.Graph;
import it.uniroma1.dis.wsngroup.gexf4j.core.Node;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.Attribute;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeClass;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeList;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeType;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.GexfImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.StaxGraphWriter;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.data.AttributeListImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.viz.ColorImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.viz.Color;
import it.uniroma1.dis.wsngroup.gexf4j.core.viz.EdgeShape;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.MetaDataInterface;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;


/**
 *
 * @author Matthew Collison and Joe
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class GephiExporter {

    //set visualisation important: adds xmlns:viz = "http://www.gephi.org/gexf/viz" to op
    private Gexf gexf = new GexfImpl().setVisualization(true);
    private Graph graph = gexf.getGraph();
    //map for uid to java bean nodes 
    private Map<String, MetaDataInterface> jbeanMap = new HashMap<String, MetaDataInterface>();
    //map for uid to geph nodes 
    private Map<String, Node> gephiNodeMap = new HashMap<String, Node>();
    //create GEXF attribute list for nodes the graph
    private AttributeList nodeAttrList;
    //create GEXF attribute list for the edges of graph
    private AttributeList edgeAttrList;
    //testing coloured nodes this will need to be put in a seperate class
    private Map<String, Color> dreninColorNodes = new HashMap<String, Color>() {
        {

            //red
            put("Small_Molecule", new ColorImpl(255, 0, 0));
            //green
            put("Biotech", new ColorImpl(0, 255, 0));
            //faded pink
            put("MoA", new ColorImpl(255, 204, 204));
            //yellow
            put("Disease", new ColorImpl(255, 255, 0));
            //mustard
            put("Rare_Disease", new ColorImpl(204, 204, 0));
            //light blue
            put("Cellular_Component", new ColorImpl(51, 255, 255));
            //lighter blue
            put("Moleular_Function", new ColorImpl(102, 255, 255));
            //lighter blue
            put("Biological_Process", new ColorImpl(153, 255, 255));
            //purple
            put("Pathway", new ColorImpl(255, 0, 255));
            //diff purple
            put("Reactome_Pathway", new ColorImpl(240, 0, 240));
            //orange
            put("Gene", new ColorImpl(255, 128, 0));
            //blue
            put("Protein", new ColorImpl(0, 0, 255));
            //light green
            put("Protein_Family", new ColorImpl(153, 255, 153));
            //brown
            put("Protein_Complex", new ColorImpl(51, 25, 0));
            //lighter brown
            put("Protein_Domain", new ColorImpl(102, 51, 0));
            //lighter brown
            put("Ligand_Binding_Site", new ColorImpl(153, 76, 0));
            //lighter brown almost orange
            put("Tissue", new ColorImpl(255, 128, 0));
            //lighter orange
            put("Cell_Line", new ColorImpl(255, 153, 51));
            //lighter blue than protein
            put("Protein_Complex", new ColorImpl(0, 128, 255));

        }
    };

    public GephiExporter() {
    }

    public void export(Map<String, MetaDataInterface> nodeMap, String filename) {

        this.jbeanMap = nodeMap;
        this.nodeAttrList = new AttributeListImpl(AttributeClass.NODE);
        this.edgeAttrList = new AttributeListImpl(AttributeClass.EDGE);
        itterateObjects();
        writeGexf(filename);

    }

    public void exportMetadata(String filename) {
    }

    private void itterateObjects() {

        //just a quick patch-need to sort properly
        Set<String> relationsAdded = new HashSet<String>();

        //add the attribute lists to the graph
        graph.getAttributeLists().add(nodeAttrList);

        //iterate through java bean list to create gephi nodes 
        for (String key : jbeanMap.keySet()) {
            String type = jbeanMap.get(key).getType();
//            System.out.println(type);

            Node n = graph.createNode(jbeanMap.get(key).getType() + jbeanMap.get(key).getId());

            n.setLabel(jbeanMap.get(key).getId());

            //get attributes from the SSIPNodes
            Map<String, Object> SSIPattributes = jbeanMap.get(key).getAttributes();
            n = addGexAttributes(SSIPattributes, n);
            gephiNodeMap.put(jbeanMap.get(key).getType() + jbeanMap.get(key).getId(), n);
        }
        //iterate through java bean list again to create gephi node relations 
        for (String fromNodeString
                : jbeanMap.keySet()) {
            MetaDataInterface fromNode = jbeanMap.get(fromNodeString);
            //get each java bean object relations as set of destination java bean objects
            Set<MetaDataInterface> rels = fromNode.getRelations().keySet();
            Map<MetaDataInterface, SSIPRelationType> relations = fromNode.getRelations();
            //if the relations set is not empty iterate through it
            if (!relations.isEmpty()) {
                for (MetaDataInterface toNode : relations.keySet()) {
                    SSIPRelationType  r = relations.get(toNode);
                    String relType = r.getType();   
                    Set<String> props = toNode.getAllPropertyNames();
                    //needed to check that the gephiNodeMap contains the keys we wish to extracts
                    if (gephiNodeMap.containsKey(fromNode.getType() + fromNode.getId()) && gephiNodeMap.containsKey(toNode.getType() + toNode.getId())) {
                        //only if the edge hasn't already been created
                        if (!relationsAdded.contains(gephiNodeMap.get(fromNode.getType() + fromNode.getId()).toString() + gephiNodeMap.get(toNode.getType() + toNode.getId()).toString())) {
                            //create the edge

                            Node from = gephiNodeMap.get(fromNode.getType() + fromNode.getId());
                            Node to = gephiNodeMap.get(toNode.getType() + toNode.getId());
                            //colour inferred relation red
                            if (relType.equals("INFERRED_RELATION")) {
                                Edge edge = from.connectTo(to).setLabel(relType).setColor(new ColorImpl(255, 0, 0)).setThickness(10).setShape(EdgeShape.DASHED).setWeight(3);
                            } else {
                                Edge edge = from.connectTo(to).setLabel(relType).setColor(new ColorImpl(255, 255, 255));
                            }
                            //how do we add properties to edges????
//                            Map<String, Object> edgeAttrs = toNode.getAttributes();
//                            addGexAttributes(edgeAttrs, edge);

                            //add the relation to the duplication check (String)
                            relationsAdded.add(gephiNodeMap.get(fromNode.getType() + fromNode.getId()).toString() + gephiNodeMap.get(toNode.getType() + toNode.getId()).toString());
                        }
                    }
                }
            }
            
            
            
            
//            if (!rels.isEmpty()) {
//                for (MetaDataInterface toNode : rels) {
//                    String relType = toNode.getType();
//                    String id = toNode.getId();
//                    Set<String> props = toNode.getAllPropertyNames();
//                    //needed to check that the gephiNodeMap contains the keys we wish to extracts
//                    if (gephiNodeMap.containsKey(fromNode.getType() + fromNode.getId()) && gephiNodeMap.containsKey(toNode.getType() + toNode.getId())) {
//                        //only if the edge hasn't already been created
//                        if (!relationsAdded.contains(gephiNodeMap.get(fromNode.getType() + fromNode.getId()).toString() + gephiNodeMap.get(toNode.getType() + toNode.getId()).toString())) {
//                            //create the edge
//
//                            Node from = gephiNodeMap.get(fromNode.getType() + fromNode.getId());
//                            Node to = gephiNodeMap.get(toNode.getType() + toNode.getId());
//                            //colour inferred relation red
//                            if (relType.equals("INFFERRED_RELATION")) {
//                                Edge edge = from.connectTo(to).setLabel(id).setColor(new ColorImpl(255, 0, 0));
//                            } else {
//                                Edge edge = from.connectTo(to).setLabel(id);
//                            }
//                            //how do we add properties to edges????
////                            Map<String, Object> edgeAttrs = toNode.getAttributes();
////                            addGexAttributes(edgeAttrs, edge);
//
//                            //add the relation to the duplication check (String)
//                            relationsAdded.add(gephiNodeMap.get(fromNode.getType() + fromNode.getId()).toString() + gephiNodeMap.get(toNode.getType() + toNode.getId()).toString());
//                        }
//                    }
//                }
//            }
        }
    }

    /**
     * Not working
     *
     * @param SSIPattributes
     * @param n
     * @return
     */
    private Edge addGexAttributes(Map<String, Object> SSIPattributes, Edge n) {

        //iterate through the SSIP attributes and add them to the gex4f  node
        for (String attKeys : SSIPattributes.keySet()) {
            System.out.println("Edge: " + attKeys);
            Attribute at = null;
            //iterate through all attributes allready created
            Iterator itr = edgeAttrList.iterator();
            while (itr.hasNext()) {
                //if we have already created it use it
                Attribute local = (Attribute) itr.next();
                if (local.getId().equals(attKeys)) {

                    at = local;
                }
            }
            //if not then we have to create the attribute
            if (at == null) {
                at = edgeAttrList.createAttribute(attKeys, AttributeType.STRING, attKeys);
            }
            //add the attribute to the node
            n.getAttributeValues().addValue(at, (String) SSIPattributes.get(attKeys));
        }
        return n;

    }

    private Node addGexAttributes(Map<String, Object> SSIPattributes, Node n) {

        //iterate through the SSIP attributes and add them to the gex4f  node
        for (String attKeys : SSIPattributes.keySet()) {
            Attribute at = null;
            //iterate through all attributes allready created
            Iterator itr = nodeAttrList.iterator();
            while (itr.hasNext()) {
                //if we have already created it use it
                Attribute local = (Attribute) itr.next();
                if (local.getId().equals(attKeys)) {
                    at = local;
                }
            }
            //if not then we have to create the attribute
            if (at == null) {
                at = nodeAttrList.createAttribute(attKeys, AttributeType.STRING, attKeys);
            }

            //if we have the type value we can change the colour of the node
            if (at.getId().equals("type")) {
                if (dreninColorNodes.containsKey((String) SSIPattributes.get(attKeys))) {
                    n.setColor(dreninColorNodes.get((String) SSIPattributes.get(attKeys)));
                }
            }


            //add the attribute to the node
            n.getAttributeValues().addValue(at, (String) SSIPattributes.get(attKeys));
        }
        return n;

    }

    private void writeGexf(String filename) {
        StaxGraphWriter graphWriter = new StaxGraphWriter();
        File f = new File(filename);
        Writer out;
        try {
            out = new FileWriter(f, false);
            graphWriter.writeToStream(gexf, out, "UTF-8");
            System.out.println(f.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
