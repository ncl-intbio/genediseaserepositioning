/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.neo4j.graphdb.RelationshipType;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class holds all metadata used in the DReNIn graph as well as those that are
 * used in the GDAss graph.
 * <TODO> This class needs to be able to speak to the ontology manager class.
 *
 */
public class DReNInMetadata {

    public enum RelTypes implements RelationshipType {

        IS_ENCODED_BY("IS_ENCODED_BY"),
        BINDS_TO("BINDS_TO"),
        INTERACTS_WITH_DRUG("INTERACTS_WITH_DRUG"),
        INVOLVED_IN_DISEASE("INVOLVED_IN_DISEASE"),
        PART_OF_RARE_DISEASE("PART_OF_RARE_DISEASE"),
        IS_A_RARE_DISEASE("IS_A_RARE_DISEASE"),
        IS_A_DISEASE("IS_A_DISEASE"),
        HAS_INDICATION("HAS_INDICATION"),
        INTERACTS_WITH_PROTEIN("INTERACTS_WITH_PROTEIN"),
        IS_A_GO_TERM("IS_A_GO_TERM"),
        LOCATED_IN_CELLULAR_COMPONENT("LOCATED_IN_CELLULAR_COMPONENT"),
        HAS_MOLECULAR_FUNCTION("HAS_MOLECULAR_FUNCTION"),
        PART_OF_BIOLOGICAL_PROCESS("PART_OF_BIOLOGICAL_PROCESS"),
        PART_OF_PATHWAY("PART_OF_PATHWAY"),
        HAS_SIM_SEQUENCE("HAS_SIM_SEQUENCE"),
        HAS_2D_SIMILARITY("HAS_2D_SIMILARITY"),
        SIDE_EFFECT("SIDE_EFFECT"),
        //------------------------------- ONLY for the dis-Gene Ass graph
        LOF_INVOLVED_IN_DISEASE("LOF_INVOLVED_IN_DISEASE"),
        GOF_INVOLVED_IN_DISEASE("GOF_INVOLVED_IN_DISEASE"),
        INVOLVED_IN_RARE_DISEASE("INVOLVED_IN_RARE_DISEASE"),
        DISGENET_INVOLVED_IN("DISGENET_INVOLVED_IN"),
        OMIM_ASSOCIATED_WITH("OMIM_ASSOCIATED_WITH");

        //------------------------------- Yet to be included::
        //        KNOWS("KNOWS"),
        //        HAS_GO_TERM("HAS_GO_TERM"),
        //        PART_OF_REACTOME_PATHWAY("PART_OF_REACTOME_PATHWAY"),
        //        IS_A("IS_A"),
        //        HAS_SIM_STRUCTURE("HAS_SIM_STRUCTURE"),
        //        EXPRESSED_IN("EXPRESSED_IN"),
        //        HAS_LIGAND_BINDING_SITE("HAS_LIGAND_BINDING_SITE"),
        //        PART_OF_COMPLEX("PART_OF_COMPLEX"),
        //        SIM_SEQUENCE("SIM_SEQUENCE"),
        //        MEMBER_OF("MEMBER_OF"),
        //        TRANSCRIBES("TRANSCRIBES"),
        //        HAS_CHILD("HAS_CHILD"),
        //        HAS_PARENT("HAS_PARENT"),
        //        HAS_SIMILAR_CHEMICAL("HAS_SIMILAR_CHEMICAL"),
        //        MAY_TREAT("MAY_TREAT"),
        //        MAY_PREVENT("MAY_PREVENT"),
        //        HAS_INDICATION("HAS_INDICATION"),
        //        SIDE_EFFECT("SIDE_EFFECT"),
        //        LOCATED_IN("LOCATED_IN"),
        //        HAS_CONSERVED_PART("HAS_CONSERVED_PART"),
        //        IS_A_RARE_DISEASE("IS_A_RARE_DISEASE"),       
        private String text;

        RelTypes(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public static RelTypes fromString(String text) {
            if (text != null) {
                for (RelTypes b : RelTypes.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static RelTypes fromValue(String value) throws IllegalArgumentException {
            try {
                return RelTypes.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    public enum NodeType {

        GENE("Gene"),
        PROTEIN("Protein"),
        SMALL_MOLECULE("Small_Molecule"),
        BIOTECH("Biotech"),
        COMMON_DISEASE("Common_Disease"),
        RARE_DISEASE("Rare_Disease"),
        CELLULAR_COMPONENT("Cellular_Component"),
        MOLECULAR_FUNCTION("Moleular_Function"),
        BIOLOGICAL_PROCESS("Biological_Process"),
        PATHWAY("Pathway");

        //--------------------------------- YET to be included:::
        //        DRUG("Drug"),
        //        MoA("MoA"),
        //        DRUG_COMBINATION("Drug_Combination"),
        //        LIGAND("Ligand"),
        //        REACTOME_PATHWAY("Reactome_Pathway"),
        //        TRAIT("Trait"),
        //        TARGET("Target"),
        //        PROTEIN_FAMILY("Protein_Family"),
        //        PROTEIN_COMPLEX("Protein_Complex"),
        //        PROTEIN_DOMAIN("Protein_Domain"),
        //        LIGAND_BINDING_SITE("Ligand_Binding_Site"),
        //        TISSUE("Tissue"),
        //        CELL_LINE("Cell_Line"),
        //         DNA("DNA");
        private String text;

        NodeType(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public static NodeType fromString(String text) {
            if (text != null) {
                for (NodeType b : NodeType.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static NodeType fromValue(String value) throws IllegalArgumentException {
            try {
                return NodeType.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    public enum DataSource {

        NCBIBLAST("NCBI_BLAST", "-", getDate()),
        OPENBABEL("OpenBABEL", "-", getDate()),
        CTD("CTD", "-", getDate()),
        GOA("GOA", "-", getDate()),
        GO("GeneOntology", "-", getDate()),
        UNIPROTSWISSPROT("UniProt", "-", getDate()),
        NDFRT("NDFRT", "-", getDate()),
        STRING("STRING", "-", getDate()),
        ORDO("OrphnetRDO", "-", getDate()),
        ORPHNET("Orphnet", "-", getDate()),
        DISGENET("DisGENet", "-", getDate()),
        DO("DiseaseOntology", "-", getDate()),
        DRUGBANKv3("DrugBank", "3.0", getDate()),
        DRUGBANKv4("DrugBank", "4.0", getDate()),
        PREDICT("PREDICT", "-", getDate()),
        SIDR2("SIDR2", "4.0", getDate()),
        SIDR4("SIDR4", "4.0", getDate());

        private String name;
        private String version;
        private String dateAccessed;

        DataSource(String name, String version, String date) {
            this.dateAccessed = date;
            this.version = version;
            this.name = name;
        }

        public String getDataSourceName() {
            return this.name;
        }

        public String getDataSourceVersion() {
            return this.name;
        }

        public static DataSource fromString(String text) {
            if (text != null) {
                for (DataSource b : DataSource.values()) {
                    if (text.equalsIgnoreCase(b.toString())) {
                        return b;
                    }
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return name + "\t" + version + "\t" + dateAccessed;
        }

        public static DataSource fromValue(String value) throws IllegalArgumentException {
            try {
                return DataSource.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    /**
     * Returns today's date. The date when the data was acccessed,
     *
     * @return
     */
    public static String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Returns the property name with the Provenance identifier appended.
     *
     * @param propName
     * @return
     */
    public String addProv(String propName) {
        return propName + "_Prov";
    }
}
