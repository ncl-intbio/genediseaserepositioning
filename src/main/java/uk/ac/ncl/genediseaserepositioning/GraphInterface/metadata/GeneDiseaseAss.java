/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.neo4j.graphdb.RelationshipType;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class GeneDiseaseAss {

    public static void main(String[] args) {

        GeneDiseaseAss dm = new GeneDiseaseAss();

    }

    public enum RelTypes implements RelationshipType {

        KNOWS("KNOWS"),
        BINDS_TO("BINDS_TO"),
        BINDS_TO_DBv2_5("BINDS_TO_DBv2_5"),
        BINDS_TO_DBv3("BINDS_TO_DBv3"),
        INTERACTS_WITH_PROTEIN("INTERACTS_WITH_PROTEIN"),
        IS_ENCODED_BY("IS_ENCODED_BY"),
        HAS_GO_TERM("HAS_GO_TERM"),
        PART_OF_PATHWAY("PART_OF_PATHWAY"),
        PART_OF_REACTOME_PATHWAY("PART_OF_REACTOME_PATHWAY"),
        INVOLVED_IN_DISEASE("INVOLVED_IN_DISEASE"),
        DISGENET_INVOLVED_IN("DISGENET_INVOLVED_IN"),
        INVOLVED_IN_RARE_DISEASE("INVOLVED_IN_RARE_DISEASE"),
        IS_A("IS_A"),
        HAS_SIM_STRUCTURE("HAS_SIM_STRUCTURE"),
        HAS_SIM_SEQUENCE("HAS_SIM_SEQUENCE"),
        EXPRESSED_IN("EXPRESSED_IN"),
        HAS_LIGAND_BINDING_SITE("HAS_LIGAND_BINDING_SITE"),
        PART_OF_COMPLEX("PART_OF_COMPLEX"),
        SIM_SEQUENCE("SIM_SEQUENCE"),
        MEMBER_OF("MEMBER_OF"),
        TRANSCRIBES("TRANSCRIBES"),
        HAS_CHILD("HAS_CHILD"),
        HAS_PARENT("HAS_PARENT"),
        INTERACTS_WITH_DRUG("INTERACTS_WITH_DRUG"),
        HAS_SIMILAR_CHEMICAL("HAS_SIMILAR_CHEMICAL"),
        MAY_TREAT("MAY_TREAT"),
        MAY_PREVENT("MAY_PREVENT"),
        HAS_INDICATION("HAS_INDICATION"),
        SIDE_EFFECT("SIDE_EFFECT"),
        LOCATED_IN("LOCATED_IN"),
        HAS_CONSERVED_PART("HAS_CONSERVED_PART"),
        LOCATED_IN_CELLULAR_COMPONENT("LOCATED_IN_CELLULAR_COMPONENT"),
        HAS_MOLECULAR_FUNCTION("HAS_MOLECULAR_FUNCTION"),
        PART_OF_BIOLOGICAL_PROCESS("PART_OF_BIOLOGICAL_PROCESS"),
        IS_A_RARE_DISEASE("IS_A_RARE_DISEASE"),
        PART_OF_RARE_DISEASE("PART_OF_RARE_DISEASE"),
        OMIM_ASSOCIATED_WITH("OMIM_ASSOCIATED_WITH"),
        HAS_TANIMOTO_SIMILARITY("HAS_TANIMOTO_SIMILARITY");
        private String text;

        RelTypes(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public static RelTypes fromString(String text) {
            if (text != null) {
                for (RelTypes b : RelTypes.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static RelTypes fromValue(String value) throws IllegalArgumentException {
            try {
                return RelTypes.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    public enum NodeType {

        DRUG("Drug"),
        MoA("MoA"),
        SMALL_MOLECULE("Small_Molecule"),
        BIOTECH("Biotech"),
        SMALL_MOLECULE_DBv2_5("Small_Molecule_DBv2_5"),
        BIOTECH_DBv2_5("Biotech_DBv2_5"),
        SMALL_MOLECULE_DBv3("Small_Molecule_DBv3"),
        BIOTECH_DBv3("Biotech_DBv3"),
        DRUG_COMBINATION("Drug_Combination"),
        DISEASE("Disease"),
        RARE_DISEASE("Rare_Disease"),
        LIGAND("Ligand"),
        CELLULAR_COMPONENT("Cellular_Component"),
        MOLECULAR_FUNCTION("Moleular_Function"),
        BIOLOGICAL_PROCESS("Biological_Process"),
        PATHWAY("Pathway"),
        REACTOME_PATHWAY("Reactome_Pathway"),
        TRAIT("Trait"),
        GENE("Gene"),
        PROTEIN("Protein"),
        TARGET("Target"),
        PROTEIN_FAMILY("Protein_Family"),
        PROTEIN_COMPLEX("Protein_Complex"),
        PROTEIN_DOMAIN("Protein_Domain"),
        LIGAND_BINDING_SITE("Ligand_Binding_Site"),
        TISSUE("Tissue"),
        CELL_LINE("Cell_Line"),
        DNA("DNA");
        private String text;

        NodeType(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public static NodeType fromString(String text) {
            if (text != null) {
                for (NodeType b : NodeType.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static NodeType fromValue(String value) throws IllegalArgumentException {
            try {
                return NodeType.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    public enum DataSource {

        ORDO("OrphnetRDO", "-", getDate()),
        ORPHANET("Orphanet", "-", getDate()),
        DISGENET("DisGENet", "-", getDate()),
        SIDR2("SIDR2", "4.0", getDate());

        private String name;
        private String version;
        private String dateAccessed;

        DataSource(String name, String version, String date) {
            this.dateAccessed = date;
            this.version = version;
            this.name = name;
        }

        public String getDataSourceName() {
            return this.name;
        }

        public String getDataSourceVersion() {
            return this.name;
        }

        public static DataSource fromString(String text) {
            if (text != null) {
                for (DataSource b : DataSource.values()) {
                    if (text.equalsIgnoreCase(b.toString())) {
                        return b;
                    }
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return name + "\t" + version + "\t" + dateAccessed;
        }

        public static DataSource fromValue(String value) throws IllegalArgumentException {
            try {
                return DataSource.fromString(value);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + value);
            }
        }
    }

    /**
     * Returns today's date. The date when the data was acccessed,
     *
     * @return
     */
    public static String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Returns the property name with the Provenance identifier appended.
     *
     * @param propName
     * @return
     */
    public String addProv(String propName) {
        return propName + "_Prov";
    }
}
