/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch;

import java.util.Map;
import java.util.Set;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class SSSQuery {

    private SSIPNode from;
    private Set<String> type;
    private SSIPNode to;
    private boolean extend;
    private Map<String, Object> toNodeProperties = null;
    private Map<String, Object> relationProperties = null;
    private Dir direction;

    public enum Dir {

        OUTGOING, INCOMING
    }

    public Map<String, Object> getToNodeProperties() {
        return toNodeProperties;
    }

    public void setToNodeProperties(Map<String, Object> props) {
        this.toNodeProperties = props;
    }

    public Map<String, Object> getRelationProperties() {
        return relationProperties;
    }

    public void setRelationProperties(Map<String, Object> props) {
        this.relationProperties = props;
    }

    public SSSQuery(SSIPNode from, SSIPNode to, Set<String> type, Dir traverseDirection, boolean extend) {
        this.extend = extend;
        this.from = from;
        this.type = type;
        this.to = to;
        this.direction = traverseDirection;
    }

    public SSSQuery(SSIPNode from, SSIPNode to, Set<String> type, Dir traverseDirection, boolean extend, Map<String, Object> toNodeProps, Map<String, Object> relProps) {
        this.extend = extend;
        this.from = from;
        this.type = type;
        this.to = to;
        this.toNodeProperties = toNodeProps;
        this.relationProperties = relProps;
        this.direction = traverseDirection;
    }

    public Dir getDirection() {
        return direction;
    }

    public boolean getExtend() {
        return extend;
    }

    public void setExtend(boolean extend) {
        this.extend = extend;
    }

    public SSIPNode getFrom() {
        return from;
    }

    public Set<String> getType() {
        return type;
    }

    public SSIPNode getTo() {
        return to;
    }

    public Set<String> getReltypes() {
        return type;
    }

    public void setFrom(SSIPNode from) {
        this.from = from;
    }

    public void setType(Set<String> type) {
        this.type = type;
    }

    public void setTo(SSIPNode to) {
        this.to = to;
    }

    public void printOutQuery() {
        System.out.println("FROM: " + from.getId());
        System.out.println("TO: " + to.getId());
        System.out.println("Types: " + type.toString());
    }
}
