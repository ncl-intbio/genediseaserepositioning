/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.DataStructure;

import java.util.logging.Logger;
import org.jgrapht.graph.DefaultEdge;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 * 
 * Extends Default Edge in the JGraphT library. This edge is used in SSSDataStructure
 * to allow for SSIPNodes to be easily extracted from edges.
 */
public class SSSEdge<V> extends DefaultEdge {

    private V v1;
    private V v2;
    private String label;
    private static final long serialVersionUID = 1L;
    private SSSNode from;
    private SSSNode to;
    private static final Logger LOG = Logger.getLogger(SSSEdge.class.getName());

    public SSSEdge(V v1, V v2) {
        this.v1 = v1;
        this.v2 = v2;
        this.from= (SSSNode) v1;
        this.to = (SSSNode) v2;

    }

    /**
     * Get edge from SSIPNode
     * @return 
     */
    public SSSNode getFrom() {
        return from;
    }

    /**
     * Get edge to SSIPNode
     * @return 
     */
    public SSSNode getTo() {
        return to;
    }

    /**
     * Set edge from SSIPNode
     * @param from 
     */
    public void setFrom(SSSNode from) {
        this.from = from;
    }

    /**
     * Set edge to SSIPNode
     * @param to 
     */
    public void setTo(SSSNode to) {
        this.to = to;
    }
}
