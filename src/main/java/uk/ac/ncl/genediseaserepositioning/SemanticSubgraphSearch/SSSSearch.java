/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.GephiExporter;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.SSSStepDescription;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.MetaDataInterface;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.DataStructure.DSVisualisation;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.DataStructure.DataStructure;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.DataStructure.SSSNode;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.InferredRelation;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.MappingTextSummary.GSKPhaseIII;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.SubgraphManager;



/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class completes an exhaustive and exact search for a subgraph using one
 * initial node. To complete an iterative search for a set of initial nodes
 * please use Multiple Search.
 */
public class SSSSearch {

    public SubgraphManager query;
    public SSSQuery[] querySteps;
    public SSIPNode[] queryOrder;
    public SSSNode[] MappingOrder;
    public DataStructure dat;
    private String startingClass;
    private boolean closedWorld;
    private boolean output;
    private final static Logger LOGGER = Logger.getLogger(SSSSearch.class.getName());
    public Neo4J handler;
    public File directory;
    private int STATE = 0;
    private SSIPNode initialNodeSSIP;
    private SSSNode initialNodeSSS;
    private int mappings = 0;
    private boolean outputDONE = false;
    private long startTime;
    private long endTime;
    private long searchTime;
    private long exportTime;
    private FileManager fman;
    private BufferedWriter inferredRelLog;
    private boolean dsOutput;
    private GSKPhaseIII export;
    private SideEffectCheck sideEffectCheck;
    private File outPutDirectory;

    /**
     * Constructor.
     *
     * @param handler Target graph.
     * @param query Query graph manager.
     * @param startingClass Starting class for a search.
     * @param initialNode Initial node for the search.
     * @param closedWorld Is a closed world approach to be taken?
     * @param output Mappings to be output as a visualisation?
     * @param DSoutPut Visualisation of the data structure?
     * @param limit Do we want to limit the number of the initial nodes that we
     * complete a search for??
     */
    public SSSSearch(Neo4J handler, SubgraphManager query, String startingClass, SSIPNode initialNode, boolean closedWorld, boolean output, boolean DSoutPut, File outPutDirectory) {
        this.handler = handler;
        this.outPutDirectory = outPutDirectory;
        this.query = query;
        this.startingClass = startingClass;
        this.query.getFirstNode(startingClass);
        this.query.calcSubOrderSub(startingClass);
        this.queryOrder = query.getQueryOrder();
        this.querySteps = query.getQuerySteps();
        this.closedWorld = closedWorld;
        this.output = output;
        this.dsOutput = DSoutPut;
        this.initialNodeSSIP = initialNode;
        this.fman = new FileManager();
        this.directory = new File(outPutDirectory + fman.getFileSeparator() + initialNodeSSIP.getId().replace("/", ""));
        if (!directory.exists()) {
            directory.mkdir();
        }
        if (query.getInferredRelation() != null) {
            try {           
                this.inferredRelLog = new BufferedWriter(new FileWriter(outPutDirectory + fman.getFileSeparator() + initialNodeSSIP.getId().replace("/", "") + fman.getFileSeparator() + initialNodeSSIP.getId().replace("/", "") + ".txt"));
            } catch (IOException ex) {
                Logger.getLogger(SSSSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }

        this.export = new GSKPhaseIII();

    }

    /**
     * Perform an iterative, exhaustive search for a subgraph.
     */
    public void run() {

        startTime = System.currentTimeMillis();
        int count = 0;
        handler.initialiseDatabaseConnection();
        //resync the indexes
        handler.syncIndexes(10);

        this.sideEffectCheck = new SideEffectCheck(handler);

        SSIPNode temp = handler.getInitialSSIPNodeFromDB(initialNodeSSIP);
        LOGGER.log(Level.INFO, "Started search with initial node: {0}", initialNodeSSIP.getId());

        initialNodeSSIP = temp;
        //create a new datastructure
        dat = new DataStructure();
        //convert the SSIP node to an SSSnode to add to the datastructure
        SSSNode init = new SSSNode(initialNodeSSIP);
        initialNodeSSS = init;
        //add the seed node to the data structure
        dat.addNode(initialNodeSSS);
        STATE++;

        //start at one as we have already assigned our first node from the initial candidate set
        Set<SSSNode> nextNodes = new HashSet<>();
        if (STATE == 1) {
            nextNodes.add(initialNodeSSS);
        }

        //have our initial node
        for (SSSQuery queryStep : querySteps) {
            //break search if there are no new nodes to search for in this state
            if (nextNodes.isEmpty()) {
                LOGGER.log(Level.INFO, "Finished searching with initial node: {0} at STATE {1}, no mappings were found.", new Object[]{initialNodeSSIP.getId(), STATE});
                break;
            }
            Set<SSIPNode> nextNodesFromThisState = new HashSet<>();
            Set<SSSNode> nextNodesFromThisStateSSNode = new HashSet<>();
            int posInOrder = getPositionInOrder(queryStep.getFrom());
            Map<SSSNode, Set<SSSNode>> nodeStructure = dat.getAllNodesAndChildrenAtDepth(posInOrder + 1);
            //add the start nodes from the nodeStructure to the set of nodes to search
            //ONLY if the state > 1. Otherwise we only have a single node and it is already in the set
            if ((STATE > 1) && (nextNodes != null) && nextNodes.size() > 1) {
                nextNodes.clear();
                for (SSSNode from : nodeStructure.keySet()) {
                    nextNodes.add(from);
                }
            }
            //if this is the node to extend from then we do things differently 
            //everthing will be topologically and semantically fine from here
            //so we simply do a traversal with the reltype and these are our results
            if (queryStep.getExtend()) {
                for (SSSNode thisNode : nextNodes) {
                    //get all possible outgoing nodes
                    SSSStepDescription[] steps = new SSSStepDescription[1];
                    steps[0] = new SSSStepDescription(1, true);
                    Set<String> relations = queryStep.getReltypes();
                    for (String r : relations) {
                        steps[0].addRelation(r);
                    }
                    steps[0].setNodeProps(queryStep.getToNodeProperties());
                    steps[0].setRelProps(queryStep.getRelationProperties());
                    //get next nodes as SSIPNodes
                    Map<String, MetaDataInterface> nextNodess = handler.getNextNodes(thisNode.getSsip(), steps);
                    //only do this if we have at least one node mapped to the exhaustive node
                    //if the new nodeset comtains the previous node - delete it
                    String toDelete = null;
                    //repeatability check
                    for (String key : nextNodess.keySet()) {
                        if (nextNodess.get(key).getId().equals(thisNode.getSsip().getId())) {
                            toDelete = key;
                        }
                    }
                    if (toDelete != null) {
                        nextNodess.remove(toDelete);
                    }
                    if (!nextNodess.isEmpty()) {
                        //mappings!!!!!    
                        SSSNode[] nodes = dat.getShortestPathNodes(dat.getParentNode(), thisNode);
                        //add all new nodes  
                        SSIPNode[] ssipNodes = new SSIPNode[nodes.length + nextNodess.size()];
                        //add all nodes from the mapping to the array
                        for (int w = 0; w < nodes.length; w++) {
                            ssipNodes[w] = nodes[w].getSsip();
                        }
                        //add all nodes from the exhaustive traversal to the array
                        int count2 = 0;
                        //couldn't find any nodes at the final stage

                        for (String s : nextNodess.keySet()) {
                            ssipNodes[nodes.length + count2] = (SSIPNode) nextNodess.get(s);
                            count2++;
                        }

                        exportSubToGephi(ssipNodes, "Mapping_" + count);
                        count++;

                        outputDONE = true;
                    }
                }
                break;
            }
            for (SSSNode thisNode : nextNodes) {
                SSIPNode ssipNode = thisNode.getSsip();
                //get next nodes
                SSSStepDescription[] steps = new SSSStepDescription[1];
                steps[0] = new SSSStepDescription(1, true);
                Set<String> relations = queryStep.getReltypes();
                for (String r : relations) {
                    steps[0].addRelation(r);
                }
                steps[0].setNodeProps(queryStep.getToNodeProperties());
                steps[0].setRelProps(queryStep.getRelationProperties());
                steps[0].setDirection(queryStep.getDirection());
                //get next nodes as SSIPNodes
                Map<String, MetaDataInterface> nextNodess = handler.getNextNodes(ssipNode, steps);
                Set<SSIPNode> nodes = new HashSet<>();
                for (String name : nextNodess.keySet()) {
                    nodes.add((SSIPNode) nextNodess.get(name));
                    nextNodesFromThisState.add((SSIPNode) nextNodess.get(name));
                }
                //if the state isn't one (we only have a single node)
                //or we do need to do state 'jumping' then add the found nodes
                //to all of the leaf node of the datastructure
                if (STATE > 1 && (STATE) != posInOrder + 1) {
                    for (SSSNode sets : nodeStructure.keySet()) {
                        Set<SSSNode> to = nodeStructure.get(sets);
                        for (SSSNode indiNode : to) {
                            Set<SSSNode> converted = new HashSet<>();
                            for (SSIPNode s : nodes) {
                                SSSNode e = new SSSNode(s);
                                converted.add(e);
                                nextNodesFromThisStateSSNode.add(e);
                            }
                            dat.addNextNodes(converted, indiNode);
                        }

                    }
                } else {
                    //covert SSSNodes to 
                    Set<SSSNode> converted = new HashSet<>();
                    for (SSIPNode s : nodes) {
                        SSSNode e = new SSSNode(s);
                        converted.add(e);
                        nextNodesFromThisStateSSNode.add(e);
                    }

                    dat.addNextNodes(converted, thisNode);

                }
            }
            //duplicate it so we can delete without concurrentModException
            Set<SSSNode> nextNodesFromThisStateDUPE = new HashSet<>();
            nextNodesFromThisStateDUPE.addAll(nextNodesFromThisStateSSNode);
            //need to check the graph for the ACTUAL nodes; not those that are in the
            //path as these don't contain all the info. Need to get the node from the graph to get an exact match
            //check for node duplication and if we want closed world matches check for these
            for (SSSNode checker : nextNodesFromThisStateDUPE) {
                MappingOrder = dat.getShortestPathNodes(dat.getParentNode(), checker);
                if (duplicateNodesCheck() == false) {
                    dat.removeNodesFromDat(MappingOrder);
                    nextNodesFromThisStateSSNode.remove(checker);
                }
                if (closedWorld) {
                    System.out.println("we are checking for closed world");
                    if (topologyCheck() == false) {
                        dat.removeNodesFromDat(MappingOrder);
                        nextNodesFromThisStateSSNode.remove(checker);
                    }
                }
            }
            nextNodes = nextNodesFromThisStateSSNode;
            STATE++;
        }

        searchTime = ((System.currentTimeMillis() - startTime) / 1000);
        startTime = System.currentTimeMillis();

        //if we did an extended search the output would have already been completed by now
        if (outputDONE == false) {
            Set<SSSNode> leaves = dat.getLeafNodes();
            for (SSSNode s : leaves) {
                //mappings!!!!!       
                count++;
                SSSNode[] nodes = dat.getShortestPathNodes(dat.getParentNode(), s);
                SSIPNode[] ssipNodes = new SSIPNode[nodes.length];
                if (ssipNodes.length == query.getTestSize()) {

                    for (int i = 0; i < nodes.length; i++) {
                        ssipNodes[i] = nodes[i].getSsip();
                    }
                    if (query.getInferredRelation() != null) {
                        SSIPNode[] dupe = ssipNodes;
                        ssipNodes = addInferredRelation(dupe, query.getInferredRelation());
                        try {                     
                            //if we have an sub exporter then need to appemd this info- if not we need to simply add the below
                            inferredRelLog.append(getInferredRelationINFO(dupe, query.getInferredRelation(), initialNodeSSIP.getId() + "_" + count + ".gexf") + "\t" + export.exportSummary(dupe) + "\t" + sideEffectCheck.sideEffectCheck(dupe) + "\t" + getDrugMech(dupe[0]) + "\t" + export.getMechanism(DReNInMetadata.RelTypes.BINDS_TO.getText(), 0, 1, dupe) + "\n");
                        } catch (IOException ex) {
                            Logger.getLogger(SSSSearch.class.getName()).log(Level.SEVERE, null, ex);
                        }                 
                    }
                    if (output == true) {
                        exportSubToGephi(ssipNodes, initialNodeSSIP.getId() + "_" + count);
                    }
                    mappings++;
                }
            }
        }

        exportTime = ((System.currentTimeMillis() - startTime) / 1000);

        if (query.getInferredRelation() != null) {
            try {           
                inferredRelLog.close();
            } catch (IOException ex) {
                Logger.getLogger(SSSSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }

        LOGGER.log(Level.INFO, "Finished searching with initial node: {0} finding {1} mappings.", new Object[]{initialNodeSSIP.getId(), mappings});

        handler.finaliseDatabaseConnectionNoUPDATES();
        if (dsOutput) {
            DSVisualisation s = new DSVisualisation(dat, initialNodeSSIP.getId(), true);
            s.visualise();

        }
        dat = null;
    }

    /**
     * Add the inferred relation to the mapping. This method allows us to
     * visualise the inferred relation in any exporters.
     *
     * @param map
     * @param inf
     * @return
     */
    public SSIPNode[] addInferredRelation(SSIPNode[] map, InferredRelation inf) {
        int fromPos = 0;
        int toPos = 0;
        for (int i = 0; i < queryOrder.length; i++) {
            if (queryOrder[i].equals(inf.getFrom())) {
                fromPos = i;
            }
            if (queryOrder[i].equals(inf.getTo())) {
                toPos = i;
            }
        }
        SSIPNode fromMap = map[fromPos];
        fromMap.addRelation(map[toPos], new SSIPRelationType(inf.getType()));
        map[fromPos] = fromMap;
        return map;
    }

    public String getDrugMech(SSIPNode drug) {

        String drugMech = "NOT_KNOWN";
        Map<String, Object> drugProps = drug.getAttributes();
        if (drugProps.containsKey("Drug_Mechanism")) {
            drugMech = (String) drugProps.get("Drug_Mechanism");
        }

        return drugMech;
    }

    /**
     * Get the inferred relation from a mapping in String format. [From id]
     * [From type][To id] [To type]
     *
     * @param map
     * @param inf
     * @return
     */
    public String getInferredRelationINFO(SSIPNode[] map, InferredRelation inf, String fileLocation) {
        int fromPos = 0;
        int toPos = 0;
        for (int i = 0; i < queryOrder.length; i++) {
            if (queryOrder[i].equals(inf.getFrom())) {
                fromPos = i;
            }
            if (queryOrder[i].equals(inf.getTo())) {
                toPos = i;
            }
        }
        SSIPNode fromMap = map[fromPos];
        SSIPNode toMap = map[toPos];
        String inferred = fromMap.getId() + "\t" + fromMap.getPropertyMap().get("type") + "\t" + toMap.getId() + "\t" + toMap.getPropertyMap().get("type") + "\t" + fileLocation;
        return inferred;
    }

    /**
     * Allows a mapping to be exported to a .gexf file.
     *
     * @param nodes
     * @param name
     */
    public void exportSubToGephi(SSIPNode[] nodes, String name) {
        GephiExporter geph = new GephiExporter();
        Map<String, MetaDataInterface> nodeMap = new HashMap<>();
        for (SSIPNode node : nodes) {
            nodeMap.put(node.getType() + node.getId(), node);
        }
        geph.export(nodeMap, directory + fman.getFileSeparator() + name.replace("/", "") + ".gexf");
    }

    /**
     * Get the state at which the search currently is.
     *
     * @return
     */
    public int getState() {
        return STATE;
    }

    /**
     * Set the state of the search.
     *
     * @param state
     */
    public void setState(int state) {
        this.STATE = state;
    }

    /**
     * Get the next relation types that are to be used to traversed to get the
     * next nodes.
     *
     * @return
     */
    public SSIPRelationType getNextRel() {
        SSIPNode now = queryOrder[STATE];
        Map<MetaDataInterface, SSIPRelationType> rel = now.getRelations();
        SSIPRelationType reltype = null;
        for (MetaDataInterface r : rel.keySet()) {
            reltype = rel.get(r);
        }
        return reltype;
    }

    /**
     * Returns a set of initial nodes for the search. This initial candidate set
     * should be provided by the user.
     *
     * @return
     */
    public Set<SSIPNode> initialNodes() {
        Set<SSIPNode> candidateSet = new HashSet<>();
        SSIPNode sil = new SSIPNode("Protein", "5HT3B_HUMAN");
        candidateSet.add(sil);
        return candidateSet;
    }

    /**
     * Returns the position of the node in the order.
     *
     * @param node
     * @return
     */
    public int getPositionInOrder(SSIPNode node) {
        // System.out.println("Checjing order of: " + node.getId());
        int pos = 0;
        for (int p = 0; p < queryOrder.length; p++) {
            if (queryOrder[p].equals(node)) {
                pos = p;
            }
        }

        return pos;

    }

    /**
     * Checks to see if the same SSIPNode is included in the mapping twice.
     *
     * @return
     */
    private boolean duplicateNodesCheck() {
        boolean check = true;
        for (int one = 0; one < STATE + 1; one++) {
            for (int two = 0; two < STATE + 1; two++) {
                if (one != two) {
                    if (MappingOrder[one].getSsip().getId().equals(MappingOrder[two].getSsip().getId())) {
                        return false;
                    }
                }
            }
        }
        return check;

    }

    /**
     * Check that the potential mapping has the same number of relations between
     * other nodes in the mapping as its corresponding node in the query.
     *
     * @return
     */
    public boolean topologyCheck() {

        boolean check = true;
        //state +1 (we have not yet increased state but have added the latest node to the mapping)
        for (int one = 0; one < STATE + 1; one++) {
            for (int two = 0; two < STATE + 1; two++) {
                //matches topology
                //ignore self loops!
                if (!MappingOrder[one].equals(MappingOrder[two])) {
                    if (queryOrder[one].hasRelaionTo(queryOrder[two])) {
                        if (!MappingOrder[one].getSsip().hasRelaionTo(MappingOrder[two].getSsip())) {
                            return false;
                        }
                    }
                    //if closed world make sure there are no MORE relations
                    if (closedWorld) {
                        if (!queryOrder[one].hasRelaionTo(queryOrder[two])) {
                            if (MappingOrder[one].getSsip().hasRelaionTo(MappingOrder[two].getSsip())) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return check;
    }

    /**
     * Prints the node structure to console.
     *
     * @param struc
     */
    public void printNodeStructure(Map<SSSNode, Set<SSSNode>> struc) {

        for (SSSNode key : struc.keySet()) {
            // System.out.println("KEY: " + key.getSsip().getId());
            Set<SSSNode> leaves = struc.get(key);
            for (SSSNode to : leaves) {
                //  System.out.println("\t" + "TO: " + to.getSsip().getId());
            }

        }

    }

    public long getSearchTime() {
        return searchTime;
    }

    public long getExportTime() {
        return exportTime;
    }

    public int numberOfMappings() {
        return mappings;
    }
}
