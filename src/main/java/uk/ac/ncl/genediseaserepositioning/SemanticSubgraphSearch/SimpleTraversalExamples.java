/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch;

import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.SSSStepDescription;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;



/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class SimpleTraversalExamples {

    public SSSStepDescription[] getAllTargets() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.BINDS_TO.toString());
        return steps;
    }

    public SSSStepDescription[] getAllNeighbours(int depth) {
        SSSStepDescription[] steps = new SSSStepDescription[depth];
        for (int i = 0; i < depth; i++) {
            steps[i] = new SSSStepDescription(1, true);

        }
        return steps;
    }

    public SSSStepDescription[] getAllRareDiseases() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.INVOLVED_IN_RARE_DISEASE.toString());
        return steps;
    }

    public SSSStepDescription[] getSideEffects() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.SIDE_EFFECT.toString());
        return steps;
    }

    public SSSStepDescription[] getDrugDrugInteractions() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.INTERACTS_WITH_DRUG.toString());
        return steps;
    }

    public SSSStepDescription[] getDODiseases() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        //steps[0].addRelation(SSSStepDescription.RelTypes.INVOLVED_IN);
        return steps;
    }

    public SSSStepDescription[] getDISGENETDiseases() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.DISGENET_INVOLVED_IN.getText());
        return steps;
    }

    public SSSStepDescription[] getAllAssociatedGENES() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.INVOLVED_IN_DISEASE.getText());
        return steps;
    }

    public SSSStepDescription[] getAllCCGOTerms() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.LOCATED_IN_CELLULAR_COMPONENT.toString());
        return steps;
    }

    public SSSStepDescription[] getAllMFGOTerms() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.HAS_MOLECULAR_FUNCTION.toString());
        return steps;
    }

    public SSSStepDescription[] getAllBPGOTerms() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.PART_OF_BIOLOGICAL_PROCESS.toString());
        return steps;
    }

    public SSSStepDescription[] getEncodedProteins() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.IS_ENCODED_BY.toString());
        return steps;
    }

    public SSSStepDescription[] getAllGOTerms() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.PART_OF_BIOLOGICAL_PROCESS.toString());
        steps[0].addRelation(DReNInMetadata.RelTypes.HAS_MOLECULAR_FUNCTION.toString());
        steps[0].addRelation(DReNInMetadata.RelTypes.LOCATED_IN_CELLULAR_COMPONENT.toString());
        return steps;
    }

    public SSSStepDescription[] getGene() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.IS_ENCODED_BY.toString());
        return steps;
    }
    
     public SSSStepDescription[] getIndications() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.HAS_INDICATION.toString());
        return steps;
    }

    public SSSStepDescription[] getProteinInteractions() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.INTERACTS_WITH_PROTEIN.toString());
        return steps;
    }

    public SSSStepDescription[] getAllSimProts() {
        SSSStepDescription[] steps = new SSSStepDescription[1];
        steps[0] = new SSSStepDescription(1, true);
        steps[0].addRelation(DReNInMetadata.RelTypes.HAS_SIM_SEQUENCE.toString());
        return steps;

    }
}
