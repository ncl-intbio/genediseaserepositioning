/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode;

import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;



/**
 *
 * @author joe
 */
public interface RuleNode {

    SSIPNode rule = null;
    String descritpion = null;

    public SSIPNode getRuleNode();
    
    public void setRuleNode(SSIPNode rule);
    
    public void createRuleNode();
    
    public String getDescription();
    

}
