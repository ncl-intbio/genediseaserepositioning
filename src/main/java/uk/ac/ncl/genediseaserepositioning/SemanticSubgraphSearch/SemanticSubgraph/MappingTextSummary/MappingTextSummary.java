/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.MappingTextSummary;

import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;



/**
 *
 * @author joe
 */
public interface MappingTextSummary {
    
    public String exportSummary(SSIPNode [] mapping);
    
}
