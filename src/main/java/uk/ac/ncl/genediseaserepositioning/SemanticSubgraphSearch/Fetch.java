/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphMine.PropComparator.Comparator;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode.ApprovedSmallMolecule;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode.RuleNode;

/**
 *
 * @author joe
 *
 * Class takes a rule node and then extracts all nodes from the graph that
 * satisfy these rules. Can be used for the production of an initial candidate
 * set prior to a SSSearch of a subgraph.
 */
public class Fetch {

    private RuleNode rule;
    private FileManager fm;
    private Neo4J backend;
    private Set<String> nodeIDs;
    private HashMap<String, String> succIDsandTypes;
    private HashSet<SSIPNode> succesfulNodes;
    private final static Logger LOGGER = Logger.getLogger(Fetch.class.getName());

    public Fetch(RuleNode r, Neo4J backend) {
        this.fm = new FileManager();
        this.backend = backend;
        this.rule = r;
        this.succIDsandTypes = new HashMap<>();
        this.succesfulNodes = new HashSet<SSIPNode>();
    }

    /**
     * Method returns all matches of a particular rule node that is found in a
     * graph. Identifies all occurrences that match the type of the rule in the
     * first step and THEN scores these for properties.
     */
    public void goFetch() {

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(fm.getSuppFolder() + fm.getFileSeparator() + "DBIDs_after_prune.txt"));
        } catch (IOException ex) {
            Logger.getLogger(Fetch.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.info("Starting extraction of an initial candidate set");
        //STEP 1 - retireve all ndoes that match the type of the rule node     
        backend.initialiseDatabaseConnection();
        nodeIDs = backend.getAllNodeUIDSOfType(rule.getRuleNode().getType());
        LOGGER.log(Level.INFO, "Found {0} nodes of type {1}", new Object[]{nodeIDs.size(), rule.getRuleNode().getType()});
        //STEP 1 - check which of these nodes satisfy the rules of the rule node
        int count = 0;
        for (String name : nodeIDs) {
            count++;
            SSIPNode ssip = backend.getInitialSSIPNodeFromDB(new SSIPNode(rule.getRuleNode().getType(), name));
            int removeCount = 0;
            if (rule.getRuleNode().getAttributes() != null && rule.getRuleNode().getAttributes().size() > 0) {
                for (String propName : rule.getRuleNode().getAttributes().keySet()) {
                    boolean singleCheck = true;
                    Comparator check = (Comparator) rule.getRuleNode().getAttributes().get(propName);
                    String key = check.getPropKey();
                    for (String att : ssip.getAttributes().keySet()) {
                        String attName = att.replaceAll("[^a-zA-Z]", "");
                        if (attName.equals(key)) {
                            if (!check.Compare(key, (String) ssip.getProperty(att))) {
                                singleCheck = false;
                                break;

                            }
                        }
                    }

                    if (singleCheck) {
                        removeCount++;
                    }
                }
            }

            if (removeCount < 1) {
                succIDsandTypes.put(name, rule.getRuleNode().getType());
                try {
                    bw.append(name+"\n");
                } catch (IOException ex) {
                    Logger.getLogger(Fetch.class.getName()).log(Level.SEVERE, null, ex);
                }
                succesfulNodes.add(ssip);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(Fetch.class.getName()).log(Level.SEVERE, null, ex);
        }
        backend.finaliseDatabaseConnectionNoUPDATES();
        LOGGER.log(Level.INFO,
                "After scoring we have {0} nodes that satisfy the rules set by the rule Node {1}", new Object[]{succIDsandTypes.size(), rule.getDescription()});

    }

    /**
     * Returns all the SSIPNodes.
     *
     * @return
     */
    public HashSet<SSIPNode> getSuccesfulNodes() {
        return succesfulNodes;
    }

    /**
     * Returns the ruleNode.
     *
     * @return
     */
    public RuleNode getRule() {
        return rule;
    }

    /**
     * Returns a map of the drugBankID and node types.
     *
     * @return
     */
    public HashMap<String, String> getSuccIDs() {
        return succIDsandTypes;
    }
}
