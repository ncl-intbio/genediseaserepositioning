/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.SubgraphLibrary;

import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.InferredRelation;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.Subgraph;


/**
 *
 * @author joe
 *
 * This is the graph as described in PhaseII of the mini project completed at
 * GSK.
 *
 */
public class GeneDiseaseSub implements Subgraph {

    private Set<SSIPNode> queryGraph;
    private InferredRelation inf;
    private String name;

    @Override
    public void drawGraph() {

        Set<SSIPNode> graph = new HashSet<SSIPNode>();
        //----------------------------------------------------------------------------------------------------- NODES
        SSIPNode node1 = new SSIPNode(DReNInMetadata.NodeType.SMALL_MOLECULE.getText(), "comp_1");
        //node1.addProperty("Name", new StringComp("DBID", Comparator.compare.EQUALS, "DB00203"));
        SSIPNode node2 = new SSIPNode(DReNInMetadata.NodeType.PROTEIN.getText(), "prot_1");
        //node2.addProperty("test", new StringComp("Name", Comparator.compare.CONTAINS, "GMP"));
        SSIPNode node3 = new SSIPNode(DReNInMetadata.NodeType.GENE.getText(), "gene_1");
        SSIPNode node4 = new SSIPNode(DReNInMetadata.NodeType.COMMON_DISEASE.getText(), "disease");
        //----------------------------------------------------------------------------------------------------- RELATIONS
        SSIPRelationType rel1 = new SSIPRelationType(DReNInMetadata.RelTypes.BINDS_TO.toString());
        node1.addRelation(node2, rel1);
        SSIPRelationType rel2 = new SSIPRelationType(DReNInMetadata.RelTypes.IS_ENCODED_BY.toString());
        node2.addRelation(node3, rel2);
        SSIPRelationType rel3 = new SSIPRelationType(DReNInMetadata.RelTypes.INVOLVED_IN_DISEASE.getText());
        node3.addRelation(node4, rel3);
        //----------------------------------------------------------------------------------------------------- ADD TO GRAPH
        graph.add(node1);
        graph.add(node2);
        graph.add(node3);
        graph.add(node4);

        InferredRelation indication = new InferredRelation(node1, node4, "hasIndication");
        inf = indication;
        name = "GeneDisease";
        queryGraph = graph;
    }

    @Override
    public Set<SSIPNode> getQueryGraph() {
        return queryGraph;
    }

    @Override
    public void setQueryGraph(Set<SSIPNode> graph) {
        this.queryGraph = graph;
    }

    @Override
    public InferredRelation getInferredRelation() {
        return inf;
    }

    @Override
    public void setInferrredRelation(InferredRelation rel) {
        this.inf = rel;
    }

    @Override
    public SSIPNode getExhaustiveStepFrom() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setExhaustiveStepFrom() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getName() {
        return name;
    }
}
