/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.GephiExporter;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.MetaDataInterface;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SSSQuery;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SSSQuery.Dir;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.SubgraphLibrary.GeneDiseaseSub;



/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Takes a subgraph and manages it through a search.
 */
public class SubgraphManager {

    private Subgraph sub;
    private SSIPNode[] queryOrder;
    private SSSQuery[] querySteps;
    public InferredRelation inf;
    private boolean visualise;
    private SSIPNode exhaustiveStepFrom = null;
    int testSize = 0;
    private static int STATE = 1;
    SSIPNode queryFrom = null;
    SSIPRelationType.RelTypes relType = null;
    private Query semSubType;
    private final static Logger LOGGER = Logger.getLogger(SubgraphManager.class.getName());

    public static void main(String[] args) {
        String startingClass = DReNInMetadata.NodeType.GENE.getText();
        SubgraphManager s = new SubgraphManager(SubgraphManager.Query.GeneDisease, startingClass, false);
        s.printQuerySteps();
    }

    /**
     * Every subgraph that is in the subgraph library should be listed in this
     * enum.
     */
    public enum Query {

        GeneDisease;
    }

    /**
     * Constructor.
     *
     * @param subName A query graph to be searched.
     * @param startingClass The semantic 'type' at which you wish the search to
     * be started.
     * @param visualise Do we want to visualise the subgraph?
     */
    public SubgraphManager(Query subName, String startingClass, boolean visualise) {

        this.semSubType = subName;
        this.inf = null;

        switch (semSubType) {
            case GeneDisease:
                this.sub = new GeneDiseaseSub();
                break;

        }
        sub.drawGraph();
        if (sub.getInferredRelation() != null) {
            inf = sub.getInferredRelation();
        }
        testSize = sub.getQueryGraph().size();
        queryOrder = new SSIPNode[testSize];
        querySteps = new SSSQuery[testSize - 1];
        getFirstNode(startingClass);
        calcSubOrderSub(startingClass);

        if (inf != null && visualise) {
            exportSubToGephi();
        }

    }

    /**
     * Add nodes with no outgoing edges to the order
     *
     * @return
     */
    public SSIPNode incomingNodes() {

        SSIPNode next = null;
        for (SSIPNode toAdd : sub.getQueryGraph()) {
            if (alreadyAdded(toAdd) == false) {
                if (attachedToOrderINCOMING(toAdd)) {
                    next = toAdd;
                }
            }
        }
//        /System.out.println("Returning: " + next.getId());
        return next;
    }

    /**
     * Calculates the order at which the subgraph will be searched for. This is
     * done using the outgoing edges of the nodes and traversing these to get
     * the next node. If there are no more outgoing edges then incoming edges of
     * nodes that have not been added to the order are looked at.
     */
    public final void calcSubOrderSub(String startingClass) {
        //we have already added the first node
        getFirstNode(startingClass);
        String DIRECTION = "IN";

        //while we haven't added all the nodes to the queryOrder
        while (STATE < testSize) {
            DIRECTION = "IN";
            SSIPNode previous = queryOrder[STATE - 1];
            queryFrom = previous;
            SSIPNode toAdd = null;
            int toAddRels = 0;
            Set<SSIPNode> connected = getAllOutgoingNodes(previous);
            //if there are no more outgoing nodes to go through then go back
            //or all of thepossible outgoing nodes have already been added
            //then look at the outgoing nodes
            if (connected.isEmpty() || alreadyAddedSet(connected)) {
                toAdd = incomingNodes();
                //if there is still nothing to add then we must be at a dead end (leaf node whose parent has already been added)
                if (toAdd == null) {
                    toAdd = getAnother();
                    DIRECTION = "OUT";
                }
            } //            //when we leave the while loop state may be increased; so leave if it has surpassed
            else {
                for (SSIPNode q : connected) {
                    //add the node with the greates number of outgoing edges to the order first
                    //if it is has not already been added to the order array
                    if (alreadyAdded(q) == false) {
                        int relSize = getEdgeset(q);
                        if (relSize >= toAddRels) {
                            toAdd = q;
                            toAddRels = relSize;
                        }
                    }
                }
            }
            STATE++;
            queryOrder[STATE - 1] = toAdd;
            SSSQuery s = null;
            //if there are properties add them

            //AT THIS POINT WE NEEED TO KNOW WHETHER THE RELATION IS ONCOMING OR OUTGOING!!!!!!
            if (DIRECTION.equals("IN")) {
                if (toAdd != null) {
                    if (exhaustiveStepFrom != null && queryFrom.getId().equals(exhaustiveStepFrom.getId())) {
                        s = new SSSQuery(queryFrom, toAdd, getRelationTypes(queryFrom, toAdd), Dir.INCOMING, true, toAdd.getAttributes(), queryFrom.getRelation(toAdd).getAttributes());
                        //   }
                    } else {
                        s = new SSSQuery(queryFrom, toAdd, getRelationTypes(queryFrom, toAdd), Dir.INCOMING, false, toAdd.getAttributes(), queryFrom.getRelation(toAdd).getAttributes());
                    }
                } else if (exhaustiveStepFrom != null && queryFrom.getId().equals(exhaustiveStepFrom.getId())) {
                    s = new SSSQuery(queryFrom, toAdd, getRelationTypes(queryFrom, toAdd), Dir.INCOMING, true);
                    //   }
                } else {
                    s = new SSSQuery(queryFrom, toAdd, getRelationTypes(queryFrom, toAdd), Dir.INCOMING, false);
                }
                //don't have a relation from nothing to the first node and so the querysteps start
                //at the second node= when state =2
                querySteps[STATE - 2] = s;
            }

            if (DIRECTION.equals("OUT")) {
                if (toAdd != null) {
                    if (exhaustiveStepFrom != null && queryFrom.getId().equals(exhaustiveStepFrom.getId())) {
                        s = new SSSQuery(queryFrom, toAdd, getRelationTypes(toAdd, queryFrom), Dir.OUTGOING, true, toAdd.getAttributes(), toAdd.getRelation(queryFrom).getAttributes());
                        //   }
                    } else {
                        s = new SSSQuery(queryFrom, toAdd, getRelationTypes(toAdd, queryFrom), Dir.OUTGOING, false, toAdd.getAttributes(), toAdd.getRelation(queryFrom).getAttributes());
                    }
                } else if (exhaustiveStepFrom != null && queryFrom.getId().equals(exhaustiveStepFrom.getId())) {
                    s = new SSSQuery(queryFrom, toAdd, getRelationTypes(toAdd, queryFrom), Dir.OUTGOING, true);
                    //   }
                } else {
                    s = new SSSQuery(queryFrom, toAdd, getRelationTypes(toAdd, queryFrom), Dir.OUTGOING, false);
                }
                //don't have a relation from nothing to the first node and so the querysteps start
                //at the second node= when state =2
                querySteps[STATE - 2] = s;
            }
        }

    }

    /**
     * Returns the first node that will be in the search. This is calculated as
     * the node with the highest outgoing connectivity of the specified
     * startingClass.
     *
     * @param startingClass
     */
    public final void getFirstNode(String startingClass) {
        SSIPNode first = null;
        int outGoingEdges = 0;
        for (SSIPNode q : sub.getQueryGraph()) {
            if (q.getType().equals(startingClass)) {
                if (getEdgeset(q) >= outGoingEdges) {
                    first = q;
                    outGoingEdges = getEdgeset(q);
                }
            }
        }
        queryOrder[0] = first;
    }

    /**
     * Returns all nodes that have an outgoing relation from check.
     *
     * @param check
     * @return
     */
    public Set<SSIPNode> getAllOutgoingNodes(SSIPNode check) {
        Set<SSIPNode> outGoing = new HashSet<SSIPNode>();
        Map<MetaDataInterface, SSIPRelationType> rels = check.getRelations();
        for (MetaDataInterface node : rels.keySet()) {
            outGoing.add((SSIPNode) node);
        }
        return outGoing;
    }

    /**
     * Returns all nodes that have an ingoing relation to check.
     *
     * @param check
     * @return
     */
    public Set<SSIPNode> getAllIncomingNodes(SSIPNode check) {
        Set<SSIPNode> inComing = new HashSet<SSIPNode>();
        for (SSIPNode s : sub.getQueryGraph()) {
            Map<MetaDataInterface, SSIPRelationType> rels = s.getRelations();
            for (MetaDataInterface node : rels.keySet()) {
                if ((SSIPNode) node == check) {
                    inComing.add(s);
                }
            }
        }
        return inComing;
    }

    /**
     * Checks if the node in question has already been added to queryOrder.
     *
     * @param check
     * @return
     */
    public boolean alreadyAdded(SSIPNode check) {

        boolean duplicate = false;
        for (int i = 0; i < queryOrder.length; i++) {
            if (queryOrder[i] != null) {
                if (queryOrder[i].equals(check)) {
                    return true;
                }
            }
        }
        return duplicate;

    }

    /**
     * Checks the QueryOrder for a set of nodes. If all the nodes in the Set
     * have been added to the QueryOrder the method returns true.
     *
     * @param check
     * @return
     */
    public boolean alreadyAddedSet(Set<SSIPNode> check) {

        boolean duplicate = false;
        int nodes = check.size();
        int added = 0;
        for (SSIPNode dup : check) {
            for (int i = 0; i < queryOrder.length; i++) {
                if (queryOrder[i] != null) {
                    if (queryOrder[i].equals(dup)) {
                        //return true;
                        added++;
                    }
                }
            }
        }
        if (added == nodes) {
            return true;
        }

        return duplicate;

    }

    /**
     * Checks to see if a node with no outgoing edges is attached to another
     * node in the graph through an incoming edge.
     *
     * @param check
     * @return
     */
    public SSIPNode getAnother() {
        SSIPNode next = null;
        for (SSIPNode toAdd : sub.getQueryGraph()) {
            if (alreadyAdded(toAdd) == false) {
                if (attachedToOrderOUTGOING(toAdd)) {

                    next = toAdd;
                }
            }
        }
        return next;

    }

    /**
     * Checks to see if a node with no outgoing edges is attached to another
     * node in the graph through an incoming edge.
     *
     * @param check
     * @return
     */
    public boolean attachedToOrderINCOMING(SSIPNode check) {
        boolean duplicate = false;
        for (int y = 0; y < queryOrder.length; y++) {
            if (queryOrder[y] != null) {
                Set<SSIPNode> outGoing = new HashSet<SSIPNode>();
                Map<MetaDataInterface, SSIPRelationType> rels = queryOrder[y].getRelations();
                for (MetaDataInterface node : rels.keySet()) {
                    if (check.equals(node)) {
                        queryFrom = queryOrder[y];
                        return true;
                    }
                }
            }
        }
        return duplicate;

    }

    public boolean attachedToOrderOUTGOING(SSIPNode check) {
        boolean duplicate = false;
        Map<MetaDataInterface, SSIPRelationType> rels = check.getRelations();

        for (MetaDataInterface node : rels.keySet()) {
            for (int y = 0; y < queryOrder.length; y++) {
                if (queryOrder[y] != null) {
                    if (node == queryOrder[y]) {
                        queryFrom = queryOrder[y];
                        return true;
                    }
                }
            }
        }
        return duplicate;

    }

    /**
     * Method returns the number of outgoing edges from a node.
     *
     * @param sip
     * @return
     */
    public int getEdgeset(SSIPNode sip) {
        int edgeset = 0;
        Map<MetaDataInterface, SSIPRelationType> rels = sip.getRelations();
        edgeset = rels.size();
        return edgeset;

    }

    /**
     * Returns the relation types that are present between node From and node
     * To.
     *
     * @param from
     * @param to
     * @return
     */
    public Set<String> getRelationTypes(SSIPNode from, SSIPNode to) {
        Set<String> rels = new HashSet<String>();
        Map<MetaDataInterface, SSIPRelationType> rela = from.getRelations();
        for (MetaDataInterface node : rela.keySet()) {
            if (to.equals(node)) {
                rels.add(rela.get(node).name());
            }
        }
        return rels;
    }

    /**
     * Export the subgraph to a gephi visualisation.
     *
     * @param nodes
     * @param name
     */
    private void exportSubToGephi() {

        Set<SSIPNode> queryGraphDUPE = sub.getQueryGraph();
        Map<String, MetaDataInterface> nodes = new HashMap<String, MetaDataInterface>();
        if (inf != null) {
            SSIPNode from = inf.getFrom();
            SSIPNode to = inf.getTo();
            for (SSIPNode s : queryGraphDUPE) {
                if (s.equals(from)) {
                    s.addRelation(to, new SSIPRelationType("INFERRED_RELATION"));
                }
                nodes.put(s.getId() + s.getType(), s);
            }
        } else {
            for (SSIPNode s : queryGraphDUPE) {
                nodes.put(s.getId() + s.getType(), s);
            }
        }

        GephiExporter geph = new GephiExporter();
        LOGGER.info("Exported sub to .gexf");
        geph.export(nodes, "subvis.gexf");
    }

    /**
     * Prints out the QueryOrder to the console.
     */
    public void printOrder() {
        for (int i = 0; i < queryOrder.length; i++) {
            if (queryOrder[i] != null) {
                System.out.println(i + ":  " + queryOrder[i].getId());
            }
        }
    }

    public void printQuerySteps() {
        for (int i = 0; i < querySteps.length; i++) {
            if (querySteps[i] != null) {
                querySteps[i].printOutQuery();
            }
        }
    }

    public Subgraph getSub() {
        return sub;
    }

    public InferredRelation getInferredRelation() {
        return inf;
    }

    public SSSQuery[] getQuerySteps() {
        return querySteps;
    }

    public int getTestSize() {
        return testSize;
    }

    public static int getSTATE() {
        return STATE;
    }

    public SSIPNode getQueryFrom() {
        return queryFrom;
    }

    public SSIPRelationType.RelTypes getRelType() {
        return relType;
    }

    public SSIPNode[] getQueryOrder() {
        return queryOrder;
    }
}
