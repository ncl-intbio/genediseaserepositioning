/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.SubgraphManager;

/**
 *
 * Class takes an initial candidate set and runs a search for every node that is
 * in this set.
 *
 * @author joe
 */
public final class MultipleSearch {

    private HashMap<String, String> nodes;
    private SubgraphManager sub;
    private String startingClass;
    private Neo4J handler;
    private boolean closedWorld;
    private BufferedWriter searchSummary;
    private String ruleNodeName = "NONE";
    private int overallMappings = 0;
    private long overallSearchTime = 0;
    private long overallExportTime = 0;
    private boolean output;
    private boolean dsVisOP;
    private final static Logger LOGGER = Logger.getLogger(MultipleSearch.class.getName());
    private int limit;
    private FileManager fman;
    private File outPutDirectory;

    /**
     * Constructor for a multiple search.
     *
     * @param nodes
     * @param sub
     * @param startingClass
     * @param handler
     * @param closedWorld
     * @param output
     * @param DSVisOP
     */
    public MultipleSearch(HashMap<String, String> nodes, SubgraphManager sub, String startingClass, Neo4J handler, boolean closedWorld, boolean output, boolean DSVisOP, String limit) {
        this.handler = handler;
        this.sub = sub;
        this.nodes = nodes;
        this.startingClass = startingClass;
        this.closedWorld = closedWorld;
        this.output = output;
        this.dsVisOP = DSVisOP;
        
        if (limit.toLowerCase().equals("all")) {
            this.limit = nodes.size();
        } else {
            this.limit = Integer.parseInt(limit);
        }
        this.fman = new FileManager();
        this.outPutDirectory = fman.getMappingsFolder();
        if (!outPutDirectory.exists()) {
            LOGGER.log(Level.INFO, "Creating directory: {0}", outPutDirectory);
            boolean result = false;
            try {
                outPutDirectory.mkdir();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                LOGGER.info("DIR created");
            }
        }
        try {
            this.searchSummary = new BufferedWriter(new FileWriter(outPutDirectory + fman.getFileSeparator() + "searchSummary.txt"));
        } catch (IOException ex) {
            Logger.getLogger(MultipleSearch.class.getName()).log(Level.SEVERE, null, ex);
        }

        startSummaryFile();
    }

    /**
     * Run the search; by iterating through every element in an initial
     * candidate set.
     */
    public void run() {

        int canSetCount = 1;
        for (String name : nodes.keySet()) {

            if (canSetCount > limit) {
                break;
            }
            LOGGER.log(Level.INFO, "Started search with seed {0} of {1} initial Nodes", new Object[]{canSetCount, nodes.size()});
            SSIPNode initialNode = new SSIPNode(nodes.get(name), name);
            SSSSearch sw = new SSSSearch(handler, sub, startingClass, initialNode, closedWorld, output, dsVisOP, outPutDirectory);
            sw.run();
            try {
                searchSummary.append(name + "\t" + sw.numberOfMappings() + "\t" + sw.getSearchTime() + "\t" + sw.getExportTime() + "\n");
            } catch (IOException ex) {
                Logger.getLogger(MultipleSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
            overallSearchTime += sw.getSearchTime();
            overallExportTime += sw.getExportTime();
            overallMappings += sw.numberOfMappings();

            //  }
            canSetCount++;
        }
        try {

            searchSummary.append("[TOTAL]" + "\t" + overallMappings + "\t" + overallSearchTime + "\t" + overallExportTime);
        } catch (IOException ex) {
            Logger.getLogger(MultipleSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            searchSummary.close();
        } catch (IOException ex) {
            Logger.getLogger(MultipleSearch.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    /**
     * Appends important information re: a search to the start of the search
     * summary file.
     *
     */
    public void startSummaryFile() {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();

            searchSummary.append("**********************************************************" + "\n");
            searchSummary.append("*" + "\t" + "[SEARCH SUMMARY] " + dateFormat.format(date) + "\n");
            searchSummary.append("*" + "\t" + "[SUB_NAME]" + "\t" + sub.getSub().getName() + "\n");
            searchSummary.append("*" + "\t" + "[STARTING_CLASS] " + "\t" + startingClass + "\n");
            searchSummary.append("*" + "\t" + "[PRUNED_USING_RULENODE]" + "\t" + ruleNodeName + "\n");
            searchSummary.append("*" + "\t" + "[INITIAL_CANDIDATE_SET_SIZE]" + "\t" + nodes.size() + "\n");
            searchSummary.append("*" + "\t" + "[CLOSED_WORLD]" + "\t" + closedWorld + "\n");
            searchSummary.append("**********************************************************" + "\n");
            searchSummary.append("\n");
        } catch (IOException ex) {
            Logger.getLogger(MultipleSearch.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Set the name of the rule node used to prune the initial candidate set.
     *
     * @param name
     */
    public void setRulEnodename(String name) {
        this.ruleNodeName = name;
    }
}
