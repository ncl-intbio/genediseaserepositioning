/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph;

import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;



/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class InferredRelation {
    
    private SSIPNode from;
    private SSIPNode to;
    private String type;
    
    public InferredRelation(SSIPNode from, SSIPNode to, String type){
        this.from = from;
        this.to = to;
        this.type = type; 
    }

    public void setFrom(SSIPNode from) {
        this.from = from;
    }

    public void setTo(SSIPNode to) {
        this.to = to;
    }

    public void setType(String type) {
        this.type = type;
    }

    public SSIPNode getFrom() {
        return from;
    }

    public SSIPNode getTo() {
        return to;
    }

    public String getType() {
        return type;
    }
    
    
}
