/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.DataStructure;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleGraph;


/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * This class provides an undirected data structure to store potential mappings
 * during a semantic subgraph search. Allowing for an exhaustive search to be
 * implemented.
 */
public class DataStructure {

    SimpleGraph<SSSNode, SSSEdge> mappingsTree;
    private int UID = 0;
    private SSSNode parentNode;

    public DataStructure() {
        this.mappingsTree = new SimpleGraph<>(
                SSSEdge.class);
    }

    /**
     * Returns the parent node of the data structure.
     *
     * @return
     */
    public SSSNode getParentNode() {
        return parentNode;
    }

    /**
     * Returns the graph data structure..
     *
     * @return
     */
    public SimpleGraph<SSSNode, SSSEdge> getGraph() {
        return mappingsTree;
    }

    /**
     * Adds a node to the graph.
     *
     * @param add
     */
    public void addNode(SSSNode add) {
        if (mappingsTree.vertexSet().isEmpty()) {
            this.parentNode = add;
        }
        //System.out.println("added: " + add.getSsip().getId() + " UID: "+add.getUID());
        mappingsTree.addVertex(add);
    }

    /**
     * Adds all potential next nodes to the data structure and a relation to the
     * node from which the traversal identified them.
     *
     * @param add
     * @param from
     */
    public void addNextNodes(Set<SSSNode> add, SSSNode from) {
        //System.out.println(toString());
        for (SSSNode s : add) {        
            mappingsTree.addVertex(s);
            mappingsTree.addEdge(from, s, new SSSEdge<SSSNode>(from, s));

        }
    }

    /**
     * Allows relations to be added to the data structure.
     *
     * @param from
     * @param to
     */
    public void addRelations(SSSNode from, SSSNode to) {
        mappingsTree.addEdge(from, to);
    }

    /**
     * Returns a string representation of the data structure.
     *
     * @return
     */
    public String toString() {
        String graph = "\n";
        int node = 1;
        for (SSSNode v : mappingsTree.vertexSet()) {
            graph = graph + "[NODES" + node + "]: " + v.getSsip().getId() + "\t" + v.getUID() + "\n";
            node++;
        }
        for (SSSNode v : mappingsTree.vertexSet()) {
            int intcheck = mappingsTree.vertexSet().size();
            for (SSSNode v2 : mappingsTree.vertexSet()) {
                if (mappingsTree.containsEdge(v, v2)) {
                    graph = graph + "\t" + "[EDGE]: " + v.getSsip().getId() + "\t" + v2.getSsip().getId() + "\n";

                }
            }
        }
        return graph;
    }

    /**
     * Returns all leaf nodes of the data structure. Leaf nodes are identified
     * by the fact that they have only one associated edge.
     *
     * @return
     */
    public Set<SSSNode> getLeafNodes() {
        Set<SSSNode> leaf = new HashSet<SSSNode>();
        for (SSSNode v : mappingsTree.vertexSet()) {
            if (mappingsTree.edgesOf(v).size() == 1 && !v.getSsip().getId().equals(parentNode.getSsip().getId())) {
                leaf.add(v);
            }
        }
        return leaf;
    }

    /**
     * Returns the depth of the data structure.
     *
     * @return
     */
    public int getTreeDepth() {
        int depth = 0;
        for (SSSNode v : mappingsTree.vertexSet()) {
            int shortestPathLength = getShortestPathLength(parentNode, v);
            if (shortestPathLength > depth) {
                depth = shortestPathLength;
            }
        }
        return depth;
    }

    /**
     * Returns the edge length of a shortest path between to nodes in the data
     * structure.
     *
     * @param from
     * @param to
     * @return
     */
    public int getShortestPathLength(SSSNode from, SSSNode to) {
        @SuppressWarnings("unchecked")
        DijkstraShortestPath tr2 = new DijkstraShortestPath(mappingsTree,
                from, to);
        List<Object> why = tr2.getPath().getEdgeList();
        return why.size();
    }

    /**
     * Returns all SSIPNodes which are present on the shortest path between two
     * nodes in the data structure.
     *
     * This is also used to get the mapping so far.
     *
     * @param from
     * @param to
     * @return
     */
    public SSSNode[] getShortestPathNodes(SSSNode from, SSSNode to) {
        @SuppressWarnings("unchecked")
        DijkstraShortestPath tr2 = new DijkstraShortestPath(mappingsTree,
                from, to);
        Object[] why = tr2.getPath().getEdgeList().toArray();
        SSSNode[] nodes = new SSSNode[why.length + 1];
        for (int i = 0; i < why.length; i++) {
            SSSEdge se = (SSSEdge) why[i];
            //last node need to add both Node
            if (i == why.length - 1) {
                nodes[i] = se.getFrom();
                nodes[i + 1] = se.getTo();
            } else {
                nodes[i] = se.getFrom();
            }
        }
        return nodes;
    }

    /**
     * Return all nodes at a certain depth in the data structure. This will be
     * used when we have more complicated subgraphs, i.e. when there are more
     * than one edge coming out of them. We can only search for one at a time.
     *
     * @param depth
     * @return
     */
    public Map<SSSNode, Set<SSSNode>> getAllNodesAndChildrenAtDepth(int depth) {
        Map<SSSNode, Set<SSSNode>> nodes = new HashMap<SSSNode, Set<SSSNode>>();
        //keys = node at depth [depth]
        Set<SSSNode> leafNodes = getLeafNodes();
        for (SSSNode sip : leafNodes) {
            SSSNode[] no = getShortestPathNodes(parentNode, sip);
//            for (int g = 0; g < no.length; g++) {
//                System.out.println("getChildrenNodes:" + g + ":" + no[g].getSsip().getId());
//            }

            //something wrong
            if (depth <= no.length) {

                SSSNode keyNode = no[depth - 1];
                SSSNode childNode = no[no.length - 1];
                if (nodes.containsKey(keyNode)) {
                    Set<SSSNode> children = nodes.get(keyNode);
                    children.add(childNode);
                    nodes.put(keyNode, children);
                } else {
                    Set<SSSNode> children = new HashSet<SSSNode>();
                    children.add(childNode);
                    nodes.put(keyNode, children);
                }
            }
        }
        //values = all leaf nodes
        // that is so we can find all the nodes that come from KEY and add them to the
        // datastructure as being related to each of the values

        //get the leaf nodes
        //get the shortest path from them to the parent node
        //then extract the node from the array at point [depth]

        return nodes;
    }

    public void removeNodesFromDat(SSSNode[] MappingOrder) {
        //remove the last node of the path as other nodes may come off the 
        //rest of the path
        Set<SSSEdge> toDelete = new HashSet<SSSEdge>();
        int lastNode = MappingOrder.length - 1;

        Set<SSSEdge> rels = mappingsTree.edgesOf(MappingOrder[lastNode]);
        for (SSSEdge r : rels) {
            //check it hasn't already been deleted
            if (mappingsTree.containsEdge(r)) {
                toDelete.add(r);// mappingsTree.removeEdge(r);
            }
        }

        for (SSSEdge rem : toDelete) {
            mappingsTree.removeEdge(rem);

        }


        mappingsTree.removeVertex(MappingOrder[lastNode]);

        //  }

    }
}
