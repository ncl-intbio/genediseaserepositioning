/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.MappingTextSummary;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.MetaDataInterface;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SubgraphScoring;

/**
 *
 * @author joe
 */
public class GSKPhaseIII implements MappingTextSummary {

    private SubgraphScoring score;
    //The variables below are used when we want to create an instnace from a String
    private String diseaseName;
    private String diseaseType;
    private String diseaseID;
    private String diseaseMeSH;
    private Set<String> diseaseMeshTree;
    private String drugName;
    private String drugType;
    private String geneName;
    private double subScore;
    private String bindsScore;
    private double gdAssScore;
    private String opLocation;
    private String diseaseMeshTreeSTRING;
    private String type;
    private double mapSEScore;
    private String seMeSHTerm;
    private String drugMech;
    private String DBknownAction;
    private String DBaction;

    public String getDrugMech() {
        return drugMech;
    }

    public String getDBknownAction() {
        return DBknownAction;
    }

    public String getDBaction() {
        return DBaction;
    }

    /**
     * To create an instance AND score during a search.
     */
    public GSKPhaseIII() {
        this.score = new SubgraphScoring();
    }

    /**
     * To repopulate an instance when manipulating output.
     */
    public GSKPhaseIII(String mapping) {
        //System.out.println(mapping);
        String[] split = mapping.split("\t");
        //System.out.println(split.length);
        this.drugName = split[0];
        //System.out.println(drugName);
        this.drugType = split[1];
        this.diseaseID = split[2];
        this.diseaseType = split[3];
        this.opLocation = split[4];
        this.diseaseMeSH = split[5];
        this.diseaseName = split[6];
        Set<String> treeTerms = new HashSet<String>();
        //System.out.println(split[7]);
        String[] all = split[7].split("\\|");

        for (int i = 0; i < all.length; i++) {
            treeTerms.add(all[i]);
        }
        this.diseaseMeshTree = treeTerms;
        this.diseaseMeshTreeSTRING = split[7];
        this.geneName = split[8];
        this.gdAssScore = Double.parseDouble(split[9]);
        this.bindsScore = split[10];
        this.subScore = Double.parseDouble(split[11]);
        this.type = split[12].split("\\|")[0];
        if (type.equals("SE")) {
            this.mapSEScore = Double.parseDouble(split[12].split("\\|")[1]);
            if (split[12].split("\\|").length > 2) {
                this.seMeSHTerm = split[12].split("\\|")[2];
            } else {
                this.seMeSHTerm = "NONE";
            }
        } else {
            this.mapSEScore = 0.0;
            this.seMeSHTerm = "none";
        }
        this.drugMech = split[13];
        if (split.length > 14) {
            this.DBknownAction = split[14];
            this.DBaction = split[15];
        }
    }

    public String getDrugmech() {
        return drugMech;
    }

    public String getType() {
        return type;
    }

    public double getMapSEScore() {
        return mapSEScore;
    }

    public String getSeMeSHTerm() {
        return seMeSHTerm;
    }

    /*
     * @param mapping
     * @return
     */
    //getInferredRelationINFO(dupe, query.getInferredRelation(), initialNodeSSIP.getId() + "_" + count + ".gexf") + +"\n"
    @Override
    public String exportSummary(SSIPNode[] mapping) {
        double geneDiseaseAss = getHighestGDAss("EvidenceScore", DReNInMetadata.RelTypes.INVOLVED_IN_DISEASE.getText(), 2, 3, mapping);
        String bindsToLowest = getLowestActivityProp(DReNInMetadata.RelTypes.BINDS_TO.getText(), 0, 1, mapping);
        String meshTreeTerms = getNodeINFO(mapping, 3, "MeSHTree");
        String diseaseName = getDisName(mapping[3]);
        String diseaseMeSH = getMeSHID(mapping[3]);
        double subScore = score.phase3Scoring(Double.parseDouble(bindsToLowest.split("\\|")[1]), geneDiseaseAss);
        String gene = mapping[2].getId();
        // String drugAction = getMechanism(DReNInMetadata.RelTypes.BINDS_TO.getText(), 0, 1, mapping);

        return diseaseMeSH + "\t" + diseaseName + "\t" + meshTreeTerms + "\t" + gene + "\t" + geneDiseaseAss + "\t" + bindsToLowest + "\t" + subScore;
    }

    /**
     * Get the name of a disease if it has one; for MeSH terms this is saved as
     * [MeSHHeader] and for rare diseases it is [Name].
     *
     * @param disease
     * @return
     */
    public String getDisName(SSIPNode disease) {

        String name = "NO_NAME";

        Map<String, Object> props = disease.getAttributes();
        for (String attNames : props.keySet()) {
            if (attNames.equals("Name")) {
                return (String) props.get(attNames);
            }
        }

        for (String attNames : props.keySet()) {
            if (attNames.equals("MeSHHeader")) {
                return (String) props.get(attNames);
            }
        }

        return name;
    }

    /**
     * Get the MeSHUi of a disease if it has one.
     *
     * @param disease
     * @return
     */
    public String getMeSHID(SSIPNode disease) {
        String MeSH = "NO_MESH";
        if (disease.getId().length() > 7) {
            Map<String, Object> props = disease.getAttributes();
                if (props.containsKey("MESH1")) {
                    MeSH = (String) props.get("MESH1");
                }
        } else {
            MeSH = disease.getId();
        }
        //System.out.println("\t\t"+disease.getId()+"\t"+MeSH);
        return MeSH;
    }

    /**
     * Allows for the value of specific attribute to be extracted from a
     * mapping. May be useful when outputting mappings to file.
     *
     * @param key
     * @param relType
     * @param fromPos
     * @param toPos
     * @param mapping
     * @return
     */
    public String getMechanism(String relType, int fromPos, int toPos, SSIPNode[] mapping) {

        String knownAction = "NONE";
        String action = "NONE";

        SSIPNode from = mapping[fromPos];
        SSIPNode to = mapping[toPos];
        Map<MetaDataInterface, SSIPRelationType> rels = from.getRelations();
        for (MetaDataInterface node : rels.keySet()) {
            SSIPNode check = (SSIPNode) node;
            if (check.getId().equals(to.getId())) {
                if (rels.get(check).name().contains(relType)) {

                    Map<String, Object> allProps = rels.get(check).getAttributes();

                    for (String name : allProps.keySet()) {
                        if (name.equals("DrugBank_knownAction")) {
                            knownAction = (String) allProps.get(name);
                        }
                        if (name.equals("DrugBank_action")) {
                            action = (String) allProps.get(name);
                        }
                    }

                }
            }
        }
        //if there are no chembl relations capturing the binds to info THEN we use drugbank
        return knownAction + "\t" + action;
    }

    /**
     * Allows for the value of specific attribute to be extracted from a
     * mapping. May be useful when outputting mappings to file.
     *
     * @param key
     * @param relType
     * @param fromPos
     * @param toPos
     * @param mapping
     * @return
     */
    public String getLowestActivityProp(String relType, int fromPos, int toPos, SSIPNode[] mapping) {

        String toRet = "";
        String activityType = "";
        double score = 10000000000000000000000.0;
        String drugBank = "DB";
        double drugBankScore = 100.0;
        boolean inDB = false;

        SSIPNode from = mapping[fromPos];
        SSIPNode to = mapping[toPos];
        Map<MetaDataInterface, SSIPRelationType> rels = from.getRelations();
        for (MetaDataInterface node : rels.keySet()) {
            SSIPNode check = (SSIPNode) node;
            if (check.getId().equals(to.getId())) {
                if (rels.get(check).name().contains(relType)) {

                    Map<String, Object> allProps = rels.get(check).getAttributes();

                    for (String name : allProps.keySet()) {
                        //System.out.println(name+"\t"+ allProps.get(name));
                        if (name.contains("Prov")) {
                            String source = (String) allProps.get(name);
                            if (source.contains("DrugBank")) {
                                inDB = true;
                            }
                        } else {
                            if (isDouble((String) allProps.get(name))) {
                                if (Double.parseDouble((String) allProps.get(name)) < score) {
                                    activityType = (String) allProps.get("ActivityType");
                                    score = Double.parseDouble((String) allProps.get(name));
                                }
                            }
                        }

                    }

                }
            }
        }
        //if there are no chembl relations capturing the binds to info THEN we use drugbank
        if (activityType.equals("") && inDB == true) {
            return drugBank + "|" + drugBankScore;
        } else {
            return activityType + "|" + score;
        }
    }

    /**
     * Allows for the value of specific attribute to be extracted from a
     * mapping. May be useful when outputting mappings to file.
     *
     * @param key
     * @param relType
     * @param fromPos
     * @param toPos
     * @param mapping
     * @return
     */
    public double getHighestGDAss(String key, String relType, int fromPos, int toPos, SSIPNode[] mapping) {
        // System.out.println("key " + key);
        // System.out.println("reltype  " + relType);

        double score = 0.0;

        SSIPNode from = mapping[fromPos];
        SSIPNode to = mapping[toPos];
        Map<MetaDataInterface, SSIPRelationType> rels = from.getRelations();
        for (MetaDataInterface node : rels.keySet()) {
            SSIPNode check = (SSIPNode) node;
            if (check.getId().equals(to.getId())) {
                if (rels.get(check).name().contains(relType)) {
                    if (key.toLowerCase().equals("all")) {
                        Map<String, Object> allProps = rels.get(check).getAttributes();

                        for (String name : allProps.keySet()) {
                            if ((Double) allProps.get(name) > score) {
                                score = (Double) allProps.get(name);
                            }
                        }

                    } else {

                        //if (Double.parseDouble((String) rels.get(check).getAttribute(key)) > score) {
//                            score = Double.parseDouble((String) rels.get(check).getAttribute(key));

//                        }

                        if ((double) rels.get(check).getAttribute(key) > score) {
                            score = (double) rels.get(check).getAttribute(key);

                        }
                    }
                }
            }
        }

        //remove last pipe
        return score;
    }

    /**
     * Get the inferred relation from a mapping in String format. [From id]
     * [From type][To id] [To type]
     *
     * @param map
     * @param inf
     * @return
     */
    public String getNodeINFO(SSIPNode[] map, int pos, String att) {
        //"MeSHTree"
        int fromPos = 0;
        String data = "FIRST";
        SSIPNode inQuestion = map[pos];
        Map<String, Object> disAtss = inQuestion.getAttributes();
        for (String na : disAtss.keySet()) {
            if (na.startsWith(att)) {
                if (data.equals("FIRST")) {
                    data = (String) disAtss.get(na);
                } else {
                    data = data.trim() + "|" + disAtss.get(na);
                }
            }
        }
        return data;
    }

    public SubgraphScoring getScore() {
        return score;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public String getDiseaseType() {
        return diseaseType;
    }

    public String getDiseaseID() {
        return diseaseID;
    }

    public String getDiseaseMeSH() {
        return diseaseMeSH;
    }

    public String getDiseaseMeshTreeSTRING() {
        return diseaseMeshTreeSTRING;
    }

    public Set<String> getDiseaseMeshTree() {
        return diseaseMeshTree;
    }

    public String getDrugName() {
        return drugName;
    }

    public String getDrugType() {
        return drugType;
    }

    public String getGeneName() {
        return geneName;
    }

    public double getSubScore() {
        return subScore;
    }

    public String getBindsScore() {
        return bindsScore;
    }

    public double getGdAssScore() {
        return gdAssScore;
    }

    public String getOpLocation() {
        return opLocation;
    }

    public boolean isDouble(String string) {
        try {
            Double.valueOf(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
