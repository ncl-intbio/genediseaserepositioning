/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch;

/**
 *
 * @author joe
 */
public class SubgraphScoring {
    
    private double BIN = 0.1;

    public static void main(String[] args) {

        SubgraphScoring s = new SubgraphScoring();
        System.out.println(s.phase3Scoring(1000.0, 0.9));
        System.out.println(Math.log10(100));

    }

    /**
     * Method completes a very simple scoring of PhaseIII subs using the GDAss
     * scores and the CTAss binding scores.
     *
     * @return
     */
    public double phase3Scoring(double compTar, double gdAss) {
        double overallScore = 0.0;
        overallScore = (gdAss / 2);
        if (compTar <= 1.0) {
            overallScore += 0.5;
        } else {
            double normalizedCompTar = (1 - (Math.log10(compTar) * BIN)) / 2;
            if (normalizedCompTar < 0.0) {
                normalizedCompTar = 0.0;
            }
            overallScore += normalizedCompTar;
        }
        return overallScore;
    }

}
