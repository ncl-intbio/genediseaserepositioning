/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph;

import java.util.Set;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;


/**
 *
 * @author joe
 *
 * Represents a semantic subgraph.
 *
 */
public interface Subgraph {

    Set<SSIPNode> queryGraph = null;
    InferredRelation inf = null;
    SSIPNode exhaustiveStepFrom = null;
    String name = null;

    public SSIPNode getExhaustiveStepFrom();

    public void setExhaustiveStepFrom();

    public Set<SSIPNode> getQueryGraph();

    public void setQueryGraph(Set<SSIPNode> graph);

    public InferredRelation getInferredRelation();

    public void setInferrredRelation(InferredRelation rel);

    public void drawGraph();

    public String getName();
}
