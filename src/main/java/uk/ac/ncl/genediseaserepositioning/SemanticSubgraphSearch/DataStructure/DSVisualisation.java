/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.DataStructure;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.graphstream.graph.*;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.stream.file.FileSinkImages;
import org.graphstream.stream.file.FileSinkImages.LayoutPolicy;
import org.graphstream.stream.file.FileSinkImages.OutputType;
import org.graphstream.stream.file.FileSinkImages.RendererType;
import org.graphstream.stream.file.FileSinkImages.Resolutions;
import uk.ac.ncl.genediseaserepositioning.FileManager;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;

/**
 *
 * @author joemullen
 *
 * Allows a simple visualisation of the data structure; either to the console
 * and to file or straight to file.
 */
public class DSVisualisation {

    private File opdirec;
    private DataStructure ssd;
    private Graph graph;
    private String name;
    private Set<String> highLight;
    private boolean onlyToFile;
    private FileManager fman;

    public DSVisualisation(DataStructure ssd, String name, boolean onlyToFile) {

        this.name = name;
        graph = new MultiGraph(name);
        this.ssd = ssd;
        this.onlyToFile = onlyToFile;
        this.fman = new FileManager();
        opdirec = fman.getDSVis();

    }

    /**
     * Uses graphstream to visualise the SSS data structure.
     */
    public void visualiseSub() {

        FileSinkImages pic = new FileSinkImages(OutputType.PNG, Resolutions.VGA);
        pic.setRenderer(RendererType.SCALA);

        pic.setLayoutPolicy(LayoutPolicy.COMPUTED_FULLY_AT_NEW_IMAGE);

        if (onlyToFile == false) {
            graph.display();
        }

        graph.addAttribute(
                "ui.stylesheet", "url('SSSgraphstreamStyle.css')");
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");

        graph.addNode("craig1").addAttribute("ui.class", "gene");
        graph.addNode("craig2").addAttribute("ui.class", "disease");

        for (Node node : graph) {
            node.addAttribute("ui.label", node.getId());
        }

       // graph.addEdge("david", graph.getNode("craig1"), graph.getNode("craig2"), true).addAttribute("ui.class", "disgenet");
       // graph.addEdge("david2", graph.getNode("craig1"), graph.getNode("craig2"), true).addAttribute("ui.class", "orphanet");
       // graph.addEdge("david3", graph.getNode("craig1"), graph.getNode("craig2"), true).addAttribute("ui.class", "omim");

        if (onlyToFile == false) {
            graph.display(false);
        }
        try {

            pic.writeAll(graph, "Datastructure/" + name + ".png");
        } catch (IOException ex) {
            Logger.getLogger(DSVisualisation.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    /**
     * Uses graphstream to visualise the SSS data structure.
     */
    public void visualise() {

        FileSinkImages pic = new FileSinkImages(OutputType.PNG, Resolutions.VGA);
        //pic.setRenderer(RendererType.SCALA);

        pic.setLayoutPolicy(LayoutPolicy.COMPUTED_FULLY_AT_NEW_IMAGE);

        if (onlyToFile == false) {
            graph.display();
        }

        graph.addAttribute(
                "ui.stylesheet", "url('SSSgraphstreamStyle.css')");
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");

//        System.setProperty("org.graphstream.ui.renderer",
//                "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        //colour nodes based on their state
        int count1 = 1;

        Iterator<SSSNode> it = ssd.getGraph().vertexSet().iterator();
        while (it.hasNext()) {
            SSSNode v = it.next();
            SSIPNode v2 = v.getSsip();
            int depth = ssd.getShortestPathLength(ssd.getParentNode(), v);
            switch (depth) {
                case 0:
                    graph.addNode(v2.getId() + "_"+count1).addAttribute("ui.class", "state0");

                    count1++;
                    break;
                case 1:
                    graph.addNode(v2.getId() + "_"+count1).addAttribute("ui.class", "state1");
                    count1++;
                    break;
                case 2:
                    graph.addNode(v2.getId() + "_"+count1).addAttribute("ui.class", "state2");
                    count1++;
                    break;

                case 3:
                    graph.addNode(v2.getId() + "_"+count1).addAttribute("ui.class", "state3");
                    count1++;
                    break;

                case 4:
                    graph.addNode(v2.getId() + "_"+count1).addAttribute("ui.class", "state4");
                    count1++;
                    break;
                case 5:
                    graph.addNode(v2.getId() + count1).addAttribute("ui.class", "state5");
                    count1++;
                    break;
            }

        }

        for (Node node : graph) {
            node.addAttribute("ui.label", node.getId());
        }

        Set<String> dupedEdges = new HashSet<String>();
        count1 = 1;
        int count2 = 1;
        //addrels
        for (SSSNode v : ssd.getGraph().vertexSet()) {
            count2 = 1;
            for (SSSNode v2 : ssd.getGraph().vertexSet()) {
                if (ssd.getGraph().containsEdge(v, v2) && !dupedEdges.contains(v2.getSsip().getId() + "_"+count2 + v.getSsip().getId() + "_"+count1)) {
                    graph.addEdge(v.getSsip().getId() + "_"+count1 + v2.getSsip().getId() + "_"+count2, v.getSsip().getId() + "_"+count1, v2.getSsip().getId() + "_"+count2);
                    dupedEdges.add(v.getSsip().getId() + "_"+count1 + v2.getSsip().getId() + "_"+count2);
                }
                count2++;
            }
            count1++;

        }

        if (onlyToFile == false) {
            graph.display();
        }
        try {
            pic.writeAll(graph, opdirec +fman.getFileSeparator()+ name + ".png");
        } catch (IOException ex) {
            Logger.getLogger(DSVisualisation.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
}
