/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode;

import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphMine.PropComparator.Comparator;
import uk.ac.ncl.genediseaserepositioning.GraphMine.PropComparator.StringComp;



/**
 *
 * @author joe
 *
 * A rule node that can be used to extract all approved small molecules present
 * in a graph.
 *
 */
public class ApprovedSmallMolecule implements RuleNode {

    private SSIPNode ruleNode;
    private String description;

    @Override
    public SSIPNode getRuleNode() {
        return ruleNode;
    }

    @Override
    public void setRuleNode(SSIPNode rule) {
        this.ruleNode = rule;
    }

    @Override
    public void createRuleNode() {
        SSIPNode rules = new SSIPNode(DReNInMetadata.NodeType.SMALL_MOLECULE.getText(), "RULE");
        rules.addProperty("Name", new StringComp("Group", Comparator.checkType.EQUALS, "approved"));
        rules.addProperty("Humans", new StringComp("AffectedOrganism", Comparator.checkType.EQUALS, "Humans and other mammals"));
        description = "Approved_Small_Molecule";
        ruleNode = rules;
    }

    @Override
    public String getDescription() {
        return description;
    }

}
