/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.MeSHMapper;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.SSSStepDescription;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.MetaDataInterface;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPNode;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.SSIPRelationType;


/**
 * @author joe Class takes a potential mapping and checks to see whether it is a
 * known side effect or whether it is indeed a valid inferred indication.
 *
 * @return
 */
public class SideEffectCheck {

    private MeSHMapper map;
    private Neo4J handler;

    public SideEffectCheck(Neo4J handler) {
        this.map = new MeSHMapper(true);
        this.handler = handler;
    }

    public String sideEffectCheck(SSIPNode[] dupe) {
        String info = "";
        double mappingScore = 0.0;
        String seName = "";
        String seMesh = "";
        SSIPNode drug = dupe[0];
        Map<MetaDataInterface, SSIPRelationType> relations = drug.getRelations();
        Set<SSIPNode> sideEffects = getSideEffects(drug);
        Set<SSIPNode> sideEffectsCHECKED = new HashSet<SSIPNode>();
        for (MetaDataInterface met : relations.keySet()) {
            if (relations.get(met).getType().contains(DReNInMetadata.RelTypes.SIDE_EFFECT.getText())) {
                String id = met.getId();
                for (SSIPNode s : sideEffects) {
                    if (s.getId().equals(id)) {
                        sideEffectsCHECKED.add(s);
                    }
                }
            }
        }

        SSIPNode disease = dupe[dupe.length - 1];
        Set<String> indMeshterms = getMeSHTreeTerms(disease);

        for (MetaDataInterface se : sideEffectsCHECKED) {
            if (!se.getId().equals(disease.getId())) {
                Set<String> seTreeTerms = getMeSHTreeTerms((SSIPNode) se);
                if (!seTreeTerms.isEmpty()) {
                    double mapScore = map.LeacAndChod(seTreeTerms, indMeshterms);

                    /**
                     * Need to double check the relation type!!!
                     */
                    if (mapScore > mappingScore) {
                        mappingScore = mapScore;
                        seName = se.getId();
                        seMesh = seTreeTerms.toString();
                    }
                }

            }
        }
        info = "SE" + "|" + mappingScore + "|" + seName;
        return info;
    }

    public Set<SSIPNode> getSideEffects(SSIPNode drug) {

        SimpleTraversalExamples dm = new SimpleTraversalExamples();
        SSSStepDescription[] steps = null;
        steps = dm.getSideEffects();
        Map<String, MetaDataInterface> ses = handler.traversal(drug, steps);

        Set<SSIPNode> seNodes = new HashSet<SSIPNode>();

        for (String d : ses.keySet()) {
            seNodes.add((SSIPNode) ses.get(d));
        }
        return seNodes;
    }

    public Set<String> getMeSHTreeTerms(SSIPNode disease) {
        Map<String, Object> props = disease.getAttributes();
        Set<String> meshTerms = new HashSet<String>();
        for (String prop : props.keySet()) {
            if (prop.startsWith("MeSHTree")) {
                meshTerms.add((String) props.get(prop));
            }
        }
        return meshTerms;
    }

}
