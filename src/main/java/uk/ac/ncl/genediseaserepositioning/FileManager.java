package uk.ac.ncl.genediseaserepositioning;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

/**
 * Hello world!
 *
 */
public class FileManager implements java.io.Serializable{
    private static final long serialVersionUID = 1L;

    private XMLConfiguration configRead;
    public final String LOCATIONSEPARATOR;
    private final static Logger LOGGER = Logger.getLogger(FileManager.class.getName());

    public static void main(String[] args) {
        FileManager fm = new FileManager();
        System.out.println(fm.getGD_OPFolder().getAbsolutePath());

    }

    /**
     * Constructor creates an XMLConfiguration using the fileSettings file found
     * in the project.
     */
    public FileManager() {
        try {
            this.configRead = new XMLConfiguration("fileSettings.xml");
        } catch (ConfigurationException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (getOSName().toLowerCase().contains("window")) {
            this.LOCATIONSEPARATOR = OS.WINDOWS.getText();
        } else {
            this.LOCATIONSEPARATOR = OS.LINUX.getText();
        }
    }

    /**
     * Checks to see if the file exists- if not throws an exception.
     *
     * If so returns the location with the correct OS location separator.
     *
     * @param check
     * @return
     */
    public File exists(String check) {
        LOGGER.log(Level.INFO, "Checking file exists with path...{0}", check);
        File filePath = new File(check);
        if (!filePath.exists()) {
            try {
                throw new FileNotFoundException("File does not exist: " + check);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            return filePath;
        }
        return null;
    }

    /**
     * Project folders.
     *
     * @return
     */
    public File getGraphLocation() {
        return new File(configRead.getString("Folder.Graph"));
    }

    public File getDSVis() {
        return new File(configRead.getString("Folder.DS_Visualisation"));
    }

    public File getMappingsFolder() {
        return new File(configRead.getString("Folder.Mappings"));
    }

    public File getFilteredMappingsFolder() {
        return new File(configRead.getString("Folder.FilteredMappings"));
    }

    public File getGD_OPFolder() {
        return exists(configRead.getString("Folder.GeneDisease_OP"));
    }
    
    public File getSEMapping_OPFolder() {
        return new File(configRead.getString("Folder.SE_Analysis"));
    }
    
    public File getLeacockScores_OPFolder() {
        return new File(configRead.getString("Folder.Leacock_Scores"));
    }

    public File getLOSDataSetScoreFile() {
        return exists(configRead.getString("Folder.LLS_OP"));
    }

    public File getIntegratedIndicationsFile() {
        return exists(configRead.getString("Folder.IntegratedIndications"));
    }

    public File getGDQuery() {
        return exists(configRead.getString("Folder.Query_OP"));
    }

    public File getScoredAssociationFolder() {
        return exists(configRead.getString("Folder.LLS_ScoredAssociations"));
    }

    public File getScoredAssociationDistributionFolder() {
        return exists(configRead.getString("Folder.HyperGeometricDistribution"));
    }

    public File getHyperFile() {
        return exists(configRead.getString("Folder.HyperGeometric_OP"));
    }

    public File getLeacockFile() {
        return exists(configRead.getString("Folder.Leacock"));
    }

    public File getLLSToPlotFolder() {
        return exists(configRead.getString("Folder.LLS_toPlot"));
    }

    public File getSerializedFolder() {
        return exists(configRead.getString("Folder.SerializedAssociations"));
    }

    public File getStandardisedFolder() {
        return exists(configRead.getString("Folder.StandardisedAssociations"));
    }

    public File getSuppFolder() {
        return exists(configRead.getString("Folder.SuppFiles"));
    }

    public File getROC_OPFolder() {
        return exists(configRead.getString("Folder.ROC_OP"));
    }
    /**
     * Datasets
     *
     * @return
     */
    public File getBeFreeFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.BeFree_TXT"));
    }

    public File getCTDFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.CTD_CSV"));
    }

    public File getDisGENetFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.DisGeNet_TXT"));
    }

    public File getGOFFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.GOF_TXT"));
    }

    public File getLOFFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.LOF_TXT"));
    }

    public File getGWASFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.GWAS_TXT"));
    }

    public File getMGDFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.MGD_TXT"));
    }

    public File getOMIMFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.OMIM_TXT"));
    }

    public File getOrphanetFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.Orphanet_XML"));
    }

    public File getRGDFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.RGD_TXT"));
    }

    public File getSemRepFile() {
        return exists(configRead.getString("GeneDiseaseDatasets.SemRep_TXT"));
    }

    public File getUniGeneDiseases() {
        return exists(configRead.getString("GeneDiseaseDatasets.UniProt"));
    }

    public File getDrugBankFile() {
        return exists(configRead.getString("GraphDatasets.DrugBank"));
    }

    public File getORDOFile() {
        return exists(configRead.getString("GraphDatasets.ORDO"));
    }

    public File getMeSHDFile() {
        return exists(configRead.getString("GraphDatasets.MeSH_D"));
    }

    public File getMeSHCFile() {
        return exists(configRead.getString("GraphDatasets.MeSH_C"));
    }

    public File getChEMBLDrugMech() {
        return exists(configRead.getString("GraphDatasets.ChEMBL_DRUGMECH"));
    }

    public File getChEMBLBindsTo() {
        return exists(configRead.getString("GraphDatasets.ChEMBL_BindsTo"));
    }

    public File getUniChemsrc1src2() {
        return exists(configRead.getString("Mapping.UniChem_src1src2"));
    }

    public File getSIDERIndications() {
        return exists(configRead.getString("GraphDatasets.SIDER_indications"));
    }

    public File getSIDERSideEffects() {
        return exists(configRead.getString("GraphDatasets.SIDER_sideEffects"));
    }

    public File getNDFRTIndications() {
        return exists(configRead.getString("GraphDatasets.NDFRT_indications"));
    }

    public File getUniProtFile() {
        return exists(configRead.getString("GraphDatasets.UniProt_XML"));
    }

    public File getPREDICTIndications() {
        return new File(configRead.getString("GraphDatasets.PREDICT_GD"));
    }

    public File getPREDICTUMLS() {
        return new File(configRead.getString("GraphDatasets.PREDIC_UMLS"));
    }

    public File getCTDIndications() {
        return new File(configRead.getString("GraphDatasets.CTD_Indications"));
    }

    public File getCTDChemicals() {
        return new File(configRead.getString("GraphDatasets.CTD_Chemicals"));
    }

    public File getADMECore() {
        return new File(configRead.getString("GraphDatasets.ADME_CORE"));
    }

    public File getADMEExtended() {
        return new File(configRead.getString("GraphDatasets.ADME_EXTENDED"));
    }

    /*
     * Mapping files
     */
    public File getOMIM2MeSHGSK() {
        return exists(configRead.getString("Mapping.GSK_OMIM2MeSH"));
    }

    public File getOMIM2MeSHORDO() {
        return exists(configRead.getString("Mapping.ORDO_OMIM2MeSH"));
    }

    public File getORDO2MeSHFile() {
        return exists(configRead.getString("Mapping.ORDO_OMIM2MeSH_File"));
    }

    public File getPharmGKBDrug() {
        return exists(configRead.getString("Mapping.PharmGKB_Drug"));
    }

//    public String getUniProtAssociations() {
//        return "UniProt_GeneDisease.txt";/Volumes/MyPassport/Datasets_DReNIn_v1/UniProt
//    }
    public String getGWAS2MeSHHeaderFile() {
        return "/Volumes/MyPassport/Datasets/Gene-Disease/disease_mesh_category-2014_11_04.txt";
    }

    public String getRGD2EntrezFile() {
        return "/Volumes/MyPassport/Datasets/Gene-Disease/GENES_HUMAN_RGD.txt";
    }

    public String getUMLS2MeSHFolder() {
        return "Mappings/UMLS2MeSH/";
    }

    public String getMetatheaurusFile() {
        return "/Volumes/MyPassport/Datasets_GSK/Metathesaurus/MRCONSO.RRF";
    }

    public enum OS {

        WINDOWS("\\"),
        LINUX("/");
        private final String text;

        OS(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }
    }

    /**
     * Returns the operating system name.
     *
     * @return
     */
    private String getOSName() {
        return System.getProperty("os.name");
    }

    /**
     * Return file separator
     *
     * @return
     */
    public String getFileSeparator() {
        return LOCATIONSEPARATOR;
    }
}
