/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.genediseaserepositioning;

import java.io.File;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Set;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Analysis.LLSMatrix;
import uk.ac.ncl.genediseaserepositioning.GraphAnalysis.ValidateAssociations_Hyper;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DataManager;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DiseaseCat;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.DrugDiseaseData.DiseaseType;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow.CreateScoredMatrices;
import uk.ac.ncl.genediseaserepositioning.GeneDiseaseAssociations.Mapping.LeacockAndChodorow.LeacockAndChodorow;
import uk.ac.ncl.genediseaserepositioning.GraphAnalysis.ADMEGenes.ADMEGeneSet;
import uk.ac.ncl.genediseaserepositioning.GraphAnalysis.ExtractHasIndications;
import uk.ac.ncl.genediseaserepositioning.GraphAnalysis.FilterMappings;
import uk.ac.ncl.genediseaserepositioning.GraphAnalysis.GetGraphProperties;
import uk.ac.ncl.genediseaserepositioning.GraphAnalysis.ValidateAssociations_Leacock_ROC;
import uk.ac.ncl.genediseaserepositioning.GraphAnalysis.ValidateAssociations_ROC;
import uk.ac.ncl.genediseaserepositioning.GraphAnalysis.alteringSEScore;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.Build;
import uk.ac.ncl.genediseaserepositioning.GraphBuild.IntegrateGD;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.dataaccesslayer.Neo4J;
import uk.ac.ncl.genediseaserepositioning.GraphInterface.metadata.DReNInMetadata;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.Fetch;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.MultipleSearch;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode.ApprovedSmallMolecule;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.RuleNode.RuleNode;
import uk.ac.ncl.genediseaserepositioning.SemanticSubgraphSearch.SemanticSubgraph.SubgraphManager;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Main {

    public static void main(String[] args) {

        FileManager fm = new FileManager();
//        Serialize ser = new Serialize();
//        HashMap<String,Set<String>> mesh2type = ser.deserializeHashMapSet(fm.getSuppFolder() + fm.getFileSeparator() + "ORDO_mesh2diseasetype.ser");
//        System.out.println(mesh2type.get("Rare_Disease").size());

        /**
         * 1. Build the graph
         */
      //Build graph = new Build(fm.getGraphLocation().toString());
        //graph.createGraph();
        /**
         * 2. Create scored gene-disease associations
         */
        /*
         * 2.1 Parse all gene-disease associations
         */
        //DataManager dm = new DataManager();
        // dm.ParseAllDataSources();  
        /*
         * 2.2 Create the LLS matrix
         */
        //getLLSMatrix();
        /**
         * At this point we need some manual analysis- what is the best source
         * to use as the GS?
         */
        /**
         * 3. Integrate scored associations
         */
//        IntegrateGD gdgraph = new IntegrateGD(fm.getGraphLocation().toString(), fm.getGraphLocation().toString() + "_GD", "/Users/joemullen/Desktop/GeneDiseaseRepositioning/GeneDisease_OP/LLS_OP/ScoredAssociations/scored_Associations_5.0_UNIPROT.txt");
//        gdgraph.duplicateGraph();
//        gdgraph.IntegrateGDAss();
        /**
         * 4. Mine for repositioning opportunities
         */
        String startingClass = DReNInMetadata.NodeType.SMALL_MOLECULE.getText();
        SubgraphManager sub = new SubgraphManager(SubgraphManager.Query.GeneDisease, startingClass, false);
        RuleNode smallMol = new ApprovedSmallMolecule();
        smallMol.createRuleNode();
        Fetch initSet = new Fetch(smallMol, new Neo4J(fm.getGraphLocation().toString() + "_GD"));
        initSet.goFetch();
        MultipleSearch ms = new MultipleSearch(initSet.getSuccIDs(), sub, startingClass, new Neo4J(fm.getGraphLocation().toString() + "_GD"), false, true, false, "ALL");
        ms.setRulEnodename(smallMol.getDescription());
        ms.run();
        /**
         * 5. Analyse results
         */
        /**
         * 5.1 Get graph properties
         */
        // GetGraphProperties gr = new GetGraphProperties(new Neo4J(fm.getGraphLocation().toString() + "_GD"));
        //gr.traverse();
        /**
         * 5.2 Identify the SE threshold to be used for the pruning of the
         * results
         */
//        alteringSEScore a = new alteringSEScore();
//        a.run();
        /**
         * 5.2 Filter results
         */
        filterAssociations(fm.getFilteredMappingsFolder());
        /*
         * 5.3 Get indications
         */
        //ExtractHasIndications getInds = new ExtractHasIndications(new Neo4J(fm.getGraphLocation().toString() + "_GD"));
        //getInds.run();
        /**
         * 5.4 What Sim value do we use for mapping the inferred indications to
         * knowns?
         */
        //create the scoring matrices
        //CreateScoredMatrices mat = new CreateScoredMatrices();
        //mat.run();
        //run the leacock scoring using the matrices
        //getLeacockForMappingIndications();
        //getROCForThreshold();
        /**
         * 5.4.1 Geometric
         *
         * getGeometric();
         */
        /**
         * 5.4.2 Geometric
         *
         * getGeometric();
         */
        /**
         * 5.4.3 ROC curves for each therapeutic class and each disease area
         *
         *
         */
        //getROC();
        //getROCForThreshold();
    }

    public static void getGeometric() {
        //FileManager String project = "UniProtGS3_DB42";
        //String dirFilteredMappings = "/Volumes/MyPassport/SearchOP/" + project;
        FileManager fm = new FileManager();
        DrugDiseaseData.DataSourceKNOWNS[] dats = new DrugDiseaseData.DataSourceKNOWNS[5];
        dats[0] = DrugDiseaseData.DataSourceKNOWNS.CTD;
        dats[1] = DrugDiseaseData.DataSourceKNOWNS.PREDICT;
        dats[2] = DrugDiseaseData.DataSourceKNOWNS.SIDER;
        dats[3] = DrugDiseaseData.DataSourceKNOWNS.NDFRT;
        dats[4] = DrugDiseaseData.DataSourceKNOWNS.ALL;

        for (int i = 0; i < dats.length; i++) {//1 ; i++){//

            File dir = fm.getFilteredMappingsFolder();
            File[] directoryListing = dir.listFiles();
            if (directoryListing != null) {
                for (File child : directoryListing) {
                    if (!child.getName().contains("MeSH") && !child.getName().contains("FILTERED") && !child.getName().contains(".DS_") && child.getAbsolutePath().contains("UNIQUE")) {
                        //get the disease cat and the disease type from the file names of the filtered associations.
                        DiseaseCat disCat = DiseaseCat.fromValue(child.getName().split("_")[0]);
                        DiseaseType disType = null;
                        String t = child.getName().split("_")[3];
                        if (t.contains("RARE")) {
                            disType = DiseaseType.RARE;
                        } else if (t.contains("COMMON")) {
                            disType = DiseaseType.COMMON;
                        } else if (t.contains("ALL")) {
                            disType = DiseaseType.ALL;
                        }

                        ValidateAssociations_Hyper calc = new ValidateAssociations_Hyper(1000, 500, child.getAbsolutePath(), 0, 1, 2, "TEST", dats[i], disType, disCat);
                        calc.getKnowns();
                        calc.getAllAssociations();
                        calc.calcSlidingWindow();
                        calc.calculateValues();
                    }
                }
            }
        }

    }

    public static void getLLSMatrix() {
        DataManager dm = new DataManager();
        LLSMatrix lls = new LLSMatrix(dm);
        lls.run();
        lls.printMatrix();
        lls.writeMatrixToFile(true);
    }

    /**
     * Filter the mappings, remove those involving: 1. ADME genes 2. Whose SE
     * score is greater than? 3. Whose disease is of type null.
     *
     * @param analysisOp
     * @param outPut
     */
    public static void filterAssociations(File outPut) {
        DiseaseType[] disease = new DiseaseType[3];
        disease[0] = null;
        disease[1] = DiseaseType.COMMON;
        disease[2] = DiseaseType.RARE;

        String[] diseaseName = new String[3];
        diseaseName[0] = "ALLdis_MINUSADME";
        diseaseName[1] = "COMMONdis_MINUSADME";
        diseaseName[2] = "RAREdis_MINUSADME";
        double threshold = 0.768;

        for (int i = 0; i < disease.length; i++) {//1; i++){///

            FilterMappings ex = new FilterMappings(outPut.getPath(), "ALL_minusSEs_" + threshold + "_" + diseaseName[i], null, ADMEGeneSet.EXTENDED, disease[i], null, true, false, threshold, true);
            ex.extractAssociations();
//
            FilterMappings ex2 = new FilterMappings(outPut.getPath(), "C10_minusSEs_" + threshold + "_" + diseaseName[i], DiseaseCat.C10, ADMEGeneSet.EXTENDED, disease[i], null, true, false, threshold, true);
            ex2.extractAssociations();

            FilterMappings ex3 = new FilterMappings(outPut.getPath(), "C04_minusSEs_" + threshold + "_" + diseaseName[i], DiseaseCat.C04, ADMEGeneSet.EXTENDED, disease[i], null, true, false, threshold, true);
            ex3.extractAssociations();

        }
    }

    public static void getLeacockForMappingIndications() {

        FileManager fm = new FileManager();
        DrugDiseaseData.DataSourceKNOWNS[] dats = new DrugDiseaseData.DataSourceKNOWNS[1];
        DecimalFormat df = new DecimalFormat("#.####");

        LeacockAndChodorow leac = new LeacockAndChodorow(true);
        double[] thresholds = leac.getAllScores();
        //just do the first 2!! - and it is ALL, ALL,ALL
        for (int i = 1; i < 10; i++) {
            //for (int i = 0; i < 1; i++) {//dats.length; i++) {
            File dir = fm.getFilteredMappingsFolder();
            File[] directoryListing = dir.listFiles();
            if (directoryListing != null) {
                for (File child : directoryListing) {
                    if (!child.getName().contains("MeSH") && !child.getName().contains("FILTERED") && !child.getName().contains(".DS_") && child.getAbsolutePath().contains("UNIQUE") && !child.getName().contains("filterSE_")) {
                        //get the disease cat and the disease type from the file names of the filtered associations.
                        DrugDiseaseData.DiseaseCat disCat = DrugDiseaseData.DiseaseCat.fromValue(child.getName().split("_")[0]);
                        DrugDiseaseData.DiseaseType disType = null;
                        String t = child.getName().split("_")[3];
                        if (t.contains("RARE")) {
                            disType = DrugDiseaseData.DiseaseType.RARE;
                        } else if (t.contains("COMMON")) {
                            disType = DrugDiseaseData.DiseaseType.COMMON;
                        } else if (t.contains("ALL")) {
                            disType = DrugDiseaseData.DiseaseType.ALL;
                        }
                        if (t.contains("ALL") && disCat.equals(DiseaseCat.ALL)) {
                            //when we populate the matrices we use the following rounding
                            double threshold = Double.parseDouble(
                                    df.format(thresholds[i]));

                            System.out.println("Validating all asssocations using a threshold of " + threshold);
                            ValidateAssociations_Leacock_ROC roc = new ValidateAssociations_Leacock_ROC(threshold, child.getAbsolutePath().toString(), "Leacock", DrugDiseaseData.DataSourceKNOWNS.ALL, disType, disCat, 20);
                            roc.getKnowns();
                            roc.getAllAssociations();
                            roc.getScoresToPlot();
                            roc.calculateValues();
                        }
                    }
                }
            }
        }
    }

    public static void getROCForThreshold() {

        FileManager fm = new FileManager();
        DrugDiseaseData.DataSourceKNOWNS[] dats = new DrugDiseaseData.DataSourceKNOWNS[1];
        DecimalFormat df = new DecimalFormat("#.####");

        LeacockAndChodorow leac = new LeacockAndChodorow(true);
        double threshold = 0.6333;

        File dir = fm.getFilteredMappingsFolder();
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (!child.getName().contains("MeSH") && !child.getName().contains("FILTERED") && !child.getName().contains(".DS_") && child.getAbsolutePath().contains("UNIQUE") && !child.getName().contains("filterSE_")) {
                    //get the disease cat and the disease type from the file names of the filtered associations.
                    DrugDiseaseData.DiseaseCat disCat = DrugDiseaseData.DiseaseCat.fromValue(child.getName().split("_")[0]);
                    DrugDiseaseData.DiseaseType disType = null;
                    String t = child.getName().split("_")[3];
                    if (t.contains("RARE")) {
                        disType = DrugDiseaseData.DiseaseType.RARE;
                    } else if (t.contains("COMMON")) {
                        disType = DrugDiseaseData.DiseaseType.COMMON;
                    } else if (t.contains("ALL")) {
                        disType = DrugDiseaseData.DiseaseType.ALL;
                    }

                    if (disType.equals(DrugDiseaseData.DiseaseType.ALL)) {
                        System.out.println("FILE!!!!!!>>>>   " + child.getName());
                        ValidateAssociations_Leacock_ROC roc = new ValidateAssociations_Leacock_ROC(threshold, child.getAbsolutePath().toString(), "Final", DrugDiseaseData.DataSourceKNOWNS.ALL, disType, disCat, 100);
                        roc.getKnowns();
                        roc.getAllAssociations();
                        roc.getScoresToPlot();
                        roc.calculateValues();
                    }
                }
            }

        }
    }
//    public static void getROC() {
//        FileManager fm = new FileManager();
//        DrugDiseaseData.DataSourceKNOWNS[] dats = new DrugDiseaseData.DataSourceKNOWNS[5];
//        dats[0] = DrugDiseaseData.DataSourceKNOWNS.CTD;
//        dats[1] = DrugDiseaseData.DataSourceKNOWNS.PREDICT;
//        dats[2] = DrugDiseaseData.DataSourceKNOWNS.SIDER;
//        dats[3] = DrugDiseaseData.DataSourceKNOWNS.NDFRT;
//        dats[4] = DrugDiseaseData.DataSourceKNOWNS.ALL;
//        for (int i = 0; i < dats.length; i++) {//1 ; i++){//
//            File dir = fm.getFilteredMappingsFolder();
//            File[] directoryListing = dir.listFiles();
//            if (directoryListing != null) {
//                for (File child : directoryListing) {
//                    if (!child.getName().contains("MeSH") && !child.getName().contains("FILTERED") && !child.getName().contains(".DS_") && child.getAbsolutePath().contains("UNIQUE")) {
//                        //get the disease cat and the disease type from the file names of the filtered associations.
//                        DrugDiseaseData.DiseaseCat disCat = DrugDiseaseData.DiseaseCat.fromValue(child.getName().split("_")[0]);
//                        DrugDiseaseData.DiseaseType disType = null;
//                        String t = child.getName().split("_")[3];
//                        if (t.contains("RARE")) {
//                            disType = DrugDiseaseData.DiseaseType.RARE;
//                        } else if (t.contains("COMMON")) {
//                            disType = DrugDiseaseData.DiseaseType.COMMON;
//                        } else if (t.contains("ALL")) {
//                            disType = DrugDiseaseData.DiseaseType.ALL;
//                        }
//                        ValidateAssociations_ROC roc = new ValidateAssociations_ROC(child.getAbsolutePath().toString(), "TEST", dats[i], disType, disCat);
//                        roc.getKnowns();
//                        roc.getAllAssociations();
//                        roc.getScoresToPlot();
//                        roc.calculateValues();
//                    }
//                }
//            }
//        }
//    }
}
